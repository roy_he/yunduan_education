<?php
declare (strict_types=1);
namespace app\subscribe;

class TestSub
{
    public function onTestSub1($name)
    {
        echo eJson("$name, 欢迎光临");
    }
}
