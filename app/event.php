<?php
// 事件定义文件
return [
    'bind' => [
    ],
    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'UserLogin' => ['app\listener\UserLogin'],
        'AdminLogin' => ['app\listener\AdminLogin'],
    ],
    'subscribe' => [
        app\subscribe\TestSub::class,
    ],
];
