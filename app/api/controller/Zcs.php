<?php
namespace app\api\controller;

use app\common\lib\CryptoJsAes;
use app\common\model\DocModel;
use app\common\model\UserLogModel;
use app\MyServiceDemo;
use think\facade\Log;

class Zcs extends Common
{
    /**
     * 测试 aes 加密
     * @author 贺强
     * @time   2023/6/8 9:53
     */
    public function encrypt()
    {
        $str = $this->request->param('str');
        $res = CryptoJsAes::encrypt($str, '123456');
        echo json_encode($res);
        exit;
    }

    /**
     * 测试 aes 解密
     * @author 贺强
     * @time   2023/6/8 9:53
     */
    public function decrypt()
    {
        $str = $this->request->param('str');
        $res = CryptoJsAes::decrypt($str, '123456');
        echo $res;
        exit;
    }

    /**
     * 获取用户登录日志
     * @author 贺强
     * @time   2023/6/8 9:52
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function getUserLog()
    {
        $param = $this->param;
        $debug = $param['debug'] ?? '';
        if (empty($param['is_es'])) {
            UserLogModel::$elastic = 0;
        }
        $where = [];
        if (!empty($param['username'])) {
            $where[] = ['username', '=', $param['username']];
        }
        if (!empty($param['ctime'])) {
            $ctime = strtotime($param['ctime']);
            $where[] = ['ctime', '>=', $ctime];
        }
        if ($debug === 'where') {
            var_dump($where);
            exit;
        }
        $order = ['ctime' => 'desc'];
        if (!empty($param['order'])) {
            $order = [$param['order']];
            if (!empty($param['order_way'])) {
                $order = [$param['order'] => $param['order_way']];
            }
        }
        $list = UserLogModel::getList($where, true, null, $order);
        foreach ($list as &$item) {
            if ($item['ctime']) {
                $item['ctime'] = date('Y-m-d H:i:s', $item['ctime']);
            }
        }
        $count = UserLogModel::getCount($where);
        return ajax('获取成功', 200, ['count' => $count, 'data' => $list]);
    }

    /**
     * 测试记录日志
     * @author 贺强
     * @time   2023/6/8 9:51
     */
    public function test_log()
    {
        // $properties = [
        //     'id' => ['type' => 'keyword'],
        //     'type' => ['type' => 'keyword'],
        //     'time' => ['type' => 'integer'],
        //     'msg' => ['type' => 'text'],
        // ];
        // $flag = EsModel::createIndex('cn_log_api', $properties);
        // $flag = EsModel::delIndex('cn_log_api');
        // var_dump($flag);exit;
        Log::error('测试 api 错误日志');
    }

    /**
     * 测试mysql事务
     * @author 贺强
     * @time   2023/6/8 9:50
     * @return string|\think\response\Json
     */
    public function testTrans()
    {
        $fn = function () {
            UserLogModel::$datatable = 'user_log_20230331';
            UserLogModel::$elastic = 0;
            $res = UserLogModel::add(['user_id' => 'wed34539df90ikjkd', 'username' => 'ttt', 'mobile' => '13333333333', 'ip' => '127.0.0.1']);
            return $res;
        };
        $flag = UserLogModel::transaction($fn);
        if ($flag) {
            return ajax('插入成功');
        } else {
            return ajax('插入失败', 400);
        }
    }

    /**
     * 获取身份证相关信息
     * @author 贺强
     * @time   2023/6/8 9:47
     */
    public function getUserInfoByIdcard()
    {
        $id_card = $this->request->param('id_card', '');
        $info = get_id_card_info($id_card, 1);
        print_r($info);
    }

    /**
     * 测试订阅
     * @author 贺强
     * @time   2023/6/8 9:46
     */
    public function testSubscribe()
    {
        event('TestSub1', '贺强');
        exit;
    }

    /**
     * 导出多表格 excel
     * @author 贺强
     * @time   2023/6/8 9:46
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function exportMultiSheetExcel()
    {
        $params = [
            'firstSheetActive' => true,
            'filename' => 'multi',
            'path' => 'uuuu',
            'ttl' => [['title' => '人员'], [], ['title' => '单位']],
            'data' => [
                [
                    ['name' => 'roy', 'age' => 36, 'gender' => '男'],
                    ['name' => '朱佩', 'age' => 21, 'gender' => '女'],
                    ['name' => '佩佩', 'age' => 28, 'gender' => '女'],
                ],
                [
                    ['name' => 'royhe', 'position' => '技术总监', 'salary' => '20K'],
                    ['name' => 'xixi', 'position' => 'php', 'salary' => '10K'],
                ],
                [
                    ['school' => '襄州一中', 'count' => 2300],
                    ['school' => '万垱小学', 'count' => 320],
                ],
            ],
            'cols' => [
                [
                    ['title' => '姓名', 'field' => 'name'],
                    ['title' => '年龄', 'field' => 'age'],
                    ['title' => '性别', 'field' => 'gender'],
                ],
                [
                    ['title' => '姓名', 'field' => 'name'],
                    ['title' => '职位', 'field' => 'position'],
                    ['title' => '薪水', 'field' => 'salary'],
                ],
                [
                    ['title' => '学校名称', 'field' => 'school'],
                    ['title' => '人数', 'field' => 'count'],
                ],
            ],
        ];
        excel_multi_sheet_export($params);
    }

    /**
     * 导出单表格 excel
     * @author 贺强
     * @time   2023/6/8 9:46
     * @throws \PHPExcel_Exception
     */
    public function exportExcel()
    {
        $params = [
            'columns' => [
                ['title' => '姓名', 'field' => 'name'],
                ['title' => '年龄', 'field' => 'age'],
                ['title' => '性别', 'field' => 'gender'],
            ],
            'title' => 'hahaha',
            'filename' => get_microsecond(),
            'props' => [
                'creator' => '贺强',
            ],
            'path' => 'uuu',
        ];
        $data = [
            ['name' => 'roy', 'age' => 36, 'gender' => '男'],
            ['name' => '朱佩', 'age' => 21, 'gender' => '女'],
            ['name' => '佩佩', 'age' => 28, 'gender' => '女'],
        ];
        $flag = excel_export($data, $params);
        if ($flag) {
            echo eJson('导出成功');
        } else {
            echo eJson('导出失败', 400);
        }
        exit;
    }

    /**
     * 向数据库表中插入一条数据
     * @author 贺强
     * @time   2023/6/8 9:47
     */
    public function tadd()
    {
        $data = [
            'name' => '11111222222',
            'description' => '11111222222',
        ];
        $res = DocModel::add($data);
        var_dump($res);
        exit;
    }

    /**
     * 测试服务
     * @author 贺强
     * @time   2023/6/8 10:01
     */
    public function testService(MyServiceDemo $demo)
    {
        $msg1 = $demo->getVal(1);
        $msg2 = $demo::getVal(2);
        $msg3 = MyServiceDemo::getVal(3);
        var_dump($msg1, $msg2, $msg3);
        exit;
    }
}
