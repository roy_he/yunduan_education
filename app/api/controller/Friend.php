<?php
namespace app\api\controller;

use app\common\model\UserFriendModel;
use app\common\model\UserModel;
use app\middleware\Auth;

class Friend extends Common
{
    protected $middleware = [Auth::class];

    /**
     * 好友列表
     * @author 贺强
     * @time   2022/8/4 11:02
     */
    public function lists()
    {
        $user_id = $this->request->user_id;
        if (empty($user_id)) {
            return ajax('登录超时', 100);
        }
        $param = $this->param;
        $where = ['user_id' => $user_id];
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('index.pagesize', 10);
        $list = UserFriendModel::getList($where, ['id', 'friend_id', 'friend_name', 'friend_avatar', 'is_star', 'name_letter'], [$page, $pagesize], ['is_star' => 'desc', 'name_letter' => 'asc']);
        foreach ($list as &$item) {
            if (!empty($item['friend_avatar'])) {
                $item['friend_avatar'] = config('app.params.website.img') . str_replace('\\', '/', $item['friend_avatar']);
            }
        }
        $count = UserFriendModel::getCount($where);
        return ajax('获取成功', 200, compact('list', 'count'));
    }

    /**
     * 获取单个好友信息
     * @author 贺强
     * @time   2022/8/30 19:11
     */
    public function friend()
    {
        $user_id = $this->request->user_id;
        if (empty($user_id)) {
            return ajax('登录超时', 100);
        }
        $param = $this->param;
        if (empty($param['friend_id'])) {
            return ajax('参数缺失', 201);
        }
        $where = ['user_id' => $user_id, 'friend_id' => $param['friend_id']];
        $friend = UserFriendModel::getModel($where);
        if (!empty($friend['friend_avatar'])) {
            $friend['friend_avatar'] = config('app.params.website.img') . str_replace('\\', '/', $friend['friend_avatar']);
        }
        return ajax('获取成功', 200, $friend);
    }

    /**
     * 添加好友
     * @author 贺强
     * @time   2022/8/4 14:44
     */
    public function add()
    {
        $user_id = $this->request->user_id;
        if (empty($user_id)) {
            return ajax('登录超时', 100);
        }
        $param = $this->param;
        if (empty($param['friend_id'])) {
            return ajax('好友ID不能为空', 301);
        }
        $user = UserModel::getModel([['id', '=', $param['friend_id'], ['status', '=', 2]]]);
        if (empty($user)) {
            return ajax('用户不存在', 302);
        }
        $letter = '';
        if (!empty($user['name'])) {
            $letter = get_initial($user['name']);
        }
        $data = ['user_id' => $user_id, 'friend_id' => $user['id'], 'friend_name' => $user['name'], 'friend_avatar' => $user['avatar'], 'name_letter' => $letter, 'is_star' => 0, 'mtime' => 0];
        $res = UserFriendModel::add($data);
        if (!$res) {
            return ajax('添加失败', 401);
        }
        return ajax('添加成功');
    }

    /**
     * 删除好友
     * @author 贺强
     * @time   2022/8/4 15:26
     */
    public function del()
    {
        $user_id = $this->request->user_id;
        if (empty($user_id)) {
            return ajax('登录超时', 100);
        }
        $param = $this->param;
        if (empty($param['friend_id'])) {
            return ajax('参数缺失', 201);
        }
        $res = UserFriendModel::delByWhere([['friend_id', '=', $param['friend_id']], ['user_id', '=', $user_id]]);
        if (!$res) {
            return ajax('删除失败', 401);
        }
        return ajax('删除成功');
    }

    /**
     * 标记/取消星标
     * @author 贺强
     * @time   2022/8/4 15:34
     */
    public function modify()
    {
        $user_id = $this->request->user_id;
        if (empty($user_id)) {
            return ajax('登录超时', 100);
        }
        $param = $this->param;
        if (empty($param['friend_id']) || !isset($param['is_star'])) {
            return ajax('参数缺失', 201);
        }
        $data['is_star'] = $param['is_star'];
        $where = [['user_id', '=', $user_id], ['friend_id', '=', $param['friend_id']]];
        $res = UserFriendModel::modify($data, $where);
        if (!$res) {
            return ajax('修改失败', 400);
        }
        return ajax('修改成功');
    }
}
