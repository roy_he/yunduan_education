<?php
namespace app\api\controller;

use app\middleware\Auth;

/**
 * 接口首页控制器
 * @author 贺强
 * @time   2021-10-09 10:27:40
 */
class Index extends Common
{
    protected $middleware = [Auth::class];

    public function index()
    {
        exit('接口首页');
    }
}
