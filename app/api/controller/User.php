<?php
namespace app\api\controller;

use app\common\model\UserLogModel;
use app\common\model\UserModel;
use app\middleware\Auth;

/**
 * 用户控制器
 * @author 贺强
 * @time   2021-10-19 16:37:32
 */
class User extends Common
{
    protected $middleware = [Auth::class];

    /**
     * 修改密码
     * @author 贺强
     * @time   2022/7/13 16:10
     */
    public function modify_password()
    {
        $param = $this->param;
        if (empty($param['old_password']) || empty($param['password']) || empty($param['new_password'])) {
            return ajax('参数缺失', 101);
        }
        if ($param['password'] !== $param['new_password']) {
            return ajax('两次输入的密码不一致', 202);
        }
        $user_id = $this->request->user_id ?? '';
        if (empty($user_id)) {
            return ajax('非法操作', 401);
        }
        $pwd = UserModel::getFieldValue('password', ['id' => $user_id]);
        if (!password_verify($param['old_password'], $pwd) && $pwd !== get_md5_password($param['old_password'])) {
            return ajax('原始密码错误', 201);
        }
        $res = UserModel::modify(['password' => get_md5_password($param['password'])], ['id' => $user_id]);
        if (!$res) {
            return ajax('修改失败', 402);
        }
        return ajax('修改成功');
    }

    /**
     * 获取用户登录日志
     * @author 贺强
     * @time   2022/8/31 12:47
     */
    public function get_user_log()
    {
        $param = $this->param;
        $user_id = $this->request->user_id ?? '';
        $where = [];
        if (!empty($user_id)) {
            $where = ['user_id' => $user_id];
        }
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('index.pagesize', 10);
        $list = UserLogModel::getList($where, true, [$page, $pagesize], ['ctime' => 'desc']);
        foreach ($list as &$item) {
            if (!empty($item['ctime'])) {
                $item['ctime'] = date('Y-m-d H:i:s', $item['ctime']);
            }
        }
        return ajax('获取成功', 200, $list);
    }

    /**
     * 获取用户详情
     * @author 贺强
     * @time   2022/10/15 15:49
     */
    public function detail()
    {
        $param = $this->param;
        if (!empty($param['token'])) {
            $user_id = $this->request->user_id ?? '';
            $where = ['id' => $user_id];
        } else {
            if (empty($param['id'])) {
                return ajax('参数缺失', 101);
            }
            $where = ['id' => $param['id']];
        }
        $user = UserModel::getModel($where);
        unset($user['password']);
        format_timestamp($user['login_time'], $user['ctime'], $user['mtime']);
        if (!empty($user['avatar']) && preg_match('/^uploads*/', $user['avatar'])) {
            $user['avatar'] = str_replace('\\', '/', $user['avatar']);
            $user['avatar'] = config('app.params.website.img') . $user['avatar'];
        }
        if (!empty($user['user_attr'])) {
            $user['user_attr'] = json_decode($user['user_attr'], true);
        }
        $category = get_user_category();
        $user['category'] = $category[$user['category']] ?? '';
        $status = get_user_status();
        $user['status'] = $status[$user['status']] ?? '';
        $user['gender'] = $user['gender'] == 1 ? '男' : ($user['gender'] == 2 ? '女' : '保密');
        return ajax('获取成功', 200, $user);
    }
}
