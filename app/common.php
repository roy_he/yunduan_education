<?php
/**
 * 生成随机字符串
 * @author 贺强
 * @time   2018-11-05 14:54:23
 * @param integer $num 生成字符串的长度
 * @param bool $is_special_char 是否包含特殊字符(@$#*...)
 * @return string       返回生成的随机字符串
 */
function get_random_str(int $num = 8, bool $is_special_char = false) : string
{
    $pattern = 'AaZzBb0YyCc9XxDd8Ww7EeVvF6fUuG5gTtHhS4sIiRr3JjQqKkP2pLlO1oMmNn';
    $max = 61;
    if ($is_special_char) {
        $pattern .= '!-)@(#_*$&~%^';
        $max = 74;
    }
    $str = '';
    for ($i = 0; $i < $num; $i++) {
        $str .= $pattern[mt_rand(0, $max)]; //生成 php 随机数
    }
    return $str;
}

/**
 * 生成随机数字串
 * @author 贺强
 * @time   2018-11-05 15:14:37
 * @param integer $num 要生成数字串的长度
 * @return string       返回生成的数字串
 */
function get_random_num(int $num = 6) : string
{
    $pattern = '9482135706';
    $str = '';
    for ($i = 0; $i < $num; $i++) {
        $str .= $pattern[mt_rand(0, 9)]; //生成 php 随机数
    }
    return $str;
}

/**
 * 生成唯一加密串
 * @author 贺强
 * @time   2023/3/1 10:03
 * @param string $fixed 参与加密串生成的固定值
 * @param string $salt 参与加密串生成的盐
 * @return string 生成的加密串
 */
function get_rid(string $fixed = '', string $salt = '') : string
{
    if (!empty($fixed)) {
        $fixed = md5($fixed);
    }
    if (!empty($salt)) {
        $salt = md5($salt);
    }
    $time = get_microsecond();
    $num = get_random_str(16, 1);
    $str = $fixed . $time . $salt . $num;
    return md5($str);
}

/**
 * 格式化时间戳为日期+时间
 * @author 贺强
 * @time   2022/5/27 11:13
 * @param string $format 时间格式
 * @param mixed ...$var_names 要格式化的时间戳，可以传多个
 */
function format_timestamp($format = 'Y-m-d H:i:s', &...$var_names)
{
    foreach ($var_names as &$name) {
        if (!empty($name)) {
            $name = date($format, $name);
        }
    }
}

/**
 * 格式化日期或时间
 * @author 贺强
 * @time   2019-11-20 14:01:08
 * @param mixed $datetime 要格式化的日期或时间
 * @param integer $flag 格式化后类型，1-日期+时间 2-时间戳 3-多久前
 * @param integer $agoType 多久前类型，1-刚刚 2-秒前 3-分钟前 4-小时前 5-天前 6-月前 7-年前
 * @return false|int|string
 */
function format_datetime(&$datetime, int $flag = 1, int $agoType = 0)
{
    if (empty($datetime)) {
        $datetime = '';
        return '';
    }
    if ($flag === 1) {
        if (!is_numeric($datetime)) {
            $datetime = '';
            return '';
        }
        $datetime = date('Y-m-d H:i:s', $datetime);
    } elseif ($flag === 2) {
        $datetime = strtotime($datetime);
    } elseif ($flag === 3) {
        if (!is_numeric($datetime)) {
            $datetime = '';
            return '';
        }
        $diff = time() - $datetime;
        if ($diff < 10 && $agoType > 0) {
            $datetime = '刚刚';
        } elseif ($diff < 60 && $agoType > 1) {
            $datetime = $diff . '秒前';
        } elseif ($diff < 3600 && $agoType > 2) {
            $datetime = intval($diff / 60) . '分钟前';
        } elseif ($diff < 24 * 3600 && $agoType > 3) {
            $datetime = intval($diff / 3600) . '小时前';
        } elseif ($diff < 30 * 24 * 3600 && $agoType > 4) {
            $datetime = intval($diff / (24 * 3600)) . '天前';
        } elseif ($diff < 365 * 24 * 3600 && $agoType > 5) {
            $datetime = intval($diff / (30 * 24 * 3600)) . '个月前';
        } elseif ($diff < 3652 * 24 * 3600 && $agoType > 6) {
            $datetime = intval($diff / (3652 * 24 * 3600)) . '年前';
        } else {
            $datetime = date('Y-m-d H:i:s', $datetime);
        }
    }
    return $datetime;
}

/**
 * 获取毫秒
 * @author 贺强
 * @time   2019-06-29 14:26:45
 */
function get_millisecond() : int
{
    [$microsecond, $time] = explode(' ', microtime()); //' '中间是一个空格
    // 毫秒
    return (int)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000);
}

/**
 * 获取微秒
 * @author 贺强
 * @time   2022/9/9 17:19
 */
function get_microsecond() : int
{
    [$microsecond, $time] = explode(' ', microtime()); //' '中间是一个空格
    // 微秒
    return (integer)sprintf('%.0f', (floatval($microsecond) + floatval($time)) * 1000000);
}

/**
 * 下划线或其它特殊符号转驼峰
 * @author 贺强
 * @time   2022/6/14 9:40
 * @param string $str 要改变的字符串
 * @param int $first 第一个单词首字母是否大写，0-否 1-是
 * @param string $separator 特殊符号
 * @return string
 */
function hump(string $str, int $first = 1, string $separator = '_') : string
{
    if (empty($str)) {
        return '';
    }
    $str_arr = explode($separator, $str);
    $new_str = '';
    foreach ($str_arr as $k => $s) {
        if ($k === 0) {
            if ($first === 1) {
                $new_str .= ucfirst($s);
            } else {
                $new_str .= $s;
            }
        } else {
            $new_str .= ucfirst($s);
        }
    }
    return $new_str;
}

/**
 * 数组各键值转驼峰
 * @author 贺强
 * @time   2022/6/27 15:57
 * @param array $arr 要转换为驼峰的数组
 * @param int $first 第一个单词首字母是否大写，0-否 1-是
 * @param string $separator 特殊符号
 * @return array
 */
function key2hump(array $arr, int $first = 1, string $separator = '_') : array
{
    foreach ($arr as &$item) {
        foreach ($item as $k => $v) {
            if ($k === 'ctime' && is_numeric($v)) {
                $v = date('Y-m-d H:i:s', $v);
            }
            $item[hump($k, $first, $separator)] = $v;
            unset($item[$k], $item['ctime']);
        }
    }
    return $arr;
}

/**
 * 驼峰字符串转特殊符号分隔
 * @author 贺强
 * @time   2022/6/14 17:39
 * @param string $str 要修改的字符串
 * @param string $separator 特殊符号
 * @return string
 */
function unhump(string $str, string $separator = '_') : string
{
    $transform = config('upper2lower');
    foreach ($transform as $k => $v) {
        $str = str_replace($k, $separator . $v, $str);
    }
    $str = ltrim($str, $separator);
    return $str;
}

/**
 * 生成密码
 * @param string $str 明文密码
 * @return string  返回加密后的密码
 */
function get_password(string $str) : string
{
    return password_hash($str, PASSWORD_DEFAULT);
}

/**
 * 生成 md5 密码
 * @param string $str 明文密码
 * @return string 返回 md5 密码
 */
function get_md5_password(string $str) : string
{
    $pwd = env('app.pwd_prefix', '');
    $pwd = md5($pwd . $str);
    return $pwd;
}

/**
 * 按字节数截取字符串
 * @author 贺强
 * @time   2021-03-17 16:25:57
 * @param string $str 要截取的字符串
 * @param integer $length 截取长度
 * @param string $suffix 截取后添加后缀
 * @return string          返回截取后的字符串
 */
function eSubstr(string $str, int $length = 0, string $suffix = '') : string
{
    // 如果截取长度小于 1 则直接返回原字符串
    if ($length < 1) {
        return $str;
    }
    $new_str = mb_substr($str, 0, $length);
    if ($new_str !== $str) {
        $new_str .= $suffix;
    }
    return $new_str;
}

/**
 * 获取客户端IP
 * @author 贺强
 * @time   2018-11-13 10:05:32
 * @return string 返回获取的IP
 */
function get_client_ip() : string
{
    // 判断服务器是否允许$_SERVER
    if (isset($_SERVER)) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $realIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realIp = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $realIp = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        //不允许就使用get env获取
        if (getenv('HTTP_X_FORWARDED_FOR')) {
            $realIp = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_CLIENT_IP')) {
            $realIp = getenv('HTTP_CLIENT_IP');
        } else {
            $realIp = getenv('REMOTE_ADDR');
        }
    }
    return $realIp;
}

/**
 * 数组转 xml
 * @author 贺强
 * @time   2018-11-13 15:03:03
 * @param array $arr 被转换的数组
 * @param boolean $root 是否添加根标签
 * @return string 返回转换后的 xml 字符串
 */
function array2xml(array $arr, bool $root = true) : string
{
    $xml = "";
    if ($root) {
        $xml = "<xml>";
    }
    foreach ($arr as $key => $val) {
        if (is_array($val)) {
            $child = array2xml($val, false);
            $xml .= "<$key><![CDATA[$child]]></$key>";
        } else {
            $xml .= "<$key><![CDATA[$val]]></$key>";
        }
    }
    if ($root) {
        $xml .= "</xml>";
    }
    return $xml;
}

/**
 * xml 转换为数组
 * @author 贺强
 * @time   2018-11-13 15:12:04
 * @param string $xml 被转换的 xml
 * @return array       返回转换后的数组
 */
function xml2array(string $xml) : array
{
    libxml_disable_entity_loader();
    return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
}

/**
 * 输出 json 字符串
 * @author 贺强
 * @time   2021-06-01 12:03:04
 * @param string $message 返回的消息
 * @param integer $code 状态
 * @param mixed $data 返回数据
 * @param string $callback 是否是 jsonp 格式数据
 * @return string
 */
function eJson(string $message = '', int $code = 200, $data = [], string $callback = '') : string
{
    if ($callback) {
        return $callback . '(' . json_encode(['code' => $code, 'message' => $message, 'data' => $data, 'callback' => $callback]) . ')';
    }
    return json_encode(['code' => $code, 'message' => $message, 'data' => $data]);
}

/**
 * 返回 ajax 数据
 * @author 贺强
 * @time   2021-06-01 13:36:15
 * @param string $message 返回的消息
 * @param integer $code 状态
 * @param mixed $data 返回数据
 * @param string $callback 是否是 jsonp 格式数据
 * @return string|\think\response\Json
 */
function ajax(string $message = '', int $code = 200, $data = [], string $callback = '')
{
    if ($callback) {
        return $callback . '(' . json_encode(['code' => $code, 'message' => $message, 'data' => $data, 'callback' => $callback]) . ')';
    }
    return json(['code' => $code, 'message' => $message, 'data' => $data]);
}

/**
 * 获取 json 参数
 * @author 贺强
 * @time   2022/6/8 16:52
 */
function get_json_params() : array
{
    $param = file_get_contents("php://input");
    $param = json_decode($param, true);
    return $param ?? [];
}

/**
 * 格式化数字
 * @author 贺强
 * @time   2018-11-30 16:12:39
 * @param integer $num 要格式化的数字
 * @param string $unit 单位
 * @return string       返回格式化后的字符串
 */
function format_number(int $num = 0, string $unit = '万') : string
{
    $arr = [
        '千' => 1000,
        '万' => 10000,
        '十万' => 100000,
        '百万' => 1000000,
        '千万' => 10000000,
        '亿' => 100000000,
    ];
    $divisor = $arr[$unit] ?? 1;
    $num = bcdiv($num, $divisor, 2) . $unit;
    return $num;
}

/**
 * 格式化金额
 * @author 贺强
 * @time   2022/3/17 15:54
 * @param int $money 要格式化的金额
 * @param string $unit 单位 千(K) 万(W) 百万(M)
 * @return string       返回格式化后的字符串
 */
function format_money(int $money = 0, string $unit = 'K') : string
{
    $arr = [
        'K' => 1000,
        'W' => 10000,
        'M' => 1000000,
    ];
    $divisor = $arr[$unit] ?? 1;
    $money = bcdiv($money, $divisor, 2) . $unit;
    return $money;
}

/**
 * 格式化数据
 * @author 贺强
 * @time   2021-04-14 14:20:00
 * @param int $number 格式化数据
 * @param string $unit 格式化之后的单位
 * @param string $divisor 除数
 * @return int|string 字符串
 */
function format_number_myriad(int $number = 0, string $unit = '万', $divisor = 10000)
{
    if ($number > 0 && $divisor > 0) {
        $number = number_format($number / $divisor, 2, ".", "") . $unit;
    }
    return $number;
}

/**
 * 校验身份证格式是否合法
 * @author 贺强
 * @time   2023/11/7 10:28
 * @param $idCard string 身份证号
 * @return bool
 */
function is_legal_idcard(string $idCard = '') : bool
{
    $pattern = "/^[1-8][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/";
    return preg_match($pattern, $idCard);
}

/**
 * 隐藏敏感信息
 * @author 贺强
 * @time   2024/6/2 10:58
 * @param string $info
 * @param int $flag 数据类型，1-身份证 2-手机号
 * @return string
 */
function hide_sensitive(string $info = '', int $flag = 0)
{
    if (empty($info)) {
        return '';
    }
    if ($flag === 1) {
        return substr($info, 0, 1) . '***' . substr($info, -1);
    } elseif ($flag === 2) {
        return substr($info, 0, 3) . '***' . substr($info, -4);
    }
    return $info;
}

/**
 * 根据身份证号获取所在地、生日、年龄、性别
 * @author 贺强
 * @time   2022/12/20 13:26
 * @param string $idCard 身份证号
 * @param bool $ask_ample 年龄是否要求足日
 * @return array
 */
function get_idcard_info(string $idCard = '', bool $ask_ample = false) : array
{
    $pattern = "/^[1-8][0-9]{5}(19|20|21)[0-9]{2}(((01|03|05|07|08|10|12)((0[1-9])|([1-2][0-9])|(3[0-1])))|((04|06|09|11)((0[1-9])|[1-2][0-9]|30))|(02(0[1-9]|[1-2][0-9])))[0-9]{3}([0-9]|x|X)$/";
    if (!preg_match($pattern, $idCard)) {
        return ['code' => 1, 'message' => '非法身份证号'];
    }
    $region = substr($idCard, 0, 6);
    $birth_y = substr($idCard, 6, 4);
    $birth_m = substr($idCard, 10, 2);
    $birth_d = substr($idCard, 12, 2);
    $birth = $birth_y . '-' . $birth_m . '-' . $birth_d;
    if ($ask_ample) {
        if (intval(date('m')) > intval($birth_m)) {
            $age = intval(date('Y')) - intval($birth_y);
        } elseif (intval(date('m')) === intval($birth_m)) {
            if (intval(date('d')) >= intval($birth_d)) {
                $age = intval(date('Y')) - intval($birth_y);
            } else {
                $age = intval(date('Y')) - intval($birth_y) - 1;
            }
        } else {
            $age = intval(date('Y')) - intval($birth_y) - 1;
        }
    } else {
        $age = intval(date('Y')) - intval($birth_y);
    }
    $gender = substr($idCard, 16, 1);
    $gender = $gender % 2 ? '男' : '女';
    return compact('region', 'birth', 'age', 'gender');
}

/**
 * 计算两点地理坐标之间的距离
 * @param string $longitude1 起点经度
 * @param string $latitude1 起点纬度
 * @param string $longitude2 终点经度
 * @param string $latitude2 终点纬度
 * @param int $unit 单位 1:米 2:公里
 * @param int $decimal 精度 保留小数位数
 * @return float
 */
function get_distance(string $longitude1, string $latitude1, string $longitude2, string $latitude2, int $unit = 1, int $decimal = 0) : float
{
    $EARTH_RADIUS = 6370.996; // 地球半径系数
    $PI = 3.1415926;
    $radLat1 = $latitude1 * $PI / 180.0;
    $radLat2 = $latitude2 * $PI / 180.0;
    $radLng1 = $longitude1 * $PI / 180.0;
    $radLng2 = $longitude2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;
    $distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $distance = $distance * $EARTH_RADIUS * 1000;
    if ($unit === 2) {
        $distance = $distance / 1000;
    }
    return round($distance, $decimal);
}

/**
 * 获取具体位置
 * @author 贺强
 * @time   2019-01-16 09:22:55
 * @param float $lat 纬度
 * @param float $lng 经度
 * @return string      返回位置信息
 */
function get_really_address($lat = 0, $lng = 0) : string
{
    $address = '';
    if ($lat && $lng) {
        $arr = change_to_baidu($lat, $lng);
        $url = 'http://api.map.baidu.com/geocoder/v2/?callback=&location=' . $arr['y'] . ',' . $arr['x'] . '.&output=json&pois=1&ak=fKvpmBXsoCcx8AMGqOThmd2ZEXHpniVq';
        $content = file_get_contents($url);
        $place = json_decode($content, true);
        $address = $place['result']['formatted_address'];
    }
    return $address;
}

/**
 * 转换为百度经纬度
 * @author 贺强
 * @time   2019-01-16 09:21:52
 * @param float $lat 纬度
 * @param float $lng 经度
 * @return float      百度经纬度
 */
function change_to_baidu($lat = 0, $lng = 0) : float
{
    $apiUrl = 'http://api.map.baidu.com/geoconv/v1/?coords=' . $lng . ',' . $lat . '&from=1&to=5&ak=fKvpmBXsoCcx8AMGqOThmd2ZEXHpniVq';
    $file = file_get_contents($apiUrl);
    $arrPoint = json_decode($file, true);
    return $arrPoint['result'][0];
}

/**
 * 获取地理位置信息
 * @author 贺强
 * @time   2019-01-05 10:35:18
 * @param string $latLng 经纬度
 * @param string $place 要获取的信息,all/location/formatted_address/city/province/district/direction/distance/street/street_number
 * @param string $type 返回数据格式
 * @return false|mixed|string
 */
function get_address_info(string $latLng, string $place = 'all', string $type = 'json')
{
    // $url = "http://api.map.baidu.com/geocoder?location=30.990998,103.645966&output=json&key=28bcdd84fae25699606ffad27f8da77b";
    $url = "http://api.map.baidu.com/geocoder?location={$latLng}&output={$type}&key=28bcdd84fae25699606ffad27f8da77b";
    $info = file_get_contents($url);
    if ($place === 'all') {
        return $info;
    } else {
        $info = json_decode($info, true);
        $info = $info['result'];
        if ($place !== 'location' && $place !== 'formatted_address') {
            $info = $info['addressComponent'];
        }
        if (empty($info[$place])) {
            return '';
        }
        return $info[$place];
    }
}

/**
 * 获取视频文件的缩略图
 * @author 贺强
 * @time   2019-01-16 09:47:14
 * @param string $file 视频文件路径
 * @param integer $s 指定从什么时间开始截取，单位秒
 * @param boolean $is_win 是否是 windows 系统
 * @return string          返回截取的图片保存路径
 */
function get_video_cover(string $file, int $s = 11, bool $is_win = false) : string
{
    $ffmpeg = '/path/ffmpeg/bin/ffmpeg';
    $root = root_path() . 'public';
    if ($is_win) {
        $ffmpeg = 'D:/ffmpeg/ffmpeg.exe';
    }
    $path = '/uploads/cli/video/' . date('Y') . '/' . date('m') . '/' . date('d');
    if (!is_dir($root . $path)) {
        @mkdir($root . $path, 0755, true);
    }
    $path .= '/' . get_millisecond() . '.jpg';
    $str = "{$ffmpeg} -i {$file} -ss {$s} {$root}{$path}";
    system($str);
    return $path;
}

/**
 * 把文件转为二进制流
 * @author 贺强
 * @time   2023/2/1 9:02
 * @param $path string 文件路径
 * @return false|string
 */
function get_file_binary_info(string $path)
{
    $suffix = pathinfo($path, 4);
    $types = [
        'gif' => 'image/gif',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'png' => 'image/png',
        'mp3' => 'audio/mp3',
        'mp4' => 'video/mpeg4',
        'rm' => 'application/vnd.rn-realmedia',
        'rmvb' => 'application/vnd.rn-realmedia-vbr',
        'avi' => 'video/avi',
        'wmv' => 'video/x-ms-wmv',
        'txt' => 'text/plain',
        'doc' => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        'exe' => 'application/x-msdownload',
    ];
    $type = $types[$suffix] ?? '';
    if (empty($type)) {
        return '';
    }
    $size = filesize($path);
    $data = fread(fopen($path, 'r'), $size);
    header("Content-type: $type;charset=UTF-8");
    header("Content-Length: " . $size);
    return $data;
}

/**
 * UTF8转换为GB2312编码
 * @author 贺强
 * @time   2019-01-25 17:26:00
 * @param string $s 要转换编码的文字
 * @return string    返回转换编码后的文字
 */
function utf8_to_gb2312(string $s) : string
{
    return iconv('UTF-8', 'GB2312//IGNORE', $s);
}

/**
 * 获取汉字拼音
 * @author 贺强
 * @time   2019-01-24 17:34:19
 * @param string $s 要获取拼音的汉字
 * @param boolean $isFirst 是否只获取每个字的首字母
 * @return string           返回获取到的拼音
 */
function get_pinyin(string $s, bool $isFirst = false) : string
{
    $res = '';
    $s = iconv('UTF-8', 'GB2312//IGNORE', $s);
    $len = strlen($s);
    $pinyin_arr = get_pinyin_array();
    for ($i = 0; $i < $len; $i++) {
        $ascii = ord($s[$i]);
        if ($ascii > 0x80) {
            $ascii2 = ord($s[++$i]);
            $ascii = $ascii * 256 + $ascii2 - 65536;
        }
        if ($ascii < 255 && $ascii > 0) {
            if (($ascii >= 48 && $ascii <= 57) || ($ascii >= 97 && $ascii <= 122)) {
                $res .= $s[$i]; // 0-9 a-z
            } elseif ($ascii >= 65 && $ascii <= 90) {
                $res .= strtolower($s[$i]); // A-Z
            } else {
                $res .= '_'; ////将符号转义 不替换符号$res .= $s{$i};
            }
        } elseif ($ascii < -20319 || $ascii > -10247) {
            $res .= '_';
        } else {
            foreach ($pinyin_arr as $py => $asc) {
                if ($asc <= $ascii) {
                    $res .= $isFirst ? $py[0] : $py;
                    break;
                }
            }
        }
    }
    return $res;
}

/**
 * 获取汉字的首字母
 * @author 贺强
 * @time   2019-01-24 17:37:19
 * @param string $s 要获取首字母的汉字
 * @return string    返回获取到的首字母
 */
function get_initial(string $s)
{
    $ascii = ord($s[0]);
    if ($ascii > 0xE0) {
        $s = utf8_to_gb2312($s[0] . $s[1] . $s[2]);
    } elseif ($ascii < 0x80) {
        if ($ascii >= 65 && $ascii <= 90) {
            return strtolower($s[0]);
        } elseif ($ascii >= 97 && $ascii <= 122) {
            return $s[0];
        } else {
            return false;
        }
    }
    if (strlen($s) < 2) {
        return false;
    }
    $asc = ord($s[0]) * 256 + ord($s[1]) - 65536;
    if ($asc >= -20319 && $asc <= -20284) {
        return 'a';
    }
    if ($asc >= -20283 && $asc <= -19776) {
        return 'b';
    }
    if ($asc >= -19775 && $asc <= -19219) {
        return 'c';
    }
    if ($asc >= -19218 && $asc <= -18711) {
        return 'd';
    }
    if ($asc >= -18710 && $asc <= -18527) {
        return 'e';
    }
    if ($asc >= -18526 && $asc <= -18240) {
        return 'f';
    }
    if ($asc >= -18239 && $asc <= -17923) {
        return 'g';
    }
    if ($asc >= -17922 && $asc <= -17418) {
        return 'h';
    }
    if ($asc >= -17417 && $asc <= -16475) {
        return 'j';
    }
    if ($asc >= -16474 && $asc <= -16213) {
        return 'k';
    }
    if ($asc >= -16212 && $asc <= -15641) {
        return 'l';
    }
    if ($asc >= -15640 && $asc <= -15166) {
        return 'm';
    }
    if ($asc >= -15165 && $asc <= -14923) {
        return 'n';
    }
    if ($asc >= -14922 && $asc <= -14915) {
        return 'o';
    }
    if ($asc >= -14914 && $asc <= -14631) {
        return 'p';
    }
    if ($asc >= -14630 && $asc <= -14150) {
        return 'q';
    }
    if ($asc >= -14149 && $asc <= -14091) {
        return 'r';
    }
    if ($asc >= -14090 && $asc <= -13319) {
        return 's';
    }
    if ($asc >= -13318 && $asc <= -12839) {
        return 't';
    }
    if ($asc >= -12838 && $asc <= -12557) {
        return 'w';
    }
    if ($asc >= -12556 && $asc <= -11848) {
        return 'x';
    }
    if ($asc >= -11847 && $asc <= -11056) {
        return 'y';
    }
    if ($asc >= -11055 && $asc <= -10247) {
        return 'z';
    }
    return false;
}

/**
 * 得到拼音和ASCII码数组
 * @author 贺强
 * @time   2019-01-24 17:39:37
 * @return array 返回得到的数组
 */
function get_pinyin_array() : array
{
    $py_arr = config('app.PY');
    arsort($py_arr);
    return $py_arr;
}

/**
 * 同步 URL 请求
 * @author 贺强
 * @time   2019-05-31 10:30:26
 * @param string $url 请求的 URL
 * @param mixed $params 传递的参数
 * @param string $method 请求方式
 * @param array $header 头参数
 * @param mixed $cert 证书
 * @param integer $expire 超时时间
 * @return array|string   返回请求结果
 */
function curl(string $url, $params = null, string $method = 'GET', array $header = [], $cert = null, int $expire = 30)
{
    $cookieFile = env('runtime_path') . 'temp/' . md5(time()) . '.txt';
    $userAgent = !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36';
    $ch = curl_init();
    $opt = [];
    // 请求参数
    $opt[CURLOPT_COOKIEJAR] = $cookieFile;
    $opt[CURLOPT_COOKIEFILE] = $cookieFile;
    $opt[CURLOPT_USERAGENT] = $userAgent;
    $opt[CURLOPT_CONNECTTIMEOUT] = $expire;
    $opt[CURLOPT_TIMEOUT] = $expire;
    $opt[CURLOPT_HTTPHEADER] = $header ?: ['Expect:'];
    $opt[CURLOPT_FOLLOWLOCATION] = true;
    $opt[CURLOPT_RETURNTRANSFER] = true;
    if (substr($url, 0, 8) == 'https://') {
        $opt[CURLOPT_SSL_VERIFYPEER] = false;
        $opt[CURLOPT_SSL_VERIFYHOST] = 2;
    }
    if (!empty($cert)) {
        if (is_array($cert)) {
            $opt[CURLOPT_SSLCERT] = $cert['cert_url'] ?? '';
            $opt[CURLOPT_SSLKEY] = $cert['cert_key_url'] ?? '';
        } else {
            $opt[CURLOPT_SSLCERT] = $cert;
        }
    }
    $method = strtoupper($method);
    if (is_array($params) && $method === 'GET') {
        $params = http_build_query($params);
    } elseif (is_array($params) && $method === 'POST') {
        $params = json_encode($params);
    }
    switch ($method) {
        case 'GET':
            $extStr = (strpos($url, '?') !== false) ? '&' : '?';
            $url = $url . (($params) ? $extStr . $params : '');
            $opt[CURLOPT_URL] = $url . '?' . $params;
            break;
        case 'POST':
            $opt[CURLOPT_POST] = true;
            $opt[CURLOPT_POSTFIELDS] = $params;
            $opt[CURLOPT_URL] = $url;
            break;
        case 'PUT':
            $opt[CURLOPT_CUSTOMREQUEST] = 'PUT';
            $opt[CURLOPT_POSTFIELDS] = $params;
            $opt[CURLOPT_URL] = $url;
            break;
        case 'DELETE':
            $opt[CURLOPT_CUSTOMREQUEST] = 'DELETE';
            $opt[CURLOPT_POSTFIELDS] = $params;
            $opt[CURLOPT_URL] = $url;
            break;
        default:
            return ['error' => 0, 'info' => '请求的方法不存在', 'data' => []];
    }
    curl_setopt_array($ch, $opt);
    $result = curl_exec($ch);
    $error = curl_error($ch);
    if (empty($result) || !empty($error)) {
        $errno = curl_errno($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return [
            'errno' => $errno,
            'info' => $error,
            'data' => $info,
        ];
    }
    curl_close($ch);
    return $result;
}

/**
 * 异步 URL 请求
 * @author 贺强
 * @time   2019-05-28 17:47:29
 * @param string $url 要请求的URL
 * @param mixed $data 请求参数
 * @param string $type 请求方式
 */
function curl_async(string $url, $data = [], string $type = 'form')
{
    if ($type == 'json') {
        //以json的方式传输数据
        $data = json_encode($data);
        $content_type = 'application/json';
    } else {
        //以form表单的方式传输数据
        $data = http_build_query($data);
        $content_type = 'application/x-www-form-urlencoded';
    }
    $len = strlen($data);
    $url_array = parse_url($url);
    if (!isset($url_array['port'])) {
        $fp = fsockopen($url_array['host'], 80, $errno, $errStr, 3);
    } else {
        $fp = fsockopen($url_array['host'], $url_array['port'], $errno, $errStr, 3);
    }
    if ($fp) {
        $out = "POST {$url_array['path']} HTTP/1.1\r\n";
        $out .= "Host: {$url_array['host']}\r\n";
        $out .= "Content-type: $content_type\r\n";
        $out .= "Content-Length: $len\r\n";
        $out .= "Connection: close\r\n";
        $out .= $data . "\r\n";
        fwrite($fp, $out);
        // 实现异步把下面注释掉，意思是不处理返回,如要查看返回结果可打开注释
        //echo fread($fp, 1024);
        usleep(100);
        fclose($fp);
    }
}

/**
 * 读取 excel 数据
 * @author 贺强
 * @time   2023/11/9 13:26
 * @param string $path 文件路径
 * @param array $fields 字段，格式：['A' => '字段1', 'B' => '字段2' ……]
 * @param int $startRow 从第几行开始读取
 * @param int|null $endRow 最大行数
 * @return array
 * @throws PHPExcel_Exception
 * @throws PHPExcel_Reader_Exception
 */
function get_excel_data(string $path, array $fields = [], int $startRow = 2, int $endRow = null)
{
    require_once '../extend/PHPExcel/PHPExcel.php';
    $PHPExcel = PHPExcel_IOFactory::load($path);
    $sheet = $PHPExcel->getActiveSheet();
    $rows = $sheet->getRowIterator($startRow, $endRow);
    $data = [];
    foreach ($rows as $k => $row) {
        $item = ['id' => 'data' . $k];
        foreach ($row->getCellIterator() as $c => $cell) {
            if (!empty($fields[$c])) {
                $item[$fields[$c]] = $cell->getValue();
            } else {
                $item[] = $cell->getValue();
            }
        }
        $data[] = $item;
    }
    return $data;
}

/**
 * 导出excel表格
 * @author 贺强
 * @time   2021-03-17 14:06:03
 * @param array $list 表格数据
 * @param array $params 表格信息，filename:文件名，title:表格名，默认Sheet1，columns: 列标题,key(int)-value(array){field: 字段名, title:列名, width:列宽}
 * @param string $excel_type 表格类型
 * @return string
 * @throws PHPExcel_Exception
 */
function excel_export(array $list, array $params, string $excel_type = 'xls')
{
    if (empty($params['columns'])) {
        return '非法操作';
    }
    $title = $params['title'] ?? 'Sheet1';
    require_once '../extend/PHPExcel/PHPExcel.php';
    $PHPExcel = new PHPExcel(); //实例化
    // 设置文档基本属性
    $PHPProp = $PHPExcel->getProperties();
    $props = $params['props'] ?? [];
    if (!empty($props['creator'])) {
        $PHPProp->setCreator($props['creator']); // 设置创建者
    }
    if (!empty($props['is_date'])) {
        $PHPProp->setLastModifiedBy(date('Y/m/d H:i:s')); // 设置最后修改时间
    }
    if (!empty($props['title'])) {
        $PHPProp->setTitle($props['title']); // 设置标题
    }
    if (!empty($props['subject'])) {
        $PHPProp->setSubject($props['subject']); // 设置备注
    }
    if (!empty($props['keywords'])) {
        $PHPProp->setKeywords($props['keywords']); // 设置关键字 | 标记
    }
    if (!empty($props['category'])) {
        $PHPProp->setCategory($props['category']); // 设置类别
    }
    $PHPSheet = $PHPExcel->getActiveSheet();
    $PHPSheet->setTitle($title); //给当前活动sheet设置名称
    // 行数
    $rowNum = 1;
    // 列标题
    $columns = $params['columns'];
    foreach ($columns as $key => $column) {
        $PHPSheet->setCellValue(row_column($key, $rowNum), $column['title']);
        if (!empty($column['width'])) {
            $PHPSheet->getColumnDimension(column($key))->setWidth($column['width']);
        }
    }
    $rowNum++;
    //数据
    $len = count($columns);
    foreach ($list as $row) {
        for ($i = 0; $i < $len; $i++) {
            $value = $row[$columns[$i]['field']] ?? '';
            $value = strval($value);
            $PHPSheet->setCellValue(row_column($i, $rowNum), $value);
        }
        $rowNum++;
    }
    $filename = $params['filename'] ?? date('YmdHis');
    $writeType = 'Excel5';
    if ($excel_type === 'xls') {
        $writeType = 'Excel2007';
    }
    $PHPWriter = PHPExcel_IOFactory::createWriter($PHPExcel, $writeType);         //创建生成的格式
    $path = $params['path'] ?? '';
    if (!empty($path)) {
        $root = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;
        $directory = $root . $path;
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        $path = $directory . DIRECTORY_SEPARATOR . $filename . '.' . $excel_type;
        $PHPWriter->save($path);
        return true;
    } else {
        header("Content-Disposition: attachment;filename={$filename}.{$excel_type}"); //下载下来的文件名
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        $PHPWriter->save("php://output");
        exit();
    }
}

/**
 * 导出多表格excel
 * @author 贺强
 * @time   2023/4/13 15:38
 * @param array $params
 * @param string $excel_type 表格类型
 * @return string
 * @throws PHPExcel_Exception
 * @throws PHPExcel_Reader_Exception
 * @throws PHPExcel_Writer_Exception
 */
function excel_multi_sheet_export(array $params, string $excel_type = 'xlsx')
{
    $ttl = $params['ttl'] ?? [];                          // 各 sheet 名称
    $data = $params['data'] ?? [];                        // 各 sheet 数据
    $cols = $params['cols'] ?? [];                        // 各 sheet 表头
    $firstSheetActive = $params['firstSheetActive'] ?? 0; // 是否默认显示第一个 sheet
    if (empty($cols) || count($data) > count($cols)) {
        return '非法操作';
    }
    require_once '../extend/PHPExcel/PHPExcel.php';
    $PHPExcel = new PHPExcel(); //实例化
    // 设置文档基本属性
    $PHPProp = $PHPExcel->getProperties();
    $props = $params['props'] ?? [];
    if (!empty($props['creator'])) {
        $PHPProp->setCreator($props['creator']); // 设置创建者
    }
    if (!empty($props['is_date'])) {
        $PHPProp->setLastModifiedBy(date('Y/m/d H:i:s')); // 设置最后修改时间
    }
    if (!empty($props['title'])) {
        $PHPProp->setTitle($props['title']); // 设置标题
    }
    if (!empty($props['subject'])) {
        $PHPProp->setSubject($props['subject']); // 设置备注
    }
    if (!empty($props['keywords'])) {
        $PHPProp->setKeywords($props['keywords']); // 设置关键字 | 标记
    }
    if (!empty($props['category'])) {
        $PHPProp->setCategory($props['category']); // 设置类别
    }
    foreach ($data as $k => $list) {
        $title = $ttl[$k]['title'] ?? ('Sheet' . ($k + 1));
        if ($k > 0) {
            $PHPExcel->createSheet();
        }
        $PHPSheet = $PHPExcel->setActiveSheetIndex($k);
        $PHPSheet->setTitle($title); // 给当前活动sheet设置名称
        $rowNum = 1;                 // 行数
        $columns = $cols[$k];
        foreach ($columns as $key => $column) {
            $PHPSheet->setCellValue(row_column($key, $rowNum), $column['title']);
            if (!empty($column['width'])) {
                $PHPSheet->getColumnDimension(column($key))->setWidth($column['width']);
            }
        }
        $rowNum++;
        $len = count($columns);
        foreach ($list as $row) {
            for ($i = 0; $i < $len; $i++) {
                $value = $row[$columns[$i]['field']] ?? '';
                $value = strval($value);
                $PHPSheet->setCellValue(row_column($i, $rowNum), $value);
            }
            $rowNum++;
        }
    }
    if ($firstSheetActive) {
        $PHPExcel->setActiveSheetIndex();
    }
    $filename = $params['filename'] ?? date('YmdHis');
    $writeType = 'Excel5';
    if ($excel_type === 'xls') {
        $writeType = 'Excel2007';
    }
    $PHPWriter = PHPExcel_IOFactory::createWriter($PHPExcel, $writeType);         //创建生成的格式
    $path = $params['path'] ?? '';
    if (!empty($path)) {
        $root = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;
        $directory = $root . $path;
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        $path = $directory . DIRECTORY_SEPARATOR . $filename . '.' . $excel_type;
        $PHPWriter->save($path);
        return true;
    } else {
        header("Content-Disposition: attachment;filename={$filename}.{$excel_type}"); //下载下来的文件名
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        $PHPWriter->save("php://output");
        exit();
    }
}

/**
 * 获取某一列
 * @author 贺强
 * @time   2023/4/3 10:36
 * @param int $key 列索引
 * @return string
 */
function column($key = '') : string
{
    $array = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
    ];
    return $array[$key] ?? '';
}

/**
 * 获取某一行某一列
 * @author 贺强
 * @time   2023/4/3 10:38
 * @param int $key 列索引
 * @param int $rowNum 行索引
 * @return string
 */
function row_column($key = '', int $rowNum = 1) : string
{
    return column($key) . $rowNum;
}

/**
 * 判断字符串是否是手机号
 * @author 贺强
 * @time   2023/5/19 14:11
 * @param string $mobile 要判断的字符串
 * @return bool
 */
function is_mobile(string $mobile = '') : bool
{
    $pattern = '/^1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}$/';
    return preg_match($pattern, $mobile);
}

/**
 * 判断字符串是否为 json 格式
 * @author 贺强
 * @time   2023/5/16 15:51
 * @param $str string 要判断的字符串
 * @return bool
 */
function is_json(string $str = '') : bool
{
    if (!is_string($str)) {
        return false;
    }
    $arr = json_decode($str, true);
    if (!empty($arr) && is_array($arr)) {
        return true;
    }
    return false;
}

/**
 * 获取字段类型
 * @author 贺强
 * @time   2023/6/26 16:26
 * @param string $type
 * @return string|string[]
 */
function get_field_type(string $type = '')
{
    $types = [
        'int' => '整形',
        'string' => '字符串型',
        'date' => '日期',
        'datetime' => '日期+时间',
        'radio' => 'radio单选',
        'select' => '下拉框',
    ];
    if (empty($type)) {
        return $types;
    }
    return $types[$type] ?? '';
}

// 具体项目业务方法
/**
 * 把状态转换成对错图标
 * @author 贺强
 * @time   2021-03-30 18:24:10
 * @param integer $status 状态
 * @return string          返回状态图标
 */
function get_status(int $status = 0) : string
{
    $status_arr = [
        0 => '×',
        1 => '√',
    ];
    return $status_arr[$status] ?? '';
}

/**
 * 把客服状态转换成功文字描述
 * @author 贺强
 * @time   2021-10-03 16:18:21
 * @param integer $status 客服状态
 * @return string          返回状态描述
 */
function get_ke_fu_status(int $status = -1) : string
{
    $status_arr = [
        1 => '可用',
        4 => '不可用',
    ];
    return $status_arr[$status] ?? '';
}

/**
 * 把管理员状态转换成文字描述
 * @author 贺强
 * @time   2019-06-10 17:03:37
 * @param integer $status 管理员状态
 * @return string         返回状态描述
 */
function get_admin_status(int $status = 0) : string
{
    $status_arr = [
        1 => '正常',
        4 => '禁用',
    ];
    return $status_arr[$status] ?? '';
}

/**
 * 把区划状态转换成文字描述
 * @author 贺强
 * @time   2022/4/25 10:46
 * @param int $status
 * @return string 返回状态描述
 */
function get_district_status(int $status = 0) : string
{
    $status_arr = [
        1 => '正常',
        2 => '撤销',
    ];
    return $status_arr[$status] ?? '待审核';
}

/**
 * 获取区划级别文字描述
 * @author 贺强
 * @time   2022/4/25 10:49
 * @param int $level
 * @return string 返回区划级别
 */
function get_district_level(int $level = 0) : string
{
    $level_arr = [
        1 => '省级',
        2 => '地级',
        3 => '县级',
        4 => '乡级',
    ];
    return $level_arr[$level] ?? '未知';
}

/**
 * 把用户状态转换成文字描述
 * @author 贺强
 * @time   2022/3/22 16:40
 * @param int $status 用户状态
 * @return string|string[] 返回状态描述
 */
function get_user_status(int $status = -1)
{
    $status_arr = [
        0 => '待审核',
        1 => '正常',
        4 => '禁用',
    ];
    if ($status < 0) {
        return $status_arr;
    }
    return $status_arr[$status] ?? '';
}

/**
 * 把商品状态转换成文字描述
 * @author 贺强
 * @time   2022/10/22 10:56
 * @param int $status 状态
 * @return string|string[]
 */
function get_goods_status(int $status = -1)
{
    $status_arr = [
        4 => '下架',
        1 => '上架',
    ];
    if ($status < 0) {
        return $status_arr;
    }
    return $status_arr[$status] ?? '';
}

/**
 * 把用户类型转为文字描述
 * @author 贺强
 * @time   2022/9/18 14:35
 * @param int $category 用户类型编码
 * @return string|string[]
 */
function get_user_category(int $category = -1)
{
    $category_arr = [
        1 => '学生',
        2 => '家长',
        3 => '教师',
        4 => '校长',
        5 => '教师、家长',
        6 => '家长、校长',
        7 => '教师、校长',
        9 => '家长、教师、校长',
        10 => '机构人员',
    ];
    if ($category < 0) {
        return $category_arr;
    }
    return $category_arr[$category] ?? '';
}

/**
 * 把商品类别转为文字描述
 * @author 贺强
 * @time   2022/10/21 23:35
 * @param int $category 商品类别编码
 * @return string|string[]
 */
function get_goods_category(int $category = -1)
{
    $category_arr = [
        1 => '推荐',
        2 => '手机',
        3 => '水果',
        4 => '食品',
        5 => '男装',
        6 => '女装',
        7 => '美妆',
        8 => '鞋包',
        9 => '内衣',
        10 => '家纺',
    ];
    if ($category < 0) {
        return $category_arr;
    }
    return $category_arr[$category] ?? '';
}

/**
 * 把办学性质转为文字描述
 * @author 贺强
 * @time   2022/9/18 14:27
 * @param int $nature 办学性质编码
 * @return string|string[]
 */
function get_school_nature(int $nature = -1)
{
    $nature_arr = [
        1 => '公办',
        2 => '民办',
        3 => '其它办',
    ];
    if ($nature < 0) {
        return $nature_arr;
    }
    return $nature_arr[$nature] ?? '';
}

/**
 * 把学校状态转换为文字描述
 * @author 贺强
 * @time   2022/9/11 11:12
 * @param int $status 状态编号
 * @return string|string[]
 */
function get_school_status(int $status = -1)
{
    $status_arr = [
        1 => '正常',
        4 => '注销',
        9 => '删除',
    ];
    if ($status < 0) {
        return $status_arr;
    }
    return $status_arr[$status] ?? '';
}

/**
 * 把任务状态转换为文字描述
 * @author 贺强
 * @time   2022/10/25 18:21
 * @param int $status 状态
 * @return string|string[] 返回状态描述
 */
function get_crontab_status(int $status = -1)
{
    $status_arr = [
        0 => '禁用',
        1 => '启用',
    ];
    if ($status < 0) {
        return $status_arr;
    }
    return $status_arr[$status] ?? '';
}

/**
 * 把学校类别转为文字描述
 * @author 贺强
 * @time   2022/9/11 11:37
 * @param int $type 类别编号
 * @return string|string[]
 */
function get_school_category(int $type = -1)
{
    $type_arr = [
        1 => '幼儿园',
        2 => '小学',
        3 => '初中',
        4 => '高中',
        5 => '大专',
        6 => '大学',
        7 => '职校',
    ];
    if ($type < 0) {
        return $type_arr;
    }
    return $type_arr[$type] ?? '';
}

/**
 * 把机构类别转为文字描述
 * @author 贺强
 * @time   2022/9/18 14:17
 * @param int $type 类别编号
 * @return string|string[]
 */
function get_org_category(int $type = -1)
{
    $type_arr = [
        1 => '机构',
    ];
    if ($type < 0) {
        return $type_arr;
    }
    return $type_arr[$type] ?? '';
}

/**
 * 把机构类别转为文字描述
 * @author 贺强
 * @time   2022/9/18 15:06
 * @param int $status
 * @return string|string[]
 */
function get_org_status(int $status = -1)
{
    $status_arr = [
        1 => '正常',
        4 => '注销',
        9 => '删除',
    ];
    if ($status < 0) {
        return $status_arr;
    }
    return $status_arr[$status] ?? '';
}

/**
 * 管理员角色状态
 * @author 贺强
 * @time   2021-09-14 15:14:34
 * @param integer $status 角色状态
 * @return string          返回状态描述
 */
function get_role_status(int $status = 0) : string
{
    $status_arr = [
        1 => '正常',
        4 => '禁用',
    ];
    return $status_arr[$status] ?? '';
}

/**
 * 获取订单支付方式
 * @param integer $type 支付方式
 * @return string        返回文字支付方式
 */
function get_pay_type(int $type = 0) : string
{
    $type_arr = [
        1 => '支付宝',
        2 => '微信',
        3 => '苹果',
        4 => '后台',
    ];
    return $type_arr[$type] ?? '-';
}

/**
 * 反馈问题类型
 * @author 贺强
 * @time   2021-03-30 18:15:17
 * @param integer $type 反馈问题类型
 * @return string          返回反馈问题类型描述
 */
function get_feedback_type(int $type = 0) : string
{
    $type_arr = [
        1 => '阅读设置',
        2 => '评论问答',
        3 => '充值问题',
        4 => '账号问题',
        5 => '其它',
    ];
    return $type_arr[$type] ?? '-';
}

/**
 * 把回复状态转换成文字描述
 * @author 贺强
 * @time   2021-03-30 18:24:10
 * @param integer $status 回复状态
 * @return string          返回状态描述
 */
function get_reply_status(int $status = 0) : string
{
    $status_arr = [
        0 => '否',
        1 => '是',
    ];
    return $status_arr[$status] ?? '';
}

/**
 * 设备类型
 * @author 贺强
 * @time   2021-03-30 18:28:53
 * @param integer $type 设备类型
 * @return string          设备类型描述
 */
function get_device_type(int $type = 0) : string
{
    $type_arr = [
        1 => 'IOS',
        2 => '安卓',
    ];
    return $type_arr[$type] ?? '-';
}

/**
 * 举报来源
 * @author 贺强
 * @time   2021-04-06 10:54:25
 * @param integer $type 举报来源类型
 * @return string          返回举报来源描述
 */
function get_report_type(int $type = 0) : string
{
    $type_arr = [
        1 => '贴子',
        2 => '帖子评论',
        3 => '书箱评论',
    ];
    return $type_arr[$type] ?? '';
}

/**
 * 签约类型
 * @author 贺强
 * @time   2021-04-13 17:56:56
 * @param integer $type 签约类型
 * @return string          返回签约类型描述
 */
function get_sign_type(int $type = 0) : string
{
    $type_arr = [
        1 => '字母签',
        2 => '保底签',
        3 => '非独家',
        4 => '电子版权',
    ];
    return $type_arr[$type] ?? '-';
}

/**
 * 系统消息类型
 * @author 贺强
 * @time   2021-04-19 16:47:55
 * @param integer $type 系统消息类型
 * @return string          返回系统消息类型描述
 */
function get_notice_type(int $type = 0) : string
{
    $type_arr = [
        1 => '网站公告',
        2 => '作者消息',
        3 => '用户消息',
    ];
    return $type_arr[$type] ?? '-';
}
