<?php
declare (strict_types = 1);

namespace app\listener;

use app\common\model\AdminUserModel;
use app\common\model\AdminUserLogModel;

class AdminLogin
{
    /**
     * 事件监听处理
     * @param $admin
     * @return mixed
     */
    public function handle($admin)
    {
        $data = $admin['data'] ?? null;
        unset($admin['data']);
        AdminUserModel::modify($admin);
        $data = ['username' => $admin['username'], 'login_time' => time(), 'login_ip' => $admin['login_ip'], 'action' => 'login_dologin', 'log' => '登录', 'data' => $data, 'ctime' => time()];
        AdminUserLogModel::add($data);
        return true;
    }
}
