<?php
declare (strict_types=1);
namespace app\listener;

use app\common\model\UserLogModel;
use app\common\model\UserModel;

class UserLogin
{
    /**
     * 事件监听处理
     * @return mixed
     */
    public function handle($user)
    {
        UserModel::modify($user);
        $data = ['user_id' => $user['id'], 'username' => $user['username'], 'mobile' => $user['mobile'], 'ip' => $user['login_ip'], 'ctime' => $user['login_time'] ?? time()];
        UserLogModel::$elastic = 0;
        UserLogModel::$datatable = 'user_log_' . date('Ymd');
        UserLogModel::add($data);
        return true;
    }
}
