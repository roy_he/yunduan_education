<?php
namespace app\index\controller;

use think\facade\Log;

/**
 * 首页控制器
 */
class Index extends Common
{
    public function index()
    {
        if ($this->request->param('log','')) {
            Log::info('index message');
        }
        return view();
    }
}
