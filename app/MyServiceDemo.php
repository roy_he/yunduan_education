<?php
namespace app;

class MyServiceDemo
{
    // 定义一个静态类成员变量
    protected static $myStaticVar = '';

    // 设置静态类型成员变量的值
    public static function setVar($value)
    {
        self::$myStaticVar = $value;
    }

    // 显示静态类成员变量
    public static function getVal($extra = '')
    {
        return self::$myStaticVar . $extra;
    }
}