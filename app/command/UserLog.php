<?php
declare (strict_types=1);
namespace app\command;

use app\common\model\UserLogModel;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Log;

class UserLog extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('userlog')
            ->addArgument('date', 2, '要同步的日期数据', date('Ymd', strtotime('-1 day')))
            ->setDescription('同步用户登录日志');
    }

    protected function execute(Input $input, Output $output)
    {
        $date = $input->getArgument('date');
        $table = 'user_log_' . $date;
        UserLogModel::$datatable = $table;
        UserLogModel::$elastic = 0;
        $list = UserLogModel::getList();
        if (!empty($list['code']) && $list['code'] === 333) {
            Log::warning(env('database.prefix') . $table . ' 表不存在');
            $output->writeln(env('database.prefix') . $table . ' 表不存在');
            exit;
        }
        UserLogModel::$elastic = 1;
        UserLogModel::$datatable = '';
        UserLogModel::addArr($list);
        $output->writeln("success");
        $output->writeln($date);
    }
}
