<?php
namespace app\admin\controller;

use app\common\model\DocArticleModel;
use app\common\model\DocModel;

class Doc extends Common
{
    /**
     * 文档列表
     * @author 贺强
     * @time   2023/4/21 16:15
     * @return array|\think\response\View
     * @throws \Exception
     */
    public function lists()
    {
        $where = [];
        $param = $this->request->param();
        if (!empty($param['keyword'])) {
            $where[] = ['name', 'like', "%{$param['keyword']}%"];
        }
        // 分页参数
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('app.pagesize', 20);
        // 排序参数
        $order = ['ctime' => 'desc'];
        if (!empty($param['sort_field'])) {
            $order = [$param['sort_field']];
            if (!empty($param['sort_type'])) {
                $order = [$param['sort_field'] => $param['sort_type']];
            }
        }
        $list = DocModel::getList($where, true, [$page, $pagesize], $order);
        foreach ($list as &$item) {
            format_timestamp($item['ctime'], $item['mtime']);
            $item['count'] = DocArticleModel::getCount(['doc_id' => $item['id']]);
        }
        if ($this->request->isAjax()) {
            return ajax('获取成功', 200, $list);
        }
        $count = DocModel::getCount($where);
        $pages = ceil($count / $pagesize);
        return view('list', compact('list', 'pages', 'param', 'count'));
    }

    /**
     * 创建文档
     * @author 贺强
     * @time   2023/4/21 16:18
     * @return string|\think\response\Json|\think\response\View
     */
    public function add()
    {
        if ($this->request->isPost()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['name'])) {
                return ajax('文档名称不能为空', 101);
            }
            if ($param['is_open'] === '0' && empty($param['password'])) {
                return ajax('不公开文档密码不能为空', 102);
            }
            if (!empty($param['password'])) {
                $param['password'] = get_md5_password($param['password']);
            }
            $res = DocModel::add($param);
            if (!$res) {
                return ajax('创建失败', 400);
            }
            return ajax('创建成功');
        }
        return view('add');
    }

    /**
     * 修改文档
     * @author 贺强
     * @time   2023/4/21 16:57
     * @return string|\think\response\Json|\think\response\View
     */
    public function edit()
    {
        $id = $this->request->param('id', '');
        $doc = DocModel::getModel(['id' => $id]);
        if ($this->request->isPost()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['name'])) {
                return ajax('文档名称不能为空', 101);
            }
            if ($param['is_open'] === '0' && empty($param['password']) && empty($doc['password'])) {
                return ajax('不公开文档密码不能为空', 102);
            }
            if (!empty($param['password'])) {
                $param['password'] = get_md5_password($param['password']);
            } else {
                unset($param['password']);
            }
            $res = DocModel::modify($param, ['id' => $id]);
            if (!$res) {
                return ajax('修改失败', 400);
            }
            return ajax('修改成功');
        }
        $count = DocArticleModel::getCount(['doc_id' => $id]);
        $pagesize = $param['pagesize'] ?? env('app.pagesize', 10);
        return view('edit', compact('doc', 'count', 'pagesize'));
    }

    /**
     * 删除文档
     * @author 贺强
     * @time   2023/4/24 21:13
     * @return string|\think\response\Json
     */
    public function del()
    {
        if (!$this->request->isPost()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $id = $this->request->post('id', '');
        if (empty($id)) {
            return ajax('参数缺失', 101);
        }
        $res = DocModel::delById($id);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        // 删除文档下的接口
        DocArticleModel::delByWhere(['doc_id' => $id]);
        return ajax('删除成功');
    }

    /**
     * ajax 翻页
     * @author 贺强
     * @time   2023/4/22 14:26
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function ajaxDoc()
    {
        $id = $this->request->param('id', '');
        // 分页参数
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? 10;
        $list = DocArticleModel::getList(['doc_id' => $id], true, [$page, $pagesize], ['sort' => 'asc']);
        foreach ($list as &$item) {
            format_timestamp($item['ctime'], $item['mtime']);
        }
        return ajax('获取成功', 200, $list);
    }

    /**
     * 创建接口
     * @author 贺强
     * @time   2023/4/23 10:53
     * @return string|\think\response\Json|\think\response\View|null
     */
    public function addArticle()
    {
        if ($this->request->isPost()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['doc_id'])) {
                return ajax('所属文档不能为空', 102);
            }
            if (empty($param['name'])) {
                return ajax('接口名称不能为空', 101);
            }
            $res = DocArticleModel::add($param);
            if (!$res) {
                return ajax('创建失败', 400);
            }
            return ajax('创建成功');
        }
        return view();
    }

    /**
     * 修改接口
     * @author 贺强
     * @time   2023/4/23 11:01
     * @return string|\think\response\Json|\think\response\View
     */
    public function editArticle()
    {
        $id = $this->request->param('id', '');
        if ($this->request->isPost()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['name'])) {
                return ajax('接口名称不能为空', 101);
            }
            $res = DocArticleModel::modify($param, ['id' => $id]);
            if (!$res) {
                return ajax('修改失败', 400);
            }
            return ajax('修改成功');
        }
        $article = DocArticleModel::getModel(['id' => $id]);
        return view('', compact('article'));
    }

    /**
     * 删除接口
     * @author 贺强
     * @time   2023/4/24 21:12
     * @return string|\think\response\Json
     */
    public function delArticle()
    {
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $id = $this->request->post('id', '');
        if (empty($id)) {
            return ajax('非法操作', 101);
        }
        $res = DocArticleModel::delById($id);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        return ajax('删除成功');
    }
}
