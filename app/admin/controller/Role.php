<?php

namespace app\admin\controller;

use app\common\model\AdminRoleModel;

/**
 * 管理员角色
 */
class Role extends Common
{
    /**
     * 添加角色
     * @author 贺强
     * @time   2021-09-14 14:43:50
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['role_name'])) {
                return ajax('非法参数', 101);
            }
            $has = AdminRoleModel::getCount(['role_name' => $param['role_name']]);
            if (!empty($has)) {
                return ajax('该角色已存在', 102);
            }
            $param['menus'] = '';
            $param['status'] = 1;
            $param['mtime'] = 0;
            $res = AdminRoleModel::add($param);
            if (!$res) {
                return ajax('添加失败', 400);
            }
            return ajax('添加成功');
        }
        return view('add');
    }

    /**
     * 设置管理员角色
     * @author 贺强
     * @time   2021-09-14 15:17:24
     */
    public function operate()
    {
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $ids = $this->request->post('ids');
        if (empty($ids)) {
            return ajax('非法参数', 101);
        }
        $type = $this->request->post('type');
        if (empty($type)) {
            return ajax('非法操作', 102);
        }
        if ($type === 'del' || $type === 'delAll') {
            $res = AdminRoleModel::delByWhere([['id', 'in', $ids]]);
            if (!$res) {
                return ajax('删除失败', 400);
            }
            return ajax('删除成功');
        } elseif ($type === 'disable' || $type === 'disableAll') {
            $field = 'status';
            $value = 4;
            $msg = '禁用';
        } elseif ($type === 'enable' || $type == 'enableAll') {
            $field = 'status';
            $value = 1;
            $msg = '启用';
        } else {
            return ajax('非法操作', 103);
        }
        $res = AdminRoleModel::modify([$field => $value], [['id', 'in', $ids]]);
        if ($res) {
            return ajax($msg . '成功');
        } elseif ($res === false) {
            return ajax($msg . '失败', 400);
        } else {
            return ajax('该角色已' . $msg, 201);
        }
    }

    /**
     * 管理员角色
     * @author 贺强
     * @time   2021-09-14 15:15:29
     */
    public function lists()
    {
        $where = [['id', '<>', 1]];
        $param = $this->request->get();
        // 分页参数
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('app.pagesize', 20);
        $list = AdminRoleModel::getList($where, true, [$page, $pagesize]);
        // var_dump($list);exit;
        foreach ($list as &$item) {
            $item['status_txt'] = get_role_status($item['status']);
            if (!empty($item['ctime'])) {
                format_datetime($item['ctime']);
            }
        }
        if ($this->request->isAjax()) {
            return ajax('获取成功', 200, $list);
        }
        $count = AdminRoleModel::getCount($where);
        $pages = ceil($count / $pagesize);
        return view('list', compact('list', 'pages'));
    }
}
