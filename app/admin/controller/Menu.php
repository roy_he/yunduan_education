<?php
namespace app\admin\controller;

use app\common\model\FieldAccessModel;
use app\common\model\MenuFieldModel;
use app\common\model\AdminMenuModel;
use app\common\model\AdminRoleModel;

class Menu extends Common
{
    /**
     * 添加菜单
     * @Author 贺强
     * @date   2018-08-23
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['identity']) || empty($param['title'])) {
                return ajax('非法操作', 101);
            }
            $menu = AdminMenuModel::getModel([['is_delete', '=', 0], ['identity', '=', $param['identity']]]);
            if (!empty($menu)) {
                return ajax('菜单标识重复，请重新填写', 102);
            }
            if (empty($param['sort'])) {
                $param['sort'] = 99;
            }
            if (empty($param['url'])) {
                $param['url'] = '#';
            }
            $param['is_delete'] = 0;
            $param['mtime'] = 0;
            $res = AdminMenuModel::add($param);
            if (!$res) {
                return ajax('添加失败', 400);
            }
            return ajax('添加成功');
        }
        $mods = AdminMenuModel::getList([['modular', '=', ''], ['is_delete', '=', 0]], ['id', 'title']);
        return view('add', ['mods' => $mods]);
    }

    /**
     * AJAX获取模块下的菜单
     * @author 贺强
     * @time   2019-05-27 14:56:13
     */
    public function get_menu()
    {
        $mid = $this->request->post('mid');
        $menu = [];
        if ($mid) {
            $menu = AdminMenuModel::getList([['modular', '=', $mid], ['pid', '=', ''], ['is_delete', '=', 0]], ['id', 'title']);
        }
        return ajax('获取成功', 200, $menu);
    }

    /**
     * 删除菜单
     * @Author 贺强
     * @date   2018-08-23
     */
    public function del()
    {
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $id = $this->request->post('id');
        $res = AdminMenuModel::modify(['is_delete' => 1], ['id' => $id]);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        return ajax('删除成功');
    }

    /**
     * 编辑菜单
     * @Author 贺强
     * @date   2018-08-23
     */
    public function edit()
    {
        $id = $this->request->param('id', '');
        if ($this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            if (empty($param['identity']) || empty($param['title'])) {
                return ajax('非法操作', 101);
            }
            if (empty($param['url'])) {
                $param['url'] = '#';
            }
            $res = AdminMenuModel::modify($param, ['id' => $id]);
            if ($res === false) {
                return ajax('修改失败', 400);
            }
            AdminMenuModel::modify(['modular' => $param['modular']], ['pid' => $id]);
            return ajax('修改成功');
        }
        $menu = AdminMenuModel::getModel(['id' => $id], true, '', 0);
        $mods = AdminMenuModel::getList([['modular', '=', ''], ['is_delete', '=', 0]], ['id', 'title']);
        $p_menu = [];
        if ($menu['modular']) {
            $p_menu = AdminMenuModel::getList([['modular', '=', $menu['modular']], ['pid', '=', ''], ['is_delete', '=', 0]], ['id', 'title']);
        }
        return view('edit', compact('menu', 'mods', 'p_menu'));
    }

    /**
     * 菜单列表
     * @Author 贺强
     * @date   2018-08-23
     */
    public function lists()
    {
        $where = [['is_delete', '=', 0]];
        $param = $this->request->param();
        if (!empty($param['keyword'])) {
            $where[] = ['title', 'like', "%{$param['keyword']}%"];
        } else {
            $param['keyword'] = '';
        }
        if (!empty($param['identity'])) {
            $where[] = ['identity', 'like', "%{$param['identity']}%"];
        } else {
            $param['identity'] = '';
        }
        if (!empty($param['modular'])) {
            $where[] = ['modular', '=', $param['modular']];
        } else {
            $param['modular'] = '';
        }
        // 分页参数
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('app.pagesize', 20);
        $list = AdminMenuModel::getList($where, true, [$page, $pagesize], ['modular' => 'asc', 'pid' => 'asc', 'sort' => 'asc']);
        if ($this->request->isAjax()) {
            return ajax('获取成功', 200, $list);
        }
        $count = AdminMenuModel::getCount($where);
        $pages = ceil($count / $pagesize);
        $modular = AdminMenuModel::getList([['modular', '=', ''], ['is_delete', '=', 0]], ['id', 'title']);
        return view('list', compact('list', 'pages', 'count', 'param', 'modular'));
    }

    /**
     * 修改排序
     * @author 贺强
     * @time   2019-06-26 14:23:35
     */
    public function operatesort()
    {
        if (!$this->request->isAjax()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $param = $this->request->post();
        if (empty($param['id']) || empty($param['sort'])) {
            return ajax('参数缺失', 101);
        }
        $res = AdminMenuModel::modify(['sort' => $param['sort']], ['id' => $param['id']]);
        if ($res !== false) {
            return ajax('修改成功');
        }
        return ajax('修改失败', 400);
    }

    /**
     * 菜单权限管理
     * @Author 贺强
     * @date   2018-08-23
     */
    public function power()
    {
        if ($this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $param = $this->request->post();
            $menu_ids = '';
            if (!empty($param['menu_ids'])) {
                $menu_ids = implode(',', $param['menu_ids']);
            }
            $res = AdminRoleModel::modify(['menus' => $menu_ids], ['id' => $param['role_id']]);
            if ($res !== false) {
                return ajax('修改成功');
            }
            return ajax('修改失败', 400);
        } else {
            $roles = AdminRoleModel::getList([['id', '<>', 1]]);
            $role_id = $this->request->get('role_id', 0);
            $has = AdminRoleModel::getFieldValue('menus', ['id' => $role_id]);
            $list = AdminMenuModel::getList(['is_delete' => 0], true, null, 'modular,pid,sort');
            $arr = [];
            foreach ($list as $item) {
                if ($item['pid'] === '') {
                    $arr[] = $item;
                } else {
                    foreach ($arr as &$item2) {
                        if ($item['pid'] === $item2['id']) {
                            $item2['children'][] = $item;
                            break;
                        }
                    }
                }
            }
            $has = explode(',', $has);
            return view('power', ['list' => $arr, 'roles' => $roles, 'has' => $has, 'role_id' => $role_id]);
        }
    }

    /**
     * 添加字段权限
     * @author 贺强
     * @time   2019-07-17 10:52:42
     */
    public function addfield()
    {
        if (!$this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $role = AdminRoleModel::getList([], ['id', 'role_name']);
            $menu = AdminMenuModel::getList(['type' => 'list'], ['id', 'title']);
            return view('addfield', ['role' => $role, 'menu' => $menu]);
        }
        $param = $this->request->post();
        if (empty($param['role_id'])) {
            return ajax('请选择角色', 101);
        }
        if (empty($param['menu_id'])) {
            return ajax('请选择菜单', 102);
        }
        if (empty($param['ziduan'])) {
            return ajax('请选择字段', 103);
        }
        $ziduan = implode(',', $param['ziduan']);
        $param['ziduan'] = $ziduan;
        if (!empty($param['btn'])) {
            $btn = implode(',', $param['btn']);
            $param['btn'] = $btn;
        }
        $res = FieldAccessModel::add($param);
        if (!$res) {
            return ajax('添加失败', 400);
        }
        return ajax('添加成功');
    }

    /**
     * 删除字段权限
     * @author 贺强
     * @time   2019-07-17 13:40:59
     */
    public function delfield()
    {
        if (!$this->request->isAjax()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $id = $this->request->post('id');
        if (empty($id)) {
            return ajax('非法参数', 101);
        }
        $res = FieldAccessModel::delById($id);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        return ajax('删除成功');
    }

    /**
     * 修改字段权限
     * @author 贺强
     * @time   2019-07-17 11:53:49
     */
    public function editfield()
    {
        if ($this->request->isAjax()) {
            if (!empty($this->admin['code'])) {
                return $this->admin;
            }
            $id = $this->request->post('id');
            $param = $this->request->post();
            if (empty($id) || empty($param)) {
                return ajax('参数缺失', 101);
            }
            $param['ziduan'] = implode(',', $param['ziduan']);
            if (!empty($param['btn'])) {
                $param['btn'] = implode(',', $param['btn']);
            } else {
                $param['btn'] = '';
            }
            $res = FieldAccessModel::modify($param, ['id' => $id]);
            if ($res === false) {
                return ajax('修改失败', 400);
            }
            return ajax('修改成功');
        }
        $id = $this->request->get('id');
        $field = FieldAccessModel::getModel([['id', '=', $id]]);
        // 修改字段数据类型
        $field['ziduan'] = explode(',', $field['ziduan']);
        $field['btn'] = explode(',', $field['btn']);
        // 该菜单字段
        $fields = MenuFieldModel::getList(['menu_id' => $field['menu_id']], ['id', 'field_name', 'type']);
        return view('editfield', ['field' => $field, 'fields' => $fields]);
    }

    /**
     * 获取菜单字段
     * @author 贺强
     * @time   2019-07-17 11:14:16
     */
    public function getfields()
    {
        $param = $this->request->post();
        if (empty($param['menu_id'])) {
            return ajax('菜单ID不能为空', 101);
        }
        $list = MenuFieldModel::getList([['menu_id', '=', $param['menu_id']]], ['id', 'field_name', 'type']);
        if (empty($list)) {
            return ajax('获取失败', 400);
        }
        return ajax('获取成功', 200, $list);
    }

    /**
     * 字段权限管理
     * @author 贺强
     * @time   2019-07-17 10:25:52
     */
    public function fields()
    {
        $where = [];
        $param = $this->request->get();
        if (!empty($param['menu_id'])) {
            $where[] = ['menu_id', '=', $param['menu_id']];
        }
        $page = !empty($param['page']) ? $param['page'] : 1;
        $pagesize = env('app.pagesize', 20);
        if (!empty($param['role_id'])) {
            $where[] = ['role_id', '=', $param['role_id']];
        }
        $list = FieldAccessModel::getList($where, true, [$page, $pagesize]);
        if ($list) {
            $role = AdminRoleModel::getList([], ['id', 'role_name']);
            $role = array_column($role, 'role_name', 'id');
            $menu = AdminMenuModel::getList([['type', '=', 'list']], ['id', 'title']);
            $menu = array_column($menu, 'title', 'id');
            foreach ($list as &$item) {
                if (!empty($role[$item['role_id']])) {
                    $item['role_name'] = $role[$item['role_id']];
                } else {
                    $item['role_name'] = '';
                }
                if (!empty($menu[$item['menu_id']])) {
                    $item['title'] = $menu[$item['menu_id']];
                } else {
                    $item['title'] = '';
                }
            }
        }
        $count = FieldAccessModel::getCount($where);
        $pages = ceil($count / $pagesize);
        return view('fields', ['list' => $list, 'param' => $param, 'pages' => $pages]);
    }
}
