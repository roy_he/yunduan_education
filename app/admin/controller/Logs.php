<?php
namespace app\admin\controller;

use app\common\model\LogsModel;

class Logs extends Common
{
    /**
     * 日志列表
     * @author 贺强
     * @time   2023/3/4 20:47
     */
    public function lists()
    {
        $modules = ['api' => '接口日志', 'index' => 'PC端日志', 'm' => '移动端日志', 'admin' => '管理日志', 'command' => '任务日志'];
        $module = $this->request->get('module', 'api');
        LogsModel::$module = $module;
        $param = $this->request->param();
        // 分页参数
        $page = $param['page'] ?? 1;
        $pagesize = $param['pagesize'] ?? env('app.pagesize', 20);
        $where = [];
        if (!empty($param['keyword'])) {
            $where[] = ['msg', 'like', "%{$param['keyword']}%"];
        }
        $list = LogsModel::getList($where, true, [$page, $pagesize], ['time' => 'desc']);
        foreach ($list as &$item) {
            if (!empty($item['time'])) {
                $item['time'] = date('Y-m-d H:i:s', $item['time']);
            }
        }
        if ($this->request->isAjax()) {
            return ajax('获取成功', 200, $list);
        }
        $count = LogsModel::getCount($where);
        $pages = ceil($count / $pagesize);
        return view('list', compact('list', 'count', 'pages', 'param', 'modules', 'module'));
    }

    /**
     * 单条删除
     * @author 贺强
     * @time   2023/3/4 19:56
     */
    public function del()
    {
        if (!$this->request->isAjax()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $id = $this->request->post('id', '');
        if (empty($id)) {
            return ajax('非法参数', 101);
        }
        $res = LogsModel::delById($id);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        return ajax('删除成功');
    }

    /**
     * 批量删除
     * @author 贺强
     * @time   2023/3/4 19:55
     */
    public function delAll()
    {
        if (!$this->request->isAjax()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $ids = $this->request->post('ids', []);
        $module = $this->request->post('module', '');
        if (empty($ids) || empty($module)) {
            return ajax('非法参数', 101);
        }
        LogsModel::$module = $module;
        $res = LogsModel::delByWhere([['id', 'in', $ids]]);
        if (!$res) {
            return ajax('删除失败', 400);
        }
        return ajax('删除成功');
    }

    /**
     * 清空日志
     * @author 贺强
     * @time   2023/3/4 20:49
     */
    public function clearLog()
    {
        if (!$this->request->isAjax()) {
            $this->error('非法操作');
        }
        if (!empty($this->admin['code'])) {
            return $this->admin;
        }
        $module = $this->request->post('module', '');
        if (empty($module)) {
            return ajax('参数缺失', 101);
        }
        LogsModel::$module = $module;
        $res = LogsModel::delIndex();
        if (!$res) {
            return ajax('清空失败', 400);
        }
        return ajax('清空成功');
    }
}
