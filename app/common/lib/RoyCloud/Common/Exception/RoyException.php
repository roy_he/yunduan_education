<?php
namespace app\common\lib\RoyCloud\Common\Exception;

use Exception;

class RoyException extends Exception
{
    private $requestId;
    private $errorCode;

    public function __construct($message = "", $code = 0, $requestId = '')
    {
        parent::__construct($message, $code);
        $this->errorCode = $code;
        $this->requestId = $requestId;
    }

    public function getRequestId()
    {
        return $this->requestId;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function __toString()
    {
        return '[' . __CLASS__ . '] code:' . $this->errorCode .
            ' message:' . $this->getMessage() .
            ' RequestId:' . $this->requestId;
    }
}
