<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class SendMessageRequest extends AbstractModel
{
    /**
     * @var string $Id 消息ID
     */
    public $Id;
    /**
     * @var string $MsgBody 消息内容
     */
    public $MsgBody;
    /**
     * @var string $TopicName 消息所属主题
     */
    public $TopicName;
    /**
     * @var string $QueueName 消息所属队列
     */
    public $QueueName;
    /**
     * @var string $Ctime 创建时间
     */
    public $Ctime;
    /**
     * @var string $Mtime 修改时间
     */
    public $Mtime;

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('id', $param)) {
            $this->Id = $param['id'];
        }
        if (array_key_exists('msg_body', $param)) {
            $this->MsgBody = $param['msg_body'];
        }
        if (array_key_exists('topic_name', $param)) {
            $this->TopicName = $param['topic_name'];
        }
        if (array_key_exists('queue_name', $param)) {
            $this->QueueName = $param['queue_name'];
        }
        if (array_key_exists('ctime', $param)) {
            $this->Ctime = $param['ctime'];
        } else {
            $this->Ctime = time();
        }
        if (array_key_exists('mtime', $param)) {
            $this->Mtime = $param['mtime'];
        } else {
            $this->Mtime = time();
        }
    }
}
