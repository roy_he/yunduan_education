<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class CreateQueueResponse extends AbstractModel
{
    /**
     * @var string $Id 队列ID
     */
    public $Id;
    /**
     * @var string $QueueName 队列名称
     */
    public $QueueName;
    /**
     * @var string $Remark 队列说明
     */
    public $Remark;
    /**
     * @var string $CreateTime 创建时间
     */
    public $CreateTime;
    /**
     * @var string $ModifyTime 修改时间
     */
    public $ModifyTime;

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('Id', $param)) {
            $this->Id = $param['Id'];
        }
        if (array_key_exists('QueueName', $param)) {
            $this->QueueName = $param['QueueName'];
        }
        if (array_key_exists('Remark', $param)) {
            $this->Remark = $param['Remark'];
        }
        if (array_key_exists('CreateTime', $param)) {
            $this->CreateTime = $param['CreateTime'];
        } else {
            unset($this->CreateTime);
        }
        if (array_key_exists('ModifyTime', $param)) {
            $this->ModifyTime = $param['ModifyTime'];
        } else {
            unset($this->ModifyTime);
        }
    }
}
