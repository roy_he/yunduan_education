<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class CreateTopicResponse extends AbstractModel
{
    /**
     * @var string $Id 主题ID
     */
    public $Id;
    /**
     * @var string $TopicName 主题名称
     */
    public $TopicName;
    /**
     * @var string $Remark 说明，最多 128 个字符
     */
    public $Remark;
    /**
     * @var string $CreateTime 创建时间
     */
    public $CreateTime;
    /**
     * @var string $ModifyTime 修改时间
     */
    public $ModifyTime;

    /**
     * @param string $TopicId    主题ID
     * @param string $TopicName  主题名称
     * @param string $Remark     说明，最多 128 个字符
     * @param string $CreateTime 创建时间
     * @param string $ModifyTime 修改时间
     */
    public function __construct()
    {
    }

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('Id', $param)) {
            $this->Id = $param['Id'];
        }
        if (array_key_exists('TopicName', $param)) {
            $this->TopicName = $param['TopicName'];
        }
        if (array_key_exists('Remark', $param)) {
            $this->Remark = $param['Remark'];
        }
        if (array_key_exists('CreateTime', $param)) {
            $this->CreateTime = $param['CreateTime'];
        } else {
            unset($this->CreateTime);
        }
        if (array_key_exists('ModifyTime', $param)) {
            $this->ModifyTime = $param['ModifyTime'];
        } else {
            unset($this->ModifyTime);
        }
    }
}
