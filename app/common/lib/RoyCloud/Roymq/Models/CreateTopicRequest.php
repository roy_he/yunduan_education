<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class CreateTopicRequest extends AbstractModel
{
    /**
     * @var string $Id 主题ID
     */
    public $Id;
    /**
     * @var string $TopicName 主题名称，长度 3-64 个字符
     */
    public $TopicName;
    /**
     * @var string $Remark 主题说明
     */
    public $Remark;
    /**
     * @var string $Ctime 创建时间
     */
    public $Ctime;
    /**
     * @var string $Mtime 修改时间
     */
    public $Mtime;

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('id', $param)) {
            $this->Id = $param['id'];
        }
        if (array_key_exists('topic_name', $param)) {
            $this->TopicName = $param['topic_name'];
        }
        if (array_key_exists('remark', $param)) {
            $this->Remark = $param['remark'];
        }
        if (array_key_exists('ctime', $param)) {
            $this->Ctime = $param['ctime'];
        } else {
            $this->Ctime = time();
        }
        if (array_key_exists('mtime', $param)) {
            $this->Mtime = $param['mtime'];
        } else {
            $this->Mtime = time();
        }
    }
}
