<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class CreateQueueRequest extends AbstractModel
{
    /**
     * @var string $Id 队列ID
     */
    public $Id;
    /**
     * @var string $QueueName 队列名称
     */
    public $QueueName;
    /**
     * @var string $Remark 队列说明
     */
    public $Remark;
    /**
     * @var string $Ctime 创建时间
     */
    public $Ctime;
    /**
     * @var string $Mtime 修改时间
     */
    public $Mtime;

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('id', $param)) {
            $this->Id = $param['id'];
        }
        if (array_key_exists('queue_name', $param)) {
            $this->QueueName = $param['queue_name'];
        }
        if (array_key_exists('remark', $param)) {
            $this->Remark = $param['remark'];
        }
        if (array_key_exists('ctime', $param)) {
            $this->Ctime = $param['ctime'];
        } else {
            $this->Ctime = time();
        }
        if (array_key_exists('mtime', $param)) {
            $this->Mtime = $param['mtime'];
        } else {
            $this->Mtime = time();
        }
    }
}
