<?php
namespace app\common\lib\RoyCloud\Roymq\Models;

use app\common\lib\RoyCloud\Common\AbstractModel;

class SendMessageResponse extends AbstractModel
{
    /**
     * @var string $Id 消息ID
     */
    public $Id;
    /**
     * @var string $MsgBody 消息内容
     */
    public $MsgBody;
    /**
     * @var string $Topic 消息所属主题
     */
    public $Topic;
    /**
     * @var string $Queue 消息所属队列
     */
    public $Queue;
    /**
     * @var string $CreateTime 创建时间
     */
    public $CreateTime;

    public function deserialize($param)
    {
        if (empty($param)) {
            return;
        }
        if (array_key_exists('Id', $param)) {
            $this->Id = $param['Id'];
        }
        if (array_key_exists('MsgBody', $param)) {
            $this->MsgBody = $param['MsgBody'];
        }
        if (array_key_exists('QueueName', $param)) {
            $this->Queue = $param['QueueName'];
        }
        if (array_key_exists('TopicName', $param) && !empty($param['TopicName'])) {
            $this->Topic = $param['TopicName'];
        } else {
            unset($this->Topic);
        }
        if (array_key_exists('CreateTime', $param)) {
            $this->CreateTime = $param['CreateTime'];
        } else {
            unset($this->CreateTime);
        }
    }
}
