<?php
namespace app\common\lib\RoyCloud\Roymq;

use app\common\lib\RoyCloud\Common\AbstractClient;
use app\common\lib\RoyCloud\Roymq\Models as Models;

/**
 * @method Models\CreateTopicResponse CreateTopic(Models\CreateTopicRequest $req) 创建主题
 * @method Models\CreateQueueResponse CreateQueue(Models\CreateQueueRequest $req) 创建队列
 * @method Models\SendMessageResponse SendMessage(Models\SendMessageRequest $req) 发送消息
 */
class RoyClient extends AbstractClient
{
    public function __construct()
    {
        parent::__construct();
    }

    public function returnResponse($action, $param)
    {
        $respClass = 'app\\common\\lib\\RoyCloud\\Roymq\\Models\\' . ucfirst($action) . "Response";
        $obj = new $respClass();
        $obj->deserialize($param);
        return $obj;
    }
}
