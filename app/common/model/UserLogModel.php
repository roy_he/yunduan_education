<?php
namespace app\common\model;

class UserLogModel extends CommonModel
{
    /**
     * @var int 是否启用 es
     */
    public static $elastic = 1;

    /**
     * @var string 表名
     */
    public static $datatable = '';
}
