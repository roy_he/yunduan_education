<?php
namespace app\common\model;

class SmsCodeModel extends CommonModel
{
    /**
     * @var int 对应表名是否不需要 id 字段
     */
    public static $has_no_id = 1;
}
