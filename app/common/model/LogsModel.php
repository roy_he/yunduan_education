<?php
namespace app\common\model;

class LogsModel
{
    /**
     * @var string 模块名
     */
    public static $module = '';

    /**
     * 根据模块名组合es索引
     * @author 贺强
     * @time   2023/3/4 11:59
     * @return string
     */
    private static function getEsIndex()
    {
        if (static::$module !== 'command') {
            $prefix = config('log.es_index_of_log', 'es_index_of_log_');
            return $prefix . static::$module;
        } else {
            return 'es_index_of_command_log_app';
        }
    }

    /**
     * 删除索引
     * @author 贺强
     * @time   2023/3/4 20:57
     */
    public static function delIndex()
    {
        $index = self::getEsIndex();
        $res = EsModel::delIndex($index);
        return $res;
    }

    /**
     * 根据日志ID删除日志
     * @author 贺强
     * @time   2023/3/4 11:59
     * @param string $id 日志ID
     * @return bool
     */
    public static function delById($id)
    {
        $index = self::getEsIndex();
        if (!empty($id)) {
            queue('app\\common\\job\\EsJob@del_es', ['index' => $index, 'id' => $id]);
            return true;
        }
        return false;
    }

    /**
     * 根据条件删除数据
     * @author 贺强
     * @time   2023/3/4 14:49
     * @param string $where 删除条件，[['id','>', 3],……]
     * @return int 返回删除的条数
     */
    public static function delByWhere($where = '')
    {
        $ids = self::getField('id', $where);
        if (!empty($ids)) {
            $index = self::getEsIndex();
            foreach ($ids as $id) {
                queue('app\\common\\job\\EsJob@del_es', ['index' => $index, 'id' => $id]);
            }
            return count($ids);
        }
        return 0;
    }

    /**
     * 根据条件获取某一列的值
     * @author 贺强
     * @time   2023/3/4 14:44
     * @param string $field 要获取的字段名
     * @param array  $where 条件 [['id','=',1],……]或SQL字符串
     * @param null   $limit 查询多少条
     * @return array 返回某字段值的数组
     */
    public static function getField($field, $where = [], $limit = null)
    {
        $index = self::getEsIndex();
        $param = CommonModel::getElasticTerm($where);
        $body = [];
        if (!empty($param)) {
            $body['bool'] = $param;
        }
        $page = 1;
        $size = 10000;
        if (!empty($limit)) {
            if (is_numeric($limit)) {
                $size = $limit;
            } elseif (strpos($limit, ',')) {
                [$page, $size] = explode(',', $limit);
            }
        }
        $list = EsModel::getData($index, $field, $body, '', $page, $size);
        if (!empty($list['list'])) {
            $list = array_column($list['list'], '_source');
            $list = array_column($list, $field);
            return $list;
        }
        return [];
    }

    /**
     * 获取日志列表
     * @author 贺强
     * @time   2023/3/4 14:55
     * @param array  $where 条件 [['id','>', 3],……]
     * @param bool   $field 要查询的字段
     * @param string $limit 分页查询 '1,10' 页码和每页条数
     * @param string $order 按某(些)字段排序 ['id'=>'desc']或'id desc'
     * @return array 返回查询结果
     */
    public static function getList($where = null, $field = true, $limit = null, $order = null)
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $index = self::getEsIndex();
        $exists = EsModel::isExist($index);
        if (!$exists) {
            return [];
        }
        $param = CommonModel::getElasticTerm($where);
        $body = [];
        if (!empty($param)) {
            $body['bool'] = $param;
        }
        $page = 1;
        $size = 10000;
        if (is_array($limit)) {
            [$page, $size] = $limit;
        } elseif (is_string($limit)) {
            if (strpos($limit, ',') !== false) {
                [$page, $size] = explode(',', $limit);
            } elseif (is_numeric($limit)) {
                $size = $limit;
            }
        }
        $list = EsModel::getData($index, $field, $body, $order, $page, $size);
        if ($debug === 'model_r') {
            print_r($list);
            exit;
        }
        if (!empty($list['list'])) {
            $list = array_column($list['list'], '_source');
            return $list;
        }
        return [];
    }

    /**
     * 获取总数
     * @author 贺强
     * @time   2023/5/30 15:19
     * @param mixed  $where 条件
     * @param string $field 要获取数量的字段
     * @return int|mixed
     */
    public static function getCount($where = null, $field = '*')
    {
        $index = self::getEsIndex();
        $exists = EsModel::isExist($index);
        if (!$exists) {
            return 0;
        }
        $param = CommonModel::getElasticTerm($where);
        $body = [];
        if (!empty($param)) {
            $body['bool'] = $param;
        }
        $count = EsModel::getData($index, $field, $body, [], 1, 1);
        return $count['count'] ?? 0;
    }
}
