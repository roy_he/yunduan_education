<?php
namespace app\common\job;

use app\common\model\RoymqTopicModel;
use think\facade\Log;
use think\queue\Job;

class TopicJob
{
    /**
     * 创建主题
     * @author 贺强
     * @time   2022/6/24 13:35
     * @param Job   $job  当前任务
     * @param array $data 创建参数
     * @return bool
     */
    public function add_topic(Job $job, array $data) : bool
    {
        if (empty($data['TopicName'])) {
            $job->delete();
            return false;
        }
        try {
            $data_new = [];
            foreach ($data as $k => $v) {
                $data_new[unhump($k)] = $v;
            }
            $res = RoymqTopicModel::add($data_new);
            if ($res) {
                $job->delete();
                return true;
            } elseif ($job->attempts() > 10) {
                $job->delete();
                $error = ['jobName' => $job->getName(), 'data' => $data];
                throw new \Exception('执行次数超限:' . json_encode($error));
            } else {
                $job->release(3);
                return false;
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $job->delete();
            return false;
        }
    }
}
