<?php
namespace app\common\authorize;

class WX
{
    private $app_id;
    private $secret;
    private $redirect_uri;

    public function __construct()
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $config = config('app.params.authorized_login.wx_config');
        $this->app_id = $config['app_id'] ?? '';
        $this->secret = $config['secret'] ?? '';
        $this->redirect_uri = $config['redirect_uri'] ?? '';
        if ($debug === 'show_params') {
            var_dump("app_id:{$this->app_id}", "secret:{$this->secret}", "redirect_uri:{$this->redirect_uri}");
            exit;
        }
    }

    /**
     * 请求CODE
     * @author 贺强
     * @time   2023/1/13 10:12
     * @param string $state 自定义参数
     * @return array|string
     */
    public function getCode($state = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'appid' => $this->app_id,
            'redirect_uri' => $this->redirect_uri,
            'response_type' => 'code',
            'scope' => 'snsapi_login',
        ];
        if (!empty($state)) {
            $param['state'] = $state;
        }
        $str = http_build_query($param);
        $url = "https://open.weixin.qq.com/connect/qrconnect?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 通过 code 获取access_token
     * @author 贺强
     * @time   2023/1/13 10:17
     * @param string $code code
     * @return array|string
     */
    public function getAccessToken($code = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'appid' => $this->app_id,
            'secret' => $this->secret,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ];
        $str = http_build_query($param);
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 刷新access_token有效期
     * @author 贺强
     * @time   2023/1/13 10:24
     * @param string $refresh_token 填写通过access_token获取到的refresh_token参数
     * @return array|string
     */
    public function refreshAccessToken($refresh_token = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'appid' => $this->app_id,
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
        ];
        $str = http_build_query($param);
        $url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 获取用户个人信息
     * @author 贺强
     * @time   2023/1/13 10:30
     * @param string $access_token 调用凭证
     * @param string $openid       普通用户的标识，对当前开发者帐号唯一
     * @return array|string
     */
    public function getUserInfo($access_token = '', $openid = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }
}
