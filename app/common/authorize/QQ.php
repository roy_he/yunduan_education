<?php
namespace app\common\authorize;

class QQ
{
    private $app_id;
    private $app_key;
    private $redirect_uri;

    public function __construct()
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $config = config('app.params.authorized_login.qq_config');
        $this->app_id = $config['app_id'] ?? '';
        $this->app_key = $config['app_key'] ?? '';
        $this->redirect_uri = $config['redirect_uri'] ?? '';
        if ($debug === 'show_params') {
            var_dump("app_id:{$this->app_id}", "app_key:{$this->app_key}", "redirect_uri:{$this->redirect_uri}");
            exit;
        }
    }

    /**
     * 获取Authorization Code
     * @author 贺强
     * @time   2023/1/13 10:50
     * @param string $scope 请求用户授权时向用户显示的可进行授权的列表, 默认 get_user_info
     * @return array|string
     */
    public function getCode($scope = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $state = get_random_str();
        $param = [
            'client_id' => $this->app_id,
            'response_type' => 'code',
            'redirect_uri' => $this->redirect_uri,
            'state' => $state,
        ];
        if (!empty($scope)) {
            $param['scope'] = $scope;
        }
        $str = http_build_query($param);
        $url = "https://graph.qq.com/oauth2.0/authorize?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 通过Authorization Code获取Access Token
     * @author 贺强
     * @time   2023/1/13 11:11
     * @param string $code authorization code
     * @return array|string
     */
    public function getAccessToken($code = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'grant_type' => 'authorization_code',
            'client_id' => $this->app_id,
            'client_secret' => $this->app_key,
            'code' => $code,
            'redirect_uri' => $this->redirect_uri,
            'fmt' => 'json',
        ];
        $str = http_build_query($param);
        $url = "https://graph.qq.com/oauth2.0/token?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 权限自动续期
     * @author 贺强
     * @time   2023/1/13 11:19
     * @param string $refresh_token refresh_token
     * @return array|string
     */
    public function refreshAccessToken($refresh_token = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'grant_type' => 'refresh_token',
            'client_id' => $this->app_id,
            'client_secret' => $this->app_key,
            'refresh_token' => $refresh_token,
            'fmt' => 'json',
        ];
        $str = http_build_query($param);
        $url = "https://graph.qq.com/oauth2.0/token?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 获取用户OpenID
     * @author 贺强
     * @time   2023/1/13 11:25
     * @param string $access_token access_token
     * @return array|string
     */
    public function getOpenid($access_token = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $url = "https://graph.qq.com/oauth2.0/me?access_token={$access_token}&fmt=json";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 获取登录用户的昵称、头像、性别
     * @author 贺强
     * @time   2023/1/13 11:30
     * @param string $access_token access_token
     * @param string $openid       用户的ID, 与QQ号码一一对应
     * @return array|string
     */
    public function getUserInfo($access_token = '', $openid = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'access_token' => $access_token,
            'oauth_consumer_key' => $this->app_id,
            'openid' => $openid,
        ];
        $str = http_build_query($param);
        $url = "https://graph.qq.com/user/get_user_info?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }
}
