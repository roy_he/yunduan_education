<?php
namespace app\common\authorize;

class DD
{
    private $app_id;
    private $secret;
    private $redirect_uri;

    public function __construct()
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $config = config('app.params.authorized_login.dd_config');
        $this->app_id = $config['app_id'] ?? '';
        $this->secret = $config['secret'] ?? '';
        $this->redirect_uri = $config['redirect_uri'] ?? '';
        if ($debug === 'show_params') {
            var_dump("app_id:{$this->app_id}", "secret:{$this->secret}", "redirect_uri:{$this->redirect_uri}");
            exit;
        }
    }

    /**
     * 获取钉钉登录二维码链接
     * @author 贺强
     * @time   2023/1/13 14:19
     * @param string $state 自定义参数
     * @return array|string
     */
    public function getQrcode($state = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $param = [
            'appid' => $this->app_id,
            'response_type' => 'code',
            'scope' => 'snsapi_login',
            'redirect_uri' => urlencode($this->redirect_uri),
        ];
        if (!empty($state)) {
            $param['state'] = $state;
        }
        $str = http_build_query($param);
        $url = "https://oapi.dingtalk.com/connect/qrconnect?{$str}";
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url);
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 获取登录用户信息
     * @author 贺强
     * @time   2023/1/13 14:40
     * @param string $code
     * @return array|string
     */
    public function getUserInfo($code = '')
    {
        $debug = $_GET['debug'] ?? ($_COOKIE['debug'] ?? '');
        $url = $this->getAccessTokenUrl();
        if ($debug === 'url') {
            var_dump($url);
            exit;
        }
        $data = curl($url, ['tmp_auth_code' => $code], 'post');
        if ($debug === 'data') {
            var_dump($data);
            exit;
        }
        return $data;
    }

    /**
     * 获取 access_token url
     * @author 贺强
     * @time   2023/1/13 14:37
     */
    public function getAccessTokenUrl()
    {
        $timestamp = get_millisecond();
        $signature = $this->getSignature($this->secret, $timestamp);
        $param = [
            'signature' => $signature,
            'timestamp' => $timestamp,
            'accessKey' => $this->app_id,
        ];
        $str = http_build_query($param);
        $url = "https://oapi.dingtalk.com/sns/getuserinfo_bycode?{$str}";
        return $url;
    }

    /**
     * 获取签名
     * @author 贺强
     * @time   2023/1/13 14:28
     * @param string $secret    密钥
     * @param int    $timestamp 当前时间辍
     * @return string
     */
    public function getSignature($secret = '', $timestamp = 0)
    {
        $str = hash_hmac('sha256', $timestamp, $secret, true);
        $str = base64_encode($str);
        $str = urlencode($str);
        return $str;
    }
}
