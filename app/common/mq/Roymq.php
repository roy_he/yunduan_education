<?php
namespace app\common\mq;

use app\common\lib\RoyCloud\Common\Exception\RoyException;
use app\common\lib\RoyCloud\Roymq\Models\CreateQueueRequest;
use app\common\lib\RoyCloud\Roymq\Models\CreateTopicRequest;
use app\common\lib\RoyCloud\Roymq\Models\SendMessageRequest;
use app\common\lib\RoyCloud\Roymq\RoyClient;

class Roymq
{
    /**
     * 创建主题
     * @author 贺强
     * @time   2022/6/24 13:57
     * @param array $param 创建参数
     * @return mixed
     */
    public static function addTopic($param)
    {
        try {
            if (empty($param['topic_name'])) {
                throw new RoyException('主题名称不能为空');
            }
            $req = new CreateTopicRequest();
            $req->fromParam($param);
            $client = new RoyClient();
            return $client->CreateTopic($req);
        } catch (RoyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * 创建队列
     * @author 贺强
     * @time   2022/6/28 9:42
     * @param array $param 创建参数
     * @return mixed
     */
    public static function addQueue($param)
    {
        try {
            if (empty($param['queue_name'])) {
                throw new RoyException('队列名称不能为空');
            }
            $req = new CreateQueueRequest();
            $req->fromParam($param);
            $client = new RoyClient();
            return $client->CreateQueue($req);
        } catch (RoyException $e) {
            return $e->getMessage();
        }
    }

    /**
     * 发送消息
     * @author 贺强
     * @time   2022/6/24 15:09
     * @param array $param 消息参数
     * @return mixed
     */
    public static function sendMsg($param)
    {
        try {
            if (empty($param['msg_body'])) {
                throw new RoyException('消息内容不能为空');
            }
            $req = new SendMessageRequest();
            $req->fromParam($param);
            $client = new RoyClient();
            return $client->SendMessage($req);
        } catch (RoyException $e) {
            return $e->getMessage();
        }
    }
}
