<?php
namespace think\log\driver;

use Elasticsearch\ClientBuilder;
use GuzzleHttp\RequestOptions;
use think\App;
use think\contract\LogHandlerInterface;
use think\Exception;

class Elastic implements LogHandlerInterface
{
    protected $app;
    protected $config = [
        'host' => '127.0.0.1',
        'port' => '9200',
        'scheme' => 'http',
    ];

    // 实例化并传入参数
    public function __construct(App $app, $config = [])
    {
        if (!empty($config) && is_array($config)) {
            $this->config = array_merge($this->config, $config);
            $this->config = [$this->config];
        }
        // var_export($this->config);exit;
    }

    /**
     * 日志写入接口
     * @author 贺强
     * @time   2023/3/4 8:48
     * @param array $log 日志信息
     * @return bool
     */
    public function save($log) : bool
    {
        // print_r($log);exit;
        $es_prefix = config('log.es_index_of_log', 'es_index_of_log_');
        $path = app_path();
        $path = rtrim($path, DIRECTORY_SEPARATOR);
        $path = substr($path, strrpos($path, DIRECTORY_SEPARATOR) + 1);
        $index = $es_prefix . $path;
        // var_dump($index);exit;
        $info = [];
        foreach ($log as $type => $val) {
            foreach ($val as $msg) {
                // var_dump($msg);exit;
                if (strpos($msg, 'Failed to connect to') !== false) {
                    return true;
                }
                if (!is_string($msg)) {
                    if (method_exists($msg, 'getMessage')) {
                        $msg = $msg->getMessage();
                    } else {
                        $msg = var_export($msg, true);
                    }
                }
                $str = $index . '@#$' . get_microsecond() . '%&*' . get_random_str();
                $id = md5($str);
                $info[] = ['id' => $id, 'type' => $type, 'time' => time(), 'msg' => $msg];
            }
        }
        // print_r($info);exit;
        if ($info) {
            return $this->write($info, $index);
        }
        return true;
    }

    protected function write(array $info, string $index) : bool
    {
        // var_export($this->config);exit;
        $client = ClientBuilder::create()->setHosts($this->config)
            ->setConnectionPool('\Elasticsearch\ConnectionPool\SimpleConnectionPool', [])
            ->setConnectionParams([
                'client' => [
                    RequestOptions::CONNECT_TIMEOUT => 30,
                    RequestOptions::TIMEOUT => 30,
                ]])
            ->setRetries(0)->build();
        // var_export($client);exit;
        $exists = $client->indices()->exists(['index' => $index]);
        // var_dump($exists);exit;
        if (!$exists) {
            $param = [
                'index' => $index,
                'body' => [
                    'settings' => [
                        'number_of_shards' => 10,
                        'number_of_replicas' => 1,
                        'max_result_window' => 100000000,
                        'refresh_interval' => '10s',
                    ],
                    'mappings' => [
                        'properties' => [
                            'id' => ['type' => 'keyword'],
                            'type' => ['type' => 'keyword'],
                            'time' => ['type' => 'integer'],
                            'msg' => [
                                'type' => 'text',
                                'fields' => [
                                    'keyword' => [
                                        'type' => 'keyword',
                                        'ignore_above' => 256,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ];
            $res = $client->indices()->create($param);
            // var_dump($res);exit;
        }
        $param = ['body' => []];
        foreach ($info as $type => $item) {
            $param['body'][] = [
                'index' => ['_index' => $index, '_type' => '_doc', '_id' => $item['id']],
            ];
            $param['body'][] = $item;
        }
        if (!empty($param['body'])) {
            $client->bulk($param);
        }
        return count($info) > 0;
    }
}
