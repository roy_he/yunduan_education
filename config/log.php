<?php
// +----------------------------------------------------------------------
// | 日志设置
// +----------------------------------------------------------------------
$host = $_SERVER['HTTP_HOST'] ?? '';
$host = substr($host, strpos($host, '.') + 1);
if ($host === false) {
    $host = 'es_index_of_command';
}
$host = str_replace('.', '_', $host) . '_log_';
return [
    // 默认日志记录通道
    'default' => env('log.channel', 'file'),
    // 日志记录级别
    'level' => [],
    // 日志类型记录的通道 ['error'=>'email',...]
    'type_channel' => [],
    // 关闭全局日志写入
    'close' => false,
    // 全局日志处理 支持闭包
    'processor' => null,
    // 日志通道列表
    'channels' => [
        'file' => [
            // 日志记录方式
            'type' => 'File',
            // 日志保存目录
            'path' => '',
            // 单文件日志写入
            'single' => false,
            // 独立日志级别
            'apart_level' => [],
            // 最大日志文件数量
            'max_files' => 0,
            // 使用JSON格式记录
            'json' => true,
            // 日志处理
            'processor' => null,
            // 关闭通道日志写入
            'close' => false,
            // 日志时间格式
            'time_format' => 'Y-m-d H:i:s',
            // 日志输出格式化
            'format' => '[%s][%s] %s',
            // 是否实时写入
            'realtime_write' => false,
        ],
        // 其它日志通道配置
        'elastic' => [
            // 日志记录方式
            'type' => 'Elastic',
            // 关闭通道写入日志
            'close' => false,
            // 是否实时写入
            'realtime_write' => true,
            // host
            'host' => '127.0.0.1',
            // port
            'port' => '9200',
            // scheme
            'scheme' => 'http',
        ],
    ],
    'es_index_of_log' => $host,
];
