<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------
$py_file = __DIR__ . '/pinyin.php';
if (file_exists($py_file)) {
    $py = include_once $py_file;
} else {
    $py = [];
}
$u2l_file = __DIR__ . '/upper2lower.php';
if (file_exists($u2l_file)) {
    $upper2lower = include_once $u2l_file;
} else {
    $upper2lower = [];
}
$nation_file = __DIR__ . '/nations.php';
if (file_exists($nation_file)) {
    $nations = include_once $nation_file;
} else {
    $nations = [];
}
$host = $_SERVER['HTTP_HOST'] ?? '';
$host = substr($host, strpos($host, '.') + 1);
$path = str_replace('.', '_', $host);
$path = $path ? rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR : '';
$domain_file = __DIR__ . DIRECTORY_SEPARATOR . $path . 'domain.php';
if (file_exists($domain_file)) {
    $domain = include_once $domain_file;
} else {
    $domain = ['www' => 'index', 'admin' => 'admin', 'api' => 'api'];
}
$params_file = __DIR__ . DIRECTORY_SEPARATOR . $path . 'params.php';
if (file_exists($params_file)) {
    $params = include_once $params_file;
} else {
    $params = [];
}
$es_file = __DIR__ . DIRECTORY_SEPARATOR . $path . 'elastic.php';
if (file_exists($es_file)) {
    $elastic = include_once $es_file;
} else {
    $elastic = [];
}
return [
    // 应用地址
    'app_host' => env('app.host', ''),
    // 应用的命名空间
    'app_namespace' => '',
    // 是否启用路由
    'with_route' => true,
    // 默认应用
    'default_app' => 'index',
    // 默认时区
    'default_timezone' => env('app.default_timezone', 'Asia/Shanghai'),
    // 应用映射（自动多应用模式有效）
    'app_map' => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind' => $domain,
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list' => ['common', 'listener', 'middleware', 'subscribe'],
    // 异常页面的模板文件
    'exception_tmpl' => app()->getThinkPath() . 'tpl/think_exception.tpl',
    // 错误显示信息,非调试模式有效
    'error_message' => '出错啦！请歇会再试～',
    // 显示错误信息
    'show_error_msg' => env('app.show_error_msg', false),
    // 默认多应用
    'auto_multi_app' => true,
    // extend 目录路径
    'EXTEND_PATH' => root_path() . 'extend' . DIRECTORY_SEPARATOR,
    // 拼音
    'PY' => $py,
    // 大写字母转小写字母
    'upper2lower' => $upper2lower,
    // 民族
    'nations' => $nations,
    // 其它参数配置
    'params' => $params,
    // elastic 参数配置
    'elastic' => $elastic,
];
