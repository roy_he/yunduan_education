<?php
return [
    'custom_field_tables' => [
        env('database.prefix', '') . 'org',
        env('database.prefix', '') . 'school',
        env('database.prefix', '') . 'user',
    ],
    'param1' => 'value1',
    'param2' => 'value2',
    'param3' => 'value3',
    'authorized_login' => [
        'wx_config' => [
            'app_id' => '',
            'secret' => '',
            'redirect_uri' => '',
        ],
        'qq_config' => [
            'app_id' => '',
            'app_key' => '',
            'redirect_uri' => '',
        ],
        'dd_config' => [
            'app_id' => '',
            'secret' => '',
            'redirect_uri' => '',
        ],
    ],
];