$(function () {
    $('.royui-modular>a').click(function () {
        $(this).addClass('royui-active').siblings('a').removeClass('royui-active')
        let $modular = $(this).data('modular')
        $('.royui-nav').css('display', 'none')
        $('.royui-nav[modular="' + $modular + '"]').css('display', 'block')
    })
    $('.royui-left').on('click', '.royui-nav-li-subset1>a', function () {
        let $li = $(this).parent('li');
        $li.toggleClass('royui-nav-li-subset2').siblings('li').removeClass('royui-nav-li-subset2').find('.royui-nav-subset1').css('display', 'none')
        if ($li.hasClass('royui-nav-li-subset2')) {
            $li.find('.royui-nav-subset1').css('display', 'block')
        } else {
            $li.find('.royui-nav-subset1').css('display', 'none')
        }
    })
    $('.royui-menu-item').click(function () {
        let $url = $(this).data('url')
        $('.royui-iframe').attr('src', $url)
    })
    function s(){
        $('.royui-modular>a:first').addClass('royui-active')
        let $modular = $('.royui-modular>a:first').data('modular')
        $('.royui-nav[modular="' + $modular + '"]').css('display', 'block')
    }
    s()
})
