$(function () {
    function f(l) {
        let k = 0;
        $(l).each(function () {
            k += $(this).outerWidth(true)
        });
        return k
    }

    function g(n) {
        let o = f($(n).prevAll()), q = f($(n).nextAll());
        let l = f($('.content-tabs').children().not('.J_menuTabs'));
        let k = $('.content-tabs').outerWidth(true) - l;
        let p = 0;
        if ($('.page-tabs-content').outerWidth() < k) {
            p = 0
        } else {
            if (q <= (k - $(n).outerWidth(true) - $(n).next().outerWidth(true))) {
                if ((k - $(n).next().outerWidth(true)) > q) {
                    p = o;
                    let m = n;
                    while ((p - $(m).outerWidth()) > ($('.page-tabs-content').outerWidth() - k)) {
                        p -= $(m).prev().outerWidth();
                        m = $(m).prev()
                    }
                }
            } else {
                if (o > (k - $(n).outerWidth(true) - $(n).prev().outerWidth(true))) {
                    p = o - $(n).prev().outerWidth(true)
                }
            }
        }
        $('.page-tabs-content').animate({marginLeft: 0 - p + 'px'}, 'fast')
    }

    function a() {
        let o = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
        let l = f($('.content-tabs').children().not('.J_menuTabs'));
        let k = $('.content-tabs').outerWidth(true) - l;
        let p = 0;
        if ($('.page-tabs-content').width() < k) {
            return false
        } else {
            let m = $('.J_menuTab:first');
            let n = 0;
            while ((n + $(m).outerWidth(true)) <= o) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            n = 0;
            if (f($(m).prevAll()) > k) {
                while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
                    n += $(m).outerWidth(true);
                    m = $(m).prev()
                }
                p = f($(m).prevAll())
            }
        }
        $('.page-tabs-content').animate({marginLeft: 0 - p + 'px'}, 'fast')
    }

    function b() {
        let o = Math.abs(parseInt($('.page-tabs-content').css('margin-left')));
        let l = f($('.content-tabs').children().not('.J_menuTabs'));
        let k = $('.content-tabs').outerWidth(true) - l;
        let p = 0;
        if ($('.page-tabs-content').width() < k) {
            return false
        } else {
            let m = $('.J_menuTab:first');
            let n = 0;
            while ((n + $(m).outerWidth(true)) <= o) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            n = 0;
            while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
                n += $(m).outerWidth(true);
                m = $(m).next()
            }
            p = f($(m).prevAll());
            if (p > 0) {
                $('.page-tabs-content').animate({marginLeft: 0 - p + 'px'}, 'fast')
            }
        }
    }

    function s() {
        let u = location.href, i = '', m = '';
        if (u.indexOf('#') > 0) {
            let tabs = window.localStorage.getItem('tabs');
            i = u.substring(u.indexOf('#') + 1);
            let tabs_arr = tabs.split(',');
            for (let j = 0; j < tabs_arr.length; j++) {
                let ni = tabs_arr[j].substring(1);
                let ic = 'J_menuTab';
                if (ni === i) {
                    ic = 'J_menuTab active';
                    $('.J_menuTab:first').removeClass('active');
                    m = 'modular' + modular_str[ni]
                }
                let nu = menu_str[ni]['url'];
                let np = '<a href="#' + ni + '" class="' + ic + '" data-id="#' + ni + '" data-modular="modular' + modular_str[ni] + '">' + menu_str[ni]['title'] + ' <i class="fa fa-times-circle"></i></a>';
                $('.J_menuTabs .page-tabs-content').append(np);
                let nn = '<iframe class="J_iframe" id="' + ni + '" name="' + ni + '" width="100%" height="100%" src="' + nu + '" frameborder="0" data-id="#' + ni + '" seamless></iframe>';
                $('.J_mainContent').find('iframe.J_iframe').hide().parents('.J_mainContent').append(nn);
                g('.J_menuTab.active')
            }
        }
        $('.J_iframe').on('iframe').each(function () {
            if ($(this).data('id') === ('#' + i)) {
                $(this).show().siblings('.J_iframe').hide();
                return false
            }
        });
        $('.J_menuItem').each(function () {
            if (parseInt($(this).attr('isleft')) > 0 && $(this).attr('href') === ('#' + i)) {
                $(this).parent().parent().parent().find('a').click()
                $(this).css('color', '#ffffff')
            }
            if ($(this).hasClass('top_level') && $(this).attr('href') === ('#' + i)) {
                $(this).parent().addClass('active')
                $(this).css('color', '#ffffff')
            }
        })
        $('#one_nav>a[data-modular="' + m + '"]').addClass('btn_nav_avtice').attr('infoc', '1').siblings().removeClass('btn_nav_avtice')
        $('.modular[modular="' + m + '"]').css('display', 'block')
    }

    s();

    function c() {
        let i = $(this).attr('href'), o = $(this).data('url'), l = $.trim($(this).data('txt')), k = true, $id = i.substring(1);
        if (o === undefined || $.trim(o).length === 0) {
            return false
        }
        $('.nav li a.J_menuItem').removeAttr('style');
        $(this).not('.other').css('color', '#ffffff');
        $('.J_menuTab').each(function () {
            if ($(this).data('id') === i) {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').siblings('.J_menuTab').removeClass('active');
                    g(this);
                    $('.J_mainContent .J_iframe').each(function () {
                        if ($(this).data('id') === i) {
                            $(this).show().siblings('.J_iframe').hide();
                            return false
                        }
                    })
                }
                k = false;
                return false
            }
        });
        if (k) {
            let d = location.href, m = $(this).data('modular');
            d = d.substring(0, d.indexOf('#'));
            let p = '<a href="' + d + i + '" class="active J_menuTab" data-id="' + i + '" data-modular="' + m + '">' + l + ' <i class="fa fa-times-circle"></i></a>';
            $('.J_menuTab').removeClass('active');
            $('.J_menuTabs .page-tabs-content').append(p);
            let n = '<iframe class="J_iframe" id="' + $id + '" name="' + $id + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + i + '" seamless></iframe>';
            $('.J_mainContent').find('iframe.J_iframe').hide().parents('.J_mainContent').append(n);
            g('.J_menuTab.active')
        } else {
            $('#' + $id).attr('src', o)
        }
        if (window.localStorage) {
            let tabs = '';
            $('.J_menuTab').on('a').each(function (i) {
                if ($(this).data('id') && i > 0) {
                    tabs += ',' + $(this).data('id')
                }
            });
            window.localStorage.setItem('tabs', tabs.substring(1))
        }
    }

    $('.J_menuItem').on('click', c);

    function h() {
        let m = $(this).parents('.J_menuTab').data('id');
        let l = $(this).parents('.J_menuTab').width();
        let $href = location.href;
        if ($(this).parents('.J_menuTab').hasClass('active')) {
            let $activeTab = null;
            if ($(this).parents('.J_menuTab').next('.J_menuTab').size()) {
                let k = $(this).parents('.J_menuTab').next('.J_menuTab:eq(0)').data('id');
                $activeTab = $(this).parents('.J_menuTab').next('.J_menuTab:eq(0)');
                $activeTab.addClass('active');
                $('.J_mainContent .J_iframe').each(function () {
                    if ($(this).data('id') === k) {
                        $(this).show().siblings('.J_iframe').hide();
                        return false
                    }
                });
                let n = parseInt($('.page-tabs-content').css('margin-left'));
                if (n < 0) {
                    $('.page-tabs-content').animate({marginLeft: (n + l) + 'px'}, 'fast')
                }
                $(this).parents('.J_menuTab').remove();
                $('.J_mainContent .J_iframe').each(function () {
                    if ($(this).data('id') === m) {
                        $(this).remove();
                        return false
                    }
                })
            } else if ($(this).parents('.J_menuTab').prev('.J_menuTab').size()) {
                let k = $(this).parents('.J_menuTab').prev('.J_menuTab:last').data('id');
                $activeTab = $(this).parents('.J_menuTab').prev('.J_menuTab:last');
                $activeTab.addClass('active');
                $('.J_mainContent .J_iframe').each(function () {
                    if ($(this).data('id') === k) {
                        $(this).show().siblings('.J_iframe').hide();
                        return false
                    }
                });
                $(this).parents('.J_menuTab').remove();
                $('.J_mainContent .J_iframe').each(function () {
                    if ($(this).data('id') === m) {
                        $(this).remove();
                        return false
                    }
                })
            }
            let $m = '';
            if ($activeTab != null && $activeTab.data('modular') !== 'first') {
                $m = $activeTab.data('modular');
            }
            $('#one_nav a').attr('infoc', '0').removeClass('btn_nav_avtice').css('color', '#337ab7');
            $('#one_nav a[data-modular="' + $m + '"]').attr('infoc', '1').addClass('btn_nav_avtice').css('color', '#fff');
            $('.modular').css('display', 'none');
            $('.modular[modular="' + $m + '"]').css('display', 'block');
            $('#side-menu>li.modular').removeClass('active');
            $('#side-menu ul.collapse').removeClass('in');
            let $h = $activeTab.data('id');
            let $activeA = $('.J_menuItem[href="' + $h + '"]');
            $activeA.parents('li.modular').addClass('active');
            $activeA.parents('ul.collapse').addClass('in');
            $('.J_menuItem').removeAttr('style');
            $activeA.css('color', '#ffffff');
        } else {
            $(this).parents('.J_menuTab').remove();
            $('.J_mainContent .J_iframe').each(function () {
                if ($(this).data('id') === m) {
                    $(this).remove();
                    return false
                }
            });
            g('.J_menuTab.active')
        }
        let vi = '';
        $('.J_menuTab').on('a').not(':first').each(function () {
            if ($(this).data('id')) {
                vi += (',' + $(this).data('id'))
            }
            if ($(this).hasClass('active')) {
                history.pushState(null, '', $href.substring(0, $href.indexOf('#')) + $(this).data('id'))
            }
        });
        if (vi !== '') {
            vi = vi.substring(1)
        } else {
            history.pushState(null, '', $href.substring(0, $href.indexOf('#')));
        }
        window.localStorage.setItem('tabs', vi);
        return false
    }

    $('.J_menuTabs').on('click', '.J_menuTab i', h);

    function i() {
        $('.page-tabs-content').children('[data-id]').not(':first').not('.active').each(function () {
            $('.J_iframe[data-id="' + $(this).data('id') + '"]').remove();
            $(this).remove()
        });
        $('.page-tabs-content').css('margin-left', '0')
    }

    $('.J_tabCloseOther').on('click', i);

    function e() {
        if (!$(this).hasClass('active')) {
            let k = $(this).data('id');
            $('.J_mainContent .J_iframe').each(function () {
                if ($(this).data('id') === k) {
                    $(this).show().siblings('.J_iframe').hide();
                    return false
                }
            });
            $(this).addClass('active').siblings('.J_menuTab').removeClass('active');
            g(this);
            let $m = $(this).data('modular');
            $('#one_nav a').attr('infoc', '0').removeClass('btn_nav_avtice').css('color', '#337ab7');
            $('#one_nav a[data-modular="' + $m + '"]').attr('infoc', '1').addClass('btn_nav_avtice').css('color', '#fff');
            $('.modular').css('display', 'none');
            $('.modular[modular="' + $m + '"]').css('display', 'block');
            $('#side-menu>li.modular').removeClass('active');
            $('#side-menu ul.collapse').removeClass('in');
            let $h = $(this).data('id');
            let $activeA = $('.J_menuItem[href="' + $h + '"]');
            $activeA.parents('li.modular').addClass('active');
            $activeA.parents('ul.collapse').addClass('in');
            $('.J_menuItem').removeAttr('style');
            $activeA.css('color', '#ffffff');
        }
    }

    $('.J_menuTabs').on('click', '.J_menuTab', e);

    $('.J_tabLeft').on('click', a);
    $('.J_tabRight').on('click', b);
    $('.J_tabCloseAll').on('click', function () {
        $('.page-tabs-content').children('[data-id]').not(':first').each(function () {
            $('.J_iframe[data-id="' + $(this).data('id') + '"]').remove();
            $(this).remove()
        });
        $('.page-tabs-content').children('[data-id]:first').each(function () {
            $('.J_iframe[data-id="' + $(this).data('id') + '"]').show();
            $(this).addClass('active')
        });
        $('.page-tabs-content').css('margin-left', '0')
    })
});