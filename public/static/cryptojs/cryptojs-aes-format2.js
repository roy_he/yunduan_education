/**
 * AES JSON formatter for CryptoJS
 * @link https://github.com/brainfoolong/cryptojs-aes-php
 * @version 2.1.1
 */

const CryptoJSAesJsonT = {
    /**
     * Encrypt any str
     * @param {string} $str 要加密的字符串
     * @param {string} $key 16位的加密key
     * @param {string} $iv 加密向量
     * @return {string} 返回加密后的字符串
     */
    encrypt: function ($str, $key, $iv) {
        $key = CryptoJS.enc.Utf8.parse($key);
        $iv = CryptoJS.enc.Utf8.parse($iv);
        return CryptoJS.AES.encrypt($str, $key, {
            iv: $iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
    },
    /**
     * Decrypt a previously encrypted str
     * @param {string} $str
     * @param {string} $key 16位的解密key
     * @param {string} $iv  解密向量
     * @return {string} 返回解密后的字符串
     */
    decrypt: function ($str, $key, $iv) {
        $key = CryptoJS.enc.Utf8.parse($key);
        $iv = CryptoJS.enc.Utf8.parse($iv);
        let decrypted = CryptoJS.AES.decrypt($str, $key, {
            iv: $iv,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
};
