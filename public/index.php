<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// [ 应用入口文件 ]
namespace think;

require __DIR__ . '/../vendor/autoload.php';
// 执行HTTP应用并响应
$app = new App();
if ($app->cookie->get('_debug', '') === md5('debug_' . date('Ymd'))) {
    $app->debug();
}
if ($app->request->param('_debug', '') === md5('debug_' . date('Ymd'))) {
    $app->debug();
}
$param = json_decode(file_get_contents('php://input'), true);
if (!empty($param['_debug']) && $param['_debug'] === md5('debug_' . date('Ymd'))) {
    $app->debug();
}
$host = $_SERVER['HTTP_HOST'];
$host = substr($host, strpos($host, '.') + 1);
$envName = str_replace('.', '_', $host);
$http = $app->setEnvName($envName)->http;
$domainName = $_SERVER['SERVER_NAME'];
$name = substr($domainName, 0, strpos($domainName, '.'));
$response = $http->name($name)->run();
$response->send();
$http->end($response);
