/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : localhost:3306
 Source Schema         : yunduan

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 05/09/2023 11:09:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yunduan_admin_ips
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_admin_ips`;
CREATE TABLE `yunduan_admin_ips`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '开放IP',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员账号',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录时间',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员IP限制表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_admin_ips
-- ----------------------------

-- ----------------------------
-- Table structure for yunduan_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_admin_menu`;
CREATE TABLE `yunduan_admin_menu`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `identity` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单标识',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `modular` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模块ID',
  `pid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '父级ID',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单地址',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序',
  `is_hide` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否在菜单栏显示，0-否  1-是',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0-否  1-是',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_admin_menu
-- ----------------------------
INSERT INTO `yunduan_admin_menu` VALUES ('0153bd034db950a23cf5d1321ffd6cb1', 'crontab_lists', '任务列表', 'a37c7580ca8dc5720c3044ba057c5fea', 'f786bac127af21bd1599319e04f0b2e9', '/crontab/lists', 90, 0, 0, 1666695911, 1686815285);
INSERT INTO `yunduan_admin_menu` VALUES ('027cb3f82d7a6867ec028288ba6e004e', 'sys', '系统', '', '', '#', 10, 0, 0, 1, 1647852508);
INSERT INTO `yunduan_admin_menu` VALUES ('0499eedf2e4c9e50fe262b64b1f1a914', 'goods_lists', '商品列表', 'd919c000163941d556ec1a0f9adc1ee2', 'c66272aa6b8e5c9f9a18e5e170647f9f', '/goods/lists', 99, 0, 0, 1666408450, 1684377442);
INSERT INTO `yunduan_admin_menu` VALUES ('08be2b12e86be812951e8bde57a35fb2', 'menu_edit', '修改菜单', '027cb3f82d7a6867ec028288ba6e004e', '887c159e0615c787d7865d6294e7eee0', '/menu/edit', 99, 1, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('0d9cac8ddbce3976efec7481fe122c76', 'doc_add', '创建文档', '6b3e52354d4dafe784b1fa0be491466a', '8f1b5de63c7bd590b3141c972c84548a', '/doc/add', 99, 1, 0, 1682216928, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('0f3415016dc7d30433920bd873a442e4', 'logs_lists', '日志列表', '027cb3f82d7a6867ec028288ba6e004e', 'e96f83288ae993792d79278078538761', '/logs/lists', 99, 0, 0, 1677852866, 1678417528);
INSERT INTO `yunduan_admin_menu` VALUES ('1a79ffce3b1883b5707f5b118451d1e2', 'role_add', '添加角色', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/role/add', 99, 1, 0, 1631602228, 1664076199);
INSERT INTO `yunduan_admin_menu` VALUES ('1f9cb43537b13f2ec3f3de7201a0f3b3', 'menu_add', '添加菜单', '027cb3f82d7a6867ec028288ba6e004e', '887c159e0615c787d7865d6294e7eee0', '/menu/add', 99, 1, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('2466cfd2eee39298c8e1448707638189', 'doc_add', '创建文档', 'd919c000163941d556ec1a0f9adc1ee2', '6cd5f7d9c654b6ecd1ccbe319ca5c3a5', '/doc/add', 99, 1, 0, 1682064876, 1682216901);
INSERT INTO `yunduan_admin_menu` VALUES ('2b3e22284b31f7aca7d6d8b212c23d87', 'school', '学校管理', 'd919c000163941d556ec1a0f9adc1ee2', '', '#', 95, 0, 0, 1662883458, 1664076400);
INSERT INTO `yunduan_admin_menu` VALUES ('2b6ea8f53bf5ad4f133c9a6bf6b5508e', 'grade_lists', '年级列表', 'd919c000163941d556ec1a0f9adc1ee2', '2b3e22284b31f7aca7d6d8b212c23d87', '/grade/lists', 100, 0, 0, 1664201221, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('38b6324376e35cf99be3ee65b8d85382', 'admin_add', '添加管理员', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/admin/add', 99, 1, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('3dc1039cc5d96792df2fbeacc0303553', 'customize_lists', '字段列表', '027cb3f82d7a6867ec028288ba6e004e', '7baa362f3a5139ffd0cd7a23c35da6f6', '/customize/lists', 99, 0, 0, 1692623525, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('3e1069be0e2727c2c20deaf695adabf7', 'admin', '管理员管理', '027cb3f82d7a6867ec028288ba6e004e', '', '#', 99, 0, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('42ac20ce15250b93d0a0348fa12c2adb', 'user_logs', '用户登录日志', 'd919c000163941d556ec1a0f9adc1ee2', '610821782c1ac2f85a6512c0df23e075', '/user/logs', 99, 0, 0, 1679752482, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('436cfc1dafde933a4af3c8fd5fd08b61', 'elastic_index', '同步数据', '9e0b69fb148c1299a522f6ef74a1ee29', '636a7eba29b7f806b53e1dee7a283a2d', '/elastic/index', 99, 0, 0, 1647846109, 1685069307);
INSERT INTO `yunduan_admin_menu` VALUES ('4d59fcbe78151520d3610c28e5c7cee9', 'doc_edit', '修改文档', '6b3e52354d4dafe784b1fa0be491466a', '8f1b5de63c7bd590b3141c972c84548a', '/doc/edit', 99, 1, 0, 1682216976, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('4d709ddb6508a40dc9cf9f692f4a2cae', 'admin_ips', 'IP白名单', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/admin/ips', 99, 1, 0, 1631615415, 1686533899);
INSERT INTO `yunduan_admin_menu` VALUES ('610821782c1ac2f85a6512c0df23e075', 'user', '用户管理', 'd919c000163941d556ec1a0f9adc1ee2', '', '#', 90, 0, 0, 1647779936, 1664076376);
INSERT INTO `yunduan_admin_menu` VALUES ('636a7eba29b7f806b53e1dee7a283a2d', 'elastic', 'Elastic管理', '9e0b69fb148c1299a522f6ef74a1ee29', '', '#', 99, 0, 0, 1647845139, 1685069307);
INSERT INTO `yunduan_admin_menu` VALUES ('6b3e52354d4dafe784b1fa0be491466a', 'wd', '文档', '', '', '#', 30, 0, 0, 1682216410, 1684376161);
INSERT INTO `yunduan_admin_menu` VALUES ('6d997fbf56d714aa6176597476ce040f', 'org_lists', '组织列表', 'd919c000163941d556ec1a0f9adc1ee2', 'a9dacbfe4230e17e2556ee3415fe0372', '/org/lists', 99, 0, 0, 1663485612, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('7279827caa8044ff959b7e69bbaa65e4', 'district_lists1', '行政区划列表2', 'd919c000163941d556ec1a0f9adc1ee2', 'd5b7fc5cd02a62d0da71e6d59e058431', '/district/lists1', 99, 0, 0, 1682390144, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('7baa362f3a5139ffd0cd7a23c35da6f6', 'customize', '表字段管理', '027cb3f82d7a6867ec028288ba6e004e', '', '#', 110, 0, 0, 1692623444, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('887c159e0615c787d7865d6294e7eee0', 'menu', '菜单管理', '027cb3f82d7a6867ec028288ba6e004e', '', '#', 100, 0, 0, 1, 1664077051);
INSERT INTO `yunduan_admin_menu` VALUES ('89a3f9a6a3e0bd6f86f972a780aa09c7', 'crypto_index', '加密解密', 'a37c7580ca8dc5720c3044ba057c5fea', '', '/crypto/index', 99, 0, 0, 1684376510, 1684544056);
INSERT INTO `yunduan_admin_menu` VALUES ('8f1b5de63c7bd590b3141c972c84548a', 'doc', '文档管理', '6b3e52354d4dafe784b1fa0be491466a', '', '#', 99, 0, 0, 1682216622, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('91f62213f93071527bc0c38521f533da', 'admin_lists', '管理员列表', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/admin/lists', 99, 0, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('96e01f7b618c07bfae58529d21f5a02c', 'menu_power', '菜单权限', '027cb3f82d7a6867ec028288ba6e004e', '887c159e0615c787d7865d6294e7eee0', '/menu/power', 99, 0, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('9e0b69fb148c1299a522f6ef74a1ee29', 'es', 'Elastic', '', '', '#', 100, 0, 0, 1647842419, 1664076777);
INSERT INTO `yunduan_admin_menu` VALUES ('a1776bb4304de19ab9eed3d2d375fc31', 'doc_lists', '文档列表', '6b3e52354d4dafe784b1fa0be491466a', '8f1b5de63c7bd590b3141c972c84548a', '/doc/lists', 99, 0, 0, 1682216663, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('a37c7580ca8dc5720c3044ba057c5fea', 'tool', '工具', '', '', '#', 99, 0, 0, 1684376081, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('a59d64d5b0ef95c046fa1db542574f72', 'goods_category', '商品类别列表', 'd919c000163941d556ec1a0f9adc1ee2', 'c66272aa6b8e5c9f9a18e5e170647f9f', '/goods/category', 99, 0, 0, 1666446159, 1684377442);
INSERT INTO `yunduan_admin_menu` VALUES ('a988bd54f85d6b6c963247e23ef9d6f2', 'role_lists', '管理员角色', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/role/lists', 99, 0, 0, 1631602269, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('a9dacbfe4230e17e2556ee3415fe0372', 'org', '组织管理', 'd919c000163941d556ec1a0f9adc1ee2', '', '#', 100, 0, 0, 1663485535, 1664076416);
INSERT INTO `yunduan_admin_menu` VALUES ('abd5ac490a586214eb2165cebdd413f3', 'banji_lists', '班级列表', 'd919c000163941d556ec1a0f9adc1ee2', '2b3e22284b31f7aca7d6d8b212c23d87', '/banji/lists', 105, 0, 0, 1664245334, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('b2e7771af82c864f338e007eee0366a9', 'admin_edit', '修改管理员', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/admin/edit', 99, 1, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('c66272aa6b8e5c9f9a18e5e170647f9f', 'goods', '商品管理', 'd919c000163941d556ec1a0f9adc1ee2', '', '#', 99, 0, 0, 1666408388, 1684377442);
INSERT INTO `yunduan_admin_menu` VALUES ('d5b7fc5cd02a62d0da71e6d59e058431', 'district', '行政区划管理', 'd919c000163941d556ec1a0f9adc1ee2', '', '#', 110, 0, 0, 1647941100, 1664076432);
INSERT INTO `yunduan_admin_menu` VALUES ('d6c43b662937e2aa96fed337734a0e56', 'dev_index', '快捷开发', 'a37c7580ca8dc5720c3044ba057c5fea', '', '/dev/index', 99, 0, 0, 1684458220, 1684458451);
INSERT INTO `yunduan_admin_menu` VALUES ('d6eefd0c54869dcf05ce738487f5399a', 'admin_logs', '管理员日志', '027cb3f82d7a6867ec028288ba6e004e', '3e1069be0e2727c2c20deaf695adabf7', '/admin/logs', 99, 0, 0, 1653617532, 1664076336);
INSERT INTO `yunduan_admin_menu` VALUES ('d919c000163941d556ec1a0f9adc1ee2', 'basic_data', '数据管理', '', '', '#', 20, 0, 0, 1, 1686636712);
INSERT INTO `yunduan_admin_menu` VALUES ('df10359f4281e161fb369454915fe04b', 'customize_edit', '修改字段', '027cb3f82d7a6867ec028288ba6e004e', '7baa362f3a5139ffd0cd7a23c35da6f6', '/customize/edit', 99, 1, 0, 1692623602, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('e3bc2b80ab85aee86197c6bedb09db86', 'district_lists', '行政区划列表', 'd919c000163941d556ec1a0f9adc1ee2', 'd5b7fc5cd02a62d0da71e6d59e058431', '/district/lists', 99, 0, 0, 1647941158, 1650790587);
INSERT INTO `yunduan_admin_menu` VALUES ('e96f83288ae993792d79278078538761', 'logs', '日志管理', '027cb3f82d7a6867ec028288ba6e004e', '', '#', 99, 0, 0, 1677852766, 1678417538);
INSERT INTO `yunduan_admin_menu` VALUES ('eb5419c0cf6fa050fbc90a530b25f0e8', 'school_lists', '学校列表', 'd919c000163941d556ec1a0f9adc1ee2', '2b3e22284b31f7aca7d6d8b212c23d87', '/school/lists', 99, 0, 0, 1662883531, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('f786bac127af21bd1599319e04f0b2e9', 'crontab', '计划任务', 'a37c7580ca8dc5720c3044ba057c5fea', '', '#', 90, 0, 0, 1666695852, 1686815285);
INSERT INTO `yunduan_admin_menu` VALUES ('f802fb3d439186749ebee094d1efe2e9', 'menu_lists', '菜单列表', '027cb3f82d7a6867ec028288ba6e004e', '887c159e0615c787d7865d6294e7eee0', '/menu/lists', 99, 0, 0, 1, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('f8a166c440e3ff67d68e25e1321950bc', 'user_lists', '用户列表', 'd919c000163941d556ec1a0f9adc1ee2', '610821782c1ac2f85a6512c0df23e075', '/user/lists', 99, 0, 0, 1647914464, 0);
INSERT INTO `yunduan_admin_menu` VALUES ('fb673adbf960f37ee5ae321a2e8e89d0', 'customize_add', '添加字段', '027cb3f82d7a6867ec028288ba6e004e', '7baa362f3a5139ffd0cd7a23c35da6f6', '/customize/add', 99, 1, 0, 1692623573, 0);

-- ----------------------------
-- Table structure for yunduan_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_admin_role`;
CREATE TABLE `yunduan_admin_role`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menus` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单权限，多个以逗号(,)分隔',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '角色状态，1-启用  4-禁用',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_admin_role
-- ----------------------------
INSERT INTO `yunduan_admin_role` VALUES ('1', '超级管理员', '*', 1, 0, 0);
INSERT INTO `yunduan_admin_role` VALUES ('6bcbe06fd3fee5cbb76ddb80223c7630', '管理员', '027cb3f82d7a6867ec028288ba6e004e,d919c000163941d556ec1a0f9adc1ee2,3e1069be0e2727c2c20deaf695adabf7,d6eefd0c54869dcf05ce738487f5399a,610821782c1ac2f85a6512c0df23e075,f8a166c440e3ff67d68e25e1321950bc,d5b7fc5cd02a62d0da71e6d59e058431,e3bc2b80ab85aee86197c6bedb09db86,2b3e22284b31f7aca7d6d8b212c23d87,eb5419c0cf6fa050fbc90a530b25f0e8', 1, 1646911117, 1663557820);

-- ----------------------------
-- Table structure for yunduan_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_admin_user`;
CREATE TABLE `yunduan_admin_user`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员账号',
  `password` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员头像',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员昵称',
  `role_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色ID，关联yd_admin_role表主键',
  `is_ip` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否被限制IP，0-否  1-是',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `login_ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '管理员状态，1-正常  4-禁用',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0-否  1-是',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_admin_user
-- ----------------------------
INSERT INTO `yunduan_admin_user` VALUES ('44d89e6324f41963067a55892547c7ad', 'admin', '4b11c76fd58ef288b50cce4177667ff7', '/static/hplus/img/a6.jpg', '超级管理员', '1', 0, 1693874997, '127.0.0.1', 1, 0, 0, 1624257796, 1693874997);
INSERT INTO `yunduan_admin_user` VALUES ('657b20d68d7740ad870a9f273e7c3e1b', 'royhe', '4b11c76fd58ef288b50cce4177667ff7', '/static/hplus/img/a7.jpg', 'roy', '6bcbe06fd3fee5cbb76ddb80223c7630', 1, 1688008313, '127.0.0.1', 1, 99, 0, 1661856892, 1688008313);

-- ----------------------------
-- Table structure for yunduan_admin_user_log
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_admin_user_log`;
CREATE TABLE `yunduan_admin_user_log`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员账号',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录时间',
  `login_ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作行为',
  `log` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作说明',
  `data` json NULL COMMENT '操作参数',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员操作日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_class
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_class`;
CREATE TABLE `yunduan_class`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '班级名称',
  `school_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '学校ID，关联school表主键',
  `school_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '学校名称',
  `grade_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '年级ID，关联grade表主键',
  `grade_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '年级名称',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '班级总人数',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1-正常 4-注销 9-删除',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_crontab
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_crontab`;
CREATE TABLE `yunduan_crontab`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `minutes` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '执行时间-分钟',
  `hours` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '执行时间:小时',
  `day_of_month` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '执行时间:日',
  `month` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '执行时间:月',
  `day_of_week` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '执行时间:周',
  `task_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '命令行',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '任务状态',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '计划任务管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_crontab
-- ----------------------------
INSERT INTO `yunduan_crontab` VALUES ('42afc184adac092c10e7c646b2acfc3c', '测试', '24', '*', '*', '*', '*', 'echo 123 > test.log', 1, 1670980928, 1670981025);

-- ----------------------------
-- Table structure for yunduan_district
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_district`;
CREATE TABLE `yunduan_district`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `code` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '区划编码',
  `pcode` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '父级编码',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '区划名称',
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '区划级别，1-省 2-市 3-县 4-乡',
  `children` json NULL COMMENT '子级信息',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1-正常 2-撤销',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_code`(`code`) USING BTREE COMMENT '行政编码唯一索引'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行政区划表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_district
-- ----------------------------
INSERT INTO `yunduan_district` VALUES ('000e05151bd60e13e22e80f46714fda5', '410702', '410700', '红旗区', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('0019450a0f4d96f129d41690ad2d032f', '433125', '433100', '保靖县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('001990d967de23a44b0536a13ad286e7', '440811', '440800', '麻章区', 3, NULL, 1, 1647931271, 1681975590);
INSERT INTO `yunduan_district` VALUES ('0023252d4bb29b605626945544a5abee', '150623', '150600', '鄂托克前旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('003e032c1cd948c3e5f7eaace8f8814b', '440515', '440500', '澄海区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('007c433edc777ab1c72b7b322c40ec15', '632801', '632800', '格尔木市', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('00ae4760a897c85a41ba003e8507314b', '520527', '520500', '赫章县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('00b32d32e34bbcb6c1b3c684b93b09ae', '370785', '370700', '高密市', 3, NULL, 1, 1647931271, 1681975487);
INSERT INTO `yunduan_district` VALUES ('00b4a6f6c089f2de7b4277fe2914de33', '210311', '210300', '千山区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('00c030f2904b74eeaf22a6971e42545e', '370405', '370400', '台儿庄区', 3, NULL, 1, 1647931271, 1681975497);
INSERT INTO `yunduan_district` VALUES ('00c3599ab22aa6a92bf6266f5b49a83b', '520603', '520600', '万山区', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('00ff1c2dad605b93ce999750824be019', '540526', '540500', '措美县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('01021b1b98ee87d62fd2f5332a111086', '440100', '440000', '广州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975595);
INSERT INTO `yunduan_district` VALUES ('0107a64843b27b6d7a4672c4c6b05630', '610114', '610100', '阎良区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('0113f9c857469a7ec954a1c66a8e2072', '130408', '130400', '永年区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('012a14c895cdc8d9c9e66ae56e6e855e', '532930', '532900', '洱源县', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('012de8eb7835417571aa3cb89c37aa62', '511802', '511800', '雨城区', 3, NULL, 1, 1647931271, 1681977520);
INSERT INTO `yunduan_district` VALUES ('013c567cf95f5bb1a0459080438a79d5', '140212', '140200', '新荣区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('01748e95ba8bf9c6a969160928ec336d', '140524', '140500', '陵川县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('01821ba7563a2bf1305e119f06f3f5c4', '360124', '360100', '进贤县', 3, NULL, 1, 1647931271, 1681975450);
INSERT INTO `yunduan_district` VALUES ('019e68126997a243daa38f3d0f52d042', '350700', '350000', '南平市', 2, '{\"county\": 10}', 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('01b9e8bd325b301a70e70268e7d85201', '341502', '341500', '金安区', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('01db19db5f0b082a8d10289a249a91a4', '421083', '421000', '洪湖市', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('01e4d03c5812fc2627e0d6d0b006530c', '361181', '361100', '德兴市', 3, NULL, 1, 1647931271, 1681975456);
INSERT INTO `yunduan_district` VALUES ('01fd677be6751ec5649db3b400b78919', '211103', '211100', '兴隆台区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('0203245dc017b77ee11c88867d28cce0', '513330', '513300', '德格县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('022ad3d0946fd1b07ebb1b71b575b1a2', '632821', '632800', '乌兰县', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('0238f9275aa84eb9fe0413aea2baf356', '210102', '210100', '和平区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('024606f32e8d8e7cd6493b82c439c2f5', '370900', '370000', '泰安市', 2, '{\"county\": 6}', 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('024ab0e2371340a63ed2ee54ac00de2c', '451026', '451000', '那坡县', 3, NULL, 1, 1647931271, 1681975629);
INSERT INTO `yunduan_district` VALUES ('02685010a49e112f25ab9af21a76c745', '370828', '370800', '金乡县', 3, NULL, 1, 1647931271, 1681975495);
INSERT INTO `yunduan_district` VALUES ('0289b74ed6440a3a44402f3a6ec1bcdf', '330822', '330800', '常山县', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('0289ddc1a9f067e154877f3a4a1ce2bd', '320904', '320900', '大丰区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('028ad069a05123e1116012fda96bd1ec', '340302', '340300', '龙子湖区', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('02d933b2e38fc98775c2b5a745d68a8e', '513328', '513300', '甘孜县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('02e8f8d31b8f8757fa1b83068994b347', '410202', '410200', '龙亭区', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('02f539bdb55ea642f520d140fd8659d2', '150929', '150900', '四子王旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('030a89ee62f17810097e100482e7e2a4', '640121', '640100', '永宁县', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('0329b692909a8d377b6204f850dc850e', '331124', '331100', '松阳县', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('033471bafd456796aa7f1f403d16984a', '330122', '330100', '桐庐县', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('033b488b16c528e834aa6e6e6852bfe7', '231102', '231100', '爱辉区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('0353447cc7b76d72fcf6d4e97ef467f7', '441521', '441500', '海丰县', 3, NULL, 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('03565857cbac56fcfd112f81c03c942b', '533301', '533300', '泸水市', 3, NULL, 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('0371cef4efcc2041ce2baecfde5ab3c6', '451081', '451000', '靖西市', 3, NULL, 1, 1647931271, 1681975629);
INSERT INTO `yunduan_district` VALUES ('03956b68f9146e4bb34bdaf2229ab5cc', '220500', '220000', '通化市', 2, '{\"county\": 7}', 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('03ab38b6e46563d2258bcaf7d97b76ee', '431027', '431000', '桂东县', 3, NULL, 1, 1647931271, 1681975554);
INSERT INTO `yunduan_district` VALUES ('03af286cc5f6a971ab8925569472cb30', '410100', '410000', '郑州市', 2, '{\"county\": 12}', 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('03dfe425e81afdc84da04605aa77e4bf', '511504', '511500', '叙州区', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('041a13d96475659ee4dc79bfe0a92ba4', '141030', '141000', '大宁县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('04258d359de01f962d480bde49804927', '621122', '621100', '陇西县', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('0451bfd8b082a5a42ac8fa5e6dea28e1', '431228', '431200', '芷江侗族自治县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('0465be9a4a31ff1032302f19f3b418c1', '230503', '230500', '岭东区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('046a2b721065883c0faa6a97236a757e', '320509', '320500', '吴江区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('0476e9b83b4615dc1ff2f92724d4a4dc', '430211', '430200', '天元区', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('04900ad11c648f7b163c5587a72c56a2', '370784', '370700', '安丘市', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('04ba3090f9831eead0d4732e51667391', '411003', '411000', '建安区', 3, NULL, 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('04ccd8d4aeaea88d0a59e0598120813e', '232721', '232700', '呼玛县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('04d2d9762591b7d4d81b3b3c961d4cdb', '460204', '460200', '天涯区', 3, NULL, 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('050d2d2e69250cf107cb449512e18bcb', '371521', '371500', '阳谷县', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('051837302c319b139af62ae955624ae9', '150625', '150600', '杭锦旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('05189d62ade3ad33e8d18fa03547a4ea', '120103', '120100', '河西区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('0547c7ce4b5cbba1364000cd8e150ca1', '431223', '431200', '辰溪县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('0569fd83c026f79d95ff5bb51fe3947b', '654003', '654000', '奎屯市', 3, NULL, 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('056ba13ce9100882c8dcdba7619f7aae', '210421', '210400', '抚顺县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('056bde75f080bcc8868b41f9b23db82a', '630103', '630100', '城中区', 3, NULL, 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('057384fb30be49f89fb9b4b75c736076', '451423', '451400', '龙州县', 3, NULL, 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('0582697011e71e342a85fd71f038ac36', '371721', '371700', '曹县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('0582ffb0ad1cb3a4c64bd1df2839789f', '511129', '511100', '沐川县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('058e30c31f9c73688632a48b6f44fbf1', '140930', '140900', '河曲县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('05a49bd1e2cb6afe5a2bcf5fa793a783', '440982', '440900', '化州市', 3, NULL, 1, 1647931271, 1681975582);
INSERT INTO `yunduan_district` VALUES ('05a726424d4f804493a239f8344693cb', '340503', '340500', '花山区', 3, NULL, 1, 1647931271, 1681975390);
INSERT INTO `yunduan_district` VALUES ('05b2cfbce0cb19942422d6050609bb18', '130205', '130200', '开平区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('05c4c6772cdb00e1ad823f9bad6fd9fe', '230124', '230100', '方正县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('05ca7de483cbc253fa0d203f2aea3998', '220623', '220600', '长白朝鲜族自治县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('06040e5f2684736192b957a941826d81', '620922', '620900', '瓜州县', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('06215ef5ec665a2aec1bf79414859723', '370116', '370100', '莱芜区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('062e0482d194378d065fd8b69cea7646', '530827', '530800', '孟连傣族拉祜族佤族自治县', 3, NULL, 1, 1647931271, 1681977875);
INSERT INTO `yunduan_district` VALUES ('0630b403292a7b19e58df9abc213c8a7', '371725', '371700', '郓城县', 3, NULL, 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('06437b4adb956ee132a74dde557e09cb', '530114', '530100', '呈贡区', 3, NULL, 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('064cbd52af53adba20df2b03b17a227d', '411000', '410000', '许昌市', 2, '{\"county\": 6}', 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('064d36d52e60af2cc97b41fb6f177517', '360200', '360000', '景德镇市', 2, '{\"county\": 4}', 1, 1647931271, 1681975447);
INSERT INTO `yunduan_district` VALUES ('064dda9e59423f29065f42a8570d6194', '231084', '231000', '宁安市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('0657bdd1d435e36d5dee89ed77fcce92', '540122', '540100', '当雄县', 3, NULL, 1, 1647931271, 1681977990);
INSERT INTO `yunduan_district` VALUES ('0659934587b81c13d10722ab27941902', '430204', '430200', '石峰区', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('065c19156d3f43573d1837653feba73c', '510300', '510000', '自贡市', 2, '{\"county\": 6}', 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('0673de18caf8f267a194f3b7a7e64dfb', '140922', '140900', '五台县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('068e1735305d70f91f2ca97160f9ec75', '150524', '150500', '库伦旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('06b55a47c15c7793807069c064685320', '513427', '513400', '宁南县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('06bb0d79ed26f3e40ae8e8d4b1e3f1ca', '441323', '441300', '惠东县', 3, NULL, 1, 1647931271, 1681975584);
INSERT INTO `yunduan_district` VALUES ('06c76a8c0286287021b30461bcf1511d', '440804', '440800', '坡头区', 3, NULL, 1, 1647931271, 1681975591);
INSERT INTO `yunduan_district` VALUES ('06cfa9a85b05bdf5d6cebad71991ba0d', '150303', '150300', '海南区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('06d7d19f10716893705207e5b21c1086', '130731', '130700', '涿鹿县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('06fe977a6ca6650b8305d099f57d9302', '130923', '130900', '东光县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('072b95ebdc76dc8001686e5b5b162e8c', '522301', '522300', '兴义市', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('0730f01c45f3104342e1422b3a6bbb4b', '542521', '542500', '普兰县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('073f7766d47c327c7ff69002876a7340', '621002', '621000', '西峰区', 3, NULL, 1, 1647931271, 1681978246);
INSERT INTO `yunduan_district` VALUES ('0753c663dd303bfa6fc00c7a53e6e79d', '510411', '510400', '仁和区', 3, NULL, 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('07604a6c18a8517bd46035522f051a96', '330206', '330200', '北仑区', 3, NULL, 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('0769524feef87b7d58af3c337647fe8e', '640323', '640300', '盐池县', 3, NULL, 1, 1647931271, 1681978326);
INSERT INTO `yunduan_district` VALUES ('07889b70c065f1adb3dff27754990d09', '220221', '220200', '永吉县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('078ac110f48ab8f8339770807a8cc0c5', '511503', '511500', '南溪区', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('07c17bc1c869372d572db8bfdfb8dc56', '130582', '130500', '沙河市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('07e2d27081f4a81bfeba5d5687412e9b', '530924', '530900', '镇康县', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('080cee2796fece4dbd106674ac68a6b6', '530824', '530800', '景谷傣族彝族自治县', 3, NULL, 1, 1647931271, 1681977875);
INSERT INTO `yunduan_district` VALUES ('083775499b3e10fc497f5d1744e6ad30', '510821', '510800', '旺苍县', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('08486c73287c933c71587f7a6fd3badb', '320902', '320900', '亭湖区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('085d1c812af44c7b1143ab7b65486d86', '330881', '330800', '江山市', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('087620549844be5b8b30b4ae7a2f04c8', '320322', '320300', '沛县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('087c71ef7636995162d2015185e9510c', '421102', '421100', '黄州区', 3, NULL, 1, 1647931271, 1681975544);
INSERT INTO `yunduan_district` VALUES ('08999a00f8a331d5c7fa7c06f549601d', '430682', '430600', '临湘市', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('08bf4890b4cdff79c0e6165ddfe4634d', '532927', '532900', '巍山彝族回族自治县', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('08cdf33ee1f4fa4692c6463b1aca53e8', '341102', '341100', '琅琊区', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('08d0c8e548a9c5af9c2311a117e488e2', '640200', '640000', '石嘴山市', 2, '{\"county\": 3}', 1, 1647931271, 1681978321);
INSERT INTO `yunduan_district` VALUES ('08db20faf652d9ea4fff71406c0a9709', '420702', '420700', '梁子湖区', 3, NULL, 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('08dba3bcd25658c08807c0c95d0c9723', '340604', '340600', '烈山区', 3, NULL, 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('08e72a236a922f8877c060f9e981de29', '350100', '350000', '福州市', 2, '{\"county\": 13}', 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('08f9442d5edb96558ce527d54b0a9158', '152501', '152500', '二连浩特市', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('08fbd30788e9f37a4dac8236b82f5e31', '421381', '421300', '广水市', 3, NULL, 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('093044ba6ebadc3c9fe2caf389f4bd47', '350825', '350800', '连城县', 3, NULL, 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('093851e6685fadd0daee883475e9375a', '120102', '120100', '河东区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('094b6c8ae57052874f379088bb85c202', '513221', '513200', '汶川县', 3, NULL, 1, 1647931271, 1681977527);
INSERT INTO `yunduan_district` VALUES ('09584f76ac3e365927d8b619586b64e7', '513232', '513200', '若尔盖县', 3, NULL, 1, 1647931271, 1681977527);
INSERT INTO `yunduan_district` VALUES ('0979b99bbb9679f8ca3f018edfa135fb', '340322', '340300', '五河县', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('097caa77c703938996cf1ed44401afad', '511502', '511500', '翠屏区', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('097df932f67a4b1f6daa9830365928a4', '460300', '460000', '三沙市', 2, NULL, 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('0996a34bea44d026793a28d18527507e', '511100', '510000', '乐山市', 2, '{\"county\": 11}', 1, 1647931271, 1681977536);
INSERT INTO `yunduan_district` VALUES ('099783f84a63655783e7cc3a63c08fcc', '520123', '520100', '修文县', 3, NULL, 1, 1647931271, 1681977725);
INSERT INTO `yunduan_district` VALUES ('09ab1c497b2319332851c170501a0934', '340211', '340200', '繁昌区', 3, NULL, 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('09d12946b34816c6ebc24b62cac3ad22', '540230', '540200', '康马县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('09dcf099c2bef9434394092153ec6d8f', '422828', '422800', '鹤峰县', 3, NULL, 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('09e26751f23c99d5bf8130b655b1007b', '370812', '370800', '兖州区', 3, NULL, 1, 1647931271, 1681975495);
INSERT INTO `yunduan_district` VALUES ('09e3512f8ad8d95397149cfbebb977ba', '431023', '431000', '永兴县', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('0a22b434b2b83b5d6c37f8cdabd223e1', '150103', '150100', '回民区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('0a362d7f41d4a0a058bfc1e6e8299ff8', '141102', '141100', '离石区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('0a3be6f031bc5c7f1a78615a4d8ca644', '210800', '210000', '营口市', 2, '{\"county\": 6}', 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('0a710ceef94f8524ecf151b0238fb185', '420323', '420300', '竹山县', 3, NULL, 1, 1647931271, 1681975548);
INSERT INTO `yunduan_district` VALUES ('0a7b6c81306f39ed35bfe30a06e8c820', '422823', '422800', '巴东县', 3, NULL, 1, 1647931271, 1681975540);
INSERT INTO `yunduan_district` VALUES ('0addd691ab17160679c1cbf9a7a3380e', '530826', '530800', '江城哈尼族彝族自治县', 3, NULL, 1, 1647931271, 1681977876);
INSERT INTO `yunduan_district` VALUES ('0adf0b8f8f24914d1cfa319676c9740c', '140722', '140700', '左权县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('0ae81f4c7c0159ad4eb0c44b3846ac6b', '211104', '211100', '大洼区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('0afae73f1a88d0853c84e870105e710d', '150900', '150000', '乌兰察布市', 2, '{\"county\": 11}', 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('0b09d64c698ed6c1d7b3745779551092', '130930', '130900', '孟村回族自治县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('0b19070f4cba1ab4c01f370055ec1d43', '610303', '610300', '金台区', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('0b1bde7bc83eae371d2fe0d6c52ee633', '150723', '150700', '鄂伦春自治旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('0b23ebeecac6d9c08ba3aa816a10a3c0', '421181', '421100', '麻城市', 3, NULL, 1, 1647931271, 1681975544);
INSERT INTO `yunduan_district` VALUES ('0b25693df5a11560b83059d1a5fef0b8', '450226', '450200', '三江侗族自治县', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('0b53755b1b7928df60a75eb945fe5be2', '130431', '130400', '鸡泽县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('0b6ef66d5bc1844c1db600acf16694b6', '371103', '371100', '岚山区', 3, NULL, 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('0b71caaff885938601ecb64839c825f3', '331127', '331100', '景宁畲族自治县', 3, NULL, 1, 1647931271, 1681975341);
INSERT INTO `yunduan_district` VALUES ('0b775405ad902396642dba0b3c9823d1', '371422', '371400', '宁津县', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('0b7b7cfa96091bb785fa3f68ae48ef4c', '510600', '510000', '德阳市', 2, '{\"county\": 6}', 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('0b7cde53e88f9144ff059087f1101214', '659007', '659000', '双河市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('0b7d2e691f7586347a6b70f3e3eeeabd', '654004', '654000', '霍尔果斯市', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('0b8c8e500e020639311c31f06d1915ca', '623022', '623000', '卓尼县', 3, NULL, 1, 1647931271, 1681978243);
INSERT INTO `yunduan_district` VALUES ('0b8e8d6e27f7900fe02cd15604346e28', '230700', '230000', '伊春市', 2, '{\"county\": 10}', 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('0b923fe20ee51d813dc69fc1772de3ec', '330523', '330500', '安吉县', 3, NULL, 1, 1647931271, 1681975347);
INSERT INTO `yunduan_district` VALUES ('0ba353ed41d0cb8f5f30ae1ad0bca777', '431122', '431100', '东安县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('0bc08ae32581979bedca92f3cc3c9754', '653221', '653200', '和田县', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('0be2e2df7bba3d8e1703ca4314196371', '222405', '222400', '龙井市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('0bec44c33f951642f5649d602fc325d7', '430821', '430800', '慈利县', 3, NULL, 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('0bf370728415c66024a541251492a7f8', '441403', '441400', '梅县区', 3, NULL, 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('0c0a7457f197888599df4bb29f065699', '621121', '621100', '通渭县', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('0c18d9b48de3a78569dce13e9870f60b', '141025', '141000', '古县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('0c2b54c6c711abdc8e8766d34e3743f9', '620102', '620100', '城关区', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('0c2da0e0640f66344c798941535d890f', '140200', '140000', '大同市', 2, '{\"county\": 10}', 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('0c571dab82a4f3fa54d65d79e4415036', '440704', '440700', '江海区', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('0c6437df093ed823b04fbe2903f4b467', '429005', '429000', '潜江市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('0c668766f29425a1720048e8f9a4e583', '140827', '140800', '垣曲县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('0cb96272301456f0974f95bf030d7c59', '371522', '371500', '莘县', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('0cbb4604dbf2a49470eb886e7671dfd3', '450521', '450500', '合浦县', 3, NULL, 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('0cc9c7717c0ef8d2bf8456da6c9d77a1', '510322', '510300', '富顺县', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('0cd9d2816a4d5058a5acd6db0816e5fc', '451481', '451400', '凭祥市', 3, NULL, 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('0cda8dd042a0c4623271062c362f6e33', '440605', '440600', '南海区', 3, NULL, 1, 1647931271, 1681975594);
INSERT INTO `yunduan_district` VALUES ('0cf25817ddea85439cd63f3c60882b9c', '320116', '320100', '六合区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('0cfb3978c4aeda27e9aa365070d94d41', '360926', '360900', '铜鼓县', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('0d063652302bcfd9ac948935f6ee4776', '420500', '420000', '宜昌市', 2, '{\"county\": 13}', 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('0d190f3179fa8d5de4f5c5c2c2c46b2d', '330600', '330000', '绍兴市', 2, '{\"county\": 6}', 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('0d1b46f15747f08c0b6549b84e8678f7', '440703', '440700', '蓬江区', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('0d2a2be538f1c56f110a1e4eaaae380a', '140702', '140700', '榆次区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('0d45c88ca716e3efdf95d82345d1b11d', '430212', '430200', '渌口区', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('0d5de00a3867634bab7b48fdedcca91b', '130502', '130500', '襄都区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('0d69151e27973fa9f454e04409ab690e', '230722', '230700', '嘉荫县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('0d7c9905e571f5dc8ce534273c05ac35', '350582', '350500', '晋江市', 3, NULL, 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('0d9f495652f8406f5b5df9fb59e81645', '350823', '350800', '上杭县', 3, NULL, 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('0d9f67e5dfa8a0f99d26e4f8820bd925', '411628', '411600', '鹿邑县', 3, NULL, 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('0db2b319f6c5265226908f311ef81f4d', '520502', '520500', '七星关区', 3, NULL, 1, 1647931271, 1681977724);
INSERT INTO `yunduan_district` VALUES ('0dd46f474a94b48440e9774240d1dded', '420529', '420500', '五峰土家族自治县', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('0df2290ec8690b1b737c35cef2dffed6', '611000', '610000', '商洛市', 2, '{\"county\": 7}', 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('0df2c0b46644bee5614412249de6879b', '630105', '630100', '城北区', 3, NULL, 1, 1647931271, 1681978315);
INSERT INTO `yunduan_district` VALUES ('0df639326e5e2d3170209a46e8628f21', '141023', '141000', '襄汾县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('0e10ae55d81e4b1baf6d0c751c861159', '640221', '640200', '平罗县', 3, NULL, 1, 1647931271, 1681978322);
INSERT INTO `yunduan_district` VALUES ('0e13c127242c318e950baa3cee25e5c2', '540104', '540100', '达孜区', 3, NULL, 1, 1647931271, 1681977991);
INSERT INTO `yunduan_district` VALUES ('0e26fd777da6ac02dccf6eb64d2792b3', '341823', '341800', '泾县', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('0e27874bc98fc083b9b6afa3af0d886e', '500236', '500200', '奉节县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('0e3d10c24654ec812a436e4c967a5eee', '230300', '230000', '鸡西市', 2, '{\"county\": 9}', 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('0e776d060d3daa661223bcb502f29efb', '510183', '510100', '邛崃市', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('0e851466fa9c545fcec2106c63999e5a', '532929', '532900', '云龙县', 3, NULL, 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('0eaf4ad32e89cc4cadfb7d6f894642ec', '330400', '330000', '嘉兴市', 2, '{\"county\": 7}', 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('0eb5fd02c877adb1b165c3c575072aee', '540000', '0', '西藏自治区', 1, '{\"city\": 7, \"county\": 74}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('0ebf04504c970b32eef4b2fa2c1a8fa1', '630106', '630100', '湟中区', 3, NULL, 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('0ec6383e20027f6af9c02a53c7ed9685', '430202', '430200', '荷塘区', 3, NULL, 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('0eeb79a12ca2c34f4742f32dae2f7003', '220422', '220400', '东辽县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('0f007ba2e0fc89a34d040acae391cae1', '431100', '430000', '永州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975568);
INSERT INTO `yunduan_district` VALUES ('0f236c4db7741c9905873763e6695076', '654325', '654300', '青河县', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('0f4782b3d737d46b92300a0f25c272e7', '441704', '441700', '阳东区', 3, NULL, 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('0f5cc4dd6a705afd1c0d3d93bb6c1b46', '411625', '411600', '郸城县', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('0f6142b9613f13b888020f3f2d50a9ed', '152200', '150000', '兴安盟', 2, '{\"county\": 6}', 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('0f6f86992c607a97d2f2e5dc489396ed', '621222', '621200', '文县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('0f712c0f5e9e3ef4b87763ba813e11c5', '210283', '210200', '庄河市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('0fa3d91d5c569535173a4e56d93108c8', '441481', '441400', '兴宁市', 3, NULL, 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('0fbd58db3503f725395429b7bb095c79', '445122', '445100', '饶平县', 3, NULL, 1, 1647931271, 1681975577);
INSERT INTO `yunduan_district` VALUES ('0fde987c3d80f98e3852dad183df85f7', '540527', '540500', '洛扎县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('0fe0accd3506f774cabb73693536673b', '150223', '150200', '达尔罕茂明安联合旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('0ff943e2ba1c9a8254a5c9499c0236f7', '620826', '620800', '静宁县', 3, NULL, 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('100d2999f86888070998c4c7630480d2', '210504', '210500', '明山区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('1014458d60d7ca0216a98e386d2fc68c', '430225', '430200', '炎陵县', 3, NULL, 1, 1647931271, 1681975550);
INSERT INTO `yunduan_district` VALUES ('1019d79715d16f87686583d0c80202d6', '310113', '310100', '宝山区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('104e00da302514d85acd3b7af5315caf', '431102', '431100', '零陵区', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('106c5e648fa34d30ba51de69a7144f6a', '540629', '540600', '尼玛县 ', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('107612a561ab8fa54d6342290b40e3c6', '532326', '532300', '大姚县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('108ac452b62b96cb3fb668fa1c6773bc', '533400', '530000', '迪庆藏族自治州', 2, '{\"county\": 3}', 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('108bb4a7931df2b7ddc551ab56b2d802', '411521', '411500', '罗山县', 3, NULL, 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('109108e2b1d15783184f6c3fae06213a', '511126', '511100', '夹江县', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('10ae5da81105f157d4f69d93fa10f208', '520382', '520300', '仁怀市', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('10b625321c66e2136537c6d9d620342e', '361124', '361100', '铅山县', 3, NULL, 1, 1647931271, 1681975456);
INSERT INTO `yunduan_district` VALUES ('10bd9eb20d30e01c5ac9fbfcc06fbfdf', '210603', '210600', '振兴区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('10c25acc264fd4622f4d33e606e38cb1', '350112', '350100', '长乐区', 3, NULL, 1, 1647931271, 1681975417);
INSERT INTO `yunduan_district` VALUES ('10c458bf8233fa3b5592663baebfde74', '110117', '110100', '平谷区', 3, NULL, 1, 1647931271, 1663565933);
INSERT INTO `yunduan_district` VALUES ('10dc04166d4a8ffbafccbc7954779f4d', '430811', '430800', '武陵源区', 3, NULL, 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('10f256defe0f1e4630b6da46655cb552', '511000', '510000', '内江市', 2, '{\"county\": 5}', 1, 1647931271, 1681977512);
INSERT INTO `yunduan_district` VALUES ('110e39b0d7de660c796b0c59ce6321dd', '533423', '533400', '维西傈僳族自治县', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('11161981b1b149d2488ba4f3eab501d4', '340800', '340000', '安庆市', 2, '{\"county\": 10}', 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('112f878ac7ad40c6163395745b2566bf', '210106', '210100', '铁西区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('115f6d86a1be3aacf329f7e81b0133d1', '140725', '140700', '寿阳县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('115f93ce5d06461ec779cecdf0550f6c', '410305', '410300', '涧西区', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('117d6711e43121f372fe2f6d814a4aec', '320703', '320700', '连云区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('117ed784187ebbc2eb27a9bdebf5c2c2', '520327', '520300', '凤冈县', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('1184c342060500a2b3be247dd2dbfda6', '130804', '130800', '鹰手营子矿区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('11a9286cd6e6fa224bd1b5455bad1489', '410500', '410000', '安阳市', 2, '{\"county\": 9}', 1, 1647931271, 1681975509);
INSERT INTO `yunduan_district` VALUES ('11b1856bb93fc3e7b9167e479b45a3d2', '210423', '210400', '清原满族自治县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('11b1f9a81eddbb0a8936866c93559243', '540622', '540600', '比如县 ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('11bf9f10e63eea29ea6134d80471c5fb', '150104', '150100', '玉泉区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('11c24e06f9210e54277ee10ddaedec26', '654221', '654200', '额敏县', 3, NULL, 1, 1647931271, 1681978499);
INSERT INTO `yunduan_district` VALUES ('11e43ea2897093b9af4999c77efdca80', '422822', '422800', '建始县', 3, NULL, 1, 1647931271, 1681975540);
INSERT INTO `yunduan_district` VALUES ('1207394ffaa1ac669471063c9644888a', '511921', '511900', '通江县', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('1210917de4001c2982eb22d576f5905f', '410503', '410500', '北关区', 3, NULL, 1, 1647931271, 1681975510);
INSERT INTO `yunduan_district` VALUES ('121ab2f5531afb581b33adb39c96c869', '130606', '130600', '莲池区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('12232927f4be0f7c6ba983400a5cb5d7', '210422', '210400', '新宾满族自治县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('12252d555b9b0ce59e93d24ced39fef4', '610118', '610100', '鄠邑区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('122db2089f7e0b23382862cfcc8ed958', '441224', '441200', '怀集县', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('123443b27638d33140bd1ba845fd101e', '210105', '210100', '皇姑区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('123b456e5e927d5d1df12e775062e648', '450981', '450900', '北流市', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('1263cf170a60148ee387b32039c36597', '210900', '210000', '阜新市', 2, '{\"county\": 7}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('1270bc2a6f23c1562d92b59f972f99d6', '371403', '371400', '陵城区', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('129a0da9be227a0436c2f60462d1c9eb', '211302', '211300', '双塔区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('12ae6c0ca8cfe01a942874fc9b1fde66', '421121', '421100', '团风县', 3, NULL, 1, 1647931271, 1681975544);
INSERT INTO `yunduan_district` VALUES ('12ea07e22cf58a8ff41c24acbd52daec', '500105', '500100', '江北区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('12fa74733a8ef15c1d5f055a55bdefad', '610304', '610300', '陈仓区', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('12fbbf5ff75ce1a95191b0f97b0c9c16', '610582', '610500', '华阴市', 3, NULL, 1, 1647931271, 1681978078);
INSERT INTO `yunduan_district` VALUES ('13185990c962d49077b42939393b5c08', '410803', '410800', '中站区', 3, NULL, 1, 1647931271, 1681975516);
INSERT INTO `yunduan_district` VALUES ('131f5c81662bf0c87f6e8caf62a3327f', '340207', '340200', '鸠江区', 3, NULL, 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('1333fbb93d7941519764c56dc0851f2a', '411424', '411400', '柘城县', 3, NULL, 1, 1647931271, 1681975504);
INSERT INTO `yunduan_district` VALUES ('134c5d6bf675d50782cff8f430684988', '131024', '131000', '香河县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('1352e9fe0d968d82900f905d5d916afd', '230129', '230100', '延寿县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('1353d28475c357a0837b7011f2c33c8c', '450311', '450300', '雁山区', 3, NULL, 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('135a073390f7e401ffbc7d191c82aa3e', '510682', '510600', '什邡市', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('13851dc46bdbf172f2cbea108cc4fbc3', '140621', '140600', '山阴县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('13881c367e717e4db7b9a3d79c9439bc', '321282', '321200', '靖江市', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('13a7f33886abd498dfe35081a2faa8de', '141027', '141000', '浮山县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('13b1e02176e88819bd8e518542583604', '420200', '420000', '黄石市', 2, '{\"county\": 6}', 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('13d3e31afb4dab9783a2452c1fa62751', '654028', '654000', '尼勒克县', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('13f57929ae57bb39e8817edefa75a732', '370321', '370300', '桓台县', 3, NULL, 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('13f88a5585be50483650542dd0c26a70', '630224', '630200', '化隆回族自治县', 3, NULL, 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('140b90053a2ea7716b685a7475bd5765', '610300', '610000', '宝鸡市', 2, '{\"county\": 12}', 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('141ab7781315287e83ce352ecbdc16c1', '330782', '330700', '义乌市', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('142435b3de3154bb2117fdf0c37bfcc8', '540330', '540300', '边坝县', 3, NULL, 1, 1647931271, 1681977990);
INSERT INTO `yunduan_district` VALUES ('1461d28384dce40413beaf99f03fac62', '522722', '522700', '荔波县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('14690c72c067473349f6645c4acf6946', '510681', '510600', '广汉市', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('147bb66ad6d2e7570e82b3bc237afe05', '620123', '620100', '榆中县', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('149dd8f5b97e2663ef39a2b3748280e8', '652823', '652800', '尉犁县', 3, NULL, 1, 1647931271, 1681978501);
INSERT INTO `yunduan_district` VALUES ('149df716f13c9697ad72aeacb3fed36b', '640400', '640000', '固原市', 2, '{\"county\": 5}', 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('14ba5cc6f399b072ade761ca7c02d690', '220284', '220200', '磐石市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('14bc0a8bfd029de0bd67aa073a910e50', '620200', '620000', '嘉峪关市', 2, NULL, 1, 1647931271, 1681978238);
INSERT INTO `yunduan_district` VALUES ('14c15e7984577760825f3d859fa10180', '533401', '533400', '香格里拉市', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('14c43415c5c43fbacd05ef5b626eb1cf', '420600', '420000', '襄阳市', 2, '{\"county\": 9}', 1, 1647931271, 1681975530);
INSERT INTO `yunduan_district` VALUES ('14c8eaac2e534aa034ef913ed69db72f', '130926', '130900', '肃宁县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('14d01002d74b1735c0c556bc220a89a0', '361127', '361100', '余干县', 3, NULL, 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('14dbc7b618bb1db62ee446fcea9b9f1d', '150928', '150900', '察哈尔右翼后旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('14f49e35354eefadcafaec56d03c5b0f', '320505', '320500', '虎丘区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('14f7c7720253e40118f1b1f783463224', '652929', '652900', '柯坪县', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('150de8437c082500fad6e64e41edd2c4', '430726', '430700', '石门县', 3, NULL, 1, 1647931271, 1681975558);
INSERT INTO `yunduan_district` VALUES ('150fd9c8b8694e098dc44d63a19f9e52', '450381', '450300', '荔浦市', 3, NULL, 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('151c20f9e8ea11e6a7bfe63cdaed659e', '360603', '360600', '余江区', 3, NULL, 1, 1647931271, 1681975447);
INSERT INTO `yunduan_district` VALUES ('15266b226b970b1e2ae0169e98f44836', '330303', '330300', '龙湾区', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('152a45a7765388f379b827fee42af109', '659001', '659000', '石河子市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('153efd71dd9583556b7fdde9f497c2f1', '220700', '220000', '松原市', 2, '{\"county\": 5}', 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('15434f8713147e354f44611dde5b2f05', '433100', '430000', '湘西土家族苗族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681975561);
INSERT INTO `yunduan_district` VALUES ('155d9487e98d2992563b5c14d8cae394', '522729', '522700', '长顺县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('15670353eefa2380fb7822d5185f13d6', '430723', '430700', '澧县', 3, NULL, 1, 1647931271, 1681975558);
INSERT INTO `yunduan_district` VALUES ('15818187ca299084577e45bb8869b798', '620982', '620900', '敦煌市', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('15826f7af901ae06c320ad8e1286bcf5', '370982', '370900', '新泰市', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('1582972595648302f01abb3cc9468a9b', '450821', '450800', '平南县', 3, NULL, 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('158bc2197165ec563641e1f580d77316', '130284', '130200', '滦州市', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('159dfc54c5d4a4f7c0a1116bf3a9bc4b', '370602', '370600', '芝罘区', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('15a9d8f9ef3d33f290c6fefeb33c8295', '210381', '210300', '海城市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('15ae4eae1ee6f32937fee7a8b0ec96ae', '140405', '140400', '屯留区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('15c315d6fee98e7e7a1f0ef820c77047', '441426', '441400', '平远县', 3, NULL, 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('15dd085b11adac19e73ced8dba389fd9', '330111', '330100', '富阳区', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('15e2dc1b1dc2edb09787902e7b3ddfc7', '150423', '150400', '巴林右旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('15e4cf432ccac501a14eb6aa99ad2af5', '510904', '510900', '安居区', 3, NULL, 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('15e55feafd1b85a8a986e3364574c037', '440224', '440200', '仁化县', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('1608c1711241927b90ca649d7d7a2dd5', '410900', '410000', '濮阳市', 2, '{\"county\": 6}', 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('161450d35fb492d3f8c9551393d5b068', '410203', '410200', '顺河回族区', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('161f9a6799fda678c46a206a999d84ec', '522328', '522300', '安龙县', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('1626d8a9d3e8305abc1a02db6c84daa7', '610430', '610400', '淳化县', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('162c17e68f01a18dd62e1df8c3a5957c', '540602', '540600', '色尼区', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('1642297fd953c5ee9c747dc45299bdd0', '130107', '130100', '井陉矿区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('1674e001df8e37c21aadc40094e78768', '540228', '540200', '白朗县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('1696bc63c6250e0fa9465ef1e3d8f61d', '340803', '340800', '大观区', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('16eada0cec09538be710191ef7be800e', '513435', '513400', '甘洛县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('1727be99a29a12941beb0b5e4ccb8533', '611026', '611000', '柞水县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('172eec414e92ea06b8486360e5184a20', '410725', '410700', '原阳县', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('174964b420db8436fa5df6407bef3aef', '410482', '410400', '汝州市', 3, NULL, 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('174cf419c0e2f9309b0ea603efce99f4', '511403', '511400', '彭山区', 3, NULL, 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('1755ac697b2e9755c303100b0cc86f08', '640521', '640500', '中宁县', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('1765452279aa20411dd0360a2c7580a7', '610723', '610700', '洋县', 3, NULL, 1, 1647931271, 1681978083);
INSERT INTO `yunduan_district` VALUES ('1782f2999d0c304a460848aa30187bbb', '530502', '530500', '隆阳区', 3, NULL, 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('179c14820e654e9f3d98133527f1b919', '370100', '370000', '济南市', 2, '{\"county\": 12}', 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('179e62660593114fe44684aa9998ab6f', '371302', '371300', '兰山区', 3, NULL, 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('17b3d63ed3897b8911f977c20954e45c', '652826', '652800', '焉耆回族自治县', 3, NULL, 1, 1647931271, 1681978501);
INSERT INTO `yunduan_district` VALUES ('17e67cf85477bb9ee8551e142d33619b', '540627', '540600', '班戈县 ', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('17edb6fc96f11f9c2e647c5a15f27e22', '230102', '230100', '道里区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('17f625e3a1fe909a73b5217a23cbddca', '341125', '341100', '定远县', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('17fe3ca172f41cdad4923ac014d31601', '540402', '540400', '巴宜区', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('182e47c1f68bc31e7bac0dca16a56761', '220521', '220500', '通化县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('18486f7883cad2622040b77fe649ba1a', '370829', '370800', '嘉祥县', 3, NULL, 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('1874d5da04d2a050111eaf847af4bc26', '110101', '110100', '东城区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('187774d69fa8bcf3f0e7570088465492', '540621', '540600', '嘉黎县 ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('1891e18de656ba6903217ed3e6102520', '653129', '653100', '伽师县', 3, NULL, 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('189f12e45a42a894ae78c3e1101a34a4', '410304', '410300', '瀍河回族区', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('18b28ce29449c1e6e25b2b8a172dcd26', '130927', '130900', '南皮县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('18bedb52803c5750838ccfbd063807dc', '331083', '331000', '玉环市', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('18ec0310fe0b15822dfed489486e5e7e', '410926', '410900', '范县', 3, NULL, 1, 1647931271, 1681975526);
INSERT INTO `yunduan_district` VALUES ('1937b822e50bdcb6d35711bb4e2f5db9', '360800', '360000', '吉安市', 2, '{\"county\": 13}', 1, 1647931271, 1681975439);
INSERT INTO `yunduan_district` VALUES ('1977a1e82c4c651416ba2cb190e9a9a5', '130700', '130000', '张家口市', 2, '{\"county\": 16}', 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('19b3a6ad011d2ceb3918a095f49796b4', '430822', '430800', '桑植县', 3, NULL, 1, 1647931271, 1681975573);
INSERT INTO `yunduan_district` VALUES ('19d55c46902e6f644a3a59bc15d4c106', '620421', '620400', '靖远县', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('19e6e21a9256c2df8afbbe2c61d233ce', '450302', '450300', '秀峰区', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('19ed3d65cd9b9a5606b38d47ab0f56b0', '350000', '0', '福建省', 1, '{\"city\": 9, \"county\": 85}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('19f9e15bf0268c3229346b3cc9d561ca', '620602', '620600', '凉州区', 3, NULL, 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('19fbfba1d3fd2a5a26c9e40bb4de1d8f', '450422', '450400', '藤县', 3, NULL, 1, 1647931271, 1681975626);
INSERT INTO `yunduan_district` VALUES ('19fe7dc159f100727d100906de3e2aef', '530324', '530300', '罗平县', 3, NULL, 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('1a08b9c06ebdc9cf5edf013b24a92b59', '230921', '230900', '勃利县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('1a0e5748c9e904860de8fd91f493d9d5', '360428', '360400', '都昌县', 3, NULL, 1, 1647931271, 1681975454);
INSERT INTO `yunduan_district` VALUES ('1a1a7fd1e2d4995d6c7b2bfc073dcfa9', '230104', '230100', '道外区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('1a29033efb44e62e720d1f96a797c5dd', '410621', '410600', '浚县', 3, NULL, 1, 1647931271, 1681975500);
INSERT INTO `yunduan_district` VALUES ('1a32eaa98734e4ab3e344b4d32bf6f43', '340124', '340100', '庐江县', 3, NULL, 1, 1647931271, 1681975385);
INSERT INTO `yunduan_district` VALUES ('1a47518527af6bc6bffe0db2f862d29b', '360822', '360800', '吉水县', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('1a589ac81055615437c8f17adbc09ee7', '441400', '440000', '梅州市', 2, '{\"county\": 8}', 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('1a5f823146b73aecfc49dfdddba6902a', '513422', '513400', '木里藏族自治县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('1a6315e1dfacaafcfb01f791430b0bd0', '310107', '310100', '普陀区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('1a711554ef52181ee67bf1ec611ff348', '533300', '530000', '怒江傈僳族自治州', 2, '{\"county\": 4}', 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('1a753335299fed4cfc29819f10fa99b9', '429004', '429000', '仙桃市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('1a9003c8e0248668bc9318c26eba51e4', '230751', '230700', '金林区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('1a9088ae3487ad7ed4b367587af4309b', '330900', '330000', '舟山市', 2, '{\"county\": 4}', 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('1ab70a6eb35306cf48d3bed13bbeac74', '360481', '360400', '瑞昌市', 3, NULL, 1, 1647931271, 1681975454);
INSERT INTO `yunduan_district` VALUES ('1abd3beb10e5113188c1c760569e638a', '230523', '230500', '宝清县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('1aeac958de4b2528d6249c9e937675e4', '371703', '371700', '定陶区', 3, NULL, 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('1aec9fc0f2d2996e9e433218cfbc7c74', '610322', '610300', '凤翔县', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('1af24f5fa862603246a9f132038cc4c0', '152923', '152900', '额济纳旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('1b09442c31537df191dcb8a6a10a5e25', '350724', '350700', '松溪县', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('1b363c866c359df6c1e2a361e2730c03', '340104', '340100', '蜀山区', 3, NULL, 1, 1647931271, 1681975385);
INSERT INTO `yunduan_district` VALUES ('1b46a90750676a809faa60aba5d8413e', '522632', '522600', '榕江县', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('1b46feded548b457c8631433ef00634a', '230781', '230700', '铁力市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('1b55c3643d4e81213b7a34d8516dc3ef', '330381', '330300', '瑞安市', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('1b55c3d575baf36ca36cc1e3c45c096b', '513200', '510000', '阿坝藏族羌族自治州', 2, '{\"county\": 13}', 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('1b787500099fe846cc7d151780063e83', '330521', '330500', '德清县', 3, NULL, 1, 1647931271, 1681975347);
INSERT INTO `yunduan_district` VALUES ('1b999d68750b8663d51f93b28748d1e5', '230223', '230200', '依安县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('1b9a8cc47c6a6a6ca6ac9607d89be5b5', '330110', '330100', '余杭区', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('1bab6fb3507b5646a9080f81a624754a', '411723', '411700', '平舆县', 3, NULL, 1, 1647931271, 1681975523);
INSERT INTO `yunduan_district` VALUES ('1bb00e7dd29df86e251df3d00e6fe61d', '420882', '420800', '京山市', 3, NULL, 1, 1647931271, 1681975543);
INSERT INTO `yunduan_district` VALUES ('1bbf853d60d69b544c53efdfde727cd4', '141028', '141000', '吉县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('1bbfc3282fe8c3031082795a555eca6c', '410104', '410100', '管城回族区', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('1bdbe9163c407a2e8d238b27c5ecea5f', '140000', '0', '山西省', 1, '{\"city\": 11, \"county\": 117}', 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('1c0c768acea4f285e4663527064ee5da', '610725', '610700', '勉县', 3, NULL, 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('1c0dc026af0a4b837eec928acf1d58cf', '130208', '130200', '丰润区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('1c1502a448d51d2aeeeb88f48a90f1ef', '321323', '321300', '泗阳县', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('1c1cc08d6e38ad5aa9a3d34667d1351a', '653022', '653000', '阿克陶县', 3, NULL, 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('1c273cfa2ec89225e11312d4b82d64d2', '330424', '330400', '海盐县', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('1c34476f83934d254eeeaa8ef7eb2f77', '620522', '620500', '秦安县', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('1c598f40a35c95bfcf162adaadd02303', '410822', '410800', '博爱县', 3, NULL, 1, 1647931271, 1681975516);
INSERT INTO `yunduan_district` VALUES ('1cc297f005fea3b2ee8f14c1d27e4e0c', '370114', '370100', '章丘区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('1ccb9ee89c440687c2e2ed78a552292a', '540400', '540000', '林芝市', 2, '{\"county\": 7}', 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('1ce3b33bc9be7ae85701cfd6b71b7585', '431028', '431000', '安仁县', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('1cee0bfdfa97520f8e117f465965c902', '540521', '540500', '扎囊县', 3, NULL, 1, 1647931271, 1681977996);
INSERT INTO `yunduan_district` VALUES ('1d0d44c8e6d7cea9b2b1bfef7aa08e87', '140728', '140700', '平遥县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('1d0e6b1dc7b7db44a139e1c91cf43b48', '420922', '420900', '大悟县', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('1d2290cc8cdd308f49f9c5e60a7b919b', '510302', '510300', '自流井区', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('1d4ea4eafbb03bf4c9dc29c9aeb7f59f', '513426', '513400', '会东县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('1d53200cf16b8e0d9ef6a3b23b0a71bb', '140932', '140900', '偏关县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('1d56bb3deace58f28e940b3b5c2553e4', '130925', '130900', '盐山县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('1d597de59a5d0ed047f2fa04d0f00129', '421081', '421000', '石首市', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('1d60d32c24dd8f0c15c46afeae7fdc4e', '441303', '441300', '惠阳区', 3, NULL, 1, 1647931271, 1681975585);
INSERT INTO `yunduan_district` VALUES ('1d719705610c7c49d4650fb32f7ea43d', '510811', '510800', '昭化区', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('1d7839ac243f080a3a0b2b7e7389faae', '410481', '410400', '舞钢市', 3, NULL, 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('1db6b35be5687611d8f5a963d51b67bf', '420921', '420900', '孝昌县', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('1dc3a1caae7f91916e5f9dfa5f0057a4', '450900', '450000', '玉林市', 2, '{\"county\": 7}', 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('1de30705f8c55d5e580e6bdccf9b6e84', '411327', '411300', '社旗县', 3, NULL, 1, 1647931271, 1681975519);
INSERT INTO `yunduan_district` VALUES ('1e00099c56a36226f4dd61117d93d386', '610482', '610400', '彬州市', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('1e1f11e40f601e89d0bf078705c54c18', '120101', '120100', '和平区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('1e3e5182a0a052e8295a3fd6ca8c5620', '540121', '540100', '林周县', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('1e45f73f54088d9daa27394efe4bfa01', '610602', '610600', '宝塔区', 3, NULL, 1, 1647931271, 1681978081);
INSERT INTO `yunduan_district` VALUES ('1e4f4d6cd87c17fba136f5c2222299a0', '321200', '320000', '泰州市', 2, '{\"county\": 6}', 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('1e5014d90f4034cbf81cbe50ee834738', '340700', '340000', '铜陵市', 2, '{\"county\": 4}', 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('1e75affc8ff3137676c099a872bf4ccc', '420684', '420600', '宜城市', 3, NULL, 1, 1647931271, 1681975531);
INSERT INTO `yunduan_district` VALUES ('1e7a28a7fd114fcd3113cb693addb8a9', '433101', '433100', '吉首市', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('1e88ee850baf3be3b7ce5b63e7d897b9', '500104', '500100', '大渡口区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('1e8cc143846cff5ccc6a5c53234c347e', '350803', '350800', '永定区', 3, NULL, 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('1e9acf0a1883730562afd00688842fa5', '210600', '210000', '丹东市', 2, '{\"county\": 6}', 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('1ece217a3d63c72c5a5edb9ecfe764b0', '610722', '610700', '城固县', 3, NULL, 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('1ed6953a167d737b668f371657fbd84b', '411525', '411500', '固始县', 3, NULL, 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('1f0a969a1a2b21d344a900268cdb88c3', '520324', '520300', '正安县', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('1f15e156ebbd2d2f90e26e0acb93a404', '370611', '370600', '福山区', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('1f904c04504d354a71164929d3688a08', '620121', '620100', '永登县', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('1f9b078eb6ca4e4643d26f93a5bbd29e', '130104', '130100', '桥西区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('1f9dde217e006ae836b304800d952fcd', '210300', '210000', '鞍山市', 2, '{\"county\": 7}', 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('1fa893610d0bc65d37adbeab1a025469', '520524', '520500', '织金县', 3, NULL, 1, 1647931271, 1681977723);
INSERT INTO `yunduan_district` VALUES ('1faecfdc313f15f65906647fb64598fd', '140600', '140000', '朔州市', 2, '{\"county\": 6}', 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('2012d16c40023aa99351591434dff8e0', '522726', '522700', '独山县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('20150f6eaed0eba97739f2eaca0adea8', '652900', '650000', '阿克苏地区', 2, '{\"county\": 9}', 1, 1647931271, 1681978494);
INSERT INTO `yunduan_district` VALUES ('201976c5a0e2fd4ef3039a89e865f565', '371481', '371400', '乐陵市', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('20968b3eb4ffff7fdffcff617047b7e8', '513428', '513400', '普格县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('20b26fc7867645dd31e4c55bd2f04757', '445381', '445300', '罗定市', 3, NULL, 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('20ce392de79062aa7ce6527143e099b6', '230382', '230300', '密山市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('20d89dc7dc8d143413b25241351b923f', '130722', '130700', '张北县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('212ebb41dc5b6340e2b872699d5e8d12', '371324', '371300', '兰陵县', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('216563155687f9649c594ce4746f53a5', '320611', '320600', '港闸区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('2166fc29870f19bc02b4f6a5bad41de3', '140322', '140300', '盂县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('2194453dd9404239031f1bb8fdd341c1', '410108', '410100', '惠济区', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('2196f007d2036cad80db13e87f80459b', '211122', '211100', '盘山县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('21bfca44e5ccb24fd29cfc6913ca3815', '330921', '330900', '岱山县', 3, NULL, 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('21cb5b9ec502d04a5561eb6ce6d403a4', '220103', '220100', '宽城区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('2206a562b0b83a1494c86d3e9173c546', '230225', '230200', '甘南县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('22300fca8f7cb161ecf58f09e666dc83', '410103', '410100', '二七区', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('22391a6d839ba92c536aff0c065c0a1f', '340406', '340400', '潘集区', 3, NULL, 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('226c7e71b4010965eb2b96e88bf65425', '350424', '350400', '宁化县', 3, NULL, 1, 1647931271, 1681975419);
INSERT INTO `yunduan_district` VALUES ('227ba26a3689621301eeb3e3d119bfad', '371623', '371600', '无棣县', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('22c0df5ee582db41b3ff9ad0a1746d33', '230719', '230700', '友好区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('22cb577fd9e35cc1e5a05978302b1ea1', '511113', '511100', '金口河区', 3, NULL, 1, 1647931271, 1681977536);
INSERT INTO `yunduan_district` VALUES ('22da11a9ca306ff853f5572863e31c1d', '350203', '350200', '思明区', 3, NULL, 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('22ec5f4e4d1ef61b5a28e1d605f1da3e', '140224', '140200', '灵丘县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('22fe871c14a30a35c5cea1aa1fa57823', '422800', '420000', '恩施土家族苗族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('230666e19a44e29e7dc5b4421bc8b716', '420116', '420100', '黄陂区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('23157ca4e2624375fadd3b08984d1eda', '340504', '340500', '雨山区', 3, NULL, 1, 1647931271, 1681975391);
INSERT INTO `yunduan_district` VALUES ('231b1d5038bce01aa5b0c1112a5bfdd2', '140727', '140700', '祁县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('231da172aa06c0b2c5d374b0cc0b4103', '650121', '650100', '乌鲁木齐县', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('23242e6acd9a4e47fd643b22e37f6b5a', '230281', '230200', '讷河市', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('2397b52d711fa2e09c4f32771d04e94c', '460205', '460200', '崖州区', 3, NULL, 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('239afb62500c2b8a0ad5c48027297c2e', '370522', '370500', '利津县', 3, NULL, 1, 1647931271, 1681975493);
INSERT INTO `yunduan_district` VALUES ('23a66a7ca78ee7f3277b98c78299f7db', '232701', '232700', '漠河市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('23dccd66231f46738372aa39c09372d1', '341321', '341300', '砀山县', 3, NULL, 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('23dd0a54104170c5c2a8e72f5f3ff3de', '130984', '130900', '河间市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('23e1fe8b89687b147a89a299ee124af7', '130300', '130000', '秦皇岛市', 2, '{\"county\": 7}', 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('23f5c3de7a71ec6e96f82881410fb03c', '510727', '510700', '平武县', 3, NULL, 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('24155ac4b4218084f996cc41c1a266b3', '360321', '360300', '莲花县', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('2455d4e0dd4aa005182827137132b477', '620300', '620000', '金昌市', 2, '{\"county\": 2}', 1, 1647931271, 1681978245);
INSERT INTO `yunduan_district` VALUES ('2456f4cecc1d9e35748c3badf5f6af84', '652323', '652300', '呼图壁县', 3, NULL, 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('245bce6cf4607f1d9543fa2911abaaec', '220721', '220700', '前郭尔罗斯蒙古族自治县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('2464b4e05c8a8d644873a203845a663e', '433130', '433100', '龙山县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('24658360cf4bdb961832aa9d527201d6', '341621', '341600', '涡阳县', 3, NULL, 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('246bb72bfafa0ee617fa676a3bc618fd', '150429', '150400', '宁城县', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('24a8684c105d3ec351f8c81a9c8baa0b', '320800', '320000', '淮安市', 2, '{\"county\": 7}', 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('24b495e5aafe031b7b2bd4d386c0a4ad', '411282', '411200', '灵宝市', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('24c11f54ee1142a8cc71dd9380660b27', '430200', '430000', '株洲市', 2, '{\"county\": 9}', 1, 1647931271, 1681975550);
INSERT INTO `yunduan_district` VALUES ('24dcfd5b1b677fd6852043e900966724', '370211', '370200', '黄岛区', 3, NULL, 1, 1647931271, 1681975483);
INSERT INTO `yunduan_district` VALUES ('24e1acf1f9a57cef2ec8433122288f60', '513400', '510000', '凉山彝族自治州', 2, '{\"county\": 17}', 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('24ff49b6eac66f3832c49ed7178e3526', '321084', '321000', '高邮市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('24ff850d2d3d5d286f6c502d694e5d26', '150523', '150500', '开鲁县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('2504901f19517ef3d68d831ec0c12474', '130200', '130000', '唐山市', 2, '{\"county\": 14}', 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('25127f9acbed28e0c9e8512127f97f1b', '460400', '460000', '儋州市', 2, NULL, 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('252b472cfb0f8506e6e8ab284c145263', '211021', '211000', '辽阳县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('255970ed0c19b40e34cd498a1eb77fe7', '500151', '500100', '铜梁区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('255e44e073fb538fec513e19a457b764', '520303', '520300', '汇川区', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('255fc5c1745a6513814f17c143db8593', '152526', '152500', '西乌珠穆沁旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('25615fb9e0f755d6cc183bf2d4231cca', '330282', '330200', '慈溪市', 3, NULL, 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('257ceadf2323922c4af191ef56afa566', '431300', '430000', '娄底市', 2, '{\"county\": 5}', 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('25bdefc1e3b8674e13ffcae5efee79bc', '340400', '340000', '淮南市', 2, '{\"county\": 7}', 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('25d3490d455688cc28ac1dc79322a05d', '530322', '530300', '陆良县', 3, NULL, 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('25ebfd46812111eac225f3756a7ad0c7', '513401', '513400', '西昌市', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('25ee49168f638c9b372a85b95af5ad3d', '130630', '130600', '涞源县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('261630168c446d884b0a0ddf66c98f3d', '220702', '220700', '宁江区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('262c576bd832207cc90ea31a05f5e04e', '232700', '230000', '大兴安岭地区', 2, '{\"county\": 3}', 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('265e145810f743cc6f1aa3c102651720', '420800', '420000', '荆门市', 2, '{\"county\": 5}', 1, 1647931271, 1681975542);
INSERT INTO `yunduan_district` VALUES ('2660a7be76d692de03660ae41a52e980', '440229', '440200', '翁源县', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('266241ed9d22908b24689db81764f149', '310115', '310100', '浦东新区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('266936b1210940c88c6b717b18528407', '130132', '130100', '元氏县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('26725d1a60b8fb115ace00a40953277c', '500156', '500100', '武隆区', 3, NULL, 1, 1647931271, 1681977450);
INSERT INTO `yunduan_district` VALUES ('26823003367da2230193016bee50ba8d', '430406', '430400', '雁峰区', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('268b91c0645df3e752777237c1299b94', '610112', '610100', '未央区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('26a4f11155cb764b0b5638da03065580', '150402', '150400', '红山区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('26b2765b6a880e3bc8c44116647ddf58', '370683', '370600', '莱州市', 3, NULL, 1, 1647931271, 1681975474);
INSERT INTO `yunduan_district` VALUES ('26d60b373c92e694eb7b980d8bc65eeb', '460200', '460000', '三亚市', 2, '{\"county\": 4}', 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('26e5efd98c5b8b08b966aa11ffafe9a2', '310106', '310100', '静安区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('26e5f04d049fe1acdce38e5391d51ccd', '530581', '530500', '腾冲市', 3, NULL, 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('26efa53c4e76ca4f36cae8a01ca46e74', '450204', '450200', '柳南区', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('26f4605f81d935796043c8d82786f39e', '532500', '530000', '红河哈尼族彝族自治州', 2, '{\"county\": 13}', 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('2701644ea47727e2c5ee9cadcb83e008', '360482', '360400', '共青城市', 3, NULL, 1, 1647931271, 1681975454);
INSERT INTO `yunduan_district` VALUES ('273112e885cf1d97e3f9bcdea8baeb81', '511303', '511300', '高坪区', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('27666f53d9577cfa78c20fd86fe6542c', '141031', '141000', '隰县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('276f631f8cc191b3514952f3077ded66', '610800', '610000', '榆林市', 2, '{\"county\": 12}', 1, 1647931271, 1681978065);
INSERT INTO `yunduan_district` VALUES ('27792a49bd132d7f6db16fd98afdb898', '430703', '430700', '鼎城区', 3, NULL, 1, 1647931271, 1681975558);
INSERT INTO `yunduan_district` VALUES ('2783015054cd7eb293abd8c5adbfcdee', '411400', '410000', '商丘市', 2, '{\"county\": 9}', 1, 1647931271, 1681975504);
INSERT INTO `yunduan_district` VALUES ('2790325645c5e706f465a2ea8f74ac3b', '430523', '430500', '邵阳县', 3, NULL, 1, 1647931271, 1681975568);
INSERT INTO `yunduan_district` VALUES ('27c3ea7ab3d3b71a1c29ae4b0e3ef469', '540623', '540600', '聂荣县 ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('27d794a979e2b9a00ee0ffbe42043e52', '431126', '431100', '宁远县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('27f7d3bf17bb89cca7030f9f0f11864f', '150922', '150900', '化德县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('27fa9097acf256b6e6a136976351529a', '141024', '141000', '洪洞县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('27fe7f45c5e8d4ca7c3ba80dca08debb', '140303', '140300', '矿区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('284995476154478f8736ad710a27d750', '500101', '500100', '万州区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('2857fd0c6a4c17c8769943a05a1c62c5', '410183', '410100', '新密市', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('285cb14f68ed6f5dc7d29f26f1e7dd28', '540321', '540300', '江达县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('286927601825961d15e529eba09c3ab5', '370126', '370100', '商河县', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('286be6b98ee92ee23e8f96458e8aac4e', '610328', '610300', '千阳县', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('287263c8670e343e6427c582277f11db', '411527', '411500', '淮滨县', 3, NULL, 1, 1647931271, 1681975528);
INSERT INTO `yunduan_district` VALUES ('28749c6808a55aaea643978b20010d08', '610403', '610400', '杨陵区', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('289d583323bf9aecbe7587b95b47f631', '211324', '211300', '喀喇沁左翼蒙古族自治县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('28a87d8766cf4ce090b9c79c2448eaca', '513325', '513300', '雅江县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('28cb5ff955805151f5960b851fa11142', '441821', '441800', '佛冈县', 3, NULL, 1, 1647931271, 1681975587);
INSERT INTO `yunduan_district` VALUES ('28d52bd03cb63e296c4c00aad6f9e73e', '370406', '370400', '山亭区', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('290d1034c1d06f67c2efe7e8bf6c4d72', '211005', '211000', '弓长岭区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('29178af8175e62a3bad682d54bc576fe', '330702', '330700', '婺城区', 3, NULL, 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('292492a26f03e2a0acc1a04971b04a58', '370911', '370900', '岱岳区', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('293b82263037f611cad6240d8ce46c15', '130105', '130100', '新华区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('294a33dfb822d9062c03147a5e645dde', '469023', '469000', '澄迈县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('295b4c085c52f16f6bff3dbdb605c943', '450903', '450900', '福绵区', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('29856658c46d0ac8112ecbd85c198990', '231002', '231000', '东安区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('298c01ddeb2678014dd126cd24b3c770', '451424', '451400', '大新县', 3, NULL, 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('299d9e26e96658e88b8fea720d38074b', '610428', '610400', '长武县', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('29de46fc8db0906da9da04e3b9ffbc55', '530802', '530800', '思茅区', 3, NULL, 1, 1647931271, 1681977875);
INSERT INTO `yunduan_district` VALUES ('29e90ec8354baa274716906246cb86b6', '511803', '511800', '名山区', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('29ec06a14650c0a24b987ba4336e19cd', '350702', '350700', '延平区', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('29fc5712df4c2db51b694e90f02b9cbc', '610728', '610700', '镇巴县', 3, NULL, 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('2a0a079aa1bdac9dd792fb51b051133b', '420203', '420200', '西塞山区', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('2a0de5d7455089116e4fc9976661bbfd', '450304', '450300', '象山区', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('2a160454d3e390fa05a6153295d795da', '360100', '360000', '南昌市', 2, '{\"county\": 9}', 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('2a1faf3501bcf71491b626317f94df4d', '371083', '371000', '乳山市', 3, NULL, 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('2a33a58bfd4c58ef8f1bf031eda659ca', '341222', '341200', '太和县', 3, NULL, 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('2a3ad913c3f226c323be9bd0ca620420', '140923', '140900', '代县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('2a47cf2f1e6dc4e1c29f4274cefdd6c8', '530424', '530400', '华宁县', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('2a4b8236a4ce97e9a382134a72fae87f', '511621', '511600', '岳池县', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('2a50356838b64a08ad915e13ad1786b5', '420822', '420800', '沙洋县', 3, NULL, 1, 1647931271, 1681975542);
INSERT INTO `yunduan_district` VALUES ('2a70a2624daf971b472c67345fa71046', '421125', '421100', '浠水县', 3, NULL, 1, 1647931271, 1681975545);
INSERT INTO `yunduan_district` VALUES ('2a9884e25c41271b3f03219c76eeb633', '150122', '150100', '托克托县', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('2aa8d9b3d506235d8948f4cefae0c08b', '430424', '430400', '衡东县', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('2aac26484875948088dc5bfeaff788bd', '210404', '210400', '望花区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('2ab27fdfb2064903e5aeeca68b50d966', '410611', '410600', '淇滨区', 3, NULL, 1, 1647931271, 1681975500);
INSERT INTO `yunduan_district` VALUES ('2adf9c35131a2a490f2c9f608a92d95f', '350821', '350800', '长汀县', 3, NULL, 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('2aedc0eea16ec5ff811422e6e80275d9', '210124', '210100', '法库县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('2b09a84b5b16c41b4704ec555e8e9f22', '540237', '540200', '岗巴县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('2b199ced9713907b6f043092f035fa21', '510921', '510900', '蓬溪县', 3, NULL, 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('2b564f34708322d3d057175caa0de754', '650502', '650500', '伊州区', 3, NULL, 1, 1647931271, 1681978496);
INSERT INTO `yunduan_district` VALUES ('2b83d21603cbbc4ec92734bd257bb52b', '131121', '131100', '枣强县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('2b9272c2666db2dd09ef5ccd5e84ca7e', '341126', '341100', '凤阳县', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('2ba112aa88914ecf446e5fbafe4232b3', '420582', '420500', '当阳市', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('2baff149d3de7b27635e0e2d704f1aba', '320900', '320000', '盐城市', 2, '{\"county\": 9}', 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('2bcebdc46d40bb94485c896bab5e53e0', '210224', '210200', '长海县', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('2beabb9465c4ca8e1047da3a55783a04', '653123', '653100', '英吉沙县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('2bf160730f2c6e05a77c7e4861d357c3', '140404', '140400', '上党区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('2c0a8fb39a7870372263bc95eb6a8ec5', '310117', '310100', '松江区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('2c1997fcdf4b566320bae9d153af960a', '460203', '460200', '吉阳区', 3, NULL, 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('2c2016a93aafc209be824eeee8e471e4', '500153', '500100', '荣昌区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('2c3db3ebd7c2846f5c9e362c84452c8c', '331003', '331000', '黄岩区', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('2c47baac93c82748efaaefcb0c801498', '450924', '450900', '兴业县', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('2c51b39d5b5882c8e653ce665bf7c365', '510105', '510100', '青羊区', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('2c57b11650c523ef579fd0d609c47b13', '469021', '469000', '定安县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('2c6007823a245f65d3feffb1eb1f4a37', '360402', '360400', '濂溪区', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('2c621322c6e158f29b825d49c08d796d', '370124', '370100', '平阴县', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('2c69bf792e073ded7faf0d886b9a52c0', '340181', '340100', '巢湖市', 3, NULL, 1, 1647931271, 1681975385);
INSERT INTO `yunduan_district` VALUES ('2c6ce652a2b73cec8442712cbfe78f05', '410326', '410300', '汝阳县', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('2c739088e130aa9761bb140d991936c6', '340103', '340100', '庐阳区', 3, NULL, 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('2c92428c23ce533097b2141ef765e694', '441624', '441600', '和平县', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('2cbd2a088f926887d2be28fb03246d67', '430426', '430400', '祁东县', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('2cbe324bac39038a931d07560c77c507', '371426', '371400', '平原县', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('2cc92f6a73ee1689965c0cb4654f0693', '371325', '371300', '费县', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('2d016d62eaeea5f11f7ce4cb5154e1f4', '610631', '610600', '黄龙县', 3, NULL, 1, 1647931271, 1681978079);
INSERT INTO `yunduan_district` VALUES ('2d115ca523c340e6a6cf3d62aca27374', '150203', '150200', '昆都仑区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('2d1cde3f5e43a1a57412cd2825e2c219', '131081', '131000', '霸州市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('2d29762c0402ff018ae918e5eb21900c', '320105', '320100', '建邺区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('2d3dbbee1e5d315c9107c32e972a1f1d', '230800', '230000', '佳木斯市', 2, '{\"county\": 10}', 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('2d43133a2ebc8e16b0ea919fa952e38b', '370682', '370600', '莱阳市', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('2d67d9c3fe9bd41b4b2d7872895f1c32', '370612', '370600', '牟平区', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('2d7b2077e10fd80acf9725fe6ba8ba86', '610926', '610900', '平利县', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('2d7eda2804e683c5e43cae0250841bf3', '340100', '340000', '合肥市', 2, '{\"county\": 9}', 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('2d983164c0223c224cb75b1c4a55dd4e', '130624', '130600', '阜平县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('2ddbd01f3835cea03fd54050be366dd1', '410602', '410600', '鹤山区', 3, NULL, 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('2ded0255fd414a7d61460062626d6733', '320600', '320000', '南通市', 2, '{\"county\": 8}', 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('2e0cdcfded279268c969df33459adbcd', '140931', '140900', '保德县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('2e2f86c5a961b4e46c14f6bc03283b7e', '421182', '421100', '武穴市', 3, NULL, 1, 1647931271, 1681975543);
INSERT INTO `yunduan_district` VALUES ('2e41d920dc389ba7037d0711dcc5ec52', '513326', '513300', '道孚县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('2e50d6913d9502d549038b817bac6452', '632300', '630000', '黄南藏族自治州', 2, '{\"county\": 4}', 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('2e634d1af462776d000db6b62d1664d2', '140110', '140100', '晋源区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('2e6526dd54813186ed11b2ed440330a2', '150923', '150900', '商都县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('2e7a0055114b55f4d144bdf7f43d6309', '220303', '220300', '铁东区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('2e7c3265b1917dc26c9baebd960269fa', '230606', '230600', '大同区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('2ea3e823bacf4b029bd20dcea673f8f5', '130684', '130600', '高碑店市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('2eb2efc81886b6b28bba1b01e74d51f7', '120116', '120100', '滨海新区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('2eb3a979452ce210aa5321ba0b59ff84', '330411', '330400', '秀洲区', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('2eb4660705124651a20a20ae69710180', '350125', '350100', '永泰县', 3, NULL, 1, 1647931271, 1681975418);
INSERT INTO `yunduan_district` VALUES ('2eb48d10e88d223ffec7a803a8332405', '230407', '230400', '兴山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('2ec5ed47ab1cdba6f46f0d5840b951b4', '140729', '140700', '灵石县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('2ed382b72195e31db342f116034fb439', '532331', '532300', '禄丰县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('2ed71218cef6f3af0d43bb4f5e0407d0', '445202', '445200', '榕城区', 3, NULL, 1, 1647931271, 1681975601);
INSERT INTO `yunduan_district` VALUES ('2eea209d3f5a9e6faf5b630f18dd9cc6', '431322', '431300', '新化县', 3, NULL, 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('2eee7ec800b783a80d0343b21bc0293a', '659008', '659000', '可克达拉市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('2efb5e951fbba3dcfe0cb8525b8f3ec0', '522723', '522700', '贵定县', 3, NULL, 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('2efff574a9c3703d99c1ae7e461b299b', '530626', '530600', '绥江县', 3, NULL, 1, 1647931271, 1681977886);
INSERT INTO `yunduan_district` VALUES ('2f206b2cd3271a40927e02118041bf22', '361025', '361000', '乐安县', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('2f2241d95b34994ae9deb4244c297ea3', '360302', '360300', '安源区', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('2f385ecfdd3be666740ed2e74b6fd4de', '231224', '231200', '庆安县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('2f589da412de5fd93b4bd4f36276db48', '532624', '532600', '麻栗坡县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('2f5bb98d198abc4c7a568bcf18611384', '411104', '411100', '召陵区', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('2f86294ec6d2324e8c12920224f9a0c6', '530828', '530800', '澜沧拉祜族自治县', 3, NULL, 1, 1647931271, 1681977876);
INSERT INTO `yunduan_district` VALUES ('2fa4f0b57634d1d79ea17b2817c8178e', '232722', '232700', '塔河县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('2fb6da554ea4074502b39a3f60910a37', '441424', '441400', '五华县', 3, NULL, 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('2fbb2b50e70435d80d6846aebf6c9149', '360982', '360900', '樟树市', 3, NULL, 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('2fc4a69b5ae4b4f4979a8751666cc317', '210112', '210100', '浑南区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('2fdbb891a15745d1dead348b5b3a5ee3', '330329', '330300', '泰顺县', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('2ff6193793235725407cc5e9123b8be8', '150302', '150300', '海勃湾区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('3014f52b85b6dc8812f12779fa331821', '340722', '340700', '枞阳县', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('301f03fc39e6909fb78da2231c67722c', '211403', '211400', '龙港区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('3033e6edab3720a61936ab1d3a75358b', '511112', '511100', '五通桥区', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('305489c7369f3f19ba6005cc5715cba9', '610104', '610100', '莲湖区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('3067b6167e5737a714a3b39aed3f8557', '520201', '520200', '钟山区', 3, NULL, 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('30a65f973191f5aabfbd16c224148e7f', '110108', '110100', '海淀区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('30b6760128c4575bcb32a446f7d8ee43', '540528', '540500', '加查县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('30bacf7e24a97f693ceb0e19014f1285', '469007', '469000', '东方市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('30fae3465f521ab77b567364b06a7050', '110109', '110100', '门头沟区', 3, NULL, 1, 1647931271, 1663565983);
INSERT INTO `yunduan_district` VALUES ('30fc6a92d9a8df4ca780032c4d2bb981', '230305', '230300', '梨树区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('310fed5b259bf357042ead0513c7a972', '130636', '130600', '顺平县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('3116e095c7fee94bab0238e3f3497803', '620821', '620800', '泾川县', 3, NULL, 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('3127c0bbe9332b81c06c8cbea51b1d78', '211422', '211400', '建昌县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('3128b0dbb74e24e4ac59055184762d7b', '430203', '430200', '芦淞区', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('3132a32194fe2ed84b7386dc7d493366', '511011', '511000', '东兴区', 3, NULL, 1, 1647931271, 1681977512);
INSERT INTO `yunduan_district` VALUES ('3133a8a73f6ea880018e38f372604944', '330726', '330700', '浦江县', 3, NULL, 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('313d27dbba857d54d05069860e0abbad', '150824', '150800', '乌拉特中旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('3161c4c3007008afc61dd05c5570d75a', '140426', '140400', '黎城县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('319368ae73ebdc2db7d6e4b22662877a', '222424', '222400', '汪清县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('3195ed83fa544c2b350a17dc8801921f', '350783', '350700', '建瓯市', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('319bf9add149b4c871c3398054761d42', '431200', '430000', '怀化市', 2, '{\"county\": 12}', 1, 1647931271, 1681975570);
INSERT INTO `yunduan_district` VALUES ('31db8be1be14e340a51d712a52960f75', '522325', '522300', '贞丰县', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('31e768c09a5fa73df557002e326cf04b', '330226', '330200', '宁海县', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('322102d851a618c9447bec568bfc43eb', '231086', '231000', '东宁市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('323658996e43f0efa932aefe084130e5', '451224', '451200', '东兰县', 3, NULL, 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('326e3487df55b0c80471d80017d207b4', '441700', '440000', '阳江市', 2, '{\"county\": 4}', 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('32851855033a3dc587cb0e2b1831ca6b', '330281', '330200', '余姚市', 3, NULL, 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('32a2802553e29ce20ee5cc3418c626dc', '411722', '411700', '上蔡县', 3, NULL, 1, 1647931271, 1681975522);
INSERT INTO `yunduan_district` VALUES ('32bf44d6af2dec0b421933da3f6e0ac1', '640324', '640300', '同心县', 3, NULL, 1, 1647931271, 1681978326);
INSERT INTO `yunduan_district` VALUES ('32cb769d5b1d481b3a8fa54fa16d7313', '420000', '0', '湖北省', 1, '{\"city\": 13, \"county\": 103}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('32cc690f2670cbacd8a05f3c41ce832c', '431128', '431100', '新田县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('32d4bd5e644fdb48b0601d0bcf444342', '140109', '140100', '万柏林区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('32dc2fa85707db296706c7ce153158b4', '320831', '320800', '金湖县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('32fcf3aa54e41405e13472c77a6c03bb', '540600', '540000', '那曲市', 2, '{\"county\": 11}', 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('332d1c3c76ff03fda6a3802ef4bad6b6', '511425', '511400', '青神县', 3, NULL, 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('33360ffefe56af45be937ac00a039e4a', '310116', '310100', '金山区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('3342025ded62032eb84fa49e0a0090b8', '130121', '130100', '井陉县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('339845b70ce2dd9f249949c351925be0', '231003', '231000', '阳明区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('33cb9eebbcc6b6a32a4d3941911dd616', '371723', '371700', '成武县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('33d038a8463560a51f87e0610c7055cb', '360826', '360800', '泰和县', 3, NULL, 1, 1647931271, 1681975439);
INSERT INTO `yunduan_district` VALUES ('340da526909a31d451e882160f4727c1', '350122', '350100', '连江县', 3, NULL, 1, 1647931271, 1681975419);
INSERT INTO `yunduan_district` VALUES ('342133b23911a00123b6d0b9868b7a0c', '210213', '210200', '金州区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('3429482de850462fd48aad725b01d9e8', '532329', '532300', '武定县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('34acf7b619ec13d354f696a2e4d62e9f', '650522', '650500', '伊吾县', 3, NULL, 1, 1647931271, 1681978496);
INSERT INTO `yunduan_district` VALUES ('34b1dadd94a6b77bbf9a5aa58e4f42a4', '440306', '440300', '宝安区', 3, NULL, 1, 1647931271, 1681975602);
INSERT INTO `yunduan_district` VALUES ('34d67ddc2f6c9856448e22eb5daa6858', '450223', '450200', '鹿寨县', 3, NULL, 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('34d9e518a04caa6bb3ee27890203fc50', '360404', '360400', '柴桑区', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('3507c6834ecf79e447c1d337202977c4', '450800', '450000', '贵港市', 2, '{\"county\": 5}', 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('352b5d4f418c71d691ca2df26d4a1a6b', '331023', '331000', '天台县', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('35376f0b008c8de4be77e4ecd96fc239', '632221', '632200', '门源回族自治县', 3, NULL, 1, 1647931271, 1681978320);
INSERT INTO `yunduan_district` VALUES ('353e0781ab843f6a66cbb6a6d411e575', '360502', '360500', '渝水区', 3, NULL, 1, 1647931271, 1681975450);
INSERT INTO `yunduan_district` VALUES ('35617ad475e9aa7bd69709f7535ed447', '511923', '511900', '平昌县', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('357e9ce5551a1f6d9ce190b8197c15cc', '540530', '540500', '错那县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('3581091c58594453c6749ef5a9b2e0e4', '610111', '610100', '灞桥区', 3, NULL, 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('35ae3453f9496b621be8a8ab087d26eb', '230811', '230800', '郊区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('35c6f5dbeb13094d2766aacc1fcfc9c5', '431381', '431300', '冷水江市', 3, NULL, 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('35e14caacc467883fb4a68cf111698c6', '110100', '110000', '北京市', 2, '{\"county\": 16}', 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('35f2eb661c1898e0a64b41733c5f481d', '652926', '652900', '拜城县', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('36210c868756dc4c47530d0689371738', '230805', '230800', '东风区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('363f17bcc7c8f2bf2ae7ff7fc5ac6731', '431202', '431200', '鹤城区', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('36404723f1cfbaf9c541b75f4571eb53', '220202', '220200', '昌邑区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('3642ea6e182b9b775823ac89d0d75a46', '130000', '0', '河北省', 1, '{\"city\": 11, \"county\": 167}', 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('369c6aef04c6f81d77d229e6f8199c87', '411621', '411600', '扶沟县', 3, NULL, 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('36b6a41995abb24acb6e87bede1124bb', '510504', '510500', '龙马潭区', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('36bc2dc48f1624e8d96442fd2879a054', '410522', '410500', '安阳县', 3, NULL, 1, 1647931271, 1681975510);
INSERT INTO `yunduan_district` VALUES ('36c12dd1b4ea7f1759b627fb5f75421c', '530426', '530400', '峨山彝族自治县', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('36fd9bfff3f4b63fcc5255374ab197ab', '220113', '220100', '九台区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('37024651e8bbb04207d78a263a906748', '422827', '422800', '来凤县', 3, NULL, 1, 1647931271, 1681975540);
INSERT INTO `yunduan_district` VALUES ('3707400f134c8fc64a9dc676a326cff9', '320302', '320300', '鼓楼区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('3708b534aabbb90ca33a851415767356', '520281', '520200', '盘州市', 3, NULL, 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('370cb69761c876af684044ece61b6a3d', '511900', '510000', '巴中市', 2, '{\"county\": 5}', 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('3732e18f9fb4647879e5ba8db7c5be96', '510700', '510000', '绵阳市', 2, '{\"county\": 9}', 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('373bf19092b71562856ebb7781e32fa9', '330200', '330000', '宁波市', 2, '{\"county\": 10}', 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('3746ed6728cadb6e14a76c448a6330d0', '450305', '450300', '七星区', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('37481739df965e73ebbe5fe9d158eeb2', '230883', '230800', '抚远市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('374a0157d0fdcd3e2a646e7448db60e2', '621223', '621200', '宕昌县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('374ea3b0a395c00b0eff4716c0867459', '540302', '540300', '卡若区', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('378d85c06e9b1bb93cc3dcbe4c2a6f1d', '440882', '440800', '雷州市', 3, NULL, 1, 1647931271, 1681975591);
INSERT INTO `yunduan_district` VALUES ('3796a04168e5c39b5e14a8510ac57ebd', '411725', '411700', '确山县', 3, NULL, 1, 1647931271, 1681975523);
INSERT INTO `yunduan_district` VALUES ('37aad45e59830111172bdeb67f68c7cb', '510522', '510500', '合江县', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('37abdc732133c0926bd6326e2eecfa33', '431121', '431100', '祁阳县', 3, NULL, 1, 1647931271, 1681975568);
INSERT INTO `yunduan_district` VALUES ('37b1134c5ebf2f727335161e042e6663', '510603', '510600', '旌阳区', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('37c9003fb111bada5d5628a52945fe02', '650106', '650100', '头屯河区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('37c9a3054dd303cd3ac514b4da12f144', '330481', '330400', '海宁市', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('37d96b9e41721895ec24999930d453e9', '441284', '441200', '四会市', 3, NULL, 1, 1647931271, 1681975584);
INSERT INTO `yunduan_district` VALUES ('37e097bdd09dbf2824d4f3f26203d6e4', '130324', '130300', '卢龙县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('37e1c5de48ae0f20a6edcda671d95752', '654021', '654000', '伊宁县', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('37f85b838ebc4f3aaa26495383a51c1d', '632301', '632300', '同仁市', 3, NULL, 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('3809fc8af0e7c20320a933e3c103e85f', '610630', '610600', '宜川县', 3, NULL, 1, 1647931271, 1681978081);
INSERT INTO `yunduan_district` VALUES ('3845f64daf7a7e0bc50251df4c950c14', '532924', '532900', '宾川县', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('38541a4feff092c24f40a15dd7c2526e', '532822', '532800', '勐海县', 3, NULL, 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('38559ac87c3ef02b5ba9e959ffbbc22f', '320321', '320300', '丰县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('38658f2a02ea92c166212c3d249fddf5', '340422', '340400', '寿县', 3, NULL, 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('38af820d2148ee192bfc1b2676bc39a3', '430611', '430600', '君山区', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('38dd361c2c93f063994b358dcb64ac95', '532901', '532900', '大理市', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('38e5e71c7aa55a173b7b8dcb05d34202', '532823', '532800', '勐腊县', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('38f7665e5dc8977fd9657dd8ca483399', '652328', '652300', '木垒哈萨克自治县', 3, NULL, 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('38fb1774b1e79fc464875debe33e210c', '522624', '522600', '三穗县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('391662e3e869fdb23b3dfed37753dd81', '623026', '623000', '碌曲县', 3, NULL, 1, 1647931271, 1681978243);
INSERT INTO `yunduan_district` VALUES ('394d27febd202292280060abaddc01c7', '653226', '653200', '于田县', 3, NULL, 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('3958bcc8fee891d79e897a966eeae5ac', '340404', '340400', '谢家集区', 3, NULL, 1, 1647931271, 1681975382);
INSERT INTO `yunduan_district` VALUES ('398a9c54d3e9a485f0aaca333ef693ad', '370523', '370500', '广饶县', 3, NULL, 1, 1647931271, 1681975493);
INSERT INTO `yunduan_district` VALUES ('3998a75044c101ee5f9d87f03580097d', '640424', '640400', '泾源县', 3, NULL, 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('39da9fa36e69ad212d967dcf6d2b063a', '410381', '410300', '偃师市', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('39efab5caf08771998dd0016ed4a2806', '360728', '360700', '定南县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('3a1722da86e0d1f25e78e6ea29e2241a', '130706', '130700', '下花园区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('3a189f5d9d5aba420c7a7495137f126d', '310118', '310100', '青浦区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('3a200a6ad94dbfb18fc3ab60d05119dc', '360102', '360100', '东湖区', 3, NULL, 1, 1647931271, 1681975449);
INSERT INTO `yunduan_district` VALUES ('3a314bf229719c0cf76a4d0dc534236f', '330213', '330200', '奉化区', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('3a40177d78a6d1b0bb3fbca2a33d6b7e', '411522', '411500', '光山县', 3, NULL, 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('3a444f2edf58060acca19efe408a184d', '141100', '140000', '吕梁市', 2, '{\"county\": 13}', 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('3a46432518358c89501619a2360ff0bf', '420626', '420600', '保康县', 3, NULL, 1, 1647931271, 1681975530);
INSERT INTO `yunduan_district` VALUES ('3a485e58b4c527852afa0ea5065ccd52', '340303', '340300', '蚌山区', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('3a69c2c528cba84622674d5590cfa178', '421127', '421100', '黄梅县', 3, NULL, 1, 1647931271, 1681975545);
INSERT INTO `yunduan_district` VALUES ('3a6fb81c8ccc6b438e3dbf4d76fac0c9', '361103', '361100', '广丰区', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('3a7617bb49e7fee2030f470bd938ff89', '533325', '533300', '兰坪白族普米族自治县', 3, NULL, 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('3aa8e0f4bc7e142d29ae29bf3025d190', '522324', '522300', '晴隆县', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('3ad43a4174228b45f9ae5c6170917128', '350305', '350300', '秀屿区', 3, NULL, 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('3aeaa124227b873b05a6df7e6220d476', '371500', '370000', '聊城市', 2, '{\"county\": 8}', 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('3af7744deadb3e4708c1045a0810553a', '513332', '513300', '石渠县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('3afaaf76beae9f23911b6c1db9b0f2b4', '513323', '513300', '丹巴县', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('3b2521afac41d54dfd3f3ea67bb2eea5', '450923', '450900', '博白县', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('3b69ce463ee6160f4c937fd48e776665', '420683', '420600', '枣阳市', 3, NULL, 1, 1647931271, 1681975531);
INSERT INTO `yunduan_district` VALUES ('3b6a3567cbbb9a356d4082a7bc8090cb', '230307', '230300', '麻山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('3b7dad6b0e15b651634e8bde51dbc924', '131123', '131100', '武强县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('3b84ba2f917ebc1703fee2aed6e3e45c', '451202', '451200', '金城江区', 3, NULL, 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('3b890f13f42f6eef9eae56ae8a1b5b9d', '220503', '220500', '二道江区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('3b8bd19ae76b63b3cff3fab29d0bf512', '440604', '440600', '禅城区', 3, NULL, 1, 1647931271, 1681975593);
INSERT INTO `yunduan_district` VALUES ('3b985fd5ba0c91c4c98e0e0efabb4ac9', '450108', '450100', '良庆区', 3, NULL, 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('3b9ce702ec7df275a66a3089a7df97c3', '150121', '150100', '土默特左旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('3bef35b8b27f245154515e62e2edea81', '632523', '632500', '贵德县', 3, NULL, 1, 1647931271, 1681978318);
INSERT INTO `yunduan_district` VALUES ('3bf99c7494402be7ec9b4d84c2cd1eac', '370304', '370300', '博山区', 3, NULL, 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('3c07e371f34bb5c05d43339a68020d57', '610124', '610100', '周至县', 3, NULL, 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('3c0d9b398afd1f473c7b5929fdc919e4', '130530', '130500', '新河县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('3c254133a91161c86d8ca1b3fc451e66', '421003', '421000', '荆州区', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('3c3f50a9f9fdb5f28c3bc42f3952cd65', '150204', '150200', '青山区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('3c5530d6fa2dd311bfcd7ea689108233', '321102', '321100', '京口区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('3c6f714ba3772b87aec95aae146ac707', '320312', '320300', '铜山区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('3c70d4b25dd011f7b5f34a25fe5d1cf6', '410823', '410800', '武陟县', 3, NULL, 1, 1647931271, 1681975515);
INSERT INTO `yunduan_district` VALUES ('3c751a06e6307913af9f2a6627745797', '440904', '440900', '电白区', 3, NULL, 1, 1647931271, 1681975582);
INSERT INTO `yunduan_district` VALUES ('3c803775f932ce1fc2cabdc022e2cef3', '321202', '321200', '海陵区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('3c8bb8d8b0643b2162cb0d728a22fc06', '520121', '520100', '开阳县', 3, NULL, 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('3c98f9c19ee0a9058a21fe7f099b15e1', '430181', '430100', '浏阳市', 3, NULL, 1, 1647931271, 1681975565);
INSERT INTO `yunduan_district` VALUES ('3c9d1b85fc42ebe50ac9904a45a19bf0', '350521', '350500', '惠安县', 3, NULL, 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('3ce6c4326628920f1cf344fb2e5f4aba', '130730', '130700', '怀来县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('3cedcd5188d32264172c12258a4bd257', '341181', '341100', '天长市', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('3cf213f023e34816cb3e3852636cae06', '320106', '320100', '鼓楼区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('3cf847d2cd556deef71988d0316d74db', '150207', '150200', '九原区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('3d0421173e17e967629b56f3189a22fc', '411602', '411600', '川汇区', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('3d331f7855123342765bcb6ac8ccb417', '510500', '510000', '泸州市', 2, '{\"county\": 7}', 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('3d409b7613e19e8905e03b33d7d0c46e', '430224', '430200', '茶陵县', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('3d42d010717b7bde5901a5835dcb627e', '350423', '350400', '清流县', 3, NULL, 1, 1647931271, 1681975419);
INSERT INTO `yunduan_district` VALUES ('3d56ecf7cce94952059080f6a998230b', '632625', '632600', '久治县', 3, NULL, 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('3d5bb9fe8bd1fa70a3223aee882a9828', '530425', '530400', '易门县', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('3d6c13fa1612b5291bd47ebcdbfb07eb', '540102', '540100', '城关区', 3, NULL, 1, 1647931271, 1681977991);
INSERT INTO `yunduan_district` VALUES ('3d72011eb5e45d163226f306e7aa1833', '320507', '320500', '相城区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('3d98f83ba732b00ff4c76a336c41d33a', '222404', '222400', '珲春市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('3d9b9d57347b19486480b61ce1a6a979', '530481', '530400', '澄江市', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('3d9fe4f5ffbc06a8880209a04f9400f2', '513225', '513200', '九寨沟县', 3, NULL, 1, 1647931271, 1681977527);
INSERT INTO `yunduan_district` VALUES ('3dad22430a9ccdf882e0e68a86bd8a6d', '340828', '340800', '岳西县', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('3db06a2aebb7ec7da68816345ffd1123', '410882', '410800', '沁阳市', 3, NULL, 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('3dc661b1f61a84118bcb1309955c84f3', '341721', '341700', '东至县', 3, NULL, 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('3dd4eee14faa342b1064a07efc51d22a', '411622', '411600', '西华县', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('3deb4005add902fc28030f597e981071', '320500', '320000', '苏州市', 2, '{\"county\": 9}', 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('3dfeb9496970128767218138c049cb97', '510726', '510700', '北川羌族自治县', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('3e1d42518dcfcfd8b6bb5c67a013a71c', '532504', '532500', '弥勒市', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('3e26b289da92344cff5bc4325bbd1611', '350303', '350300', '涵江区', 3, NULL, 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('3e7846545931aab79aef32dcb7bd10b0', '513432', '513400', '喜德县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('3e9b27c017b559dd8e5f601e69e51695', '610326', '610300', '眉县', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('3ece8f3d14789d386bc2c37ac0932f0c', '360704', '360700', '赣县区', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('3eeae73b00b8d4c20d717c1f7ce6a33b', '140926', '140900', '静乐县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('3f029d8c26e840a82bca7fdded41fdc9', '330800', '330000', '衢州市', 2, '{\"county\": 6}', 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('3f072fee75d7d814cbc6b196a03b6042', '321100', '320000', '镇江市', 2, '{\"county\": 6}', 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('3f0feb2dfa644536eec36b18a828da6c', '340403', '340400', '田家庵区', 3, NULL, 1, 1647931271, 1681975382);
INSERT INTO `yunduan_district` VALUES ('3f305b599bb9bce36627e2de0328d4b8', '511424', '511400', '丹棱县', 3, NULL, 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('3f32cb48ef8e9f2065e2412a5cedefc4', '370000', '0', '山东省', 1, '{\"city\": 16, \"county\": 136}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('3f4b4ce371b4e4489506dd764e522c66', '610331', '610300', '太白县', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('3f549c75eb03739bf648d70282c9d7f9', '350925', '350900', '周宁县', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('3f84b8c1fc75dfd6903869ddd723300b', '330324', '330300', '永嘉县', 3, NULL, 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('3f98441b3b9039cc2c2ba339ed57a658', '360500', '360000', '新余市', 2, '{\"county\": 2}', 1, 1647931271, 1681975450);
INSERT INTO `yunduan_district` VALUES ('3f9de36799030a96da3d4589c624b2db', '340621', '340600', '濉溪县', 3, NULL, 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('3fbaac2c050249517f58ad10723d4024', '532628', '532600', '富宁县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('3fcdc6587eeab612ae8e1af26d6a640a', '469029', '469000', '保亭黎族苗族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('3ffb8e96492238dca205f10566826467', '360602', '360600', '月湖区', 3, NULL, 1, 1647931271, 1681975447);
INSERT INTO `yunduan_district` VALUES ('3fff0944490286ee222183afd92ae4fc', '441300', '440000', '惠州市', 2, '{\"county\": 5}', 1, 1647931271, 1681975584);
INSERT INTO `yunduan_district` VALUES ('40179a8e4d9dd16b22fd3b5e858f250f', '220602', '220600', '浑江区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('401a1df51e6ae383137df4205ae952c8', '341503', '341500', '裕安区', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('4034331e3f6148a4fa3f3e94a942039b', '150622', '150600', '准格尔旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('403695022ca7b99f384c2f7b4f64a056', '150727', '150700', '新巴尔虎右旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('40526a99b0ae6f36474ff0c46f49d6e3', '440303', '440300', '罗湖区', 3, NULL, 1, 1647931271, 1681975602);
INSERT INTO `yunduan_district` VALUES ('40626a5f7dac4b80a6ba1b04ba5b2116', '330802', '330800', '柯城区', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('407bbd2fe5b2adfc9454f11dbb0a95b1', '141082', '141000', '霍州市', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('407c9b0737941b21d5025da0b43083cc', '341881', '341800', '宁国市', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('407ca347c72099fb3acfc37933df5ddd', '110105', '110100', '朝阳区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('4081e466eb9d4e822db6df3c3f65ad98', '520521', '520500', '大方县', 3, NULL, 1, 1647931271, 1681977723);
INSERT INTO `yunduan_district` VALUES ('40962c93f7578f1fb2c9d9f27b423536', '522731', '522700', '惠水县', 3, NULL, 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('40c8fcd359a07b4bc2205d254b8a78c3', '621227', '621200', '徽县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('40cb549e2ff54dd27f6ad035fbf8393a', '622923', '622900', '永靖县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('40d4f197ddf65c30182d3c235f2900aa', '540423', '540400', '墨脱县', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('413616b51ee9a65f8f7480ea16394264', '610627', '610600', '甘泉县', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('41410cdab05b67c765b6265799b2487d', '610928', '610900', '旬阳县', 3, NULL, 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('415054f59e07e63633563cd4594a44e4', '341525', '341500', '霍山县', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('4150b31e0f2427cd0490e2d34beaecdb', '211011', '211000', '太子河区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('415ac8f37c4f0be1b2b41953f155f88e', '150502', '150500', '科尔沁区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('4183405be0b96946adcb9b4973bbc395', '532601', '532600', '文山市', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('418d2d6685dde4b31649fb074dc53538', '445102', '445100', '湘桥区', 3, NULL, 1, 1647931271, 1681975577);
INSERT INTO `yunduan_district` VALUES ('41a2b361b9512a776aa14dd5ff45eaf7', '360202', '360200', '昌江区', 3, NULL, 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('41b004dba2a4ce7ea308eb5faaee6577', '441202', '441200', '端州区', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('41b476611aec8b250a586669d54d8585', '440281', '440200', '乐昌市', 3, NULL, 1, 1647931271, 1681975576);
INSERT INTO `yunduan_district` VALUES ('41df52b864c2b414b8108c11df46007b', '422825', '422800', '宣恩县', 3, NULL, 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('41f59d4b21524bbdc7aaf4d224903735', '430721', '430700', '安乡县', 3, NULL, 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('42027cc32cefa285010c3c06907082e9', '360821', '360800', '吉安县', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('42173e09a5adab519affa92d2c23db47', '530624', '530600', '大关县', 3, NULL, 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('422320baa425deacabe9a02a0af26af6', '230624', '230600', '杜尔伯特蒙古族自治县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('4234660f90a834b00a923c70a8543902', '533324', '533300', '贡山独龙族怒族自治县', 3, NULL, 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('423b5e958af4a207f2d79a699e496bbc', '330112', '330100', '临安区', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('423e33e83378092d73ae50f154089f28', '230108', '230100', '平房区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('4245e87d2678f9f7d54e45a764c3d0c5', '530600', '530000', '昭通市', 2, '{\"county\": 11}', 1, 1647931271, 1681977886);
INSERT INTO `yunduan_district` VALUES ('425ac6aef409693448ee4ad02303e6db', '510112', '510100', '龙泉驿区', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('4271b5bd5b7dfaadc38f5baccaece6a6', '150703', '150700', '扎赉诺尔区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('427c8277c74160e7d034d1a56a4b834c', '130600', '130000', '保定市', 2, '{\"county\": 24}', 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('428c683c90dfed5fdcdd952d959cef04', '540626', '540600', '索县   ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('428ecd11d0771f189de3acb26c4ecfa0', '522702', '522700', '福泉市', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('42c24ed985070053be03fd800f322e5a', '532922', '532900', '漾濞彝族自治县', 3, NULL, 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('42c464a7ad167733186aec01fb7eb8ff', '411726', '411700', '泌阳县', 3, NULL, 1, 1647931271, 1681975523);
INSERT INTO `yunduan_district` VALUES ('42ddf8c240b89604141fa1821c134ea2', '321112', '321100', '丹徒区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('42f4ae9a019acc6c0ca5b4fed0dc2bae', '410122', '410100', '中牟县', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('42f8ead35fa5b75d5c16e3b959a294f8', '370902', '370900', '泰山区', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('42fbf11ee00febaa4aa948485dde1fc1', '421022', '421000', '公安县', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('430d49beaa5f6c35d78b7131051a17a1', '511903', '511900', '恩阳区', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('432e06339b729430eab1a653f6192777', '230502', '230500', '尖山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('43307ca713b95650be320ec14e6b4fe8', '140311', '140300', '郊区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('43643285cf7bb6cc950d77c34afa4a72', '532900', '530000', '大理白族自治州', 2, '{\"county\": 12}', 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('43896a2cd4b80d282411cdbab1eb9c7d', '410581', '410500', '林州市', 3, NULL, 1, 1647931271, 1681975510);
INSERT INTO `yunduan_district` VALUES ('439f782e2191292ea48be602306afb7e', '331024', '331000', '仙居县', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('43a2c29b3d4ec5cdb476c2f15a8ebc37', '370323', '370300', '沂源县', 3, NULL, 1, 1647931271, 1681975486);
INSERT INTO `yunduan_district` VALUES ('43c763ad9822f6a0dce4262e515bf03d', '350802', '350800', '新罗区', 3, NULL, 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('43fe5379a2496b0d9a4c5621e97da2c4', '510604', '510600', '罗江区', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('440bda7d5395cc613999445ad4481924', '140928', '140900', '五寨县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('4424c93b5200b1fd5b67ad06a74cadf6', '370687', '370600', '海阳市', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('442aca8ce543934481dfd5df059ecf1b', '341226', '341200', '颍上县', 3, NULL, 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('442c9c41b4201910ba4b94c2f54e67a5', '150403', '150400', '元宝山区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('445b6d2b57e93de1d0a91de92d1fedd3', '420982', '420900', '安陆市', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('4468b6db9bd324b92edb2b1b4c875bab', '131126', '131100', '故城县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('447d7dec9700ce5dc347029e52bf8548', '621224', '621200', '康县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('448d74e98760305587f9787154d2ec0c', '520325', '520300', '道真仡佬族苗族自治县', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('44a778b7850b9a6391c5d214919377a8', '410783', '410700', '长垣市', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('44e2e00a03c7fe776ea5b73337b86a52', '451222', '451200', '天峨县', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('4501d3d5c214929d8a1471241b5213d7', '140427', '140400', '壶关县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('451ef5bfcdcb82d61501ce2fb7564470', '640106', '640100', '金凤区', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('4533ad09c663db5ca3e6bb93dbb6588c', '430121', '430100', '长沙县', 3, NULL, 1, 1647931271, 1681975565);
INSERT INTO `yunduan_district` VALUES ('453aaddddfa070a028f8111c08ca087c', '522635', '522600', '麻江县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('453b8a51a6c9cbe2d743a5c23e5bb9c2', '441204', '441200', '高要区', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('454066d095d1b67d67b0bb4ac4b53be5', '610116', '610100', '长安区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('4579be8fda22aa4eb99008fb421adf74', '640122', '640100', '贺兰县', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('4583787abf64be98db6187f82d0d6aa6', '230221', '230200', '龙江县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('45a27eebd1a8ffedeed2996f5f17be5d', '451030', '451000', '西林县', 3, NULL, 1, 1647931271, 1681975629);
INSERT INTO `yunduan_district` VALUES ('45a33c848150682b1aa328e28aa708a6', '540325', '540300', '察雅县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('45b1136c5e377185bfa7bfcb8a30628e', '654200', '650000', '塔城地区', 2, '{\"county\": 7}', 1, 1647931271, 1681978499);
INSERT INTO `yunduan_district` VALUES ('45c6f9ccd89fd06669348b7f3a34c251', '654026', '654000', '昭苏县', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('4619ed4dd93e0a1415d5babcbd082c4d', '632624', '632600', '达日县', 3, NULL, 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('4642f2927a6d6d548493ba415ffb2e22', '420102', '420100', '江岸区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('465b3feb9805fcc857b32e9a263c30c5', '653201', '653200', '和田市', 3, NULL, 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('468045148d07ded04ecfd87685560f02', '450312', '450300', '临桂区', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('469e533d5c607cac66e58ed64224991e', '350128', '350100', '平潭县', 3, NULL, 1, 1647931271, 1681975417);
INSERT INTO `yunduan_district` VALUES ('469e79243a92931addb4a1d09fe8e496', '320826', '320800', '涟水县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('469fdac6d1b9a3f211c8367c054016af', '152221', '152200', '科尔沁右翼前旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('46ad04149856ea850b35e0217d095e0a', '653100', '650000', '喀什地区', 2, '{\"county\": 12}', 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('46c51e625d394fde16a683efa4a41d54', '430582', '430500', '邵东市', 3, NULL, 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('46ccd9f6ae00e16fc16e4a830ea7cd12', '420113', '420100', '汉南区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('46cf83236a7611d76a3f64917215c0ab', '460000', '0', '海南省', 1, '{\"city\": 4, \"county\": 23}', 1, 1647931271, 1661303833);
INSERT INTO `yunduan_district` VALUES ('46d715e7f99ca10be9e1c75fd9f71818', '621022', '621000', '环县', 3, NULL, 1, 1647931271, 1681978246);
INSERT INTO `yunduan_district` VALUES ('46e2a8e677810a7a17ffde26ad3688f8', '421000', '420000', '荆州市', 2, '{\"county\": 8}', 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('46ea6c1079bfdd76db830d29c7f4b610', '341500', '340000', '六安市', 2, '{\"county\": 7}', 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('4706a8d5e0068ac657f5ba02a3abee6e', '411002', '411000', '魏都区', 3, NULL, 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('470c32780b77e3dfba40a448c3588266', '210682', '210600', '凤城市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('472927cd35050fe651b7a1217026739e', '441823', '441800', '阳山县', 3, NULL, 1, 1647931271, 1681975588);
INSERT INTO `yunduan_district` VALUES ('472aae3fa55796b98ecdb5a4aee21ef8', '532301', '532300', '楚雄市', 3, NULL, 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('476e117006264943f26003c1a8c48c92', '610323', '610300', '岐山县', 3, NULL, 1, 1647931271, 1681978075);
INSERT INTO `yunduan_district` VALUES ('477f1df8f2ef40ba54f086a9bd45c299', '330127', '330100', '淳安县', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('4781c451d2ae499f8c386898e9198e1d', '222426', '222400', '安图县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('479d7d5cadf07bfeab5689c99cbb731c', '653126', '653100', '叶城县', 3, NULL, 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('47b4a8ddc8b22d48ab10ccfc8198eedb', '450600', '450000', '防城港市', 2, '{\"county\": 4}', 1, 1647931271, 1681975619);
INSERT INTO `yunduan_district` VALUES ('47c67a22064712b88ba353510d993fc5', '445302', '445300', '云城区', 3, NULL, 1, 1647931271, 1681975606);
INSERT INTO `yunduan_district` VALUES ('47cf87663c5c7bd13e9a43c0cdfc088b', '330326', '330300', '平阳县', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('47d34ac7e1299f1ac371f73d6a8b2d31', '451421', '451400', '扶绥县', 3, NULL, 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('47f91559ab9a36ac992a50ead9628119', '411081', '411000', '禹州市', 3, NULL, 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('48103f10f5cdc2dc9dcbc6ca286f4a3c', '340711', '340700', '郊区', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('482bdae6720c71a0c33c44c19f0b71b4', '431230', '431200', '通道侗族自治县', 3, NULL, 1, 1647931271, 1681975570);
INSERT INTO `yunduan_district` VALUES ('48300ee9d8c8992a415e5542f6306d64', '150624', '150600', '鄂托克旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('4830ebe26fbc24941f53bf9855095f88', '632700', '630000', '玉树藏族自治州', 2, '{\"county\": 6}', 1, 1647931271, 1681978311);
INSERT INTO `yunduan_district` VALUES ('48523211fe3bbae83e95be08645a3204', '220881', '220800', '洮南市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('4863f74740f0e73f5e32d0db4d4e4b1a', '610524', '610500', '合阳县', 3, NULL, 1, 1647931271, 1681978078);
INSERT INTO `yunduan_district` VALUES ('48901cb5824b79dfc0c3b85393f6cc9b', '341523', '341500', '舒城县', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('48a08d394387ca4cf915b70c28e79ece', '321023', '321000', '宝应县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('48be220a0d9a5905c3ea44624c028c40', '450222', '450200', '柳城县', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('48ca89d3cb634aabd77145388251cdb6', '522627', '522600', '天柱县', 3, NULL, 1, 1647931271, 1681977731);
INSERT INTO `yunduan_district` VALUES ('48cfd082062e3d512e27bb66331aa600', '230602', '230600', '萨尔图区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('49016f1d04441a2cb4cb2a8c94283259', '632223', '632200', '海晏县', 3, NULL, 1, 1647931271, 1681978320);
INSERT INTO `yunduan_district` VALUES ('49035d5fdf281e66e4ba43e043ba62fa', '530627', '530600', '镇雄县', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('491a60450cb8d7ce23685923e049b155', '430302', '430300', '雨湖区', 3, NULL, 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('493f9c9df01eec913a9f4e021c68aff9', '510525', '510500', '古蔺县', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('49497fcf1068aeff988fa76ed2584990', '361030', '361000', '广昌县', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('495e368328281ad321417422f492c3ee', '540624', '540600', '安多县 ', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('4964e650a882c9c394a71de4a295e590', '540625', '540600', '申扎县 ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('49788813de010e9b3632c7fc69ce5b15', '420115', '420100', '江夏区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('499dcff7def08ba57fb7cf619c2b474e', '220802', '220800', '洮北区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('49a021fc4a1902734afee53e40bbb47c', '370881', '370800', '曲阜市', 3, NULL, 1, 1647931271, 1681975495);
INSERT INTO `yunduan_district` VALUES ('49c38210a1b5997ed3926c2f3242313c', '440113', '440100', '番禺区', 3, NULL, 1, 1647931271, 1681975595);
INSERT INTO `yunduan_district` VALUES ('49cd8ca807781f6d1bc49dfef614757f', '152531', '152500', '多伦县', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('49d1b68d6ffdfd0d9328d6699004c60c', '410711', '410700', '牧野区', 3, NULL, 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('49d8822cd8cf25e07d742cd24f81bc77', '522625', '522600', '镇远县', 3, NULL, 1, 1647931271, 1681977731);
INSERT INTO `yunduan_district` VALUES ('49f1b79008c72423db2ccbd0fd90e04d', '520323', '520300', '绥阳县', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('49f2e8f9a56713ab54315ca0de146faf', '511181', '511100', '峨眉山市', 3, NULL, 1, 1647931271, 1681977536);
INSERT INTO `yunduan_district` VALUES ('4a139bd1ad4dfd08bc65e8e19c60be2f', '341022', '341000', '休宁县', 3, NULL, 1, 1647931271, 1681975390);
INSERT INTO `yunduan_district` VALUES ('4a28ffc6f62e5fe05525df7f7b87dccd', '440309', '440300', '龙华区', 3, NULL, 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('4a2c47e076bacd44586abc347aa902c3', '411322', '411300', '方城县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('4a3afc41be5284d54e1d31fa140ef6b0', '532322', '532300', '双柏县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('4a4236eb37027134e9e5c0ef78f2a587', '230902', '230900', '新兴区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('4a4dff0b35103260f75ab3c560d83f66', '540326', '540300', '八宿县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('4a5f0a21ad1b65bd37a7e2fcb9e4788e', '341302', '341300', '埇桥区', 3, NULL, 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('4a6b872325f4846955727a3e05c59097', '211282', '211200', '开原市', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('4a96b189655d4e30393ae2075b7c2cd2', '220183', '220100', '德惠市', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('4ab0f7dc526cf7fd72c6ba9bc5e9ea0a', '522622', '522600', '黄平县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('4abfe699342a03e09bc43ff209be434a', '520381', '520300', '赤水市', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('4ae217651c4397e3d61af0ba7291e3bd', '430702', '430700', '武陵区', 3, NULL, 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('4ae3da3e68d12549f2d9bf2e9f0a0d83', '341002', '341000', '屯溪区', 3, NULL, 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('4ae78db6633cae54d34953fef73f4f26', '511825', '511800', '天全县', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('4ae9954e5c47422e3f42de9fe602173e', '610927', '610900', '镇坪县', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('4af3188971a4b8cc06c5796e7f45b18b', '371402', '371400', '德城区', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('4af6bb17f1e23d543fa316de19b17f89', '371327', '371300', '莒南县', 3, NULL, 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('4aff9dcc0648369cc652dffe0d9353ba', '340304', '340300', '禹会区', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('4b0565f96f4666a3373c52089726299a', '320214', '320200', '新吴区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('4b22eaa29d8994814323a4e90c59f06a', '530423', '530400', '通海县', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('4b2698860b30de86bd4319ed46e3c845', '511524', '511500', '长宁县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('4b3c515cc0dd09a54171932a51cc5c9e', '653024', '653000', '乌恰县', 3, NULL, 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('4b5c62308deff9b73b225628a5cf7c41', '620921', '620900', '金塔县', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('4b68b55559e0678304949248d1324752', '131102', '131100', '桃城区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('4b68e17daca16b0777bd43a56c731db1', '330500', '330000', '湖州市', 2, '{\"county\": 5}', 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('4b906940cb22ad0d434bba5c7e17467c', '350723', '350700', '光泽县', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('4baaf95657ed642a5322464adcd07858', '419001', '419000', '济源市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('4bbc63e5ed686521cf8267b39cd34d03', '420923', '420900', '云梦县', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('4bbf6a27cbefb28d9f8b0e2238549a1b', '650204', '650200', '白碱滩区', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('4bd3cf5eef08513118e18cac28780608', '131025', '131000', '大城县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('4bfadb5350b7a3e952abec145d7e78fc', '542522', '542500', '札达县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('4c0507891621be2e96335ef560886cdd', '130728', '130700', '怀安县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('4c0ab167790dd71468d0ae0a74bdb76e', '220112', '220100', '双阳区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('4c10fe10c39a9c34035495e7136a6761', '532625', '532600', '马关县', 3, NULL, 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('4c32a26b52faf1f413926976df820334', '340705', '340700', '铜官区', 3, NULL, 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('4c47c0bfa74a71335cf468b98876225c', '210681', '210600', '东港市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('4c571ba2400ffbab2c12d6b5af69cd8b', '360823', '360800', '峡江县', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('4c6b02449fa9b5328b522860197cddcb', '621226', '621200', '礼县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('4c77272fd92297269e15d42d4e494fe6', '340200', '340000', '芜湖市', 2, '{\"county\": 7}', 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('4c7c29df26d5c24137747d244d19bbf7', '320206', '320200', '惠山区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('4c7c554d80637c19b87906a51adda1ae', '350627', '350600', '南靖县', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('4c82d60059d8b8ae5336f19685162e50', '220622', '220600', '靖宇县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('4ca96c0dd95ad6a07ad28c6f9652e6f0', '340521', '340500', '当涂县', 3, NULL, 1, 1647931271, 1681975391);
INSERT INTO `yunduan_district` VALUES ('4cac379707c3b9e271a5e7d5e1eda535', '220211', '220200', '丰满区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('4cc8adfee908562c35bf4ebe8fba6bcd', '150621', '150600', '达拉特旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('4ccba9e66826180423086f5d423a8476', '510703', '510700', '涪城区', 3, NULL, 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('4ce61a3107a0ea1e3ef9f0289be394c5', '370505', '370500', '垦利区', 3, NULL, 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('4cf45d807eb3165f32e4ed62f2bc2b3b', '150602', '150600', '东胜区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('4cf81942ebf9ff6b6e6f90851c6461df', '210304', '210300', '立山区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('4d010b92c740885696db5b2c2ec8439a', '620521', '620500', '清水县', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('4d23ebcd8747d88f7cf7466222a034b5', '130825', '130800', '隆化县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('4d54de2f07cb4302d4acbf49034a95c3', '511083', '511000', '隆昌市', 3, NULL, 1, 1647931271, 1681977512);
INSERT INTO `yunduan_district` VALUES ('4d5cb7ae7d7e24322ffcec4fb2d82b25', '370703', '370700', '寒亭区', 3, NULL, 1, 1647931271, 1681975487);
INSERT INTO `yunduan_district` VALUES ('4d61617cec2e1f4d9c3618bee69b7ed0', '410726', '410700', '延津县', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('4d7e45feb893ca669c1ea662e729406f', '231123', '231100', '逊克县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('4d8084a0f0185acc0162ed17ee7ff056', '140703', '140700', '太谷区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('4d8a21ee00c65832b460487304dd1636', '140603', '140600', '平鲁区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('4db552d24cb4d0cafadbda830068441c', '410603', '410600', '山城区', 3, NULL, 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('4ddcc001cf41059143968c785c8ad5f6', '520000', '0', '贵州省', 1, '{\"city\": 9, \"county\": 88}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('4de0e20ab4a564d180b61d160304c4b1', '510118', '510100', '新津区', 3, NULL, 1, 1647931271, 1681977533);
INSERT INTO `yunduan_district` VALUES ('4def237f86561c61eb62e3f1f94649a6', '210881', '210800', '盖州市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('4df2069c2123d3f53800ea4c679af149', '650109', '650100', '米东区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('4e0d27410c6a7c742071080c65304add', '350581', '350500', '石狮市', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('4e0e677c9bdd6d87a2f8f564e89284b2', '410422', '410400', '叶县', 3, NULL, 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('4e2551edea824532d5079e911ee11188', '152222', '152200', '科尔沁右翼中旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('4e2a0cbef320699bd8fa8f50758fe02c', '511827', '511800', '宝兴县', 3, NULL, 1, 1647931271, 1681977520);
INSERT INTO `yunduan_district` VALUES ('4e392b8a8e085153d5f1e799242b8a67', '210911', '210900', '细河区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('4e3fff9bfade7752366909f27f0cf488', '522634', '522600', '雷山县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('4e44d4390a3a738e989da75a641dddbf', '371400', '370000', '德州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('4e53180f91b8768177a508c7a5915279', '140225', '140200', '浑源县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('4e5a7c0feb239d40d7f22276985204e4', '330300', '330000', '温州市', 2, '{\"county\": 12}', 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('4e6a6165b29e04c78a04ac574d3302e1', '410781', '410700', '卫辉市', 3, NULL, 1, 1647931271, 1681975519);
INSERT INTO `yunduan_district` VALUES ('4e8ca566449e1cdf3638535010f5f298', '130802', '130800', '双桥区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('4e8f0ea257e1b3642d7cf0881f4359ee', '131122', '131100', '武邑县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('4ebfe5a0c15515020387af6bab9fee72', '654023', '654000', '霍城县', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('4edf51446c19e7edf9e8170f364a167d', '652828', '652800', '和硕县', 3, NULL, 1, 1647931271, 1681978501);
INSERT INTO `yunduan_district` VALUES ('4ee68a1638a288cd3cf855a3fd64b652', '440803', '440800', '霞山区', 3, NULL, 1, 1647931271, 1681975591);
INSERT INTO `yunduan_district` VALUES ('4f0686801db5356c24ca48ec70cb3771', '141029', '141000', '乡宁县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('4f280df85b468ed49ca6268fe374387b', '430781', '430700', '津市市', 3, NULL, 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('4f2f5b3f1f75122b1acf1edbc21d10f6', '130983', '130900', '黄骅市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('4f4aa3b893c2db56bf32f04819a48fc6', '230505', '230500', '四方台区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('4f509bed34f0c897e9dc4b491ba44a6c', '420300', '420000', '十堰市', 2, '{\"county\": 8}', 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('4f520906998a1939c767545a7185c5ce', '210000', '0', '辽宁省', 1, '{\"city\": 14, \"county\": 100}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('4f522bb61bd4c2ec5e7a79070e62ec98', '222403', '222400', '敦化市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('4f5346f8b381946678bed9ae3127665a', '150821', '150800', '五原县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('4f55d39a8442a156d3f72787d327fb3e', '610500', '610000', '渭南市', 2, '{\"county\": 11}', 1, 1647931271, 1681978077);
INSERT INTO `yunduan_district` VALUES ('4f580e4b6abb731420a9bba78c5afdf8', '510503', '510500', '纳溪区', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('4f6c46d6a678ac07e4aa4c4b2894fad8', '150784', '150700', '额尔古纳市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('4f6feeb0daddcfa11476bfb5cb534f08', '510923', '510900', '大英县', 3, NULL, 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('4fa9f3bc7d6551caf4a7bb95f754f1c2', '211004', '211000', '宏伟区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('4fc543d1a53cfe75dd4375b8e66ed547', '623025', '623000', '玛曲县', 3, NULL, 1, 1647931271, 1681978244);
INSERT INTO `yunduan_district` VALUES ('4fcecf85fa98ffe208360157a08d6f67', '210804', '210800', '鲅鱼圈区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('4ff41feaceff4686d04ccedf5e61f185', '350403', '350400', '三元区', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('4ff827f4461382778d5fe2f4a4f48758', '440400', '440000', '珠海市', 2, '{\"county\": 3}', 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('4fffca992a5e622b8b8b2902b9bc664c', '610203', '610200', '印台区', 3, NULL, 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('5010ed8113fbbdae8538d0baa9f49e86', '420106', '420100', '武昌区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('501a204d3a6b1a55a56ca6a842e7f13e', '513437', '513400', '雷波县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('5035d21b34bd2fad77ea9f2e65b867f9', '150205', '150200', '石拐区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('5041c5b8bb3c835f15ec532654f9e0b2', '411481', '411400', '永城市', 3, NULL, 1, 1647931271, 1681975504);
INSERT INTO `yunduan_district` VALUES ('5044fa111bcb8e06c24ec10267aef800', '150222', '150200', '固阳县', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('505eb3752618bcb84809c41f4fb8c2d8', '130628', '130600', '高阳县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('5075515e34ef70f8b33375c2a81c75b1', '620881', '620800', '华亭市', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('50766318cca7b6080933b4295f0b5107', '130126', '130100', '灵寿县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('50c8af2fa76be31c4e3f48a76ff2be04', '659002', '659000', '阿拉尔市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('5100a36bb0a4457db2b10a3bc9221a1d', '610581', '610500', '韩城市', 3, NULL, 1, 1647931271, 1681978078);
INSERT INTO `yunduan_district` VALUES ('51092297c48aed5974a48123fbe81c58', '511622', '511600', '武胜县', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('510c97dce11b5031c9622e5236297b32', '140400', '140000', '长治市', 2, '{\"county\": 12}', 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('511ede685317853c835024ddb21a9b47', '451227', '451200', '巴马瑶族自治县', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('517a4ba9c0be0db2f15abe4dfc0c80c5', '370811', '370800', '任城区', 3, NULL, 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('51b3b1ae16ffbd14bafd2bf951d2e1a0', '341700', '340000', '池州市', 2, '{\"county\": 4}', 1, 1647931271, 1681975388);
INSERT INTO `yunduan_district` VALUES ('51cb6cea826ec0bce1f47479d0a358e7', '341100', '340000', '滁州市', 2, '{\"county\": 8}', 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('51d2f1f793be7606143c3b8637d62a09', '500110', '500100', '綦江区', 3, NULL, 1, 1647931271, 1681977450);
INSERT INTO `yunduan_district` VALUES ('51d31fc4deb8df2b95c066b669c443b3', '370500', '370000', '东营市', 2, '{\"county\": 5}', 1, 1647931271, 1681975493);
INSERT INTO `yunduan_district` VALUES ('51db1aab6baa323399805b51484f7545', '230605', '230600', '红岗区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('51dd0cb6f1ed00d4626cc3a9cdd362dc', '630223', '630200', '互助土族自治县', 3, NULL, 1, 1647931271, 1681978315);
INSERT INTO `yunduan_district` VALUES ('51f7a10690a20ce0840e1f0049593ebf', '411721', '411700', '西平县', 3, NULL, 1, 1647931271, 1681975522);
INSERT INTO `yunduan_district` VALUES ('52021a5c457d165bb14ffe4903d7fc48', '321003', '321000', '邗江区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('52096e27b4df5d074e365a88905f9189', '440403', '440400', '斗门区', 3, NULL, 1, 1647931271, 1681975604);
INSERT INTO `yunduan_district` VALUES ('5224a012a5f4d16d00eb2e494ca09fba', '610113', '610100', '雁塔区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('52251128884cc2b08854df01b7742101', '510131', '510100', '蒲江县', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('5231e65dc1a1c7a6334dd435b5f469b2', '210521', '210500', '本溪满族自治县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('5241d121963a4554d158c7cf9a03cdfd', '440111', '440100', '白云区', 3, NULL, 1, 1647931271, 1681975597);
INSERT INTO `yunduan_district` VALUES ('527c34ab38959eb850ae309aa4b7a475', '610921', '610900', '汉阴县', 3, NULL, 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('5285ca4028e8e55a9f1f7ecd78beeae8', '341800', '340000', '宣城市', 2, '{\"county\": 7}', 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('528bf603e030c0d903536246afa51b73', '640181', '640100', '灵武市', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('529013cd5548a717dca4de110c1fd235', '522323', '522300', '普安县', 3, NULL, 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('5290fbac1899c3392d2d770da12b1c93', '445322', '445300', '郁南县', 3, NULL, 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('52a396c1982a319f24e15238618482b7', '360703', '360700', '南康区', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('52d0dfeceb6e38ccc0e522d45f03d91b', '450405', '450400', '长洲区', 3, NULL, 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('52df71ac72593deca9029c6a480bfd40', '320000', '0', '江苏省', 1, '{\"city\": 13, \"county\": 96}', 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('52f919a7e975c29a6e00f12dbb98c333', '510704', '510700', '游仙区', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('534dacefae51d73e020eb64810c7bfe6', '370481', '370400', '滕州市', 3, NULL, 1, 1647931271, 1681975497);
INSERT INTO `yunduan_district` VALUES ('537001f3d0af24680568ad937f91e146', '130629', '130600', '容城县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('53746e7e329f1597b7d198226f2468df', '653127', '653100', '麦盖提县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('538066f7a2b2e67edfec59dac916b4a9', '410622', '410600', '淇县', 3, NULL, 1, 1647931271, 1681975500);
INSERT INTO `yunduan_district` VALUES ('53a64c7db55cc06920cd66845f2819ff', '371102', '371100', '东港区', 3, NULL, 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('53a8880ac85f0e7ed59023312159fed5', '520322', '520300', '桐梓县', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('53c65da5aaaeb789de0e574f17a58731', '540127', '540100', '墨竹工卡县', 3, NULL, 1, 1647931271, 1681977991);
INSERT INTO `yunduan_district` VALUES ('53d5acc4a08a84bf07cd356781d00775', '433123', '433100', '凤凰县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('53df2a0869498af0fc072484d58be78e', '540525', '540500', '曲松县', 3, NULL, 1, 1647931271, 1681977996);
INSERT INTO `yunduan_district` VALUES ('53e434e1d33fa12fe7a24392fa81cfff', '350421', '350400', '明溪县', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('53f6ad53d14fafa558656a680a7e76fe', '469001', '469000', '五指山市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('54073b6efbb159f5a36246ae7ed705a2', '371311', '371300', '罗庄区', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('540a2bebe83543b0ea0085ef70a51021', '152202', '152200', '阿尔山市', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('540ca4e9cdf1b1442055cbb93db38cd0', '510100', '510000', '成都市', 2, '{\"county\": 20}', 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('542f6f4a59e1e81c51a78208af8cde60', '653128', '653100', '岳普湖县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('543aa00335b8d30cafca6b34a0a1b72b', '220781', '220700', '扶余市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('544c94a8a5fd9406737a0c9a02852159', '450512', '450500', '铁山港区', 3, NULL, 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('544fcb81db1f725c6539265bd381617d', '330106', '330100', '西湖区', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('5457cceadff0986bb2688b164f63c6af', '511421', '511400', '仁寿县', 3, NULL, 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('545b5ea38fdbc76b0470230084106b9a', '220184', '220100', '公主岭市', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('5492b5e66bda844fc79cdcf8d779f097', '610115', '610100', '临潼区', 3, NULL, 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('54ac6be65877a2d7186aa0184c5b2d79', '320213', '320200', '梁溪区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('54b6e63e6b80e31feb471b918711a700', '513433', '513400', '冕宁县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('54ba2b7383eabb1a587f84f91e8c89b2', '330182', '330100', '建德市', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('54ca96e3dc25a8110df12f7805dbd145', '530113', '530100', '东川区', 3, NULL, 1, 1647931271, 1681977892);
INSERT INTO `yunduan_district` VALUES ('54e521a727e540396100ce7945f8b697', '131002', '131000', '安次区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('54ec9c86d1d60c79a248be863e7037bd', '620900', '620000', '酒泉市', 2, '{\"county\": 7}', 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('54f0e71da877f3a2a088ff7d140b883d', '430421', '430400', '衡阳县', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('55102add2b09c0777204c2ae03f481c5', '610929', '610900', '白河县', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('55258baff10111cbb83212c3c25144ca', '210200', '210000', '大连市', 2, '{\"county\": 10}', 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('55356c52c05ee1c92951ff46f3f713b8', '330522', '330500', '长兴县', 3, NULL, 1, 1647931271, 1681975347);
INSERT INTO `yunduan_district` VALUES ('557379a33f132b2d115271e07bc671ee', '530326', '530300', '会泽县', 3, NULL, 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('558783d5f524a5b31ee961e9ea938f58', '350300', '350000', '莆田市', 2, '{\"county\": 5}', 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('55fee07ce20ee6cfba72b0b5ebecdecc', '440881', '440800', '廉江市', 3, NULL, 1, 1647931271, 1681975592);
INSERT INTO `yunduan_district` VALUES ('561eb60b88b1d73522540462435e34c3', '360830', '360800', '永新县', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('56271ead7ed23498b2f8f05c58031337', '210100', '210000', '沈阳市', 2, '{\"county\": 13}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('564497fe33ad22ce22d0031a6624f4ca', '650203', '650200', '克拉玛依区', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('5659a74d8e1af9b35bb8ebf5328567c2', '510116', '510100', '双流区', 3, NULL, 1, 1647931271, 1681977533);
INSERT INTO `yunduan_district` VALUES ('566f040a433241812cccfbfa64d8b8e9', '320923', '320900', '阜宁县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('567a81976b417348f5751e00c8b10b17', '350212', '350200', '同安区', 3, NULL, 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('56baaf2f2ad64d5772be3f74991f0c68', '520628', '520600', '松桃苗族自治县', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('56db2285a54596426cc7ac3d98cd2b2c', '140123', '140100', '娄烦县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('56dfd6dfbc8cdad1417545884365fdfb', '632224', '632200', '刚察县', 3, NULL, 1, 1647931271, 1681978320);
INSERT INTO `yunduan_district` VALUES ('57196c979ad245a58f325c7253f3bc89', '513430', '513400', '金阳县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('57198249556a6206983b50b45a3450be', '513431', '513400', '昭觉县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('571ae45fa16a1195ca1eedc36d0f59fd', '152502', '152500', '锡林浩特市', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('571fd0e157a8cef699a8d0e0fd80fe79', '460100', '460000', '海口市', 2, '{\"county\": 4}', 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('5765c2425ec308895d8a134b5929abc4', '445103', '445100', '潮安区', 3, NULL, 1, 1647931271, 1681975576);
INSERT INTO `yunduan_district` VALUES ('57720b0150d21f6d3a0e9ba37c2c02aa', '620525', '620500', '张家川回族自治县', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('577a3e67bbf80f86e6f9e8d7d522ecae', '120117', '120100', '宁河区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('57b80535732f199357015f489a45127f', '150927', '150900', '察哈尔右翼中旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('57c6f3653c464468ce527e254c676e49', '230203', '230200', '建华区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('57de84a58c1ebaf039ea9049ddade575', '110112', '110100', '通州区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('57e454a8b1a19909309a2a3cc64ff154', '220723', '220700', '乾安县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('580d8695ddf8e730d1989a726da239e6', '361129', '361100', '万年县', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('581ae264177565b47f0ee0d57f3d7e8f', '430722', '430700', '汉寿县', 3, NULL, 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('583e8c9550483f0870b022fd7e90bf59', '211322', '211300', '建平县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('5842f1ead1f8384e8786b67fddb45fa9', '440232', '440200', '乳源瑶族自治县', 3, NULL, 1, 1647931271, 1681975576);
INSERT INTO `yunduan_district` VALUES ('5844885c9899d370dec54204205665e1', '150202', '150200', '东河区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('5847effab36c27185c2c5d41782aa402', '530822', '530800', '墨江哈尼族自治县', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('58506486cc2da266a6f0ff0956e31f94', '350622', '350600', '云霄县', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('5854a6a381ae8a0c54c60560a1bee1a8', '361104', '361100', '广信区', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('586dbbf21b2c3136729c4d2065dea164', '130724', '130700', '沽源县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('5881f413bb8583f617d857bde0c16754', '610525', '610500', '澄城县', 3, NULL, 1, 1647931271, 1681978077);
INSERT INTO `yunduan_district` VALUES ('58847b16dc27ebc7688e0472304b59eb', '500117', '500100', '合川区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('58c4f1d6be85dde2566a96f7ee5e1d1f', '450109', '450100', '邕宁区', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('58eb3e5105a0c8ba6ff91b3e98207302', '360783', '360700', '龙南市', 3, NULL, 1, 1647931271, 1681975443);
INSERT INTO `yunduan_district` VALUES ('5933280b40270fc533ef601d2d2db984', '640100', '640000', '银川市', 2, '{\"county\": 6}', 1, 1647931271, 1681978322);
INSERT INTO `yunduan_district` VALUES ('5942bbe64ca8758803be18049b381724', '350583', '350500', '南安市', 3, NULL, 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('5951cef5021a38610876bac23c728ba6', '500113', '500100', '巴南区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('596fba5d6d25fc1d330216522b2cb2ab', '130725', '130700', '尚义县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('59728cea4c6e2dd3d8abd124884aa8d7', '130800', '130000', '承德市', 2, '{\"county\": 11}', 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('5994c4621652f2e67e740ed0a663e0d3', '659004', '659000', '五家渠市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('59ac992c4cea6c9e81cf831e4a2ab6e9', '640104', '640100', '兴庆区', 3, NULL, 1, 1647931271, 1681978322);
INSERT INTO `yunduan_district` VALUES ('59ade304b6c6427ab93ca8f42a47d111', '331122', '331100', '缙云县', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('59af0a1c3e1ed92affe4cfe72284d50d', '451028', '451000', '乐业县', 3, NULL, 1, 1647931271, 1681975628);
INSERT INTO `yunduan_district` VALUES ('59bc3926e39632f61818cf3441bcec3e', '231183', '231100', '嫩江市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('59c1682f03e7cf7a91c13985d63d3ae2', '431221', '431200', '中方县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('5a07351daf53f0ac58e4c15572d9c1f0', '440883', '440800', '吴川市', 3, NULL, 1, 1647931271, 1681975592);
INSERT INTO `yunduan_district` VALUES ('5a0ba493ff5bd6905a907264da9766bb', '510723', '510700', '盐亭县', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('5a104f3ef9ab8a2547ff79d13cabac59', '150425', '150400', '克什克腾旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('5a2c6c0f31e0f68f958c8c19f76c1772', '533323', '533300', '福贡县', 3, NULL, 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('5a36e5d23e34552121a4eee84437845a', '430900', '430000', '益阳市', 2, '{\"county\": 6}', 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('5a48108d72887ea8c0a6a0e4172e45a3', '511324', '511300', '仪陇县', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('5a9a2bdca5e5815515397650c207019e', '441900', '440000', '东莞市', 2, NULL, 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('5abe4492ae0250966685bb5017232ba3', '230521', '230500', '集贤县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('5abee749764708e357a0520afc100aea', '450225', '450200', '融水苗族自治县', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('5ac4ad3517a2789d54b9dae7cde5e20f', '440204', '440200', '浈江区', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('5ad5d516fea59a1126cecd26a9d87be4', '620800', '620000', '平凉市', 2, '{\"county\": 7}', 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('5ae28e85673d87d4836a9246fb9eab9f', '420902', '420900', '孝南区', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('5ae84f5ed5be3a4de513709a58c951b9', '500112', '500100', '渝北区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('5b14d8127798faab980c2cd9f71e587a', '330681', '330600', '诸暨市', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('5b624163f1fcb7bf3a0f6139ce14e315', '110107', '110100', '石景山区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('5b76a9d9c168f79e5e567b03c65886ea', '610526', '610500', '蒲城县', 3, NULL, 1, 1647931271, 1681978078);
INSERT INTO `yunduan_district` VALUES ('5b7b2ccd327e11bc0eab61b56aca0504', '411321', '411300', '南召县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('5ba86d3abdd2fb1f129f40a97b98ba39', '320582', '320500', '张家港市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('5bd53c3ce603b139004ad711079d376e', '420103', '420100', '江汉区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('5c1cd96f1a902696a96276a815202641', '652922', '652900', '温宿县', 3, NULL, 1, 1647931271, 1681978494);
INSERT INTO `yunduan_district` VALUES ('5c5987390e2dd066a7fa2711f2179205', '411402', '411400', '梁园区', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('5c7970aa7da63d50fef924daafb786ff', '610622', '610600', '延川县', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('5c84580f00959e275d52bb7b963ff2af', '653131', '653100', '塔什库尔干塔吉克自治县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('5c9a3094ba0539b0bb6398c26e5290b2', '130581', '130500', '南宫市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('5c9e523630be9cae92d8b9ad95e9c5cc', '610625', '610600', '志丹县', 3, NULL, 1, 1647931271, 1681978081);
INSERT INTO `yunduan_district` VALUES ('5cb60219f0bb8f15f813cfb415ecfbff', '530100', '530000', '昆明市', 2, '{\"county\": 14}', 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('5ce133ea6ffd6d761b4f13cb3c8c805b', '610000', '0', '陕西省', 1, '{\"city\": 10, \"county\": 107}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('5ce50f496d72b75227c14407d916bcb1', '230404', '230400', '南山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('5d0fdf873c80dedc64e6024eddf59e21', '140882', '140800', '河津市', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('5d1150b1d8d024ce759c9a26a9e5bda1', '620103', '620100', '七里河区', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('5d157314a5ec745c83be82f296aa9db6', '130434', '130400', '魏县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('5d254e077386d541c16d3606d366652f', '370117', '370100', '钢城区', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('5d2e25e8bab92477fe9615d35e92d6d1', '431124', '431100', '道县', 3, NULL, 1, 1647931271, 1681975570);
INSERT INTO `yunduan_district` VALUES ('5d5274f6ce509848a9e96d5795ae0715', '610402', '610400', '秦都区', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('5d60f61a6152d1dd688b7964fcdf6665', '331181', '331100', '龙泉市', 3, NULL, 1, 1647931271, 1681975341);
INSERT INTO `yunduan_district` VALUES ('5d786f756ebadfd5e59141e495d59296', '371323', '371300', '沂水县', 3, NULL, 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('5d8f136f50ff4bc05e3d09123fda4181', '532600', '530000', '文山壮族苗族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('5d9315e337d5563867a2013d12d19164', '340122', '340100', '肥东县', 3, NULL, 1, 1647931271, 1681975385);
INSERT INTO `yunduan_district` VALUES ('5d95163e158a6d833204026df601e704', '411381', '411300', '邓州市', 3, NULL, 1, 1647931271, 1681975520);
INSERT INTO `yunduan_district` VALUES ('5da4320de5831439a6d655bba2a6fd46', '350205', '350200', '海沧区', 3, NULL, 1, 1647931271, 1681975405);
INSERT INTO `yunduan_district` VALUES ('5dc178c6e7e51b23b9262800486c666d', '370705', '370700', '奎文区', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('5dc334c947ea757f79b9529fca5a0e93', '220104', '220100', '朝阳区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('5dd15368056bc157a53e407f88f77d57', '211400', '210000', '葫芦岛市', 2, '{\"county\": 6}', 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('5dd2d4d5d1abfc400b90d1677f127403', '433122', '433100', '泸溪县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('5e0204d49ac1e4b20fcce8747b894c1a', '431224', '431200', '溆浦县', 3, NULL, 1, 1647931271, 1681975570);
INSERT INTO `yunduan_district` VALUES ('5e20ef04f9602230aa418cb9ff678c58', '370402', '370400', '市中区', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('5e30fbcaed3c0ad2fe2769685744cfdb', '330825', '330800', '龙游县', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('5e32a440491776db6f1be4ae10898e6e', '533123', '533100', '盈江县', 3, NULL, 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('5e3d9e15a0e95f84efe4c6587c41841a', '532626', '532600', '丘北县', 3, NULL, 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('5e4b26b9adb4bcc0307bce7eb4604315', '610117', '610100', '高陵区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('5e6f57db85ec6bac2e3a3696b7ffe1c2', '350304', '350300', '荔城区', 3, NULL, 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('5e868096b45fe813587fa707fb1436ad', '632726', '632700', '曲麻莱县', 3, NULL, 1, 1647931271, 1681978311);
INSERT INTO `yunduan_district` VALUES ('5e8a4c9a39e060350fda8327d0c8b71d', '220502', '220500', '东昌区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('5e8e4f4a02c6f0d78d402acf039b6a10', '431026', '431000', '汝城县', 3, NULL, 1, 1647931271, 1681975554);
INSERT INTO `yunduan_district` VALUES ('5e93a1feb4969bafb545bbf9782bf430', '430700', '430000', '常德市', 2, '{\"county\": 9}', 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('5eaf17cce109f1602c42a181add7adb7', '411526', '411500', '潢川县', 3, NULL, 1, 1647931271, 1681975529);
INSERT INTO `yunduan_district` VALUES ('5ed4b006100fa94e27ba8274762ad5c2', '450326', '450300', '永福县', 3, NULL, 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('5f194718ae3181043d27a8c3a0c06e03', '441226', '441200', '德庆县', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('5f2c5c58587489b97ba6a119143596bc', '141127', '141100', '岚县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('5f34b7924954f4b0635ca68776b8b97f', '450621', '450600', '上思县', 3, NULL, 1, 1647931271, 1681975619);
INSERT INTO `yunduan_district` VALUES ('5f3af5fd5ff374250bfbd045c656a365', '231025', '231000', '林口县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('5f3dddc4ec6e33b66cd19c6548f5a747', '513223', '513200', '茂县', 3, NULL, 1, 1647931271, 1681977526);
INSERT INTO `yunduan_district` VALUES ('5f564b74271fb0a233fd4a157cbb1252', '150200', '150000', '包头市', 2, '{\"county\": 9}', 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('5f58d2f491b0efb62ca5e33a2fffa704', '640423', '640400', '隆德县', 3, NULL, 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('5f6b03aa20bf6bec1ca017b34d319b03', '131100', '130000', '衡水市', 2, '{\"county\": 11}', 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('5fa19aca68dce206890732fff7d7cc72', '510184', '510100', '崇州市', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('5fa2116142c61ab20ed8c5dfdb60d164', '610424', '610400', '乾县', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('5fc97836cee9656586fd9d33eb2a38c3', '520326', '520300', '务川仡佬族苗族自治县', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('5fcdcff76c420a7ed3c86935cef1d17c', '220600', '220000', '白山市', 2, '{\"county\": 6}', 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('5fdf8ae61556a8ebf2bb83e4ca7f5ef6', '430800', '430000', '张家界市', 2, '{\"county\": 4}', 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('5fea94368f3ff49039c7d221543e450d', '220100', '220000', '长春市', 2, '{\"county\": 11}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('5ff79afc092372ea9ab397431678cc7e', '410000', '0', '河南省', 1, '{\"city\": 17, \"county\": 158}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('600ad09976b0e5590ef6f13956c5362c', '650102', '650100', '天山区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('602117e61e95fd6dd066010e15839b1c', '500152', '500100', '潼南区', 3, NULL, 1, 1647931271, 1681977450);
INSERT INTO `yunduan_district` VALUES ('6028d1bd49a671a8c981be98abca2b79', '130433', '130400', '馆陶县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('603ec0a70b33071e423f484a17f49110', '610925', '610900', '岚皋县', 3, NULL, 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('6051f293aadeab147adae8d042a688ab', '450325', '450300', '兴安县', 3, NULL, 1, 1647931271, 1681975631);
INSERT INTO `yunduan_district` VALUES ('60547b1c45b3bd440d7b11f21b35514e', '451029', '451000', '田林县', 3, NULL, 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('605c3bcccc0a9cd9b4f6b5a6c36fe853', '371621', '371600', '惠民县', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('606d124fbf9b69ed77e567960839ca90', '330803', '330800', '衢江区', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('607aaf52445075df8fc9d7ff0f3cfe3c', '350623', '350600', '漳浦县', 3, NULL, 1, 1647931271, 1681975411);
INSERT INTO `yunduan_district` VALUES ('607bf71b2bedfb48d412b6d9fd7268ba', '620104', '620100', '西固区', 3, NULL, 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('608feb35fe04187521068742163e57c5', '430623', '430600', '华容县', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('6093c79314c2971aa1645bbf96842191', '532932', '532900', '鹤庆县', 3, NULL, 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('60ac71631e82e4ab514c2063f172d275', '611024', '611000', '山阳县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('60cef300a07b5fd028ed82f4adda3c6a', '370214', '370200', '城阳区', 3, NULL, 1, 1647931271, 1681975483);
INSERT INTO `yunduan_district` VALUES ('60d21ea78df2737808b401d782279f27', '530900', '530000', '临沧市', 2, '{\"county\": 8}', 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('60f480c967ddd48f8acab48f2ec15005', '653121', '653100', '疏附县', 3, NULL, 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('6100e19eb7add1bca1128ab6cda968d7', '120114', '120100', '武清区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('610ff0e6165491511bb6aa73e6ff23db', '230406', '230400', '东山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('6110fb2675dcfcbc6ec229d5b53bbc62', '451400', '450000', '崇左市', 2, '{\"county\": 7}', 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('61140791db8b25c7788edb18aa03ddfe', '150823', '150800', '乌拉特前旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('61186d75c554957bf7d3257184305e6e', '441600', '440000', '河源市', 2, '{\"county\": 6}', 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('6134f628c73451c81c190a957fd0cad0', '520523', '520500', '金沙县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('613a547d8f82ea17213e798fb17adc7f', '360521', '360500', '分宜县', 3, NULL, 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('6173c3bae7ab74fd1dda40c8ba861547', '370830', '370800', '汶上县', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('6189b35f33a5cf9a44df26f5f636dc80', '511325', '511300', '西充县', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('618b80629d5ebd10e83c637926a29cec', '371600', '370000', '滨州市', 2, '{\"county\": 7}', 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('618ee72f85e648d62ec7caa20d16853a', '469028', '469000', '陵水黎族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('61be5d7955c68c47d8123dc2cb4a2fb6', '340123', '340100', '肥西县', 3, NULL, 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('61be8b0452b4f873c7607c499951838d', '320583', '320500', '昆山市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('61ca915e47f3a79d5355aa2a797b0343', '450881', '450800', '桂平市', 3, NULL, 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('61ccddce92ceee6feacbaedfcf1be7dc', '513423', '513400', '盐源县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('6218d180c537eccb31a256021a9d8ba3', '150426', '150400', '翁牛特旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('623183e913e8a26f0606c8d000b43627', '630121', '630100', '大通回族土族自治县', 3, NULL, 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('6235b1f513c95e1a126178a76f1996e3', '340202', '340200', '镜湖区', 3, NULL, 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('628c380f54e07f53caf7d95314c35ede', '440785', '440700', '恩平市', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('62b032ad1521b98c4e8bf1c0f9426323', '350322', '350300', '仙游县', 3, NULL, 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('62b36fc74ce05f5d572be9cdd0c26771', '540531', '540500', '浪卡子县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('62b55a4d5bc798e614142a25eebc5253', '620823', '620800', '崇信县', 3, NULL, 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('62d2b4fd15504dcebc7e168ee0b33f4e', '220200', '220000', '吉林市', 2, '{\"county\": 9}', 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('631394f3cf915d4d38ba53d3448b5038', '431024', '431000', '嘉禾县', 3, NULL, 1, 1647931271, 1681975554);
INSERT INTO `yunduan_district` VALUES ('631f8291723cac222a15f852040ac7a4', '422801', '422800', '恩施市', 3, NULL, 1, 1647931271, 1681975540);
INSERT INTO `yunduan_district` VALUES ('634ce089f213b70dabac7122e5b68f6e', '210500', '210000', '本溪市', 2, '{\"county\": 6}', 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('6353825fa8dfaab498e00af5f659931e', '210624', '210600', '宽甸满族自治县', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('6365fb40fe1a943d5b72284c708d1289', '350524', '350500', '安溪县', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('636d3d21edf696376eaf55197caa7809', '469022', '469000', '屯昌县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('636ebe11ab83de47d2dd5b202525ecdb', '410400', '410000', '平顶山市', 2, '{\"county\": 10}', 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('63719be4f606facc38294e3e31ff3626', '510185', '510100', '简阳市', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('63725b5ad508c4e5cb4d9db6a7668fbf', '350629', '350600', '华安县', 3, NULL, 1, 1647931271, 1681975411);
INSERT INTO `yunduan_district` VALUES ('637ea0572347114898339e0e0074dcda', '150526', '150500', '扎鲁特旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('638e148b93b7d026936311955d3a7068', '430902', '430900', '资阳区', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('639ff654e1a625dafd434fc2c295f2b5', '370831', '370800', '泗水县', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('63b41918ea192afcedccc25cb7ecc109', '610503', '610500', '华州区', 3, NULL, 1, 1647931271, 1681978077);
INSERT INTO `yunduan_district` VALUES ('63b748f8209def58d75aecb10c79f17a', '610522', '610500', '潼关县', 3, NULL, 1, 1647931271, 1681978078);
INSERT INTO `yunduan_district` VALUES ('63c87f0ba00136fb56444a1b67254ead', '320830', '320800', '盱眙县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('63ca5e3423d917db48eeeb3757bc3011', '150721', '150700', '阿荣旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('63d5fbaad98569ddcf764f7eeea19c88', '431382', '431300', '涟源市', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('63dc71856911525cbaf855847a82da51', '141002', '141000', '尧都区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('63f9e2c21879b6891e064cddba572e24', '620723', '620700', '临泽县', 3, NULL, 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('640209c7c9ed9ac34ada509418461b2c', '371728', '371700', '东明县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('642dd7e85cb0728b8e052c0638d14e18', '654002', '654000', '伊宁市', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('6441dfa5bc9d84f8ccc7de86300a6f79', '410306', '410300', '吉利区', 3, NULL, 1, 1647931271, 1681975502);
INSERT INTO `yunduan_district` VALUES ('64425fe210faaebce6d274a3b639bbf3', '230302', '230300', '鸡冠区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('645ac30257284370e84fdfc3a400e5d2', '210502', '210500', '平山区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('64ffc714df12c7e0d4de4456de056aa6', '350982', '350900', '福鼎市', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('6516c895a6bbecccda344b6c019253f0', '440203', '440200', '武江区', 3, NULL, 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('6541350d6b8fe7352272814eda5b9331', '530129', '530100', '寻甸回族彝族自治县', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('6573802e440ebca3964650a7e3a02069', '411422', '411400', '睢县', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('6589377a2e28443b6b1980faf01ecd71', '650103', '650100', '沙依巴克区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('65930dc6eb3537face60772c81b1f855', '441622', '441600', '龙川县', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('6598b3942698e10a11524482228e34d1', '532528', '532500', '元阳县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('65bc94bc07cc5a249366a2de424ad093', '540425', '540400', '察隅县', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('65c175f5b2a99eeea9528d8126fb59d1', '152922', '152900', '阿拉善右旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('65d84fb442c30f028eca3d77f44dd173', '440205', '440200', '曲江区', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('65e7933cb746b295943b519e3a62e6a4', '620422', '620400', '会宁县', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('65ef2390b82b470a3efc6a3614af13a3', '540322', '540300', '贡觉县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('6607937d8019a40fd82b51ecf903c4a6', '520626', '520600', '德江县', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('660d19f70219842124f4b9782cd3fdc8', '341802', '341800', '宣州区', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('6616c9412941c477d8eb9c06b32f8dcf', '500116', '500100', '江津区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('662391b847dbdb9b60c30f03e978b893', '230522', '230500', '友谊县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('663f81032c85b684ed5e449a47d008d4', '650400', '650000', '吐鲁番市', 2, '{\"county\": 3}', 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('66736c1cfc5dd515077d13cf35947a76', '441523', '441500', '陆河县', 3, NULL, 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('6678df2643487bef2f83435d966bb6b3', '371082', '371000', '荣成市', 3, NULL, 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('668ecf3814f56624114c4468e2ea7262', '430529', '430500', '城步苗族自治县', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('6693229407cd2376f15af4ee7fdcfe0b', '621024', '621000', '合水县', 3, NULL, 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('669b213047dd687e3e3631ae345af50b', '130683', '130600', '安国市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('66a9d355b2dbfc79fec3f47a5315a99b', '130432', '130400', '广平县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('66dcf91c1c1ac26a4a7bb95518ade36d', '450803', '450800', '港南区', 3, NULL, 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('66e38e23a50aa8c71645ae04d994ab02', '410527', '410500', '内黄县', 3, NULL, 1, 1647931271, 1681975510);
INSERT INTO `yunduan_district` VALUES ('66ee26741047f8d0e7ae7aeb510a1d94', '530721', '530700', '玉龙纳西族自治县', 3, NULL, 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('66f02b2f4303778848c1ec77bc70c88b', '360922', '360900', '万载县', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('66f07114055c8efcda301525b71acb23', '450321', '450300', '阳朔县', 3, NULL, 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('66f4fc62bced06c2d0f42a4913cebd20', '530923', '530900', '永德县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('672cc72a66658e40d99b9487213010ad', '141124', '141100', '临县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('673f5ed4204c931884ec3a512af0b4e9', '131103', '131100', '冀州区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('674212d8738158cb8a6cd0c299f82e7e', '630100', '630000', '西宁市', 2, '{\"county\": 7}', 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('676c035dbd76b6a1ccd2766a2d13c027', '411200', '410000', '三门峡市', 2, '{\"county\": 6}', 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('6770aaece9147a2f5a53150f4025fb18', '650107', '650100', '达坂城区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('678b4a2c2009da4ab59f6f1fc3c1e476', '410804', '410800', '马村区', 3, NULL, 1, 1647931271, 1681975515);
INSERT INTO `yunduan_district` VALUES ('678dc21448dfccd5287f7588fc4e4eb7', '320413', '320400', '金坛区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('6796c714bf51a5ea326beb8fa89e66eb', '430422', '430400', '衡南县', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('67cd4cf4a1332685863f7ea524804600', '230723', '230700', '汤旺县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('67d6eea918c77aef352a4453429a5577', '140302', '140300', '城区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('67d9a1d4c766f670af0ee098b2483aff', '533124', '533100', '陇川县', 3, NULL, 1, 1647931271, 1681977876);
INSERT INTO `yunduan_district` VALUES ('67deffdc8c01fcb76d310e9309e4ecb7', '361002', '361000', '临川区', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('68034398c23ac13c4f5831f11482c96a', '152530', '152500', '正蓝旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('680db5ac693528497b26255e55f205ce', '654300', '650000', '阿勒泰地区', 2, '{\"county\": 7}', 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('68324d17cfe3460f9fa1666dd8342eee', '350400', '350000', '三明市', 2, '{\"county\": 12}', 1, 1647931271, 1681975419);
INSERT INTO `yunduan_district` VALUES ('6847bb58e9c92b071eb78c686cc497f2', '632322', '632300', '尖扎县', 3, NULL, 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('684a40fe8072c4316f8d25507ffe2672', '420704', '420700', '鄂城区', 3, NULL, 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('6859090a4018b9b8dc9db9b1f5191955', '410223', '410200', '尉氏县', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('685c4580e2b1089dc7b8feaa452325c7', '140622', '140600', '应县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('68617b62e5e43f78d6db664a61566a23', '211281', '211200', '调兵山市', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('6869df65ab7ad540c5b020b0680d7de1', '530628', '530600', '彝良县', 3, NULL, 1, 1647931271, 1681977886);
INSERT INTO `yunduan_district` VALUES ('686d63d447cbd26782cf79d6e4a0543d', '441500', '440000', '汕尾市', 2, '{\"county\": 4}', 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('687f80fa69077ca9dd057dd1800d2102', '810000', '0', '香港特别行政区', 1, NULL, 1, 1647931271, 1661303932);
INSERT INTO `yunduan_district` VALUES ('689c936dd34675c04f0cf2dc506e12d5', '450203', '450200', '鱼峰区', 3, NULL, 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('689d1677d5db2a5bcc5059a12273f192', '530902', '530900', '临翔区', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('68bc02bd5bdf043c0bbbd08a49c92e53', '130204', '130200', '古冶区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('68e26350f5430f49428decdb77e27f6d', '140802', '140800', '盐湖区', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('68f62051a2536cdcf78cc2b5b576114a', '630203', '630200', '平安区', 3, NULL, 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('690ee467ca5bb25fbb203b0e9d226cbf', '410502', '410500', '文峰区', 3, NULL, 1, 1647931271, 1681975509);
INSERT INTO `yunduan_district` VALUES ('6917129fb491791e081a8bf528bee0e4', '420583', '420500', '枝江市', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('6922ffc930120ff051f163e0a0cf0881', '340102', '340100', '瑶海区', 3, NULL, 1, 1647931271, 1681975385);
INSERT INTO `yunduan_district` VALUES ('6924f7b2f515f68663c8bd487bcb0f0e', '450400', '450000', '梧州市', 2, '{\"county\": 7}', 1, 1647931271, 1681975626);
INSERT INTO `yunduan_district` VALUES ('694fb70d227163286bb8f718def8f7ca', '140823', '140800', '闻喜县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('696129b74c63e7c670fbd2aa8a5ef241', '510108', '510100', '成华区', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('6963750bf6469ee17b6ec8204394eb3c', '440802', '440800', '赤坎区', 3, NULL, 1, 1647931271, 1681975592);
INSERT INTO `yunduan_district` VALUES ('69811b45e42862fef26655088f660e1a', '370725', '370700', '昌乐县', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('698581faf5af1651b47556ff29625ede', '440825', '440800', '徐闻县', 3, NULL, 1, 1647931271, 1681975592);
INSERT INTO `yunduan_district` VALUES ('699adf76fafc73b6069888d0f472cdbd', '130303', '130300', '山海关区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('69bdc66d45ef44f02027f7839453c656', '320382', '320300', '邳州市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('69c312261cc06c9752dfcabaa340f9b1', '210902', '210900', '海州区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('69c5b9638bcd6e5115dcb60e73351a88', '320722', '320700', '东海县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('69e69b0807a467786affa4098f407865', '621125', '621100', '漳县', 3, NULL, 1, 1647931271, 1681978254);
INSERT INTO `yunduan_district` VALUES ('6a0d4da320b7c0eb07f1b0bc7ee3b70c', '130202', '130200', '路南区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('6a1f46c01bf1ba9c8d041974cb81d44c', '360729', '360700', '全南县', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('6a24146ecf7aa1b2148f56d36e2d3edb', '420502', '420500', '西陵区', 3, NULL, 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('6a2d61542ec68f33767a567813ea7d70', '140213', '140200', '平城区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('6a353d3d7788c7a3a913c3099ff07790', '330604', '330600', '上虞区', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('6a3b3e6c2e53f441707c177ee507d03a', '532627', '532600', '广南县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('6a59549741a5e6faf136709f798ea1af', '321302', '321300', '宿城区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('6a8f922f1147e12c175b6e135cbfddfd', '370800', '370000', '济宁市', 2, '{\"county\": 11}', 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('6a9b39fd0795c1db514c2cbf78a1fa29', '370302', '370300', '淄川区', 3, NULL, 1, 1647931271, 1681975486);
INSERT INTO `yunduan_district` VALUES ('6a9dc4e3caeb5e380b7fb366d11a434c', '130929', '130900', '献县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('6ab8d36752fa0510aa78115adc5d2718', '340111', '340100', '包河区', 3, NULL, 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('6abc5faaa3cbd685b1ebb4b47c2f0fa8', '510304', '510300', '大安区', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('6abd76886e77960f88581f78d6e02d05', '211381', '211300', '北票市', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('6aed008831b6e9589c19c48b2cbcce10', '610825', '610800', '定边县', 3, NULL, 1, 1647931271, 1681978065);
INSERT INTO `yunduan_district` VALUES ('6af4090055a1e968df47744716649790', '610204', '610200', '耀州区', 3, NULL, 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('6b035e5c16513e8c7759fa97401f6741', '371329', '371300', '临沭县', 3, NULL, 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('6b1c6a0ade127b236a8198774ee72776', '320924', '320900', '射阳县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('6b2ff538926c3385bea0a13de8d17ffb', '340210', '340200', '湾沚区 ', 3, NULL, 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('6b43810162f205a2ad075047f9a969b3', '620503', '620500', '麦积区', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('6b5ec8a3495247b888c9fc272dac1807', '445200', '440000', '揭阳市', 2, '{\"county\": 5}', 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('6b64399652c19ccc4eaed5890885717d', '500238', '500200', '巫溪县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('6b6ec0599a3fa3d524ec9fce2a675cdb', '150428', '150400', '喀喇沁旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('6b7dd3a6add2a505b683df548444545f', '450123', '450100', '隆安县', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('6b805d74756543056e9744501f5ce017', '360731', '360700', '于都县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('6b841e64c25c9a91f8c9f2916df4e94a', '430522', '430500', '新邵县', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('6ba95112dc7fd057294ec59c215a1c02', '540500', '540000', '山南市', 2, '{\"county\": 12}', 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('6bac4058e2c50dfd8e24f0fd376bc13b', '231000', '230000', '牡丹江市', 2, '{\"county\": 10}', 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('6bbbb55b801a28853a9cd66d2b7f3f7f', '110115', '110100', '大兴区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('6beadf5e8bb095abe9c5ba08488d9668', '150924', '150900', '兴和县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('6bfc4927885d896bb71704cccfb12fcc', '370783', '370700', '寿光市', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('6c3be161ea533031cc28f6de6d3771f7', '350429', '350400', '泰宁县', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('6c68921d384ff71ce3b8a928da3a5ae3', '350103', '350100', '台江区', 3, NULL, 1, 1647931271, 1681975417);
INSERT INTO `yunduan_district` VALUES ('6c6e2e75fa1821a23015c614056fe705', '340826', '340800', '宿松县', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('6caa558eb0b3cea360fe5f35d93ce30c', '360000', '0', '江西省', 1, '{\"city\": 11, \"county\": 100}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('6cb18bf6aed5ff20474254b76ab0f9ca', '320921', '320900', '响水县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('6cf91a1bb540c5292dfb99b01914e34e', '231181', '231100', '北安市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('6cfcc7ffa436bcbedad7a3707c5dd18d', '450602', '450600', '港口区', 3, NULL, 1, 1647931271, 1681975619);
INSERT INTO `yunduan_district` VALUES ('6d31f6aa1616fd3c1cd93ec587097d00', '511822', '511800', '荥经县', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('6d57614fcd8c54ca3c0f453443e7f34e', '623023', '623000', '舟曲县', 3, NULL, 1, 1647931271, 1681978244);
INSERT INTO `yunduan_district` VALUES ('6d765acc463228a189da2d81db481a59', '410323', '410300', '新安县', 3, NULL, 1, 1647931271, 1681975502);
INSERT INTO `yunduan_district` VALUES ('6d82cedd13748d14c4b958965bfbbabc', '430511', '430500', '北塔区', 3, NULL, 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('6d87c3707bd51eb5c5532e9fa58801a5', '340811', '340800', '宜秀区', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('6db7a00edf196040848f2a9a0dfdf30a', '511002', '511000', '市中区', 3, NULL, 1, 1647931271, 1681977512);
INSERT INTO `yunduan_district` VALUES ('6dcfc2535af1535bb547f468909101db', '532325', '532300', '姚安县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('6de0f1f903f327725aa5843f848b2845', '421281', '421200', '赤壁市', 3, NULL, 1, 1647931271, 1681975548);
INSERT INTO `yunduan_district` VALUES ('6df61eade5930ff59b48d9fa5b537452', '630000', '0', '青海省', 1, '{\"city\": 8, \"county\": 44}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('6e0e0c9e4e1f0a9af6dd12b0293a51da', '360702', '360700', '章贡区', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('6e1c729f932125a545b047044482b926', '511525', '511500', '高县', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('6e23430caa045d3e2e518e16557872ec', '371625', '371600', '博兴县', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('6e2f8a5f17fd797e77399f9229b636a3', '411627', '411600', '太康县', 3, NULL, 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('6e533226ada1c0ed13c0676288ed62c8', '321300', '320000', '宿迁市', 2, '{\"county\": 5}', 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('6e606876cab318810a2e42225389bce3', '211404', '211400', '南票区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('6e73e25f06d3c4413f9cea4cfe2cbb8f', '513222', '513200', '理县', 3, NULL, 1, 1647931271, 1681977526);
INSERT INTO `yunduan_district` VALUES ('6e773a0c8ffc75897c6229979407f24b', '371326', '371300', '平邑县', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('6e838fe48cadd1311d89da62d7e061c0', '341203', '341200', '颍东区', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('6e84e238d8a7c2faf68db67e8dce8b17', '410184', '410100', '新郑市', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('6e8632cf967886a281fb5b69ea139bc5', '520525', '520500', '纳雍县', 3, NULL, 1, 1647931271, 1681977724);
INSERT INTO `yunduan_district` VALUES ('6e9167ed21b4fae754a74305b2359285', '320623', '320600', '如东县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('6e9d891b7fe8f70118c424f1d0f6ab19', '440513', '440500', '潮阳区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('6ea584c15695b826f0aa377208071d33', '513230', '513200', '壤塘县', 3, NULL, 1, 1647931271, 1681977526);
INSERT INTO `yunduan_district` VALUES ('6ebd5511ba2cac4cae6d1741142555ba', '420527', '420500', '秭归县', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('6ec6e7142b0da1cb23a409b5c2f4f306', '512021', '512000', '安岳县', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('6ecc55cc88ea6305037986d874c8c7ad', '411700', '410000', '驻马店市', 2, '{\"county\": 10}', 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('6ed093c3a9afdc1b906c63f2c87391a3', '360724', '360700', '上犹县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('6ed8a46cd22142e57a5580d6071041e8', '520328', '520300', '湄潭县', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('6f23a2ab6f97b77144759c13b0a05efb', '341602', '341600', '谯城区', 3, NULL, 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('6f5824e17809bac098c088d49ab691cc', '360113', '360100', '红谷滩区', 3, NULL, 1, 1647931271, 1681975450);
INSERT INTO `yunduan_district` VALUES ('6f605f2aefa44508a090e0aa4fd9ee36', '210123', '210100', '康平县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('6f6719aec1c5012c18695f6f3c66b9e4', '522727', '522700', '平塘县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('6f966bcd5a5b6f4650de58729f0b7e45', '441881', '441800', '英德市', 3, NULL, 1, 1647931271, 1681975588);
INSERT INTO `yunduan_district` VALUES ('6fcb24a9c19a1d93165deb4f0a16bb90', '533102', '533100', '瑞丽市', 3, NULL, 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('6fcb9a764c420b93d21ffe69e7311bff', '320311', '320300', '泉山区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('6fe23846091aac94c89e341971881679', '513324', '513300', '九龙县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('6ffb910574f75daa6db333bb6691f46f', '610702', '610700', '汉台区', 3, NULL, 1, 1647931271, 1681978083);
INSERT INTO `yunduan_district` VALUES ('7006ea4430a8237d92b37896715f8619', '530304', '530300', '马龙区', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('7015fc3889fa4ab3a2d057418913c8dc', '632200', '630000', '海北藏族自治州', 2, '{\"county\": 4}', 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('701c3677685a0054ffeb302a522a85d5', '450703', '450700', '钦北区', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('70294d89e444b51b0d21457935a839d2', '230400', '230000', '鹤岗市', 2, '{\"county\": 8}', 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('70583c2730e8375a07d710a1100f3bac', '141181', '141100', '孝义市', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('705e7907692179d2bb13ad3272daa3fc', '500102', '500100', '涪陵区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('7062ef4e476182f103bc9d85226e3c4e', '350881', '350800', '漳平市', 3, NULL, 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('706337a25b074da028707a8c82eb46c3', '530621', '530600', '鲁甸县', 3, NULL, 1, 1647931271, 1681977886);
INSERT INTO `yunduan_district` VALUES ('70badd0b2909191eb37629f7ec3ea9e5', '621200', '620000', '陇南市', 2, '{\"county\": 9}', 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('70c067b5e2a1e70ddbf11d1a0d25832a', '610830', '610800', '清涧县', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('70d1cc7515ea60de7a19473487fcb10b', '640105', '640100', '西夏区', 3, NULL, 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('70e0dd63670cf63076a74c55bf43e7df', '450205', '450200', '柳北区', 3, NULL, 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('70e11100e2d7e550c4d0e2e3c8d34a70', '130702', '130700', '桥东区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('71132d4624514b305bc224ae254655cb', '610724', '610700', '西乡县', 3, NULL, 1, 1647931271, 1681978083);
INSERT INTO `yunduan_district` VALUES ('712823cb1e3536c85e72c93a275b974d', '421200', '420000', '咸宁市', 2, '{\"county\": 6}', 1, 1647931271, 1681975548);
INSERT INTO `yunduan_district` VALUES ('712c3ed88fab5af0d3f3d60ed9ecfcba', '371525', '371500', '冠县', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('71794626b5fcd3ea187d98874d17ab67', '532523', '532500', '屏边苗族自治县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('71918f532d8ce4386d4300c67c289746', '350104', '350100', '仓山区', 3, NULL, 1, 1647931271, 1681975418);
INSERT INTO `yunduan_district` VALUES ('7197a7150ce0548cf8a482ce63d65a38', '430524', '430500', '隆回县', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('71c308b50494aefdea39078661a5b33b', '360112', '360100', '新建区', 3, NULL, 1, 1647931271, 1681975449);
INSERT INTO `yunduan_district` VALUES ('71d2583dc7014381be7d3f9ee726cc3d', '411624', '411600', '沈丘县', 3, NULL, 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('71da5b96fc891290ab486f2942ae92f1', '532531', '532500', '绿春县', 3, NULL, 1, 1647931271, 1681977884);
INSERT INTO `yunduan_district` VALUES ('71e48e9d55168fc2a138b2dc46ac6dcd', '411403', '411400', '睢阳区', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('71e98656877d1ba16788ef06a9bf1a02', '410328', '410300', '洛宁县', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('71eeeaeb843688d7e2cd18668c6149c5', '341622', '341600', '蒙城县', 3, NULL, 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('71fdc918787971bd3a4af09eaca93b52', '430482', '430400', '常宁市', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('71febe2996de29cad5f8f27c11166622', '371428', '371400', '武城县', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('7201bf53df18403e7e66dcc73831e3b8', '231100', '230000', '黑河市', 2, '{\"county\": 6}', 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('720642cad61264bda07a90404e55f6ca', '431227', '431200', '新晃侗族自治县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('720a22f9d5406a706e9d86a5fed58557', '654301', '654300', '阿勒泰市', 3, NULL, 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('721021594f69e049ccce49fc64e247ef', '510812', '510800', '朝天区', 3, NULL, 1, 1647931271, 1681977529);
INSERT INTO `yunduan_district` VALUES ('721431cc1694acd3e2780e6892122201', '420700', '420000', '鄂州市', 2, '{\"county\": 3}', 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('7217f6395fcb809e5979ac9d38e1c9aa', '451381', '451300', '合山市', 3, NULL, 1, 1647931271, 1681975625);
INSERT INTO `yunduan_district` VALUES ('722bff91d6ac3cc7fc0b25d82e2a5376', '410182', '410100', '荥阳市', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('723a7b4d326bebed766fa84e1c913c2e', '532324', '532300', '南华县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('727931ca85aa717e1474e28673978ff7', '620924', '620900', '阿克塞哈萨克族自治县', 3, NULL, 1, 1647931271, 1681978243);
INSERT INTO `yunduan_district` VALUES ('728b8c4c9c40a1181cf6e7b543a9f2f8', '152223', '152200', '扎赉特旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('7290cf23f55c9282860ce5dd7f61ce49', '360423', '360400', '武宁县', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('72933588ab1e1df43c4f8d13ea8983c4', '330783', '330700', '东阳市', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('72a4bfb0a4dc204f482045c2adaf9f55', '350924', '350900', '寿宁县', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('72adac18d311b222fb5411149122b41d', '410724', '410700', '获嘉县', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('72b5dde633d1f79cd42f5e26a5c42cdf', '370113', '370100', '长清区', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('72b81ec1fd4328bf8ea54ae8593b9a5c', '130681', '130600', '涿州市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('72c3cb9eed3a15d643cfd755a5dfa1e8', '331100', '330000', '丽水市', 2, '{\"county\": 9}', 1, 1647931271, 1681975341);
INSERT INTO `yunduan_district` VALUES ('72ce3ba0071127dc288878bcd5f6280d', '220105', '220100', '二道区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('72e68dc163735372de9ae9d1417198d2', '130982', '130900', '任丘市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('72ebf55021f50c7055951a0add5bd89b', '450103', '450100', '青秀区', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('72f8546379f8b7a77278aaac7dfcad33', '511725', '511700', '渠县', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('733e31da1dfadca0f0b854fe5d00befa', '341821', '341800', '郎溪县', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('73416bad3086579bdb3fe186490c6f83', '450922', '450900', '陆川县', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('7344d7632ae4632079811a7ca87e5933', '622927', '622900', '积石山保安族东乡族撒拉族自治县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('7347fa51ad0a20750bfa0f3ed9191d59', '370832', '370800', '梁山县', 3, NULL, 1, 1647931271, 1681975495);
INSERT INTO `yunduan_district` VALUES ('7349df71fdbefbd657b74128f92231a8', '610826', '610800', '绥德县', 3, NULL, 1, 1647931271, 1681978065);
INSERT INTO `yunduan_district` VALUES ('734e4d7f3367c6cff519bf39c106b868', '410927', '410900', '台前县', 3, NULL, 1, 1647931271, 1681975526);
INSERT INTO `yunduan_district` VALUES ('738fe13d8c241b3bf22afea6e7a5a301', '350525', '350500', '永春县', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('739f1c33587e3cb4b8db76c5a479e3b2', '440200', '440000', '韶关市', 2, '{\"county\": 10}', 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('73cddb72a3ef6f339ef333f4af90ed8c', '350626', '350600', '东山县', 3, NULL, 1, 1647931271, 1681975411);
INSERT INTO `yunduan_district` VALUES ('73e69ad09b304ff9a4aa92f44f503211', '140824', '140800', '稷山县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('73e6de18d076114da9a19ae7f87a09d2', '610632', '610600', '黄陵县', 3, NULL, 1, 1647931271, 1681978081);
INSERT INTO `yunduan_district` VALUES ('73f267f5c41b72a84a189b2e6fd202c8', '361130', '361100', '婺源县', 3, NULL, 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('73f60fe359fcf16996da3d5d34bd9f38', '513228', '513200', '黑水县', 3, NULL, 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('73fa7d3fb446d50efbbaff92b4b4d1fe', '130635', '130600', '蠡县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('7404552d76c949bff8be7c4b983543a8', '520400', '520000', '安顺市', 2, '{\"county\": 6}', 1, 1647931271, 1681977719);
INSERT INTO `yunduan_district` VALUES ('74085899d72eb1562916fd51f1917b63', '411025', '411000', '襄城县', 3, NULL, 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('74096a8241399b1ee317eea26bca91b4', '450324', '450300', '全州县', 3, NULL, 1, 1647931271, 1681975631);
INSERT INTO `yunduan_district` VALUES ('7409a64eefebf01522df6676782cec11', '441623', '441600', '连平县', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('741188dfbfd7b51e9d52658e3294708e', '131182', '131100', '深州市', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('745b76ccfe2b0a4cbe05ebafbe623582', '630200', '630000', '海东市', 2, '{\"county\": 6}', 1, 1647931271, 1681978315);
INSERT INTO `yunduan_district` VALUES ('745f1da74bc20ba3ed0580ca08512292', '350721', '350700', '顺昌县', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('746867065cca7e0eaf2392f666f3e01c', '420112', '420100', '东西湖区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('7473f39c23afb25c8e8ffd71b1e7b8a8', '512002', '512000', '雁江区', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('749f3ea8d1ecb92cb74837c6cc930ca0', '522700', '520000', '黔南布依族苗族自治州', 2, '{\"county\": 12}', 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('74a74c3fa7c8d681eab36a5b522f2f38', '510705', '510700', '安州区', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('74c912f1599ea420b902b54051a66050', '430304', '430300', '岳塘区', 3, NULL, 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('74fb48206bb3d480d296a3f9d5621941', '370103', '370100', '市中区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('7504366f81b11d61249f34458cb3cd02', '320282', '320200', '宜兴市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('750f32946f46f6c93f93414fcb81a0e6', '410105', '410100', '金水区', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('7511fc9762c30806450b30fe83e21695', '652928', '652900', '阿瓦提县', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('751fc9b78cbb11b634e251aab1bff870', '341103', '341100', '南谯区', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('7522b45e5c9ea2f400ac3614af375aa5', '430921', '430900', '南县', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('7527a7598cae3da9e934712d7a6d003a', '350181', '350100', '福清市', 3, NULL, 1, 1647931271, 1681975417);
INSERT INTO `yunduan_district` VALUES ('752b2a83d7b93f242098efc49cdb571b', '430381', '430300', '湘乡市', 3, NULL, 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('752d51c96dabd6553e7d4b92a9f0c939', '431021', '431000', '桂阳县', 3, NULL, 1, 1647931271, 1681975554);
INSERT INTO `yunduan_district` VALUES ('754c35b37a813b1e32372c761dfb78af', '421002', '421000', '沙市区', 3, NULL, 1, 1647931271, 1681975542);
INSERT INTO `yunduan_district` VALUES ('755d4a16eb8c33b468309bb30303d161', '350824', '350800', '武平县', 3, NULL, 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('757076b1479576270e77ed7cb109d837', '640425', '640400', '彭阳县', 3, NULL, 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('757d23b96f047bc6882089f213edafaa', '140925', '140900', '宁武县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('757e24febb59319c647aed30d3679e7a', '540630', '540600', '双湖县 ', 3, NULL, 1, 1647931271, 1681977993);
INSERT INTO `yunduan_district` VALUES ('75906f85a71618a721904b49b6e8b2d2', '140800', '140000', '运城市', 2, '{\"county\": 13}', 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('75c6ee9bd3fc2eee9244b90cd59ba5a1', '411524', '411500', '商城县', 3, NULL, 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('76141f377927967059b98a20d5c3169d', '140602', '140600', '朔城区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('76186cfedec7d61f94cbb3e2bdc83e55', '632803', '632800', '茫崖市', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('764514b84f8b540ab7539fd46b73f41e', '411724', '411700', '正阳县', 3, NULL, 1, 1647931271, 1681975522);
INSERT INTO `yunduan_district` VALUES ('765190f17c1161551138d679ced5a87c', '220722', '220700', '长岭县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('76583e2b613d574f33538875ed254c49', '411102', '411100', '源汇区', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('76674cd965e9eddc169a2d4240d50b53', '652702', '652700', '阿拉山口市', 3, NULL, 1, 1647931271, 1681978502);
INSERT INTO `yunduan_district` VALUES ('7690a38bdc2ef9db6a01a6a8b229eb6e', '511528', '511500', '兴文县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('7697f43a90246e09d3898266112999b2', '440105', '440100', '海珠区', 3, NULL, 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('76c21d453a29437d5c3da5ed51559076', '430724', '430700', '临澧县', 3, NULL, 1, 1647931271, 1681975557);
INSERT INTO `yunduan_district` VALUES ('76d261393f34b2e24584e4bebb293ac9', '410185', '410100', '登封市', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('76f25b0e7ecfb7b2240c92b31f075afa', '430300', '430000', '湘潭市', 2, '{\"county\": 5}', 1, 1647931271, 1681975558);
INSERT INTO `yunduan_district` VALUES ('76ff75b068fde34790bb605b1d826167', '150926', '150900', '察哈尔右翼前旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('771ab0038595d1e861d6acef0ad04086', '440222', '440200', '始兴县', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('771f9a6011bde8b9a45c7b5c5af15444', '150702', '150700', '海拉尔区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('773436f73e8e827c47a2b839208f7839', '540502', '540500', '乃东区', 3, NULL, 1, 1647931271, 1681977996);
INSERT INTO `yunduan_district` VALUES ('77476a5d597d08ddd94e7c59daa87ef2', '530427', '530400', '新平彝族傣族自治县', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('7769fdff02bd211b9379c183b3358125', '440507', '440500', '龙湖区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('7771178a6ee1c5700239d47ad3792419', '130125', '130100', '行唐县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('7775fea0603f127f95367fdfecfbe2ea', '350200', '350000', '厦门市', 2, '{\"county\": 6}', 1, 1647931271, 1681975405);
INSERT INTO `yunduan_district` VALUES ('7776e82bdc79087733228eaa6c035407', '340281', '340200', '无为市', 3, NULL, 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('7777a17ecca0272ec77d5f3195c063d9', '130682', '130600', '定州市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('7795cf79ec507cb5a9fd637cafbe8131', '513231', '513200', '阿坝县', 3, NULL, 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('7797ddf7316334af059df04a00cf0ce6', '210604', '210600', '振安区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('77b42385f3cac1f223a2a30de5e8760d', '652925', '652900', '新和县', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('783b056693ea3cbb577882a3666fa5c1', '350121', '350100', '闽侯县', 3, NULL, 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('785649492cdab902ad3552a5d9231e7e', '411623', '411600', '商水县', 3, NULL, 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('7869c5e6e331cc3b15293d2f79133ce4', '510981', '510900', '射洪市', 3, NULL, 1, 1647931271, 1681977544);
INSERT INTO `yunduan_district` VALUES ('787a3dc753a15bfe1e2b21bfd27d322b', '210111', '210100', '苏家屯区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('78830cff2d995d6441c2b89c3a846916', '360400', '360000', '九江市', 2, '{\"county\": 13}', 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('788ab0f0bf002ad1ee5625a9d9cb9e30', '653200', '650000', '和田地区', 2, '{\"county\": 8}', 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('788b48aa3ab17ae0f3cc81df7f161b57', '340311', '340300', '淮上区', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('789e770d9ef68ddfeac66cd265421cec', '360430', '360400', '彭泽县', 3, NULL, 1, 1647931271, 1681975454);
INSERT INTO `yunduan_district` VALUES ('78b0325767ea18f24158482447c67452', '341300', '340000', '宿州市', 2, '{\"county\": 5}', 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('78b60d642e2cc52aef808ee3eafc70ed', '610802', '610800', '榆阳区', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('78c011bc8364a686a4a6c98900654f8d', '451324', '451300', '金秀瑶族自治县', 3, NULL, 1, 1647931271, 1681975625);
INSERT INTO `yunduan_district` VALUES ('78d579811d130f10c005eeeb03a64343', '421221', '421200', '嘉鱼县', 3, NULL, 1, 1647931271, 1681975549);
INSERT INTO `yunduan_district` VALUES ('78f3261088b8b1945788b875475f0cf0', '361000', '360000', '抚州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('790b52817200804e94caeb404b464d9b', '360700', '360000', '赣州市', 2, '{\"county\": 18}', 1, 1647931271, 1681975443);
INSERT INTO `yunduan_district` VALUES ('79207e3d29dc94ffbd15fa9fcf8a53e0', '420222', '420200', '阳新县', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('792532a4793ac7e2b62abb8ce5bb2b32', '360735', '360700', '石城县', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('792c99b4ee768e1f4cd4c56c4ba9752f', '620523', '620500', '甘谷县', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('795a3a2f61dbe2d1f1c624f5a0a7942b', '410102', '410100', '中原区', 3, NULL, 1, 1647931271, 1681975507);
INSERT INTO `yunduan_district` VALUES ('79629ccd4cdfd3cb452b8866629bfd23', '231225', '231200', '明水县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('7962b94b2c8efee3b7917669bc0ab994', '130430', '130400', '邱县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('797adf8272325279d9167ce3ef9f601d', '130607', '130600', '满城区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('798c76c39688be68458c44f10ddf9cce', '350526', '350500', '德化县', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('799ef657a9c60c9f09ecdd59bb8a9eeb', '610327', '610300', '陇县', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('79a0170aa66f64ce47605d933faa041d', '621021', '621000', '庆城县', 3, NULL, 1, 1647931271, 1681978246);
INSERT INTO `yunduan_district` VALUES ('79cad505cb4cd5cd6cd59e0f180aa7ab', '310112', '310100', '闵行区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('79d03734b2f8ddeb56f49fe2816f0bd7', '610600', '610000', '延安市', 2, '{\"county\": 13}', 1, 1647931271, 1681978079);
INSERT INTO `yunduan_district` VALUES ('79d16394d820a16a177f2c7f86da176b', '431129', '431100', '江华瑶族自治县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('79d38a195988032cf30e80a834715e17', '210711', '210700', '太和区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('79e4db4495d3de7adde97879d5b6a6f5', '370923', '370900', '东平县', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('79e5fb5f6d83950f65cc1bfa7ebb0036', '320104', '320100', '秦淮区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('79ff8cf63b8543e1a0e1127d03bad474', '430281', '430200', '醴陵市', 3, NULL, 1, 1647931271, 1681975551);
INSERT INTO `yunduan_district` VALUES ('7a014b5d8dd4e1a4c2ec4ff7b1a0fe3d', '140826', '140800', '绛县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('7a0384d1c5c510519fa1640bccb81f14', '410106', '410100', '上街区', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('7a1982a5d9fb862f74cfc8d29577a29d', '140215', '140200', '云州区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('7a2d9c682e2bbed2921469540041d834', '632521', '632500', '共和县', 3, NULL, 1, 1647931271, 1681978318);
INSERT INTO `yunduan_district` VALUES ('7a3ccaa9c020037cd6da1fb3461fd56e', '430182', '430100', '宁乡市', 3, NULL, 1, 1647931271, 1681975565);
INSERT INTO `yunduan_district` VALUES ('7a3d586b1bdc7f10bfbb7661e8fba6ef', '130304', '130300', '北戴河区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('7a4d2e12dc42512ee1506b3d65c92ddc', '610423', '610400', '泾阳县', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('7a74ee78fb67180dd01bde6cb710f330', '654024', '654000', '巩留县', 3, NULL, 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('7a918a3dbe88bbb37a259b7bc88d1a25', '653122', '653100', '疏勒县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('7a920ca1c510edb5e4910c423415466f', '230183', '230100', '尚志市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('7a92b07ceebdcd82774572aa1e207c7d', '211303', '211300', '龙城区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('7aa6f8c2de4c25eed22213366ea5a34f', '331000', '330000', '台州市', 2, '{\"county\": 9}', 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('7ab36be6f8ab062bce7753210ae826ad', '371328', '371300', '蒙阴县', 3, NULL, 1, 1647931271, 1681975481);
INSERT INTO `yunduan_district` VALUES ('7ac862a313ab75b08280c3f5f1eedcab', '511423', '511400', '洪雅县', 3, NULL, 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('7ad9aa457343f8827c7bbae5ad5ff3fe', '441803', '441800', '清新区', 3, NULL, 1, 1647931271, 1681975588);
INSERT INTO `yunduan_district` VALUES ('7afaa6f483f540434f31333e3bd55328', '222406', '222400', '和龙市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('7b046f35dbaac72c138c6e163069b7ba', '140403', '140400', '潞州区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('7b21cd5a6e15809db5a01923d46f78ee', '513300', '510000', '甘孜藏族自治州', 2, '{\"county\": 18}', 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('7b32c482d288eb601ea7059dec62e4eb', '530500', '530000', '保山市', 2, '{\"county\": 5}', 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('7b438bb174dff4fc67480038d5a43340', '510900', '510000', '遂宁市', 2, '{\"county\": 5}', 1, 1647931271, 1681977543);
INSERT INTO `yunduan_district` VALUES ('7b957919dc5eb20e919e33a83ae852c6', '640381', '640300', '青铜峡市', 3, NULL, 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('7ba2c9aff3da6f7a3721d1d5bcc38637', '610626', '610600', '吴起县', 3, NULL, 1, 1647931271, 1681978081);
INSERT INTO `yunduan_district` VALUES ('7bb32bf120be39800769f24f8338fa38', '220323', '220300', '伊通满族自治县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('7bca7b0da4219fe1fe0126ac73ab6139', '140121', '140100', '清徐县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('7bddacc968c9b764223f8669d5772d40', '220822', '220800', '通榆县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('7be1cbccdf8fa446a05208b744044703', '441602', '441600', '源城区', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('7be22e73bf62b95033686189792cfc62', '140929', '140900', '岢岚县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('7be2d47d8db5897a476348858a424415', '410324', '410300', '栾川县', 3, NULL, 1, 1647931271, 1681975502);
INSERT INTO `yunduan_district` VALUES ('7be791de38b00f166e93b9df41db83ae', '500242', '500200', '酉阳土家族苗族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('7be939316afa01f35e4570a0cd62f076', '131125', '131100', '安平县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('7c07b563c73e8de2291f1bdec062d2e2', '230184', '230100', '五常市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('7c22d738aea7987187d2d7587cfbd9d4', '632522', '632500', '同德县', 3, NULL, 1, 1647931271, 1681978318);
INSERT INTO `yunduan_district` VALUES ('7c32fe0d6cf87909022a61701535da7d', '620400', '620000', '白银市', 2, '{\"county\": 5}', 1, 1647931271, 1681978238);
INSERT INTO `yunduan_district` VALUES ('7c406f7b44090a6012eefae1ae32dae5', '370104', '370100', '槐荫区', 3, NULL, 1, 1647931271, 1681975489);
INSERT INTO `yunduan_district` VALUES ('7c659161b4ed7e1f3d86d6acdada55bb', '610425', '610400', '礼泉县', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('7c69278a73bca8322aa313fe31ea6109', '152524', '152500', '苏尼特右旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('7c6f2dd6c6e3d78e834aa40c3ca92dab', '420624', '420600', '南漳县', 3, NULL, 1, 1647931271, 1681975531);
INSERT INTO `yunduan_district` VALUES ('7c720491f0410ce29edf3a48cf057b10', '120118', '120100', '静海区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('7ca3f3949522e8f7b606e6ceba9eb4ba', '513322', '513300', '泸定县', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('7cf225c382758bc1b410c01c48a530b6', '441427', '441400', '蕉岭县', 3, NULL, 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('7cf3e561a7bb925978747ff8c1c2c1a3', '360323', '360300', '芦溪县', 3, NULL, 1, 1647931271, 1681975458);
INSERT INTO `yunduan_district` VALUES ('7d046207dcd95ab87ace6be78a3f36a6', '421126', '421100', '蕲春县', 3, NULL, 1, 1647931271, 1681975545);
INSERT INTO `yunduan_district` VALUES ('7d108f30957f0f8715a4d4030ee46e16', '230229', '230200', '克山县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('7d26a1aa9190658b54827ef6d8c3c35d', '433126', '433100', '古丈县', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('7d300d6c7411f1c62ef4c3d42780f769', '460105', '460100', '秀英区', 3, NULL, 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('7d6032558b55ec03bdc8d5f698781780', '370503', '370500', '河口区', 3, NULL, 1, 1647931271, 1681975493);
INSERT INTO `yunduan_district` VALUES ('7d823ef2c8e28b2c0014bd80c7eaf4bd', '632626', '632600', '玛多县', 3, NULL, 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('7da24afd34fa1302c3112d21e8636488', '320111', '320100', '浦口区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('7da2a1f2381b9049a412cfc496026b8b', '520422', '520400', '普定县', 3, NULL, 1, 1647931271, 1681977720);
INSERT INTO `yunduan_district` VALUES ('7da56bae805c8f1ba0a7d9190719b332', '510802', '510800', '利州区', 3, NULL, 1, 1647931271, 1681977529);
INSERT INTO `yunduan_district` VALUES ('7dd76b1a5418c08e6097609c7b7ce49d', '330723', '330700', '武义县', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('7dd8c7b43ea9a614aa2068fdae8c483a', '650521', '650500', '巴里坤哈萨克自治县', 3, NULL, 1, 1647931271, 1681978496);
INSERT INTO `yunduan_district` VALUES ('7df257e0d9eab87024c1fbddad23537c', '340881', '340800', '桐城市', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('7df97d99f7e146c9c647436a71af5f89', '620122', '620100', '皋兰县', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('7e3eacfd985d61c40d605d560708b12e', '430481', '430400', '耒阳市', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('7e6130ede7a75f2d95345ccfed68f5a3', '511722', '511700', '宣汉县', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('7e76870ea4fc13d219a1fbdd4eac8980', '410883', '410800', '孟州市', 3, NULL, 1, 1647931271, 1681975516);
INSERT INTO `yunduan_district` VALUES ('7e7add093a1ce37348b90f3355d25768', '410821', '410800', '修武县', 3, NULL, 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('7eaf5bb55a322a8cf409862c853f8e70', '140924', '140900', '繁峙县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('7ed18a06461075aafad21c00c94ef3d0', '231083', '231000', '海林市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('7ee72964fb07e0b0a2669120ac91255e', '150925', '150900', '凉城县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('7f1b6897e0c5acd014705c2012e85703', '410703', '410700', '卫滨区', 3, NULL, 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('7f578479a5f0b377c92dbe81dbc0b02b', '652822', '652800', '轮台县', 3, NULL, 1, 1647931271, 1681978502);
INSERT INTO `yunduan_district` VALUES ('7f5c356db717d003377873761d1e1f43', '630222', '630200', '民和回族土族自治县', 3, NULL, 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('7f70ce161027b1c7a5f782faf17d52cc', '350502', '350500', '鲤城区', 3, NULL, 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('7fb1692299af39ade7d56be122c1e4de', '320981', '320900', '东台市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('7fcc34c4548c9c366ecb85822f83435b', '130129', '130100', '赞皇县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('7fef2f6ce0bf1b56d2b0c93db81931a0', '622900', '620000', '临夏回族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('801086e76779cfec0a674aaa49432d24', '622922', '622900', '康乐县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('8019cb01af9efb3d619c5c34813c83e2', '510000', '0', '四川省', 1, '{\"city\": 21, \"county\": 183}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('80227428267a157ad55c4993d6fb6829', '610431', '610400', '武功县', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('80256819694c2df194a277d02e351ad6', '360313', '360300', '湘东区', 3, NULL, 1, 1647931271, 1681975458);
INSERT INTO `yunduan_district` VALUES ('8028cb711fb84cf0ab93f4b334de5468', '330205', '330200', '江北区', 3, NULL, 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('802bbdc20c90681e17a528761b06e46e', '500109', '500100', '北碚区', 3, NULL, 1, 1647931271, 1681977450);
INSERT INTO `yunduan_district` VALUES ('805f4d1b0ba2fc41b116d076ad141396', '361128', '361100', '鄱阳县', 3, NULL, 1, 1647931271, 1681975456);
INSERT INTO `yunduan_district` VALUES ('806f72d2a8104a865f9f6247093eef7f', '451122', '451100', '钟山县', 3, NULL, 1, 1647931271, 1681975622);
INSERT INTO `yunduan_district` VALUES ('80ac4ac87f232c6d031ed4b2cc80a867', '231221', '231200', '望奎县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('80b794a35af649329b72f38103ab982f', '610200', '610000', '铜川市', 2, '{\"county\": 4}', 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('80cb509426fa0a84b5016146ddfbd39c', '522302', '522300', '兴仁市', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('80e3f5d191d77861b5b9dba6904167e8', '433127', '433100', '永顺县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('80e5f66ac26ab19154790bc511fecfaa', '350926', '350900', '柘荣县', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('80fca2d62b94057bfe6fd126711b23ca', '371322', '371300', '郯城县', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('810aae180b759d2c5bd227f6d2fb5ef8', '210302', '210300', '铁东区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('814e0647d97454703dabc40d4fa9c829', '411600', '410000', '周口市', 2, '{\"county\": 10}', 1, 1647931271, 1681975524);
INSERT INTO `yunduan_district` VALUES ('81565c1c76e752a19070794e8742bcd3', '450332', '450300', '恭城瑶族自治县', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('8189578abd03b5e0e3067e30391b7e56', '141081', '141000', '侯马市', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('81c185ef49cf53ad7b358c5a1cd5928d', '220821', '220800', '镇赉县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('81e38517f862b8fb1a43d91268319336', '421124', '421100', '英山县', 3, NULL, 1, 1647931271, 1681975544);
INSERT INTO `yunduan_district` VALUES ('81f0d721f83ebde537cfb3c91158e5b3', '341600', '340000', '亳州市', 2, '{\"county\": 4}', 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('820cde91b1853d8967e8901ceb930faf', '610202', '610200', '王益区', 3, NULL, 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('821440fc3588922c657df193b089b7a3', '130902', '130900', '新华区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('8241e08c6c64336eb21b569332a4d308', '421300', '420000', '随州市', 2, '{\"county\": 3}', 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('828232f4c3fb7db966d7ec968e4e9048', '220421', '220400', '东丰县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('8288bfc8dfeae08d2bacc48cf03e7c2f', '513233', '513200', '红原县', 3, NULL, 1, 1647931271, 1681977527);
INSERT INTO `yunduan_district` VALUES ('828f7eb5a95435633bfb5ece53a34eaf', '230126', '230100', '巴彦县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('82aa99b55ac9172b0c3f701325bf457b', '140830', '140800', '芮城县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('82cc797a9d64d205986573ec9e5741ce', '511603', '511600', '前锋区', 3, NULL, 1, 1647931271, 1681977520);
INSERT INTO `yunduan_district` VALUES ('82ccf72439281f03493c3cc072f76c8d', '530323', '530300', '师宗县', 3, NULL, 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('82d1d88bb98441228866eedea7a3981a', '211102', '211100', '双台子区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('82d28e05eab0f04db8f5834f263fc9c9', '460202', '460200', '海棠区', 3, NULL, 1, 1647931271, 1681976684);
INSERT INTO `yunduan_district` VALUES ('82dc3deb4863ddd704bffc35c50ed10b', '231282', '231200', '肇东市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('82e256dc20b7a60473b272181cbe0eb9', '522728', '522700', '罗甸县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('82e54433317a2ab802973f115e95096b', '450107', '450100', '西乡塘区', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('82f1e1d5473cfdadb9b538f7c9748ac9', '360730', '360700', '宁都县', 3, NULL, 1, 1647931271, 1681975446);
INSERT INTO `yunduan_district` VALUES ('82f54502196cba64a03bad01d4643e41', '141121', '141100', '文水县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('82fca6ab126024b3c2933ce41cd5f68c', '451003', '451000', '田阳区', 3, NULL, 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('82fddb2f207dce585f223ad34590690c', '370112', '370100', '历城区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('8328abe7da2af880be7cd319d5220fc7', '421222', '421200', '通城县', 3, NULL, 1, 1647931271, 1681975549);
INSERT INTO `yunduan_district` VALUES ('834da555901f00d7963b697bda8845a1', '341122', '341100', '来安县', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('835a36459535a175068d1e2f2b09b410', '321081', '321000', '仪征市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('836f7c8eb07313a14d9bb74315966f2a', '451002', '451000', '右江区', 3, NULL, 1, 1647931271, 1681975628);
INSERT INTO `yunduan_district` VALUES ('836fb3b426622ecd100dd3eef598f6b2', '230304', '230300', '滴道区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('838684aae8650ba663d4a449c14ee291', '140981', '140900', '原平市', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('83b5a3d22344d2f1e222bcb63b2f584f', '450681', '450600', '东兴市', 3, NULL, 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('83bbb84f9e05eb550c295bdfaeaa0d17', '530381', '530300', '宣威市', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('83dbe62434dc91520baf3e83f82cd4db', '441423', '441400', '丰顺县', 3, NULL, 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('84080e9015aad5726fb4ccb1880f31c0', '150430', '150400', '敖汉旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('8410bc075c511a24f45063e6c17e24ab', '360424', '360400', '修水县', 3, NULL, 1, 1647931271, 1681975454);
INSERT INTO `yunduan_district` VALUES ('841f64ea456af41abe58c1ae84a3463b', '360429', '360400', '湖口县', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('8424682d139541dfa4ec856391a0ea96', '450200', '450000', '柳州市', 2, '{\"county\": 10}', 1, 1647931271, 1681975638);
INSERT INTO `yunduan_district` VALUES ('843ba9dfeca969411596b4cc8762c2f0', '445321', '445300', '新兴县', 3, NULL, 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('845ed9a3fbcc63820852a153d59bab6c', '230205', '230200', '昂昂溪区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('8464066735e3c27c8023ea6a1fb95b23', '610103', '610100', '碑林区', 3, NULL, 1, 1647931271, 1681978072);
INSERT INTO `yunduan_district` VALUES ('8475fa91b50a223fb55417ae48dcf61f', '321181', '321100', '丹阳市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('847ab19ee3c0a26f120bd610bc1a0ce7', '630225', '630200', '循化撒拉族自治县', 3, NULL, 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('847aea69634f774d69f0a372edfa7760', '230227', '230200', '富裕县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('8497e5dd6f686a68c8dbb2d8be06a850', '152201', '152200', '乌兰浩特市', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('84a87330454970c692c02015cb707581', '513338', '513300', '得荣县', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('84b9afac359aecd5826644a624c12029', '451123', '451100', '富川瑶族自治县', 3, NULL, 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('84d97392354c47fb9dc6dc090007241b', '331022', '331000', '三门县', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('84e89a191bc55d809ffee857995de291', '230110', '230100', '香坊区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('84ed52b073be0926566c5f564e1fb575', '130184', '130100', '新乐市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('85290d40d838c2bc5f2a2836f12dabef', '522629', '522600', '剑河县', 3, NULL, 1, 1647931271, 1681977731);
INSERT INTO `yunduan_district` VALUES ('8594fe5b69e0822eaa949ffc3a4db92c', '120113', '120100', '北辰区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('859e3ef688ea39dc689a843e03b93ac2', '210921', '210900', '阜新蒙古族自治县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('85c5efa64b49bce398f19f6b899a1f53', '222402', '222400', '图们市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('85c9284bf4b97b371a3751340efbd157', '110111', '110100', '房山区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('85e5e7a4abbb3272ba199b96d1813537', '659003', '659000', '图木舒克市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('85e8a1910419cec4bda648fa9537e495', '441302', '441300', '惠城区', 3, NULL, 1, 1647931271, 1681975585);
INSERT INTO `yunduan_district` VALUES ('85f038afc079c589a791dc9eac83d348', '520623', '520600', '石阡县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('85f334207619c9645dae5ace20c8f838', '350921', '350900', '霞浦县', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('85f6ca2f4f0cdabfe2c6814980e79281', '513226', '513200', '金川县', 3, NULL, 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('85f90fe01792f2819c4efd229ecfcdca', '653001', '653000', '阿图什市', 3, NULL, 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('85f97cb946a596f6830dc05955032b6c', '630202', '630200', '乐都区', 3, NULL, 1, 1647931271, 1681978315);
INSERT INTO `yunduan_district` VALUES ('860df224b0d93b4c0ef2e068475b4843', '371502', '371500', '东昌府区', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('86152503b0f15eea8706d007dc24e5c2', '650205', '650200', '乌尔禾区', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('861aa8ffa68c0890170a038437bf72c7', '360923', '360900', '上高县', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('863c9713177ddfa8610b24be1b5ab4e9', '410923', '410900', '南乐县', 3, NULL, 1, 1647931271, 1681975526);
INSERT INTO `yunduan_district` VALUES ('863f9d7487de5b538a14afa75c9a75f6', '230206', '230200', '富拉尔基区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('8646ae2cbe9721b89e499b08a12eb149', '371312', '371300', '河东区', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('8647ce5867f6eedbb37a917b30ba8956', '451000', '450000', '百色市', 2, '{\"county\": 12}', 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('864aa2df64e3b454705cc5fcd9b8647c', '451100', '450000', '贺州市', 2, '{\"county\": 5}', 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('86684ea81b1616bdd9d0055ef8afdb55', '460106', '460100', '龙华区', 3, NULL, 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('867d264f93c57e36fbf570f8466c0a78', '140107', '140100', '杏花岭区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('8684e24d53f3d18098f6ed1b79628315', '640402', '640400', '原州区', 3, NULL, 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('869bf9d5c219595bb71167022a900079', '140222', '140200', '天镇县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('86adcbc4a192dfea0ac4770e2d5d5ff3', '610222', '610200', '宜君县', 3, NULL, 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('86aff005e8fd56aedf7dabd1e6a4e1c2', '451228', '451200', '都安瑶族自治县', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('86b3364765e034e2c2bf01cd0bf11032', '500108', '500100', '南岸区', 3, NULL, 1, 1647931271, 1681977450);
INSERT INTO `yunduan_district` VALUES ('86d27f5b59287acbddf43d681281ee88', '530623', '530600', '盐津县', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('86ec4a2a1447b71372524933b6cf1f4b', '150802', '150800', '临河区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('86f6c14ddfd9dbfc3b4f75f7fc4d21ce', '450300', '450000', '桂林市', 2, '{\"county\": 17}', 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('874b4aa8c56557c0fbcb85994aadc441', '220122', '220100', '农安县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('8766bbdaa8d936a5e1b2ccce871e93f9', '411082', '411000', '长葛市', 3, NULL, 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('8766e0e48c419ae62b7fd552d82902c1', '652723', '652700', '温泉县', 3, NULL, 1, 1647931271, 1681978502);
INSERT INTO `yunduan_district` VALUES ('877ef7bda81d40ce93150ba3ec507b81', '410311', '410300', '洛龙区', 3, NULL, 1, 1647931271, 1681975502);
INSERT INTO `yunduan_district` VALUES ('87888acfcd1589d7fa73fb364cc57d47', '360222', '360200', '浮梁县', 3, NULL, 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('878a550b6c783334268db1db2cda7434', '610902', '610900', '汉滨区', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('87b9148b55a14b13fd2f31e07e8ab445', '341003', '341000', '黄山区', 3, NULL, 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('87d9e496b55fe309b72228291990f0f7', '411702', '411700', '驿城区', 3, NULL, 1, 1647931271, 1681975522);
INSERT INTO `yunduan_district` VALUES ('87dbb5f2a435bf8fcf3e1745f20f3f1f', '530325', '530300', '富源县', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('87f539ee50591ba4072b82b4d8dbf2d3', '540424', '540400', '波密县', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('87f6c9a0037e624e34c82630b1fe4b42', '140214', '140200', '云冈区', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('87fc479c44bd555f710b37c9550bc57c', '230000', '0', '黑龙江省', 1, '{\"city\": 13, \"county\": 121}', 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('8802a8c4b8c217b2c3d060ebf03b0112', '440900', '440000', '茂名市', 2, '{\"county\": 5}', 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('8808d00754597fddd0d95b2f4358cb99', '500235', '500200', '云阳县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('880db41390bd2e9efc47acb39c5e86b8', '431000', '430000', '郴州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('8829474322a0b4f519a1da09520edc32', '341322', '341300', '萧县', 3, NULL, 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('88494a0fbbf346a500c179736ae05c51', '430922', '430900', '桃江县', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('885407a1d53860828d01c53ed43977d1', '621102', '621100', '安定区', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('885c3cee2c44556659fe8fd98a1b6a93', '371722', '371700', '单县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('885cdedb6e4aac146d4d349eb7a7fcb4', '630104', '630100', '城西区', 3, NULL, 1, 1647931271, 1681978315);
INSERT INTO `yunduan_district` VALUES ('88613c2d3eda21787b6643534ff0cc47', '370404', '370400', '峄城区', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('887ef924c6f1275d8c63513b07dcba65', '340603', '340600', '相山区', 3, NULL, 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('88821dd2d9152b8dcbf997979df503c2', '140681', '140600', '怀仁市', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('88a5d8984d0e20ecc772c306e44814f8', '500118', '500100', '永川区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('88db6f6dc1b42b732c9221b97d69915c', '621100', '620000', '定西市', 2, '{\"county\": 7}', 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('88ddbeb87fa287771078bba166f9df6d', '150627', '150600', '伊金霍洛旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('891b646d4b31b81c60023bbd8ab9aa5b', '620111', '620100', '红古区', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('89767ae987f3192f418ad9f8069561df', '512000', '510000', '资阳市', 2, '{\"county\": 3}', 1, 1647931271, 1681977527);
INSERT INTO `yunduan_district` VALUES ('897c2de2991d065c600a013dd15a19b8', '230900', '230000', '七台河市', 2, '{\"county\": 4}', 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('897d1955ebf94bc82f489b5337b9aaf9', '370305', '370300', '临淄区', 3, NULL, 1, 1647931271, 1681975486);
INSERT INTO `yunduan_district` VALUES ('8985f478d5d6af5cc889b7be2be85168', '210882', '210800', '大石桥市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('8993ceab621a4235b413a53d621694b8', '140406', '140400', '潞城区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('89b4ccd5b4c2f3995e5696589632577b', '150782', '150700', '牙克石市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('89da927fc360efda222b081127ac541f', '370203', '370200', '市北区', 3, NULL, 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('89ef1b70e05ab21097beae9c8313642c', '110118', '110100', '密云区', 3, NULL, 1, 1647931271, 1663565983);
INSERT INTO `yunduan_district` VALUES ('8a2f60c2186ec53bc51a3a56888709ec', '210212', '210200', '旅顺口区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('8a358fccc388cc04d329a11550d059c7', '520624', '520600', '思南县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('8a48a19ffc20a1d6e7116aaa61ae8827', '410200', '410000', '开封市', 2, '{\"county\": 9}', 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('8a5c63e75e22a17b572c77cf79a750e9', '530622', '530600', '巧家县', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('8a727928408050940780e9725424f528', '420304', '420300', '郧阳区', 3, NULL, 1, 1647931271, 1681975548);
INSERT INTO `yunduan_district` VALUES ('8a75bbe37d1cde393a38b0b1b8b6b822', '440606', '440600', '顺德区', 3, NULL, 1, 1647931271, 1681975593);
INSERT INTO `yunduan_district` VALUES ('8a8a4bd3c1c712748dbbeb07fb28d433', '441882', '441800', '连州市', 3, NULL, 1, 1647931271, 1681975589);
INSERT INTO `yunduan_district` VALUES ('8a9085ce073c4bafc92025f353ea4095', '230109', '230100', '松北区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('8aaf65a009ef1a8807c4a8ccc9a203da', '511124', '511100', '井研县', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('8ab3fffb034ca2c59c193bff5fc17573', '520424', '520400', '关岭布依族苗族自治县', 3, NULL, 1, 1647931271, 1681977720);
INSERT INTO `yunduan_district` VALUES ('8ac8bfe28368b0b6b9d323bd87a581f6', '431103', '431100', '冷水滩区', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('8ad464829ddddcc27a7d260754ba62d9', '130302', '130300', '海港区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('8ad99482fc157ecc13cf78a4785b8db7', '640422', '640400', '西吉县', 3, NULL, 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('8b106edbe4fa1874ae83d06d290fde27', '511529', '511500', '屏山县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('8b1c107e55c4797f6c18273046956610', '370102', '370100', '历下区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('8b25807cf09d1458f90478726f82776e', '231281', '231200', '安达市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('8b3e2bd29935bdf0d68029c70caacbc2', '421224', '421200', '通山县', 3, NULL, 1, 1647931271, 1681975549);
INSERT INTO `yunduan_district` VALUES ('8b54a99561e72bfec914136b8061b58c', '511402', '511400', '东坡区', 3, NULL, 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('8b681a7c6471c4cca31699a8d9d4182f', '420703', '420700', '华容区', 3, NULL, 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('8b6cbb7d03521f7489a2c2afda72dbb6', '341000', '340000', '黄山市', 2, '{\"county\": 7}', 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('8b771e5d9e23b39a70cf4b33988b259b', '150724', '150700', '鄂温克族自治旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('8b7ff3d9f6c09484c6655886ea8a630a', '431125', '431100', '江永县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('8b9f3d2dece0ecb31a40ed5c29a34d6c', '371581', '371500', '临清市', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('8baf18616f6a3bcc70ffc7acce705d37', '610726', '610700', '宁强县', 3, NULL, 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('8bc437979977d8e6a9b38202b196f811', '320804', '320800', '淮阴区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('8be1410ff2dcab22abb94584ec44f417', '511400', '510000', '眉山市', 2, '{\"county\": 6}', 1, 1647931271, 1681977517);
INSERT INTO `yunduan_district` VALUES ('8beb56d3e59f780ae597cfa9e247d8fa', '320585', '320500', '太仓市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('8c0b32147c36d581e75e408e78046b7f', '440118', '440100', '增城区', 3, NULL, 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('8c30fa6fd5c9faf515ed085ddead3267', '230603', '230600', '龙凤区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('8c5a4791945510e4bf89d81bcfbe380e', '341024', '341000', '祁门县', 3, NULL, 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('8c5d6cd2d2e0ceacdb59ab601a8319d9', '360881', '360800', '井冈山市', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('8c67157faa9ec5d6d98fbde0d43029e0', '450722', '450700', '浦北县', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('8c697c7384efd48ea596683148ee71d3', '152500', '150000', '锡林郭勒盟', 2, '{\"county\": 12}', 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('8c7699aca54cb83a4b657759ceef0f5b', '500119', '500100', '南川区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('8c9007bb8b7e98fbdf8db48cff763bf0', '530602', '530600', '昭阳区', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('8c91673378718a21224c4b9d2d144dfc', '510113', '510100', '青白江区', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('8c95069cad6d84a21630863e6ab04844', '370685', '370600', '招远市', 3, NULL, 1, 1647931271, 1681975474);
INSERT INTO `yunduan_district` VALUES ('8c9e5daf43111373bf851febcd6a5212', '131023', '131000', '永清县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('8cd7ee525a0c6af41d823e857080b98f', '530126', '530100', '石林彝族自治县', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('8cf03bbdbdda917183dc668f69ce69fb', '652701', '652700', '博乐市', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('8cf6c9af1472c189c6acc1cc5e30f3fd', '211221', '211200', '铁岭县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('8d03a1c07fadbdcc62d22d3f5dd86997', '451425', '451400', '天等县', 3, NULL, 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('8d3b9caffd1d598b0df831dbb684bced', '632725', '632700', '囊谦县', 3, NULL, 1, 1647931271, 1681978311);
INSERT INTO `yunduan_district` VALUES ('8d4b20c61ac248643396c69bebeabd4d', '330105', '330100', '拱墅区', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('8d5eaf8c1086ea2224c57d67923aefc2', '370306', '370300', '周村区', 3, NULL, 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('8d91b70b5c5ca35663d5e06d0276c486', '530926', '530900', '耿马傣族佤族自治县', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('8db7d1a0bb5dc83cb42808ecbf981e99', '130637', '130600', '博野县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('8dbb03a120d6945ea948ae5398629d97', '330727', '330700', '磐安县', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('8dc97415a025b8af6ef792f4f1c41a15', '510182', '510100', '彭州市', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('8df317100fc18fbf3cef460b88595dec', '321322', '321300', '沭阳县', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('8e0c7e3905b9f812edae86a4d884bb32', '130506', '130500', '南和区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('8e1b109e28419a770d0642f88112d5ba', '610831', '610800', '子洲县', 3, NULL, 1, 1647931271, 1681978065);
INSERT INTO `yunduan_district` VALUES ('8e2a43b2cd06ab6cab910ca5ef0d692d', '350624', '350600', '诏安县', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('8e2f74eed519ec7c3aeb696b4675627b', '610603', '610600', '安塞区', 3, NULL, 1, 1647931271, 1681978079);
INSERT INTO `yunduan_district` VALUES ('8e443ce45ecd47acd9fe8bf282d372d1', '230230', '230200', '克东县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('8e6ab5cb01e729672d4b47ca4bec59ff', '520329', '520300', '余庆县', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('8e70c240509c417f6a31f8a7ba48bbb2', '150603', '150600', '康巴什区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('8e738241096845dde13a883746d472d4', '520302', '520300', '红花岗区', 3, NULL, 1, 1647931271, 1681977730);
INSERT INTO `yunduan_district` VALUES ('8e73c7177e8c1c6b23533dee85e388a1', '131003', '131000', '广阳区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('8e7b5d85dc7b226a272f08f967a59e0f', '340522', '340500', '含山县', 3, NULL, 1, 1647931271, 1681975391);
INSERT INTO `yunduan_district` VALUES ('8e7bb740461a3eb8dd9015de5ec4bd3e', '530823', '530800', '景东彝族自治县', 3, NULL, 1, 1647931271, 1681977875);
INSERT INTO `yunduan_district` VALUES ('8e866e81d71a99ed84140ea2354238d6', '440514', '440500', '潮南区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('8e9ea34f793630e9907ac5b74a38dcad', '451223', '451200', '凤山县', 3, NULL, 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('8eb86030d48a8510e23f3a72f04d849c', '650500', '650000', '哈密市', 2, '{\"county\": 3}', 1, 1647931271, 1681978496);
INSERT INTO `yunduan_district` VALUES ('8ebc3b8778ce3cf652dff8e8d4cf98cc', '652301', '652300', '昌吉市', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('8ee171157761af435098d4195c8979f1', '513224', '513200', '松潘县', 3, NULL, 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('8ef89b97c3eb69444ca6b19bda7e7a67', '511323', '511300', '蓬安县', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('8f1e5ecf82de6ecb1d34e903c4c0f8bd', '340421', '340400', '凤台县', 3, NULL, 1, 1647931271, 1681975382);
INSERT INTO `yunduan_district` VALUES ('8f264b9d54fc455e9eb71773ef3a75ed', '520526', '520500', '威宁彝族回族苗族自治县', 3, NULL, 1, 1647931271, 1681977723);
INSERT INTO `yunduan_district` VALUES ('8f6d00cd539077fc1814d11e356d0cf7', '430621', '430600', '岳阳县', 3, NULL, 1, 1647931271, 1681975561);
INSERT INTO `yunduan_district` VALUES ('8f6d24c0292d43ad6ebf4260781e3866', '320411', '320400', '新北区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('8f6e90b965b13f31d48a2fb8e6a7b5f3', '652700', '650000', '博尔塔拉蒙古自治州', 2, '{\"county\": 4}', 1, 1647931271, 1681978502);
INSERT INTO `yunduan_district` VALUES ('8f6ea9886636b63344e88285c682662b', '610528', '610500', '富平县', 3, NULL, 1, 1647931271, 1681978077);
INSERT INTO `yunduan_district` VALUES ('8f9934ba8b35bef79fa39fc435d0ad48', '340223', '340200', '南陵县', 3, NULL, 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('8faac6b03ef8b5d57e5768537b11495c', '131026', '131000', '文安县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('8fb2a2efa22e9143956ac4d1d75ac360', '230231', '230200', '拜泉县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('8fbb7245c130018d1ec02117c5356e1e', '540123', '540100', '尼木县', 3, NULL, 1, 1647931271, 1681977991);
INSERT INTO `yunduan_district` VALUES ('8fbdf27e912fc0aefe567d42b5f7629e', '420607', '420600', '襄州区', 3, NULL, 1, 1647931271, 1681975531);
INSERT INTO `yunduan_district` VALUES ('8fc2219186bef77646804c6ab03636d1', '421023', '421000', '监利县', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('8fc291eb84b3e744e2031e3910b9cced', '520627', '520600', '沿河土家族自治县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('8fc4f9252cd5bc159b8a148b59687d3a', '150000', '0', '内蒙古自治区', 1, '{\"city\": 12, \"county\": 103}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('8fdc9e1f948c4fb1bf8def81f540d3f7', '520115', '520100', '观山湖区', 3, NULL, 1, 1647931271, 1681977724);
INSERT INTO `yunduan_district` VALUES ('8fee2ead4d7e1722ecdffab4a790f2c9', '469026', '469000', '昌江黎族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('8ff3f92d7596632031fbcb2f952da9dc', '222400', '220000', '延边朝鲜族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('8ff5cb8c790daf5c886ce177da1ad1c9', '513201', '513200', '马尔康市', 3, NULL, 1, 1647931271, 1681977525);
INSERT INTO `yunduan_district` VALUES ('90054c7212471e3fa2e0088ccaa07f7c', '220106', '220100', '绿园区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('900e54834219f3ec0a5c6d2b1768b5f8', '530829', '530800', '西盟佤族自治县', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('902f08faf0b2acf1d744e1e5a27f1f06', '230725', '230700', '大箐山县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('90331031e51d23904ef72f740d89444f', '340882', '340800', '潜山市', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('905cc46533cc12cdcc8ffc14442298c3', '230113', '230100', '双城区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('906bcba74c683308ec2a0d125d26b2c3', '621123', '621100', '渭源县', 3, NULL, 1, 1647931271, 1681978254);
INSERT INTO `yunduan_district` VALUES ('9081cbc5b5c17f9594844c0342059dca', '360734', '360700', '寻乌县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('908a76894e55991c26d6f181604dbe19', '361028', '361000', '资溪县', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('908e70a0fc0ff601c9543faba078b249', '430408', '430400', '蒸湘区', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('90b8c841cf65b5fa96b3867c0b22f780', '431225', '431200', '会同县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('90ce2844cb2bbc2a7e4b240047f900c1', '110102', '110100', '西城区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('90d202d5c51e600a721656523f620123', '320305', '320300', '贾汪区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('90d699897bd0f45e2f2386edf0e687f5', '510106', '510100', '金牛区', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('90fc66bb8310d2f03dba3a6fcc065f31', '130535', '130500', '临西县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('9109173934c0e1be862d9ba6a7f62548', '331126', '331100', '庆元县', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('912cfd638c9f0768cb723299bd8944e0', '652300', '650000', '昌吉回族自治州', 2, '{\"county\": 7}', 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('913832a191c80b0d550da8ccabdbe49a', '610922', '610900', '石泉县', 3, NULL, 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('915038918e88241ab6f5cec1c2ffde02', '411425', '411400', '虞城县', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('918a534ecdac3f7f301f6371f2f43aeb', '653023', '653000', '阿合奇县', 3, NULL, 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('91916eb9f2974ea621f8d1d40620392f', '350111', '350100', '晋安区', 3, NULL, 1, 1647931271, 1681975418);
INSERT INTO `yunduan_district` VALUES ('919a047b0fc00f2c7922b955eada13e2', '530112', '530100', '西山区', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('91d99b1822a0976fff0c4904bc3a138e', '320723', '320700', '灌云县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('91e156fe5285d46c5ebd008e8e46b817', '361027', '361000', '金溪县', 3, NULL, 1, 1647931271, 1681975443);
INSERT INTO `yunduan_district` VALUES ('920e8a031ebdd72d86946e14d47b6ced', '230208', '230200', '梅里斯达斡尔族区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('9226bec5e695c622d47d7528c4157aaf', '511623', '511600', '邻水县', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('9239aded6bdffea12f1aca83c75054ae', '530800', '530000', '普洱市', 2, '{\"county\": 10}', 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('924d0d65e35f0c92b9a7f9e31012cac6', '640000', '0', '宁夏回族自治区', 1, '{\"city\": 5, \"county\": 22}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('925380698d28ef5886c685077490edd4', '140723', '140700', '和顺县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('925c403e2b93ae640782f672bfa13682', '150525', '150500', '奈曼旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('925e73e74ec8efe52981a7ef6b142539', '522601', '522600', '凯里市', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('9290a3aa9b6cae07aa88fd57f012d2ac', '350527', '350500', '金门县', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('929ca9fb5fe9d8598888a527cd543c57', '150300', '150000', '乌海市', 2, '{\"county\": 3}', 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('929daea50295ec90331bc7f6b66ba672', '310114', '310100', '嘉定区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('92a67068cf7073b5607ae0f0a226e1a6', '610829', '610800', '吴堡县', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('92abe3a920f489b29ecb1204aae079fc', '500155', '500100', '梁平区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('92b84e2dc43b73ed3135c71bc24b18bf', '420900', '420000', '孝感市', 2, '{\"county\": 7}', 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('92ce377daa442eea4830901c7fd495c6', '620502', '620500', '秦州区', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('92d5a513ef50e2e98f97597a823506fa', '350425', '350400', '大田县', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('92ea6b1d7479ee246af4871ffe277d25', '330602', '330600', '越城区', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('92fd36974f0017fe79d588e85a09c609', '530821', '530800', '宁洱哈尼族彝族自治县', 3, NULL, 1, 1647931271, 1681977876);
INSERT INTO `yunduan_district` VALUES ('93414d8d4ec2a73ad6a3c5e2b024e43e', '430105', '430100', '开福区', 3, NULL, 1, 1647931271, 1681975564);
INSERT INTO `yunduan_district` VALUES ('93509cf7fd602874d891bebd0aff6693', '532931', '532900', '剑川县', 3, NULL, 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('935bb3621ceba4060bc081ec439e389a', '420804', '420800', '掇刀区', 3, NULL, 1, 1647931271, 1681975542);
INSERT INTO `yunduan_district` VALUES ('935ef9411ee33f0638e1d84d97bdf525', '360103', '360100', '西湖区', 3, NULL, 1, 1647931271, 1681975450);
INSERT INTO `yunduan_district` VALUES ('9386e0021065113595b0c97a6af701b2', '141182', '141100', '汾阳市', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('938d56f31a6e9980c31bbce43e5bdca2', '210411', '210400', '顺城区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('93a1e9c6b5c059ef322778a674f16a36', '110000', '0', '北京市', 1, '{\"city\": 1, \"county\": 16}', 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('93b0f4b7c624dbcdb70e4f2dadd6df7e', '533103', '533100', '芒市', 3, NULL, 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('93b1a7e9932fbf18f8e5d637b00b02cd', '623024', '623000', '迭部县', 3, NULL, 1, 1647931271, 1681978243);
INSERT INTO `yunduan_district` VALUES ('93c576de3b4ef996e0d38490618f94ea', '130623', '130600', '涞水县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('93d8df156ea219c8e5ecdf6f7a1f3c59', '420205', '420200', '铁山区', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('940caf313e5fb62ce2b4f6c4322e3105', '410329', '410300', '伊川县', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('940fcbb21682e82a0d28d4838a94845e', '211300', '210000', '朝阳市', 2, '{\"county\": 7}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('942690c9e2ab58e177bc5b2d4befc6c2', '450000', '0', '广西壮族自治区', 1, '{\"city\": 14, \"county\": 111}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('9427af44f2540965c5c2756a1936a459', '440104', '440100', '越秀区', 3, NULL, 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('943cd6d909491b3454d9681c5de3e9c7', '610923', '610900', '宁陕县', 3, NULL, 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('94436b863f5b10ceca086af334e6df8c', '510403', '510400', '西区', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('944d9218120a4981532d4fc74f03708d', '653125', '653100', '莎车县', 3, NULL, 1, 1647931271, 1681978490);
INSERT INTO `yunduan_district` VALUES ('9485a6dbb5b89b0eafe8f8cef0ec04e6', '140825', '140800', '新绛县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('948d50893b89576a60b8aba3b186c4b9', '350725', '350700', '政和县', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('949d368db6478c6234bdbedbbaec1d79', '440981', '440900', '高州市', 3, NULL, 1, 1647931271, 1681975582);
INSERT INTO `yunduan_district` VALUES ('94a4a1d9ed622f22657b96f3a007acfc', '450327', '450300', '灌阳县', 3, NULL, 1, 1647931271, 1681975631);
INSERT INTO `yunduan_district` VALUES ('94a96d9383aadb3976e90a24c4ca36fd', '421100', '420000', '黄冈市', 2, '{\"county\": 10}', 1, 1647931271, 1681975543);
INSERT INTO `yunduan_district` VALUES ('94ca493b276df0b98e261cdae44ed2dd', '150422', '150400', '巴林左旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('94cfb1575510bf09e1a87a49755b0c81', '610700', '610000', '汉中市', 2, '{\"county\": 11}', 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('94d264798afc46071b0ae2103e6dab87', '350981', '350900', '福安市', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('94d9ed790835a458684e18c87f57c08c', '210811', '210800', '老边区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('94ddba0714c7c9b28847f0d56c80a90a', '130183', '130100', '晋州市', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('94f168a4dcff6bb23445f16687dba613', '532529', '532500', '红河县', 3, NULL, 1, 1647931271, 1681977884);
INSERT INTO `yunduan_district` VALUES ('94f9401cdac5257c1cd469bc4b070ea7', '211382', '211300', '凌源市', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('94f9f37613b095b31d57e4c4926337b8', '610429', '610400', '旬邑县', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('94fed7ea81141a33a04dbbded386e917', '532503', '532500', '蒙自市', 3, NULL, 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('950bc07117d7faa8e08269b9267af4ea', '370724', '370700', '临朐县', 3, NULL, 1, 1647931271, 1681975487);
INSERT INTO `yunduan_district` VALUES ('95252a832ab4b7d47e9053825b35010c', '451302', '451300', '兴宾区', 3, NULL, 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('954e12a6fa2519ad9645289b40cb06b7', '620500', '620000', '天水市', 2, '{\"county\": 7}', 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('95672b7c2d3779a5ff43c9dd984c4dd2', '420504', '420500', '点军区', 3, NULL, 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('959d04bfde897be6d1ad9b9be873aa01', '361126', '361100', '弋阳县', 3, NULL, 1, 1647931271, 1681975456);
INSERT INTO `yunduan_district` VALUES ('95b307fac10f8cc62b5f98f52b316f01', '210103', '210100', '沈河区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('95b7c38e464fd035bdb96d86b1c1d616', '130534', '130500', '清河县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('95e3fcc08f630ce138ddba535660839f', '321311', '321300', '宿豫区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('95e75cb2378c0e8f03f56845d6967fd0', '410425', '410400', '郏县', 3, NULL, 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('9604aeb229dce930b77a176a5eaa4465', '532622', '532600', '砚山县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('9628dad9ff8bc53c8ed22757789c5333', '421202', '421200', '咸安区', 3, NULL, 1, 1647931271, 1681975549);
INSERT INTO `yunduan_district` VALUES ('964345981b08dfaaf10cafb422e81f6e', '220382', '220300', '双辽市', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('967690c4e7067b60e982ab5aad2855dc', '360203', '360200', '珠山区', 3, NULL, 1, 1647931271, 1681975447);
INSERT INTO `yunduan_district` VALUES ('969833d427ed54f6bc55f32dbf09a3a8', '511703', '511700', '达川区', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('969ad8f4bcab2dab1fa17ae6ef8e3c0f', '621027', '621000', '镇原县', 3, NULL, 1, 1647931271, 1681978246);
INSERT INTO `yunduan_district` VALUES ('969c37309bf2abc2415c605742925c96', '340405', '340400', '八公山区', 3, NULL, 1, 1647931271, 1681975382);
INSERT INTO `yunduan_district` VALUES ('96d18b6ff9b2b7434376c36a0c3deda5', '510400', '510000', '攀枝花市', 2, '{\"county\": 5}', 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('970ea431a02164b131d08da844b8393d', '451323', '451300', '武宣县', 3, NULL, 1, 1647931271, 1681975625);
INSERT INTO `yunduan_district` VALUES ('97131a0b7288e4b15e3aae4269481ee1', '654201', '654200', '塔城市', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('974527d678ebabe80109c50f2fb69903', '340323', '340300', '固镇县', 3, NULL, 1, 1647931271, 1681975372);
INSERT INTO `yunduan_district` VALUES ('9751365e7a4524e19383ec6b0d9cf3e9', '330203', '330200', '海曙区', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('975a2700f2db203c379d8ad4ba0f699a', '330305', '330300', '洞头区', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('975ced735dbb2d51a5089624b980ab8f', '371622', '371600', '阳信县', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('97b5e301a63b1ffd811519700934359d', '441625', '441600', '东源县', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('97b872f729e111f33ba5ccfcebb8d630', '513434', '513400', '越西县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('97dc6103abaef30d2d5a03fa2f741007', '341524', '341500', '金寨县', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('97e248c3573a86e6d18d6ef69b48dd6e', '130505', '130500', '任泽区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('97eb56cfbfa963a85dcf7dfc7cf27707', '510683', '510600', '绵竹市', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('97ffbdbebfe96d1c665f9fcb747cce5c', '330402', '330400', '南湖区', 3, NULL, 1, 1647931271, 1681975345);
INSERT INTO `yunduan_district` VALUES ('9808d987f3ed8c260fbf3a7b153605de', '371603', '371600', '沾化区', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('980cf020a1648186e5361b1cd9f447df', '210202', '210200', '中山区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('98107578153234ac0c5053f030c97991', '510824', '510800', '苍溪县', 3, NULL, 1, 1647931271, 1681977529);
INSERT INTO `yunduan_district` VALUES ('981e71e6d30dcc5ee2da9a2e85f3ac0d', '230826', '230800', '桦川县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('981e7eea9a7f9978651a10348fd221a3', '231182', '231100', '五大连池市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('983816f2008a6f5bec33bd44d77cb92a', '140423', '140400', '襄垣县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('983b7a1147bb6f4eb288317f8e368b52', '152921', '152900', '阿拉善左旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('983e25308c70c413e311ec5e993a1024', '441223', '441200', '广宁县', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('9842dc7344287cef997911c00be2f6e3', '350426', '350400', '尤溪县', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('984f289094b7b58f5490e11e5dee32cf', '150102', '150100', '新城区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('985a661f9a7e4a1f8ab6bb7d3809ea33', '513335', '513300', '巴塘县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('985aa1543693b124f1369d6dfa8ba6f5', '150124', '150100', '清水河县', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('98612f233308ee707530731756cdc8eb', '540202', '540200', '桑珠孜区', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('98676aed6818bebe896936f5a1ebfc0f', '370614', '370600', '蓬莱区', 3, NULL, 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('988ae46b55eb73b4940f2a05f39be653', '350402', '350400', '梅列区', 3, NULL, 1, 1647931271, 1681975420);
INSERT INTO `yunduan_district` VALUES ('98a5acfafe4d7cd6774b073017a17b06', '620302', '620300', '金川区', 3, NULL, 1, 1647931271, 1681978245);
INSERT INTO `yunduan_district` VALUES ('98ac01c413d3b0365a475830d0b3dbe3', '610330', '610300', '凤县', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('98bbc814a1ae14b4b9b73ae73ac3356e', '350923', '350900', '屏南县', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('98d2ef48904f9548a2341a8b079d249b', '533100', '530000', '德宏傣族景颇族自治州', 2, '{\"county\": 5}', 1, 1647931271, 1681977876);
INSERT INTO `yunduan_district` VALUES ('9909789a1ca8c9e2d08e3e0a043f31ca', '370702', '370700', '潍城区', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('9913e0bb25240be024741e9d8c8efb9f', '140428', '140400', '长子县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('992df95858644ccd3d5621c8427c97b6', '140900', '140000', '忻州市', 2, '{\"county\": 14}', 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('9959a0d7cde250c2aa47f458535b1ed0', '130602', '130600', '竞秀区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('998481d9a251a98660d91ddb304293ff', '360828', '360800', '万安县', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('99c748e93f9324616495a0e79a17198b', '620722', '620700', '民乐县', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('99daba54f1e160a388eb67ffb29bbd27', '530302', '530300', '麒麟区', 3, NULL, 1, 1647931271, 1681977879);
INSERT INTO `yunduan_district` VALUES ('99e21898da50de22b7a5998894a87ad8', '130425', '130400', '大名县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('9a21b3220af8bdcfb4aefcd479180d2f', '522730', '522700', '龙里县', 3, NULL, 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('9a305520a4210afacd3509a81e20f0b4', '130128', '130100', '深泽县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('9a511266c08e7d7d38050b44716a03f2', '130209', '130200', '曹妃甸区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('9a59ade1d7e1f4080bd6359f97df39e9', '511133', '511100', '马边彝族自治县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('9a68227b8b6c2032000936bcd252437a', '411423', '411400', '宁陵县', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('9a6ef8ea52ebfe2dd6fd8fba67b28e1a', '430528', '430500', '新宁县', 3, NULL, 1, 1647931271, 1681975568);
INSERT INTO `yunduan_district` VALUES ('9a754de93559c5467403d34f4840dab6', '422802', '422800', '利川市', 3, NULL, 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('9a9e2f31d2dd01da4f7f06160ee7c173', '630123', '630100', '湟源县', 3, NULL, 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('9a9f372c4d796f8d72da5b158371cc7c', '150400', '150000', '赤峰市', 2, '{\"county\": 12}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('9abba5d9eee3b8522f682e9e2bb4c05c', '530927', '530900', '沧源佤族自治县', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('9aefdfe8a91ac2f9624138a90e33fdef', '540300', '540000', '昌都市', 2, '{\"county\": 11}', 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('9b012b0a53187e049cd31b015210a4ed', '360732', '360700', '兴国县', 3, NULL, 1, 1647931271, 1681975446);
INSERT INTO `yunduan_district` VALUES ('9b09ceac36e315071451c889206f1dab', '654323', '654300', '福海县', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('9b1667f2653c4712e3c6d7e0fe8a8ba4', '330382', '330300', '乐清市', 3, NULL, 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('9b62635dda3c4cd62b1af96c572b32d3', '451225', '451200', '罗城仫佬族自治县', 3, NULL, 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('9b638eed805fce56379262fd1c4bcc9a', '150404', '150400', '松山区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('9b7c00754f210678e63c56cd33591341', '511111', '511100', '沙湾区', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('9baa0fed5bd3ce566a7f6b8992917113', '320300', '320000', '徐州市', 2, '{\"county\": 10}', 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('9bbefec93039f6fff245995d1b9ccae6', '340121', '340100', '长丰县', 3, NULL, 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('9bdbcaaf88daa2fc8774d5a3d04dc2f8', '361102', '361100', '信州区', 3, NULL, 1, 1647931271, 1681975456);
INSERT INTO `yunduan_district` VALUES ('9be0719a11c692cd4dc4a6994f32fa68', '520203', '520200', '六枝特区', 3, NULL, 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('9bf5f8f8ff76cab985f9a74ed9fecb3a', '652801', '652800', '库尔勒市', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('9c1f01c27cea8573c0c1037ee7f3f5d0', '410523', '410500', '汤阴县', 3, NULL, 1, 1647931271, 1681975509);
INSERT INTO `yunduan_district` VALUES ('9c46cda3b582010e8afc1be1882c9a27', '370681', '370600', '龙口市', 3, NULL, 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('9c66e0d99290562a84484d462c00490d', '620724', '620700', '高台县', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('9c7123bf6b4f0a3fa739067448303f25', '120100', '120000', '天津市', 2, '{\"county\": 16}', 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('9c74c5d8a19632c50798cb5464b3905e', '231085', '231000', '穆棱市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('9c9e4e4095188a8d23c04bc107c12a1f', '411728', '411700', '遂平县', 3, NULL, 1, 1647931271, 1681975523);
INSERT INTO `yunduan_district` VALUES ('9ca13a11e83f834a3104457c8305c488', '440512', '440500', '濠江区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('9cab13b9b0ca8e9e039da23d56131884', '511024', '511000', '威远县', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('9caf61010207b5aefda0216e8ac1a72a', '410303', '410300', '西工区', 3, NULL, 1, 1647931271, 1681975502);
INSERT INTO `yunduan_district` VALUES ('9cc38d70b6e19e892398d01a8e61722e', '420111', '420100', '洪山区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('9ce43b6c8dbd929f3bad4acfca527a73', '370105', '370100', '天桥区', 3, NULL, 1, 1647931271, 1681975490);
INSERT INTO `yunduan_district` VALUES ('9cfaea63bc0e881b48005dee117f2842', '451027', '451000', '凌云县', 3, NULL, 1, 1647931271, 1681975629);
INSERT INTO `yunduan_district` VALUES ('9d157a66bdbf645c00f4e44de80a67ca', '540524', '540500', '琼结县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('9d1aebb660e7595133ec8a94e5443c85', '371526', '371500', '高唐县', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('9d1e9d84470d7adcfc2e53f17f3ceaf0', '220322', '220300', '梨树县', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('9d22aead472f5c3f9651e8b02e4bdb8b', '510114', '510100', '新都区', 3, NULL, 1, 1647931271, 1681977533);
INSERT INTO `yunduan_district` VALUES ('9d2581598c4dbcd4915116d01079b590', '542524', '542500', '日土县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('9d34498d9177bb4e603d6a72e8c4e9a0', '370613', '370600', '莱山区', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('9d3dd067e7e95ccfd3ed372d367a1565', '430407', '430400', '石鼓区', 3, NULL, 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('9d6fde1e91fec75e0b87ff57b539df59', '610481', '610400', '兴平市', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('9d7da82910520445ef4c83fb8995bbbe', '130426', '130400', '涉县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('9d853d2c4c21f241f9c0047395fe3a4b', '210726', '210700', '黑山县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('9d894be7a5c2c1004905017a924b0f16', '131124', '131100', '饶阳县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('9da690adaefbb1a93123d8b8f11cf485', '130533', '130500', '威县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('9dadcf99ddc551297f0e624ef28dba3d', '130404', '130400', '复兴区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('9dc0ef77590780d795b2f157e972c42b', '431002', '431000', '北湖区', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('9e010e98419c6b7ac83d30ed094b7d6f', '410704', '410700', '凤泉区', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('9e65df9fd73a24f7965e48e8fbcfc12a', '431127', '431100', '蓝山县', 3, NULL, 1, 1647931271, 1681975570);
INSERT INTO `yunduan_district` VALUES ('9e7b0881d5fd0ceca451eb596fb69f57', '220605', '220600', '江源区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('9e7b3ef5a0206f81550d30cda91aff2e', '450423', '450400', '蒙山县', 3, NULL, 1, 1647931271, 1681975626);
INSERT INTO `yunduan_district` VALUES ('9e871e5712dbee8347bc454710740453', '370303', '370300', '张店区', 3, NULL, 1, 1647931271, 1681975486);
INSERT INTO `yunduan_district` VALUES ('9e8c17891e19c6bdc98e7d94c3f44858', '441322', '441300', '博罗县', 3, NULL, 1, 1647931271, 1681975586);
INSERT INTO `yunduan_district` VALUES ('9ea77d9762931cdd2d974f76270741ad', '411528', '411500', '息县', 3, NULL, 1, 1647931271, 1681975529);
INSERT INTO `yunduan_district` VALUES ('9ed8db0ecc22e29bc5cf4a76db1d66e6', '653130', '653100', '巴楚县', 3, NULL, 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('9ee5f21691ffb6ed3f267232cbd61383', '445203', '445200', '揭东区', 3, NULL, 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('9f0f44e8dc84e0938dcc4c3f31336cca', '431081', '431000', '资兴市', 3, NULL, 1, 1647931271, 1681975554);
INSERT INTO `yunduan_district` VALUES ('9f42ae0316985babd9bcaf22ad4d7fad', '451300', '450000', '来宾市', 2, '{\"county\": 6}', 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('9f548d1d6fc625c393ef484f6c2d0fd9', '540224', '540200', '萨迦县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('9f883de943f25b0fe5db1b5584be1db0', '131022', '131000', '固安县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('9f89e8a7de9254d0f5743d3714d2d7dd', '411330', '411300', '桐柏县', 3, NULL, 1, 1647931271, 1681975520);
INSERT INTO `yunduan_district` VALUES ('9f9d673c5d9195b7da578c057057b461', '620105', '620100', '安宁区', 3, NULL, 1, 1647931271, 1681978250);
INSERT INTO `yunduan_district` VALUES ('a02df8c594834e119ff9c7d8a49dfbea', '500243', '500200', '彭水苗族土家族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('a03f996d79bd0953e6035bf22838635c', '530523', '530500', '龙陵县', 3, NULL, 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('a043f8a564597cbe0410b94b44d142c8', '330922', '330900', '嵊泗县', 3, NULL, 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('a05e3d8e7da320deadc3491d4d73728f', '511304', '511300', '嘉陵区', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('a067b42da20b5e394e793a1aa6b7a5d2', '420528', '420500', '长阳土家族自治县', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('a089f634685df8e213a385baf24afed6', '632823', '632800', '天峻县', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('a096a5f367041bf8f7357a96a9522586', '441721', '441700', '阳西县', 3, NULL, 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('a0f9ba96a927fdea66f88b9fc3bdb76f', '371681', '371600', '邹平市', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('a1032aa70c44d79be0ff77923c4f81b4', '350603', '350600', '龙文区', 3, NULL, 1, 1647931271, 1681975411);
INSERT INTO `yunduan_district` VALUES ('a112bb8a6e93952b30d3052411447f50', '360123', '360100', '安义县', 3, NULL, 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('a12dc95b863f5931dca6f7e98a51099b', '210303', '210300', '铁西区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('a13769ccfac39c961910b9e575b0401d', '510800', '510000', '广元市', 2, '{\"county\": 7}', 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('a15ca3a0d7025b3264133cddd0c356b2', '451203', '451200', '宜州区', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('a16ec679de15d0ab01145ffaaeabbbf9', '532532', '532500', '河口瑶族自治县', 3, NULL, 1, 1647931271, 1681977884);
INSERT INTO `yunduan_district` VALUES ('a174aced217766fc85a6b07e8b7d8c4f', '610828', '610800', '佳县', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('a1cbb27aa76b694c16dce81b8c3a01aa', '120105', '120100', '河北区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('a1e3b394630ba0bd522bee84a29124dc', '450503', '450500', '银海区', 3, NULL, 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('a1e8b3a139a5d479dfa64cf04fe60ce7', '340802', '340800', '迎江区', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('a209358f01bec0c50e62ad69f0858ec7', '130709', '130700', '崇礼区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('a25c0ee4d437e4ba5e39b81ec15c543e', '130921', '130900', '沧县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('a29a410ca153393ac5f44ba5cb911cb4', '350600', '350000', '漳州市', 2, '{\"county\": 11}', 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('a29e666c5a9b95f350aca0a96c62a7a8', '321281', '321200', '兴化市', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('a2b60a612a4093df86081548c023460e', '540235', '540200', '聂拉木县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('a2ba30733cbd2a25bceee00ea664a215', '410800', '410000', '焦作市', 2, '{\"county\": 10}', 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('a2bedff9443bf2bcdecdb4e7a467e489', '410505', '410500', '殷都区', 3, NULL, 1, 1647931271, 1681975510);
INSERT INTO `yunduan_district` VALUES ('a2d7683b27f3848e078d3bbd0985fe7f', '320707', '320700', '赣榆区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('a3223d8e1b0e4a7f6d05860dfb031c51', '450303', '450300', '叠彩区', 3, NULL, 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('a3295d9a96ffde3e76f5dc45607daed3', '620524', '620500', '武山县', 3, NULL, 1, 1647931271, 1681978241);
INSERT INTO `yunduan_district` VALUES ('a348e861fab6ca29e361e8160ba74d46', '371003', '371000', '文登区', 3, NULL, 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('a363db842a47e471585cb702dcf3b2c0', '360426', '360400', '德安县', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('a366ab722caed2abe6bf3bc57ad2eee9', '532502', '532500', '开远市', 3, NULL, 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('a395ca9472b8db828a15b362a430ad40', '420881', '420800', '钟祥市', 3, NULL, 1, 1647931271, 1681975543);
INSERT INTO `yunduan_district` VALUES ('a39ca8ce60d3bbf76deb8a59810a6963', '320117', '320100', '溧水区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('a3aaf44ba30729a2526ac68880b4117e', '520113', '520100', '白云区', 3, NULL, 1, 1647931271, 1681977725);
INSERT INTO `yunduan_district` VALUES ('a3ac320dcd5a784925891079b4e3d07b', '230718', '230700', '乌翠区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('a3dbfb36ca42ddffea4289b1fb3d86d6', '140821', '140800', '临猗县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('a3e4107ba4312a21f81ebc1c7a43a87c', '410325', '410300', '嵩县', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('a3f4370d53834967f17ca7e5efaeca1c', '451321', '451300', '忻城县', 3, NULL, 1, 1647931271, 1681975625);
INSERT INTO `yunduan_district` VALUES ('a44f7e961e610f5df53ed6dbb3fc3902', '320812', '320800', '清江浦区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('a454ead82fe73429f484501baf9b5353', '451229', '451200', '大化瑶族自治县', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('a45e627a08ae04fc4925e589c645b541', '610523', '610500', '大荔县', 3, NULL, 1, 1647931271, 1681978077);
INSERT INTO `yunduan_district` VALUES ('a46f6dd20a41565218c81e675b34cb39', '152522', '152500', '阿巴嘎旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('a4af862feaf7b9ae2e92c858323ec855', '320200', '320000', '无锡市', 2, '{\"county\": 7}', 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('a4c469a75d37838634057df67ca4a7cd', '431281', '431200', '洪江市', 3, NULL, 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('a4c547b074dbf85a459ec533c6a7b2ff', '650100', '650000', '乌鲁木齐市', 2, '{\"county\": 8}', 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('a4cfee1b92f7fdd1a7d6edcc4f71cdaf', '640502', '640500', '沙坡头区', 3, NULL, 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('a4f9825515a30c6adb75195ab905cd49', '511723', '511700', '开江县', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('a4faf816d5c64847c24edc758c933eaf', '141122', '141100', '交城县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('a51a571c7e74a933283822a24e753933', '542526', '542500', '改则县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('a52492d27bd26178f531c0d369448822', '150921', '150900', '卓资县', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('a526eab4e5d0292ed9867b2edbdcef0f', '640202', '640200', '大武口区', 3, NULL, 1, 1647931271, 1681978321);
INSERT INTO `yunduan_district` VALUES ('a54c82b4fdfdea50385388e3be51f51d', '410222', '410200', '通许县', 3, NULL, 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('a5545f299d528876ce1ceb91cc6f6a0b', '511826', '511800', '芦山县', 3, NULL, 1, 1647931271, 1681977520);
INSERT INTO `yunduan_district` VALUES ('a55549a0de247231495ac2893f229307', '341202', '341200', '颍州区', 3, NULL, 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('a5557c1e3523efdefe42897dd93622f6', '370322', '370300', '高青县', 3, NULL, 1, 1647931271, 1681975486);
INSERT INTO `yunduan_district` VALUES ('a56c35ec950ee2d6389e2b05a8dde99b', '511526', '511500', '珙县', 3, NULL, 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('a57ab17aadc9690949d8167ba1ccc60e', '530724', '530700', '宁蒗彝族自治县', 3, NULL, 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('a57ff638eb6a3e3c01e88bb7c2a95519', '360322', '360300', '上栗县', 3, NULL, 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('a5920ae880d6c87f6f868e12fcbd040b', '430382', '430300', '韶山市', 3, NULL, 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('a5c53bece3efc0174c57f5591d310156', '341324', '341300', '泗县', 3, NULL, 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('a5defd6d3f311bc3637965be221c61ca', '230112', '230100', '阿城区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('a6011c966bc97f535620079c977d3219', '511321', '511300', '南部县', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('a606aa84f5318b12dcbb5bcb47d86929', '130981', '130900', '泊头市', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('a620cc651987f68e769a8525b347e297', '410421', '410400', '宝丰县', 3, NULL, 1, 1647931271, 1681975512);
INSERT INTO `yunduan_district` VALUES ('a6728757d569d0c749cb0c7c65eb64cf', '150522', '150500', '科尔沁左翼后旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('a689a7e9fbbbce90f63acb5bf3507e63', '654025', '654000', '新源县', 3, NULL, 1, 1647931271, 1681978494);
INSERT INTO `yunduan_district` VALUES ('a6c6873e493ed8118f8820a1f463b4d3', '621202', '621200', '武都区', 3, NULL, 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('a6c8788fa8f481b2de5be10c46056914', '500230', '500200', '丰都县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('a6d7fc60a0f969fe07a0359558ecf77d', '410204', '410200', '鼓楼区', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('a6dba01e0c353af96a1a8a143aee162b', '530625', '530600', '永善县', 3, NULL, 1, 1647931271, 1681977886);
INSERT INTO `yunduan_district` VALUES ('a6e079ca729675ab5241752182747fa1', '542500', '540000', '阿里地区', 2, '{\"county\": 7}', 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('a6e81f867a22cdba0196f66a7c3d4ac1', '370281', '370200', '胶州市', 3, NULL, 1, 1647931271, 1681975483);
INSERT INTO `yunduan_district` VALUES ('a6f57cbdd49ba4c628ba094d390db95b', '411329', '411300', '新野县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('a6f743e880f9712af050ced6e782026f', '350211', '350200', '集美区', 3, NULL, 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('a6f90e13aa9eaf87a9c67828c6829d76', '451024', '451000', '德保县', 3, NULL, 1, 1647931271, 1681975628);
INSERT INTO `yunduan_district` VALUES ('a72bc63b95c81c68cb5bbc6fccd7120b', '460108', '460100', '美兰区', 3, NULL, 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('a73687490329aa2cdd7893e01bda47b9', '441621', '441600', '紫金县', 3, NULL, 1, 1647931271, 1681975599);
INSERT INTO `yunduan_district` VALUES ('a78c3686677a4c2e203058a170df745a', '510822', '510800', '青川县', 3, NULL, 1, 1647931271, 1681977529);
INSERT INTO `yunduan_district` VALUES ('a7a2da44a8ed28a1a2e2738806b9b24b', '350681', '350600', '龙海市', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('a7d10dbc715995da9c69610365ad7019', '654027', '654000', '特克斯县', 3, NULL, 1, 1647931271, 1681978493);
INSERT INTO `yunduan_district` VALUES ('a7e61fc2c133fada325cf6279eac84dc', '532530', '532500', '金平苗族瑶族傣族自治县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('a7e9bad3c64c15d7cdbd06f28d71d669', '540233', '540200', '亚东县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('a80615c4facde1ee059f904aa87b865d', '210214', '210200', '普兰店区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('a807929c9c59a0fff09acfdd867c8bd7', '410506', '410500', '龙安区', 3, NULL, 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('a81a60550a138338aa5110301d05be39', '522623', '522600', '施秉县', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('a81ab64baa834b51ac7faab69265037c', '532300', '530000', '楚雄彝族自治州', 2, '{\"county\": 10}', 1, 1647931271, 1681977872);
INSERT INTO `yunduan_district` VALUES ('a835e2f71c13054b512104557a956d0f', '320803', '320800', '淮安区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('a83ca93ca9dda9156118bbc1f15f10ed', '441781', '441700', '阳春市', 3, NULL, 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('a83debc5f1851cba4175fde84d358324', '441422', '441400', '大埔县', 3, NULL, 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('a85e63ee96798889f7f91dd764007c85', '370215', '370200', '即墨区', 3, NULL, 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('a8823e198a38731ba082b66a71cabbcc', '341623', '341600', '利辛县', 3, NULL, 1, 1647931271, 1681975380);
INSERT INTO `yunduan_district` VALUES ('a8a7e013488b11c10ad92a31d0f7abf1', '340822', '340800', '怀宁县', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('a91f03684f58ffe5410c8dccaadd3456', '510781', '510700', '江油市', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('a9287b9bfc35f356a955c8bd26631c30', '210402', '210400', '新抚区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('a92c496c4dbcb747a1419970f340b129', '653227', '653200', '民丰县', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('a9305a4b65296dcc415e5099475d2525', '513429', '513400', '布拖县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('a93c9cb7b78c6a45b917c2bbf0f17073', '370883', '370800', '邹城市', 3, NULL, 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('a954bc60f426d20425001f5c85bf8411', '230421', '230400', '萝北县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('a969bdd12cf21eaa525fdee49bbf6535', '440311', '440300', '光明区', 3, NULL, 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('a9729201c1e0336ef406637553dbc571', '411281', '411200', '义马市', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('a97aee82faa6c0d8af7f496ade1b0955', '211321', '211300', '朝阳县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('a97ee3af4fc12b4f9dcd9a69990b4716', '652927', '652900', '乌什县', 3, NULL, 1, 1647931271, 1681978494);
INSERT INTO `yunduan_district` VALUES ('a9ba374a3939a4c130b3063fb610e932', '520181', '520100', '清镇市', 3, NULL, 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('a9e15ed2227ebf03245f5ebeacdff19d', '410928', '410900', '濮阳县', 3, NULL, 1, 1647931271, 1681975526);
INSERT INTO `yunduan_district` VALUES ('a9e559f590ff84f52be60611bdb375aa', '211421', '211400', '绥中县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('a9e99f1dd8239a15a2024fee2ee8a0c8', '632701', '632700', '玉树市', 3, NULL, 1, 1647931271, 1681978312);
INSERT INTO `yunduan_district` VALUES ('a9ebebb0c872b6b0c980cdd7bb2372c2', '130523', '130500', '内丘县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('a9f02d780805ccdcc856dbcf7c902e62', '533122', '533100', '梁河县', 3, NULL, 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('a9f9b4b6c41578cc14d2dbfcfc9bbf8c', '150785', '150700', '根河市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('a9fe8064ec3f56a211dd8b54d4eca210', '330902', '330900', '定海区', 3, NULL, 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('aa008144db007198bb8b50216f58a690', '441581', '441500', '陆丰市', 3, NULL, 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('aa1f423a0ed9cd6fbf95574b6e22f92c', '230726', '230700', '南岔县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('aa23bfc5daf483830b48dbd67395cc92', '500100', '500000', '重庆市', 2, '{\"county\": 26}', 1, 1647931271, 1681977447);
INSERT INTO `yunduan_district` VALUES ('aa4383f1574d02dfd329f22aa27ef006', '513327', '513300', '炉霍县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('aa70ff622e3cc6df080edf35ee093df2', '141021', '141000', '曲沃县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('aa8cd9a5dd622c4aee2d4f9c6fe6c94c', '140902', '140900', '忻府区', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('aa9fd3c7b43cd542ebf694ca82dfa96b', '361003', '361000', '东乡区', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('aab67286c53246dacae7661c4673518d', '330483', '330400', '桐乡市', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('aabcf9c711adbb22fdfe3681751874d6', '500229', '500200', '城口县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('aacc58abc8fa12f810ee973dbeb51a42', '140721', '140700', '榆社县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('aada50a9d9ebdb2378563639fa768982', '410327', '410300', '宜阳县', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('ab1c4bb466ec400e7b06b16a9ff3f7a8', '450500', '450000', '北海市', 2, '{\"county\": 4}', 1, 1647931271, 1681975622);
INSERT INTO `yunduan_district` VALUES ('ab1def8f9044e697f6154cd5aeaf9a2d', '431025', '431000', '临武县', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('ab20633493189f9d3476ae4a39f38302', '410600', '410000', '鹤壁市', 2, '{\"county\": 5}', 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('ab4541ad4d252e057142d5397aa13a1c', '370400', '370000', '枣庄市', 2, '{\"county\": 6}', 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('ab68766385032612bd72032301279eb7', '450100', '450000', '南宁市', 2, '{\"county\": 12}', 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('ab76206ae7ecc4eab14f4f812c630ec5', '130708', '130700', '万全区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('ab8158c167006c325abfd34cc589e6d2', '350504', '350500', '洛江区', 3, NULL, 1, 1647931271, 1681975414);
INSERT INTO `yunduan_district` VALUES ('ab935fb0cf15a6f1c131ee04182ddfc3', '130435', '130400', '曲周县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('abaed4ced6bb03f5ea54152ad5827442', '411202', '411200', '湖滨区', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('abc43c6525a80f588a1dd22be460862f', '430602', '430600', '岳阳楼区', 3, NULL, 1, 1647931271, 1681975561);
INSERT INTO `yunduan_district` VALUES ('abcea00366a11e3f6a8460b26c1ed21d', '632525', '632500', '贵南县', 3, NULL, 1, 1647931271, 1681978318);
INSERT INTO `yunduan_district` VALUES ('ac3867569c65dd86fd4460df6ec83c8a', '150700', '150000', '呼伦贝尔市', 2, '{\"county\": 14}', 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('ac3fb56487a1a20d3c204ff91d7afd27', '622901', '622900', '临夏市', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('ac456d3ad8d430db7ccbea192751c009', '150100', '150000', '呼和浩特市', 2, '{\"county\": 9}', 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('ac4b685e3a74fef6f017a225d1d298de', '321324', '321300', '泗洪县', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('ac4bf958c36a66310011721f199b12e9', '511381', '511300', '阆中市', 3, NULL, 1, 1647931271, 1681977530);
INSERT INTO `yunduan_district` VALUES ('ac532cd4cc678157e463efec49dae0a2', '130102', '130100', '长安区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('ac68dcff1c7498dcda80976aaaba7720', '532525', '532500', '石屏县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('ac6ec2eae00df2e4a15a2602a4047978', '450702', '450700', '钦南区', 3, NULL, 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('ac7c7b7ab90b6867dac1ac82ee1286e7', '451121', '451100', '昭平县', 3, NULL, 1, 1647931271, 1681975622);
INSERT INTO `yunduan_district` VALUES ('ac808a1c1d1171d2f148425f3955cae9', '410825', '410800', '温县', 3, NULL, 1, 1647931271, 1681975516);
INSERT INTO `yunduan_district` VALUES ('acaef3882b5eaab2de639e2fddb98e53', '522725', '522700', '瓮安县', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('ace3f2946705baaa84cb5e27b883a319', '530681', '530600', '水富市', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('aceb348bb7c4fcae38084b2ee3652a64', '610302', '610300', '渭滨区', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('acfd425c1394af5318d18e5854abe87a', '360425', '360400', '永修县', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('ad146e14d234b8b995e19ae4bedd6829', '410181', '410100', '巩义市', 3, NULL, 1, 1647931271, 1681975508);
INSERT INTO `yunduan_district` VALUES ('ad1cd4d77f48d72dfdc01d36ca3ddaa5', '330109', '330100', '萧山区', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('ad2700ae678d58c485dd12544e28b949', '532800', '530000', '西双版纳傣族自治州', 2, '{\"county\": 3}', 1, 1647931271, 1681977877);
INSERT INTO `yunduan_district` VALUES ('ad3622cc9725c82416c281cfc1531437', '610900', '610000', '安康市', 2, '{\"county\": 10}', 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('ad39c5fdf280d466edfd34a0cc915672', '210281', '210200', '瓦房店市', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('ad52cabdb200fe528eec56b675641252', '341824', '341800', '绩溪县', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('ad70361304941286ac792198352849d5', '530922', '530900', '云县', 3, NULL, 1, 1647931271, 1681977881);
INSERT INTO `yunduan_district` VALUES ('ad868fcebf5945b6cc43aba27a338d64', '140522', '140500', '阳城县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('adb0f36202e842882922c780850a8ea0', '620403', '620400', '平川区', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('add6d44f69ba800b9390ac9daa545706', '520304', '520300', '播州区', 3, NULL, 1, 1647931271, 1681977731);
INSERT INTO `yunduan_district` VALUES ('adde2241ac1eea1e4e341c6c40c0c971', '610824', '610800', '靖边县', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('ade59c707502eca2e4560780b59c897c', '511523', '511500', '江安县', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('ae014a86bea77d947e7cfca51b4c31bc', '540522', '540500', '贡嘎县', 3, NULL, 1, 1647931271, 1681977995);
INSERT INTO `yunduan_district` VALUES ('ae09b9b9aa5e18d65eedba0bba5d9d40', '411224', '411200', '卢氏县', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('ae19626eead62056f30b29fcad616859', '331121', '331100', '青田县', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('ae2c2510502ec6023680222e3a3742c3', '320581', '320500', '常熟市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('ae2cbb880f3b6f7467cd019e2d73ec3e', '130803', '130800', '双滦区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('ae2e304aa4753b83ecc76dcfce1c1bf6', '130500', '130000', '邢台市', 2, '{\"county\": 18}', 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('ae3e7516dfa5d4cddaf07c4108588f5f', '621228', '621200', '两当县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('ae3ee19fe69d2e021c59090c239404ad', '330624', '330600', '新昌县', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('ae5614fa9d9697f279017d744c957120', '320381', '320300', '新沂市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('ae5ea427f73b58a015a6283b4120f608', '210727', '210700', '义县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('ae75ede0f55265711fab5e043008c104', '150722', '150700', '莫力达瓦达斡尔族自治旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('aecdbe60a55fde0df3b23322145c9f3e', '411502', '411500', '浉河区', 3, NULL, 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('aeea4f58c987774794fa0eed9572094e', '130133', '130100', '赵县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('aef84f316ab9b73df61c24e81176aff2', '654326', '654300', '吉木乃县', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('aef9b6aa5e1b72a84ffecb65e25ebf76', '430725', '430700', '桃源县', 3, NULL, 1, 1647931271, 1681975558);
INSERT INTO `yunduan_district` VALUES ('af2d6a8f774fcc5cf0f41c6dade949e2', '211000', '210000', '辽阳市', 2, '{\"county\": 7}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('af2ef5c59770d26dd6c23d3adde980f2', '659005', '659000', '北屯市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('af328efffe785464ddbda2d0d0b83dae', '230224', '230200', '泰来县', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('af6587c87116fb0f238a89a607acbedd', '130283', '130200', '迁安市', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('afd3d84fe9e89956045579eb4d061fb3', '140828', '140800', '夏县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('afd9ea282167a910c18b929147205f09', '650202', '650200', '独山子区', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('afda2093b7213147d3e0e54da083f12a', '411100', '410000', '漯河市', 2, '{\"county\": 5}', 1, 1647931271, 1681975500);
INSERT INTO `yunduan_district` VALUES ('afeeec74030e592fa5f852a52827567f', '469002', '469000', '琼海市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('afffc48c26206a78aec6640d884d5ce7', '513337', '513300', '稻城县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('b00a2856be8db7b4c51b45963b52bfdd', '130827', '130800', '宽城满族自治县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('b012aad2613d76077f266248926106ac', '621023', '621000', '华池县', 3, NULL, 1, 1647931271, 1681978246);
INSERT INTO `yunduan_district` VALUES ('b01e3fc679b497289da25f6a00ce16e3', '500120', '500100', '璧山区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('b02434f0638072663b1475c2acbfc777', '340825', '340800', '太湖县', 3, NULL, 1, 1647931271, 1681975374);
INSERT INTO `yunduan_district` VALUES ('b05b3050f04161fe4f24ed6cdc338058', '450721', '450700', '灵山县', 3, NULL, 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('b077b2c66b2139ab1e8dc3bc53f5ac9b', '460107', '460100', '琼山区', 3, NULL, 1, 1647931271, 1681976683);
INSERT INTO `yunduan_district` VALUES ('b0789a4bcd4e5b26c657918ecc3f08dc', '230200', '230000', '齐齐哈尔市', 2, '{\"county\": 16}', 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('b084117c576a1313d952161c256e57d9', '140122', '140100', '阳曲县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('b08b3b0f34aab03c998e84a2a08b0152', '330503', '330500', '南浔区', 3, NULL, 1, 1647931271, 1681975347);
INSERT INTO `yunduan_district` VALUES ('b0910e04ec35af6e386ac11233cb9c75', '360981', '360900', '丰城市', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('b0bd1e63e2fed1b0265cf9ae56202ef4', '361024', '361000', '崇仁县', 3, NULL, 1, 1647931271, 1681975442);
INSERT INTO `yunduan_district` VALUES ('b0c9838815881c29fa0066ed65cec9b8', '511102', '511100', '市中区', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('b0e435199621f1a7dc0e3e14726b705b', '610703', '610700', '南郑区', 3, NULL, 1, 1647931271, 1681978082);
INSERT INTO `yunduan_district` VALUES ('b0f581c7aaa6c18be5918f6a4a25fd81', '410721', '410700', '新乡县', 3, NULL, 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('b0f8d319854677d31b4cfbed5b94edab', '230381', '230300', '虎林市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('b10ece3259f90529f6e7ec07cc7a848d', '540223', '540200', '定日县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('b11b030864ba410326724dbb8c5bc276', '130900', '130000', '沧州市', 2, '{\"county\": 16}', 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('b139099495c7663164ec27995306d69a', '420984', '420900', '汉川市', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('b14d4f2aa26b4272b8630640e8f51b73', '140300', '140000', '阳泉市', 2, '{\"county\": 5}', 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('b151a11dcd0ba3d09b03e9af7b40d137', '440823', '440800', '遂溪县', 3, NULL, 1, 1647931271, 1681975592);
INSERT INTO `yunduan_district` VALUES ('b155c71d581b2a1e080d9a507a448056', '511922', '511900', '南江县', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('b15646c6f03839ce9174a128ada8247d', '330302', '330300', '鹿城区', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('b15f18d42264c224d83d783c30ee2dbd', '522628', '522600', '锦屏县', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('b16202c09fa8fb1d15fa2dc6a083c7b3', '370200', '370000', '青岛市', 2, '{\"county\": 10}', 1, 1647931271, 1681975483);
INSERT INTO `yunduan_district` VALUES ('b1785e7261a37dab92544aeca778f1dd', '130130', '130100', '无极县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('b178f0973109485076ae2da0f6a30318', '130631', '130600', '望都县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('b17a212d527dd06967654d68bbab2f46', '140881', '140800', '永济市', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('b1956e6d44d8377000cb535e3e7621a2', '130225', '130200', '乐亭县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('b1b70eaed8139b039d42be98ce4b3e89', '130403', '130400', '丛台区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('b1e31b8ca9aa0897023cb1ab427f14ef', '451422', '451400', '宁明县', 3, NULL, 1, 1647931271, 1681975620);
INSERT INTO `yunduan_district` VALUES ('b205319e2af4a1bb724dec02e6693845', '610621', '610600', '延长县', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('b20747a6189916436e5953ee17759ab7', '410411', '410400', '湛河区', 3, NULL, 1, 1647931271, 1681975513);
INSERT INTO `yunduan_district` VALUES ('b233ae05d5a7bf30bdb6114bbbc01464', '623021', '623000', '临潭县', 3, NULL, 1, 1647931271, 1681978244);
INSERT INTO `yunduan_district` VALUES ('b23f25afdf6235c6869f6ea3c7a686d7', '230881', '230800', '同江市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('b24121b3881c7020e25bb4f8a3defd53', '511132', '511100', '峨边彝族自治县', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('b27733e0a9abf2be6b1fd5c32b048fb3', '210503', '210500', '溪湖区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('b27af4e718872e4393907eef66036bc8', '130481', '130400', '武安市', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('b28606fba40a9bb9c7437c586796d0f7', '371602', '371600', '滨城区', 3, NULL, 1, 1647931271, 1681975482);
INSERT INTO `yunduan_district` VALUES ('b2a97af27c7bdb45fc3dae0ebae1997c', '621126', '621100', '岷县', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('b2dfde4df430dd0b3bf932392444b28e', '520622', '520600', '玉屏侗族自治县', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('b2ec09701c606b11d7ecf6311abec5a2', '140425', '140400', '平顺县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('b2f71a4637605ca66c4811956c0109be', '210323', '210300', '岫岩满族自治县', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('b31d65612339cb36741011b25beb1c25', '659006', '659000', '铁门关市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('b3346124442d7f833f5407c9ac7c8fc0', '370213', '370200', '李沧区', 3, NULL, 1, 1647931271, 1681975484);
INSERT INTO `yunduan_district` VALUES ('b34062d40ff96e481352a8dc46aa4906', '141032', '141000', '永和县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('b34d6e5717cc80f40b2be86e7345b4c1', '511123', '511100', '犍为县', 3, NULL, 1, 1647931271, 1681977537);
INSERT INTO `yunduan_district` VALUES ('b3501013888e388bdcf7baa0aa433d3a', '440607', '440600', '三水区', 3, NULL, 1, 1647931271, 1681975594);
INSERT INTO `yunduan_district` VALUES ('b359a2587268146b277c629cb7f780ad', '441800', '440000', '清远市', 2, '{\"county\": 8}', 1, 1647931271, 1681975586);
INSERT INTO `yunduan_district` VALUES ('b35c029ff62b1750547d3071dcf8f435', '610329', '610300', '麟游县', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('b366d8c7220b68a29a3c57a8401117ac', '632323', '632300', '泽库县', 3, NULL, 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('b368a99f59b8ba7a6ea6ab5662bb5220', '430525', '430500', '洞口县', 3, NULL, 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('b38d424ba52d89d754cb335bc40fddba', '330700', '330000', '金华市', 2, '{\"county\": 9}', 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('b3a9f7e358aad38bc342ca454643b66a', '330102', '330100', '上城区', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('b3b41776a1d9eefc2bcc449bba560c3b', '360281', '360200', '乐平市', 3, NULL, 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('b3b8dc1a5bc0627f7dc5b4b2d7acea2c', '513227', '513200', '小金县', 3, NULL, 1, 1647931271, 1681977526);
INSERT INTO `yunduan_district` VALUES ('b3ca9b8c0ff6999ad2a0096fe19933f8', '450902', '450900', '玉州区', 3, NULL, 1, 1647931271, 1681975640);
INSERT INTO `yunduan_district` VALUES ('b3dccab9d7525d6d5b7e7bc75af70c98', '310101', '310100', '黄浦区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('b3de93dc13d286bcafe2be60ca276a25', '652924', '652900', '沙雅县', 3, NULL, 1, 1647931271, 1681978494);
INSERT INTO `yunduan_district` VALUES ('b3eb4413ba8f62834e9548d6fc5cbfa6', '330225', '330200', '象山县', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('b3ef613fcca69f945ca5e529f959b269', '610404', '610400', '渭城区', 3, NULL, 1, 1647931271, 1681978068);
INSERT INTO `yunduan_district` VALUES ('b4430c378b34e30f6dc5297ccbeb340d', '540329', '540300', '洛隆县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('b4726df4423048f23f509caf22abf3f0', '230204', '230200', '铁锋区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('b47376b428a7f68eaf4af03498ec2874', '130821', '130800', '承德县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('b48b861c68028d3521af58d40cf0264e', '220681', '220600', '临江市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('b4975fe39a0d46a5a9977d13443dd464', '360722', '360700', '信丰县', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('b4b262af1a808d60df21f73f53c18132', '540226', '540200', '昂仁县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('b4bbbbdb2ab03a772ad93d108f7cfeac', '420525', '420500', '远安县', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('b4e016407b5d6ddf4df1cbf67f287582', '341004', '341000', '徽州区', 3, NULL, 1, 1647931271, 1681975389);
INSERT INTO `yunduan_district` VALUES ('b4e4e29bdca9d5eb8e06c9bfb3f9c9d6', '610628', '610600', '富县', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('b4e8e6d636ebbc7c8f860817bcb96745', '640522', '640500', '海原县', 3, NULL, 1, 1647931271, 1681978324);
INSERT INTO `yunduan_district` VALUES ('b4f58d0cc9cdbcce97461a326505c37a', '340506', '340500', '博望区', 3, NULL, 1, 1647931271, 1681975391);
INSERT INTO `yunduan_district` VALUES ('b5228d2a39d8769e0565e672de9eab42', '520103', '520100', '云岩区', 3, NULL, 1, 1647931271, 1681977724);
INSERT INTO `yunduan_district` VALUES ('b53053537056bc1f699eb13683c0be7b', '659010', '659000', '胡杨河市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('b54eec1df22227a8c1baff29fdd88e11', '530102', '530100', '五华区', 3, NULL, 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('b56c0293439a40cfc9ae7b331b09cf58', '500154', '500100', '开州区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('b57963c84986b8552eff807dabf2bda4', '320684', '320600', '海门市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('b57bf26b1a4cd038e411dc1b99e38b86', '231004', '231000', '爱民区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('b58362133e42dece0fc9fd56fa559a5b', '210905', '210900', '清河门区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('b59f60e1aaa57c3582034ce1e89fcf42', '650200', '650000', '克拉玛依市', 2, '{\"county\": 4}', 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('b5e149f66cced642116dc8d4d4ec7f87', '440800', '440000', '湛江市', 2, '{\"county\": 9}', 1, 1647931271, 1681975590);
INSERT INTO `yunduan_district` VALUES ('b628fdbebcad6d88b95192c4ad9b3503', '130123', '130100', '正定县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('b6309c47a248e852ec7fdc1aaa1d2b72', '140623', '140600', '右玉县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('b6436b2f22ec067814a516c1732a2acd', '520621', '520600', '江口县', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('b6487d6e049f1eaa082ef4be9fd6ad80', '622924', '622900', '广河县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('b653fb5cff383ec3435fb110ede61049', '411122', '411100', '临颍县', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('b665137f96b5b1fae8f06904c90140c4', '360825', '360800', '永丰县', 3, NULL, 1, 1647931271, 1681975439);
INSERT INTO `yunduan_district` VALUES ('b68d0b48139e700dd13f91b41672cd6f', '210781', '210700', '凌海市', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('b6b1e06d31d073e4b9afcd62b91c28e7', '450406', '450400', '龙圩区', 3, NULL, 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('b6bc165bc2bcbd29db2c017de1e6c0e7', '420302', '420300', '茅箭区', 3, NULL, 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('b6c7a1361e3f243d81dce5a36a91bdc1', '430000', '0', '湖南省', 1, '{\"city\": 14, \"county\": 122}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('b6d78ce1c2c3fbfb6ae97346bdc3c589', '511824', '511800', '石棉县', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('b6f0519e46941182cfdf869a687f2c4d', '130703', '130700', '桥西区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('b712027d2d3457f047a23fb62c5198b7', '440511', '440500', '金平区', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('b71ff013843adc4fc81005d0b7a830b4', '652302', '652300', '阜康市', 3, NULL, 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('b7224082570f30a982bf841f6109b31d', '430412', '430400', '南岳区', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('b73a015ef86c15d0446043ee7cdb1a00', '120111', '120100', '西青区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('b7598239b58cc8fdb27d30ce100269ba', '330683', '330600', '嵊州市', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('b75fb987224511fc0777c57364e13045', '610729', '610700', '留坝县', 3, NULL, 1, 1647931271, 1681978083);
INSERT INTO `yunduan_district` VALUES ('b77aac19d5619783636411218566c09b', '520112', '520100', '乌当区', 3, NULL, 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('b7f166dd76c89b66d6ae88ef51ad0cec', '211204', '211200', '清河区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('b8042d5078a3e9cc4008479875cd2898', '220523', '220500', '辉南县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('b8087aac965e8441cdbbc7c2689e06cd', '370202', '370200', '市南区', 3, NULL, 1, 1647931271, 1681975484);
INSERT INTO `yunduan_district` VALUES ('b815d44fb6c7e2295611d09eca52071d', '320115', '320100', '江宁区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('b81f094d2ebab9b3c8328dda18381d55', '330100', '330000', '杭州市', 2, '{\"county\": 13}', 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('b82f10a54034025951a752e0fa9c0f90', '230500', '230000', '双鸭山市', 2, '{\"county\": 8}', 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('b834dc9beae070ad1c938bc5b27b93a7', '511025', '511000', '资中县', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('b83bde114d0b864b9ca5a8a43cdc9b58', '120110', '120100', '东丽区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('b877a98174875bd3b8073d11b260f5f3', '350602', '350600', '芗城区', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('b8c2c7d759a02f7b1064684e361e416a', '620822', '620800', '灵台县', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('b8c6ae087a1a9943da46926b0395e43e', '620000', '0', '甘肃省', 1, '{\"city\": 14, \"county\": 86}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('b8eebea6c205354aa03d9c2c2fad93aa', '320303', '320300', '云龙区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('b8f6d8839be99c69feeaafc8ae78e9aa', '522326', '522300', '望谟县', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('b8f7164beaf389922ccd48c1b2ec400f', '130227', '130200', '迁西县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('b92644a72138de0c000f798b7ed340ba', '150783', '150700', '扎兰屯市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('b92fdc0a8875e2a5442ba8eb4c0e8711', '130634', '130600', '曲阳县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('b9306142117cd7b199ead2453970da21', '321012', '321000', '江都区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('b943fb349ee8f3093a597daeb75b11a2', '340827', '340800', '望江县', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('b9450bec1a500a80431263946cc43630', '341182', '341100', '明光市', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('b94a09795664447ef79e060784e9146c', '321283', '321200', '泰兴市', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('b956f8b6ac053c93e12d134fed59adff', '441203', '441200', '鼎湖区', 3, NULL, 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('b96366bddf6d461ee8762243ea8ae771', '230623', '230600', '林甸县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('b965978ed279263a482407b46d3a4471', '320925', '320900', '建湖县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('b96d47893f54884379328bb1064114b4', '620981', '620900', '玉门市', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('b9705d6f81bf5ce78d3e09498e48f892', '632600', '630000', '果洛藏族自治州', 2, '{\"county\": 6}', 1, 1647931271, 1681978316);
INSERT INTO `yunduan_district` VALUES ('b978bdcdeaf8fb9a8cdc7373df114a26', '120119', '120100', '蓟州区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('b9a6037917485f0e82e471639f004528', '411681', '411600', '项城市', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('b9c0251bf2b1a4af7b487b7532f2aa8c', '532327', '532300', '永仁县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('b9c3d17f1e722ac27442560e5afea4eb', '210702', '210700', '古塔区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('b9f32841cc36fad5a8ab729de7a0af2c', '310104', '310100', '徐汇区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('b9fed56978de4c80885c451cf7656113', '210522', '210500', '桓仁满族自治县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('ba09081970df75c55b35588976c8e678', '450481', '450400', '岑溪市', 3, NULL, 1, 1647931271, 1681975626);
INSERT INTO `yunduan_district` VALUES ('ba0f788b3474103167b559e8252e417f', '410404', '410400', '石龙区', 3, NULL, 1, 1647931271, 1681975513);
INSERT INTO `yunduan_district` VALUES ('ba1a495ded569473bb705768f003ac96', '652325', '652300', '奇台县', 3, NULL, 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('ba25043fe072f5175d5cd617f79212b5', '130638', '130600', '雄县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('ba278cf2cae0933610b5b380face0491', '210903', '210900', '新邱区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('ba3625c5b7b166c3dffdbb42abb94c26', '370786', '370700', '昌邑市', 3, NULL, 1, 1647931271, 1681975487);
INSERT INTO `yunduan_district` VALUES ('ba3afee7c79af582adfdffd500183390', '360824', '360800', '新干县', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('ba4daddc20d6e8af79575ec2373801a5', '341522', '341500', '霍邱县', 3, NULL, 1, 1647931271, 1681975387);
INSERT INTO `yunduan_district` VALUES ('ba58cea050626928967a66f4d89101ac', '652829', '652800', '博湖县', 3, NULL, 1, 1647931271, 1681978501);
INSERT INTO `yunduan_district` VALUES ('ba6422385a3e0543d4eeab52d618dbd2', '610727', '610700', '略阳县', 3, NULL, 1, 1647931271, 1681978084);
INSERT INTO `yunduan_district` VALUES ('ba709906aab237b91477e0486d0553ab', '445224', '445200', '惠来县', 3, NULL, 1, 1647931271, 1681975601);
INSERT INTO `yunduan_district` VALUES ('ba7f6ee8a07f867c5771bf5f5167a07c', '622926', '622900', '东乡族自治县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('ba86df0545ddf6a61279ce83d0827f08', '623027', '623000', '夏河县', 3, NULL, 1, 1647931271, 1681978244);
INSERT INTO `yunduan_district` VALUES ('ba8c8dbd9f0c06d3eb015b73ececa58d', '530300', '530000', '曲靖市', 2, '{\"county\": 9}', 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('ba9fd0e5470bad3f16e3319c6379f84f', '632623', '632600', '甘德县', 3, NULL, 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('bacd7f383405e9acba6a9d6eb944eb9f', '130608', '130600', '清苑区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('bad6ddaff8689010f30694858e29e1fa', '130406', '130400', '峰峰矿区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('bae29e0223ce8add8c6e7f6fea936864', '430400', '430000', '衡阳市', 2, '{\"county\": 12}', 1, 1647931271, 1681975555);
INSERT INTO `yunduan_district` VALUES ('baebb1479781be3bc66621da34ba5e10', '130633', '130600', '易县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('bb27d6910b74556ad113c6e36ff1f5b8', '130903', '130900', '运河区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('bb2abd0716a92d57c97aabfea956960b', '152527', '152500', '太仆寺旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('bb37bde733f160ebdfeb5ba912eff999', '450603', '450600', '防城区', 3, NULL, 1, 1647931271, 1681975619);
INSERT INTO `yunduan_district` VALUES ('bb3a1895eebf87225fc57486bc6fdd82', '540327', '540300', '左贡县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('bb8089f7340f5de0afcc2e789f6a0789', '500233', '500200', '忠县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('bbbe4e2ffd48e950e5a21f4da6570cd4', '431229', '431200', '靖州苗族侗族自治县', 3, NULL, 1, 1647931271, 1681975571);
INSERT INTO `yunduan_district` VALUES ('bbbf207ecec6baa152df2cfc11bbe690', '320700', '320000', '连云港市', 2, '{\"county\": 6}', 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('bbfa5322dd69ae8f2186a85a94693dba', '320903', '320900', '盐都区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('bc1710e40ca4fe93e2501471427486bf', '331082', '331000', '临海市', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('bc1a3114a44ffdc4a9205c17f2f34342', '230306', '230300', '城子河区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('bc54617e09cd59193eb464e3b08f85c0', '152900', '150000', '阿拉善盟', 2, '{\"county\": 3}', 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('bc848e758c7d393beebe2a906c567cca', '532328', '532300', '元谋县', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('bca66ba83805d2370e11848ab63547f5', '320114', '320100', '雨花台区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('bcb4df2ea4c220951372dea9a6513160', '370826', '370800', '微山县', 3, NULL, 1, 1647931271, 1681975494);
INSERT INTO `yunduan_district` VALUES ('bcd3d865212e93b14b5678c7260cbc60', '140500', '140000', '晋城市', 2, '{\"county\": 6}', 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('bce683980285ad7eebb67a4ef9e4d56e', '130131', '130100', '平山县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('bd0a6ccd0ca5094f7e9affb43690f7d1', '220282', '220200', '桦甸市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('bd0fee6b6560ccda4bddf5971401147a', '220204', '220200', '船营区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('bd17b9271086f4ce158c67f8ed19f28e', '150826', '150800', '杭锦后旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('bd2abf518344791897057480aae5c2fc', '350500', '350000', '泉州市', 2, '{\"county\": 12}', 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('bd41d3be62016d1f389f4018524d4f6c', '440523', '440500', '南澳县', 3, NULL, 1, 1647931271, 1681975578);
INSERT INTO `yunduan_district` VALUES ('bd717117a8b0348b554fd5d50cec1399', '371503', '371500', '茌平区', 3, NULL, 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('bd91e0bb5d571b308a42de888b0800a2', '540324', '540300', '丁青县', 3, NULL, 1, 1647931271, 1681977989);
INSERT INTO `yunduan_district` VALUES ('bd9f71637936f706050e621b307cf8f2', '500241', '500200', '秀山土家族苗族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('bda5c02cbce82ac5c8d9239f57d0d78a', '150123', '150100', '和林格尔县', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('bda845919159ce27661d0ca0b8f7871a', '640500', '640000', '中卫市', 2, '{\"county\": 3}', 1, 1647931271, 1681978323);
INSERT INTO `yunduan_district` VALUES ('bdaab9f356949d002cd0cb1c6b89e942', '421024', '421000', '江陵县', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('bdc9d0cc97b6e5c68928fd8e8eee1670', '131127', '131100', '景县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('bde845687b8922aca0d6c0b64a4a6fd3', '445300', '440000', '云浮市', 2, '{\"county\": 5}', 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('bdf28d90e2910e08733ddff24a20de67', '130321', '130300', '青龙满族自治县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('be0b57b4904ab093e9b30f9677ffb08f', '110106', '110100', '丰台区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('be19e47cc71dcfe6dae21d68de548053', '321000', '320000', '扬州市', 2, '{\"county\": 6}', 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('be410db526d1673370f05c126e5e3394', '230904', '230900', '茄子河区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('be5ca945362e62a03844e4c49ef01df1', '469006', '469000', '万宁市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('be647bfb6e84ab17a4e43af2922504e6', '220400', '220000', '辽源市', 2, '{\"county\": 4}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('be9dfcbcc4bc2671b6c0736884e4794e', '321002', '321000', '广陵区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('bea0fc9e193d3a8019d48c3555e3ca86', '130306', '130300', '抚宁区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('bec97f2112acc4e8338540448170d55c', '360725', '360700', '崇义县', 3, NULL, 1, 1647931271, 1681975446);
INSERT INTO `yunduan_district` VALUES ('bef493e51baf8aa42083420cd0ddcf49', '330703', '330700', '金东区', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('befd56e3775774e7e18731f8e5474e29', '211224', '211200', '昌图县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('bf54832b08e93cee1321533619df7a7c', '370115', '370100', '济阳区', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('bf6401e0267ecc030e6c862d37e27119', '152523', '152500', '苏尼特左旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('bf69bfe178abe04c09f9be401169495e', '654322', '654300', '富蕴县', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('bf6c134b7f6d2fe00b05fb290775cd9e', '510181', '510100', '都江堰市', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('bfa8dbf45dd99ec50ff0ef538ca757b4', '330327', '330300', '苍南县', 3, NULL, 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('bfb057cefa1611141cde457bf68e50c3', '360921', '360900', '奉新县', 3, NULL, 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('bfb6d016efdb5cc01c83bc34cd1bb89f', '653000', '650000', '克孜勒苏柯尔克孜自治州', 2, '{\"county\": 4}', 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('bfc706b77c40996b50c85201eeb2d9e9', '520122', '520100', '息烽县', 3, NULL, 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('c011c2fce2c13f83061c9a45b0454f18', '610527', '610500', '白水县', 3, NULL, 1, 1647931271, 1681978079);
INSERT INTO `yunduan_district` VALUES ('c0462db9deafdc59d4ed723b1c71a2e0', '330781', '330700', '兰溪市', 3, NULL, 1, 1647931271, 1681975354);
INSERT INTO `yunduan_district` VALUES ('c07c933c1c5abc3ca01dc0f309fc47d0', '220182', '220100', '榆树市', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('c094929d8ac21ecc02884253e3ccdcaa', '421303', '421300', '曾都区', 3, NULL, 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('c094f1e0a3aab4f1d958c8c61e8efb0a', '371524', '371500', '东阿县', 3, NULL, 1, 1647931271, 1681975473);
INSERT INTO `yunduan_district` VALUES ('c097f1f26a644215206f502433ce72c4', '350625', '350600', '长泰县', 3, NULL, 1, 1647931271, 1681975410);
INSERT INTO `yunduan_district` VALUES ('c0d173e5ce52c298b13c1ebefe122813', '150206', '150200', '白云鄂博矿区', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('c0e0bdbdac63760ec9e2fdb602d8980a', '440705', '440700', '新会区', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('c0e3e4e03acea72d29e1821e622daa3c', '440103', '440100', '荔湾区', 3, NULL, 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('c0e5988f71923d0310b08fddb723c216', '450421', '450400', '苍梧县', 3, NULL, 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('c106a44d34eb2dcf78ceb019c93a3463', '130108', '130100', '裕华区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('c10a496157cf119f7cbc0fa179445f9d', '532928', '532900', '永平县', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('c12c45a2c812b58ac30757828d09d5dd', '341282', '341200', '界首市', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('c13b7b4a83ddbbda34d159a376b366bf', '350503', '350500', '丰泽区', 3, NULL, 1, 1647931271, 1681975413);
INSERT INTO `yunduan_district` VALUES ('c155e032280ce47f538decd19f7d20b2', '522626', '522600', '岑巩县', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('c158d105556e4e9c070c90960ae1b5de', '611021', '611000', '洛南县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('c161e4b783ddb89e131c848dd36e715b', '141000', '140000', '临汾市', 2, '{\"county\": 17}', 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('c170c2f9a683f79e0525ef15e307ac58', '710000', '0', '台湾省', 1, NULL, 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('c176a62a522d988a51f1a3850e810a26', '330903', '330900', '普陀区', 3, NULL, 1, 1647931271, 1681975348);
INSERT INTO `yunduan_district` VALUES ('c1857df8613a3f33843425dc4405652b', '130609', '130600', '徐水区', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('c193e833201a96503fd76e430439aa44', '210321', '210300', '台安县', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('c1ae9d2dc0be7fea57d3593a52788c8d', '211100', '210000', '盘锦市', 2, '{\"county\": 4}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('c1aef96ca3370df33cae87f54aec3caa', '441502', '441500', '城区', 3, NULL, 1, 1647931271, 1681975600);
INSERT INTO `yunduan_district` VALUES ('c1b1f75f1a660674b234f9bb16e5b033', '211081', '211000', '灯塔市', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('c1cb8558ccb9f679de8b064af8a53eb0', '411121', '411100', '舞阳县', 3, NULL, 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('c1d82c8ed794511b99a84423c9674e92', '632500', '630000', '海南藏族自治州', 2, '{\"county\": 5}', 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('c1e9f1f763c10dfcd776bf1c204de3d4', '652825', '652800', '且末县', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('c1f767c35115145dfd81e5cba87f5aab', '500237', '500200', '巫山县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('c2192fac6a7b019e72f8672049ad306a', '320724', '320700', '灌南县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('c241310f8642e54161af993314091353', '140581', '140500', '高平市', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('c24457bb1bcaf9c929c245fe8ab0c153', '420682', '420600', '老河口市', 3, NULL, 1, 1647931271, 1681975530);
INSERT INTO `yunduan_district` VALUES ('c2757ab40f572f7f983facab6fe23ccf', '370704', '370700', '坊子区', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('c281f2e9ffe8999c444fee1dc74e5007', '451221', '451200', '南丹县', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('c29f66e8f1f56042996bb2ae1d38cfc3', '120115', '120100', '宝坻区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('c2abca8ff313076b874ff20a73340fe7', '310109', '310100', '虹口区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('c2ddc1ace0f0ea032bc2d6c2bd53d894', '620700', '620000', '张掖市', 2, '{\"county\": 6}', 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('c2e8e11355bbb5f300ae2905667ec268', '320102', '320100', '玄武区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('c301aeec64b026c7a0200c9c3de9b019', '361100', '360000', '上饶市', 2, '{\"county\": 12}', 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('c30d8ed72729668db68111e531533ddc', '632621', '632600', '玛沁县', 3, NULL, 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('c323c2939fa825d387e85b5f0bbbce03', '421321', '421300', '随县', 3, NULL, 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('c34e2299f6bfff65b1c73a5e79640145', '450202', '450200', '城中区', 3, NULL, 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('c354a47db600a264ef0817667560d7c7', '542525', '542500', '革吉县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('c35c2c2eadfc4aa2565bf7928891db52', '411426', '411400', '夏邑县', 3, NULL, 1, 1647931271, 1681975504);
INSERT INTO `yunduan_district` VALUES ('c35dd6f450152b8b4affc4f823954e9d', '140521', '140500', '沁水县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('c36f726bc5fa27ec53c3866dd24a7880', '540221', '540200', '南木林县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('c4145dad96f70535826a3f418d402d71', '520300', '520000', '遵义市', 2, '{\"county\": 14}', 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('c417552cd1ef3e6579c34043afd8798f', '150125', '150100', '武川县', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('c417bc58ea597c4cbea470830430be07', '330328', '330300', '文成县', 3, NULL, 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('c4249c9c0a4e0520cba184a705d506f6', '130705', '130700', '宣化区', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('c4389ef0cd0af896c943b4b5478bdd57', '510725', '510700', '梓潼县', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('c448dd70a7dc565bac25daa749176399', '310105', '310100', '长宁区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('c45e25c74aeb7f1eebed06077d6cf249', '450124', '450100', '马山县', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('c46db38caa853bb66014e7c1a87b7bbd', '500231', '500200', '垫江县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('c46f3c22f1e20194ad636ccc3ebeeba1', '120000', '0', '天津市', 1, '{\"city\": 1, \"county\": 16}', 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('c48f311fbe76b444798601b2a4765d99', '500000', '0', '重庆市', 1, '{\"city\": 1, \"county\": 38}', 1, 1647931271, 1661303833);
INSERT INTO `yunduan_district` VALUES ('c494ab519e786dc8d97f173adf3e1689', '530303', '530300', '沾益区', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('c4a6a5271f0d9ccf32b221368aea1dea', '350902', '350900', '蕉城区', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('c4b250b60d7723eb4138ddeb47f29f11', '211402', '211400', '连山区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('c4f43deaeeeb63cbb5464e0b9e5280dd', '110113', '110100', '顺义区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('c50332755f55d6ee869361cc729bf5ae', '430581', '430500', '武冈市', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('c5093731e5b2930241b2f6d2d5adacfe', '360403', '360400', '浔阳区', 3, NULL, 1, 1647931271, 1681975453);
INSERT INTO `yunduan_district` VALUES ('c50b1f99bf0290be6ea30cfca88aa432', '520402', '520400', '西秀区', 3, NULL, 1, 1647931271, 1681977720);
INSERT INTO `yunduan_district` VALUES ('c510afc752c5b421993da15dac06f9a1', '360829', '360800', '安福县', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('c559057f7f2eacd459b131ef28b0e691', '130727', '130700', '阳原县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('c564bc6dff91a3aad947f8fdea26e17b', '429006', '429000', '天门市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('c5659fb807855df8e28611733e813d74', '220581', '220500', '梅河口市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('c571da59c0cd15eba556a2fee47a7b30', '440402', '440400', '香洲区', 3, NULL, 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('c57e87919ae53e5be14062b28ef3aa03', '371424', '371400', '临邑县', 3, NULL, 1, 1647931271, 1681975491);
INSERT INTO `yunduan_district` VALUES ('c58d2f2ea8b530b1a60774a91f127912', '510303', '510300', '贡井区', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('c5bf00a94db185562e00b0e215858d28', '130532', '130500', '平乡县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('c5c008ae42b04d4278258a968db54043', '520330', '520300', '习水县', 3, NULL, 1, 1647931271, 1681977729);
INSERT INTO `yunduan_district` VALUES ('c5c05508e9ddd107647df85c6e21bcee', '430600', '430000', '岳阳市', 2, '{\"county\": 9}', 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('c5cfd9dc270014955e8dd8b1e71ff065', '411500', '410000', '信阳市', 2, '{\"county\": 10}', 1, 1647931271, 1681975527);
INSERT INTO `yunduan_district` VALUES ('c5d06c007e66eed0c754e375392bedd0', '620702', '620700', '甘州区', 3, NULL, 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('c5dde35cd8d01fd57c4be99a401d24f0', '445303', '445300', '云安区', 3, NULL, 1, 1647931271, 1681975605);
INSERT INTO `yunduan_district` VALUES ('c5ee8d87ebbbc294a3bab39a188b98f4', '230903', '230900', '桃山区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('c5f7804e9fd73efe2147a9ff9d0c2f57', '350302', '350300', '城厢区', 3, NULL, 1, 1647931271, 1681975412);
INSERT INTO `yunduan_district` VALUES ('c60fb92ff46089bc0bc17c66ba9c9dc6', '513424', '513400', '德昌县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('c618f596b8eda4c6e5188d3ae37fcbd9', '652902', '652900', '库车市', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('c64a913d0eb371f267609b69e3628626', '320404', '320400', '钟楼区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('c64bb5835263156b07f7c8fc76392bfb', '210104', '210100', '大东区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('c6a7af97ac5c9157317401a0cf67bbbc', '440112', '440100', '黄埔区', 3, NULL, 1, 1647931271, 1681975597);
INSERT INTO `yunduan_district` VALUES ('c6d3a180c032a3aa14c29821ebc5ef4d', '610803', '610800', '横山区', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('c6e5fe5ba65ef01e18b5571307afc85d', '330304', '330300', '瓯海区', 3, NULL, 1, 1647931271, 1681975351);
INSERT INTO `yunduan_district` VALUES ('c71b63848753a284b02146ae8de8ede9', '350703', '350700', '建阳区', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('c72f5057ff9e185e04b6e0138a589ccd', '411302', '411300', '宛城区', 3, NULL, 1, 1647931271, 1681975520);
INSERT INTO `yunduan_district` VALUES ('c74c1a35dc949af18fd5ebf5aa451932', '131028', '131000', '大厂回族自治县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('c74f00214434aa0406e530301e59da25', '450330', '450300', '平乐县', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('c76316de456c0922c331f3c9c4cad6e0', '420505', '420500', '猇亭区', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('c77b83b1be34dd050bd4ba626c8a9244', '130822', '130800', '兴隆县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('c7aaf058219937843f75c698ccd86d3a', '130924', '130900', '海兴县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('c7db21bc479c22e51154f2dc2d4056ea', '370827', '370800', '鱼台县', 3, NULL, 1, 1647931271, 1681975495);
INSERT INTO `yunduan_district` VALUES ('c7f31a344bd036ca2e4910ce112609b6', '450700', '450000', '钦州市', 2, '{\"county\": 4}', 1, 1647931271, 1681975635);
INSERT INTO `yunduan_district` VALUES ('c7f59cfc8ef4dd5f5be3993687525a96', '410700', '410000', '新乡市', 2, '{\"county\": 12}', 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('c7fdc9a80c4594120712ab4da1092ada', '622921', '622900', '临夏县', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('c8133655e0511b78273e305f00c939ab', '522636', '522600', '丹寨县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('c81aaf822b2d0f3dba54849063c49588', '510121', '510100', '金堂县', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('c8255a2828f105b3885e980511c0c7ae', '513331', '513300', '白玉县', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('c82d0eb70e98461750e6c8c35f0f3075', '220402', '220400', '龙山区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('c84058928c7872fb160849cc6a59bff3', '620721', '620700', '肃南裕固族自治县', 3, NULL, 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('c872a62132217907c60702b3ea07e97a', '130407', '130400', '肥乡区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('c876dcabce4284cc53a5b46a7a569f7e', '513436', '513400', '美姑县', 3, NULL, 1, 1647931271, 1681977524);
INSERT INTO `yunduan_district` VALUES ('c883775d45f65a9b005a9acb29972229', '450921', '450900', '容县', 3, NULL, 1, 1647931271, 1681975641);
INSERT INTO `yunduan_district` VALUES ('c893f82a4cf6914c22a24bf36efff122', '421123', '421100', '罗田县', 3, NULL, 1, 1647931271, 1681975544);
INSERT INTO `yunduan_district` VALUES ('c89623a60d3fad2ee76559c62559381e', '469030', '469000', '琼中黎族苗族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('c8b5f926713978268d0f569120105ee5', '230100', '230000', '哈尔滨市', 2, '{\"county\": 18}', 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('c8e0906b5f6b319eb1a2fc58223b140d', '820000', '0', '澳门特别行政区', 1, NULL, 1, 1647931271, 1661303932);
INSERT INTO `yunduan_district` VALUES ('c92e895edbec39b41269f323a3f4597f', '360924', '360900', '宜丰县', 3, NULL, 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('c941f766dba2f773b78b73928a3a88c0', '350481', '350400', '永安市', 3, NULL, 1, 1647931271, 1681975421);
INSERT INTO `yunduan_district` VALUES ('c953637ca6c92f996d998ee0a2b83b73', '130826', '130800', '丰宁满族自治县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('c9c2cbf0729158f0407361e375a740af', '610102', '610100', '新城区', 3, NULL, 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('c9c7d9d85e7f4dd3fea71cbd70ef9bae', '440784', '440700', '鹤山市', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('c9dc1cea8539f94f17676aec21aa880a', '371002', '371000', '环翠区', 3, NULL, 1, 1647931271, 1681975472);
INSERT INTO `yunduan_district` VALUES ('c9ee63df21d66e123217e8a3feeb2cb9', '370686', '370600', '栖霞市', 3, NULL, 1, 1647931271, 1681975475);
INSERT INTO `yunduan_district` VALUES ('c9f4b3577438b460f9db69ad95f0d801', '130229', '130200', '玉田县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('c9fa597b7a1093adbbe4126bcd1596cb', '510321', '510300', '荣县', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('ca18e6b43f578b73059af87802e9527d', '210602', '210600', '元宝区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('ca2b5a73dd8cf7c970c15345c73df9ef', '130181', '130100', '辛集市', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('ca3e8e847d2712fe676ebd2c471d651e', '350427', '350400', '沙县', 3, NULL, 1, 1647931271, 1681975421);
INSERT INTO `yunduan_district` VALUES ('ca441d09d786c818a93ea9684267b343', '640303', '640300', '红寺堡区', 3, NULL, 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('ca5e9f2e3b66fe7a9cad5a8b1bf38f8a', '210802', '210800', '站前区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('ca7c3fd8807ad3e6c66e77780aa1b914', '231283', '231200', '海伦市', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('ca8c5624ecfbd915e57515e0a8a92f2d', '611022', '611000', '丹凤县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('caada0fb013c52084eca45d4bbd8d037', '632822', '632800', '都兰县', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('cb140b5ef32f2984bea4e565391b518c', '522732', '522700', '三都水族自治县', 3, NULL, 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('cb3af50e63317f7e945bcb7416131dcb', '131128', '131100', '阜城县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('cb40b56e0cc0f560749548f6d8f92815', '220203', '220200', '龙潭区', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('cb42d275259c003a06cdefb887d91321', '211202', '211200', '银州区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('cb4f25eadfaddee275427c08db40179d', '430223', '430200', '攸县', 3, NULL, 1, 1647931271, 1681975550);
INSERT INTO `yunduan_district` VALUES ('cb5620447fed27695f4fcf612fed58b3', '440000', '0', '广东省', 1, '{\"city\": 21, \"county\": 122}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('cb6711c1024ea80cdcab442456cec500', '130207', '130200', '丰南区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('cb6eaf651435f9be535588ee731bcd4a', '360802', '360800', '吉州区', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('cb7187be6450803fdd754fb148d7e966', '532524', '532500', '建水县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('cb85297364e8d5bd2e3af0c7a088bf04', '440117', '440100', '从化区', 3, NULL, 1, 1647931271, 1681975598);
INSERT INTO `yunduan_district` VALUES ('cbbd33730a92be1340e9a81f613a977c', '610122', '610100', '蓝田县', 3, NULL, 1, 1647931271, 1681978073);
INSERT INTO `yunduan_district` VALUES ('cbc57d050e2d60a6650b8abda34f8aac', '350430', '350400', '建宁县', 3, NULL, 1, 1647931271, 1681975421);
INSERT INTO `yunduan_district` VALUES ('cbc86ce0e9fd74e61392e35e03b9e7ec', '210400', '210000', '抚顺市', 2, '{\"county\": 7}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('cbcf2c640ba76f1bed06702be56454db', '152224', '152200', '突泉县', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('cc0268fb5cc0125d19ccf6944a531651', '350922', '350900', '古田县', 3, NULL, 1, 1647931271, 1681975407);
INSERT INTO `yunduan_district` VALUES ('cc1112627786010e9dcd3e8e7add9d24', '340602', '340600', '杜集区', 3, NULL, 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('cc12c28e21d04a3bf32e4b3adb2fdf94', '211200', '210000', '铁岭市', 2, '{\"county\": 7}', 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('cc48770ac0be469a268d2546ee8bc47e', '511681', '511600', '华蓥市', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('cc5826ba0ea7159231629621f012dc27', '530428', '530400', '元江哈尼族彝族傣族自治县', 3, NULL, 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('cc6e2f03da01b6a0e651e21632873e65', '150421', '150400', '阿鲁科尔沁旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('cc87b78133870f33c5913fe41a7a8995', '150500', '150000', '通辽市', 2, '{\"county\": 8}', 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('cc952e7a5113ca7f7268c4cdb2e74f81', '330104', '330100', '江干区', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('cc9fe8db837cfabbc0b5ab9a4a75055b', '371321', '371300', '沂南县', 3, NULL, 1, 1647931271, 1681975480);
INSERT INTO `yunduan_district` VALUES ('cca2f4623f4bf981947cacafabcf9418', '360733', '360700', '会昌县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('ccaa6a02e4b6d43fc9f5f0cacfb60de1', '341723', '341700', '青阳县', 3, NULL, 1, 1647931271, 1681975388);
INSERT INTO `yunduan_district` VALUES ('ccaa9bb715631c86a79d1449a88e19d4', '141034', '141000', '汾西县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('ccb31fb7da89d860a18e76ea510c7e65', '520403', '520400', '平坝区', 3, NULL, 1, 1647931271, 1681977720);
INSERT INTO `yunduan_district` VALUES ('ccd311114f0d939e8725963a6b196e55', '420104', '420100', '硚口区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('ccdde2cef29126f3eb899eae6ce031eb', '230123', '230100', '依兰县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('ccf54e3a86d337697a3f547363894c78', '451102', '451100', '八步区', 3, NULL, 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('ccf68c298841f4e181575226480cbb70', '140921', '140900', '定襄县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('ccf72bccb5d902557dab2264017737b1', '621025', '621000', '正宁县', 3, NULL, 1, 1647931271, 1681978247);
INSERT INTO `yunduan_district` VALUES ('cd0978f20a33b793313197e3d4d000f4', '511823', '511800', '汉源县', 3, NULL, 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('cd1afdc924f20ac2d8d96a4903c6920b', '150521', '150500', '科尔沁左翼中旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('cd2ad405149818461e55780a36613711', '350105', '350100', '马尾区', 3, NULL, 1, 1647931271, 1681975418);
INSERT INTO `yunduan_district` VALUES ('cd3052f4324dcb40fa7e1bf41719a4b3', '141022', '141000', '翼城县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('cd30eac4b18014ded5abe2296eeb68e4', '610629', '610600', '洛川县', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('cd656c039d75ab3e79412d663ccf0ded', '511724', '511700', '大竹县', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('cd6b18f249fed178f6d25169e935e3c6', '410403', '410400', '卫东区', 3, NULL, 1, 1647931271, 1681975514);
INSERT INTO `yunduan_district` VALUES ('cd6ee84eb0b2df9aed022d56b997f485', '420606', '420600', '樊城区', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('cdd832e5ef5879a30ef1cbadc0670ccc', '532501', '532500', '个旧市', 3, NULL, 1, 1647931271, 1681977884);
INSERT INTO `yunduan_district` VALUES ('cde506c037e8e2c9908570d50b7a9cdd', '411523', '411500', '新县', 3, NULL, 1, 1647931271, 1681975528);
INSERT INTO `yunduan_district` VALUES ('cde59d469235baa3c2c9368df5c95db1', '350213', '350200', '翔安区', 3, NULL, 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('cdff796488b206e955e2944f2b185122', '340706', '340700', '义安区', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('cdffa380d347ccf54a3e85263c0426de', '611002', '611000', '商州区', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('ce1d60e4c4386e16e0a48d8ae5aef7ef', '350722', '350700', '浦城县', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('ce3788a3355ad5210d5ae6e273184ba3', '520602', '520600', '碧江区', 3, NULL, 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('ce82da1d0934abaeed0b92c9544079f4', '652901', '652900', '阿克苏市', 3, NULL, 1, 1647931271, 1681978495);
INSERT INTO `yunduan_district` VALUES ('ce84df6ef2ce75dfef6ddfbf6c2f7686', '320681', '320600', '启东市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('ce8bf76b6b1ece4ef6c0b684670542d8', '341882', '341800', '广德市', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('ce92eacb93b539fdaa3ccbb922f1a348', '321183', '321100', '句容市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('cea008d682d330f69899c4c295a07a11', '130203', '130200', '路北区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('ceb4d11af4e2e358e3e9ffb901c8e09d', '540200', '540000', '日喀则市', 2, '{\"county\": 18}', 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('cec4b03ffc9d7a1f10e7fcafcacb093a', '430981', '430900', '沅江市', 3, NULL, 1, 1647931271, 1681975564);
INSERT INTO `yunduan_district` VALUES ('cecedbfc349e735d22d44a4219b2ec9e', '520221', '520200', '水城县', 3, NULL, 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('cecffe9ba3664c119a3fea25a5d4e6cb', '500114', '500100', '黔江区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('ced84d54ebbc14a9ba9c3b15070c7ce8', '350124', '350100', '闽清县', 3, NULL, 1, 1647931271, 1681975417);
INSERT INTO `yunduan_district` VALUES ('cee02afac8ba8c41338977b58763e841', '130424', '130400', '成安县', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('cee85fba4aad66159be41e7fcd677fa8', '450125', '450100', '上林县', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('ceec64e0889dcb5370a93de4b78aa985', '620902', '620900', '肃州区', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('cf0d6d1796cadf9806ab476a40d61c4d', '445100', '440000', '潮州市', 2, '{\"county\": 3}', 1, 1647931271, 1681975576);
INSERT INTO `yunduan_district` VALUES ('cf2a49ed3177d4ec262bde5a8056032c', '370285', '370200', '莱西市', 3, NULL, 1, 1647931271, 1681975484);
INSERT INTO `yunduan_district` VALUES ('cf2f6616b3b2c806c42a6fd487346643', '230882', '230800', '富锦市', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('cf4b87d2f1af242da450c392b2f24f75', '130503', '130500', '信都区', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('cf4c07fb1739e06b8d70ffd5edf999ba', '522633', '522600', '从江县', 3, NULL, 1, 1647931271, 1681977733);
INSERT INTO `yunduan_district` VALUES ('cfa79d2e0c1c89c39e0789710cad1cca', '230506', '230500', '宝山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('cfc32ddaab9dad94c32e311dddea9c0b', '440500', '440000', '汕头市', 2, '{\"county\": 7}', 1, 1647931271, 1681975577);
INSERT INTO `yunduan_district` VALUES ('cfcdf6e6174453b30d35b87b3887f881', '150304', '150300', '乌达区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('cfdc5b1c3c58796f1fd8cb1835810b39', '420322', '420300', '郧西县', 3, NULL, 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('cfe254d17ba26a407f7ec62656722081', '530925', '530900', '双江拉祜族佤族布朗族傣族自治县', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('cfe569291818a9c370b641f4506e62ab', '320400', '320000', '常州市', 2, '{\"county\": 6}', 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('cff209682a6b10733f664d7fbf39a7a9', '440307', '440300', '龙岗区', 3, NULL, 1, 1647931271, 1681975602);
INSERT INTO `yunduan_district` VALUES ('cff8fb4980743670498b601090d0aa92', '230125', '230100', '宾县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('cffc39e2babdc7ee63f6ebda242fe4a5', '231081', '231000', '绥芬河市', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('d0066fef79b9447486a272a5fe73fbcc', '530127', '530100', '嵩明县', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('d026629e3bd223a3f35be39660f2d256', '130824', '130800', '滦平县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('d0326b83a654cce0218b81f2d115e378', '653124', '653100', '泽普县', 3, NULL, 1, 1647931271, 1681978489);
INSERT INTO `yunduan_district` VALUES ('d050454cb4e024083a7162439a234846', '331102', '331100', '莲都区', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('d0653eb249c4d601e09048e91e49d8d1', '533422', '533400', '德钦县', 3, NULL, 1, 1647931271, 1681977874);
INSERT INTO `yunduan_district` VALUES ('d086c7ede976b900f2fc1de3837a7a34', '341702', '341700', '贵池区', 3, NULL, 1, 1647931271, 1681975388);
INSERT INTO `yunduan_district` VALUES ('d08ec239616ce7888de2a20ff3d3dae8', '141033', '141000', '蒲县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('d0918e6e26ff3fb0d0aad9a3a2c0a2f9', '430103', '430100', '天心区', 3, NULL, 1, 1647931271, 1681975565);
INSERT INTO `yunduan_district` VALUES ('d09c093b25db192b4e41efb6000372ae', '360104', '360100', '青云谱区', 3, NULL, 1, 1647931271, 1681975448);
INSERT INTO `yunduan_district` VALUES ('d0a8b0fc1911f922b7de28393c7d7fb3', '431321', '431300', '双峰县', 3, NULL, 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('d0c47bc2f19040723ea57d4c27339589', '420526', '420500', '兴山县', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('d0f1a47dc77d904385c5cb59eea17d71', '510115', '510100', '温江区', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('d0fb9496c09a20a4f8d4b2f52f1f563d', '654000', '650000', '伊犁哈萨克自治州', 2, '{\"county\": 11}', 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('d10714b6b7a74f79ace08abf70cf9d37', '230303', '230300', '恒山区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('d130598b27be154c16b3475fc2b7f78e', '371122', '371100', '莒县', 3, NULL, 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('d137325df56609458afcda9d31d720f9', '410811', '410800', '山阳区', 3, NULL, 1, 1647931271, 1681975515);
INSERT INTO `yunduan_district` VALUES ('d13967316d0c8ccb21fb6e8ecf5305d3', '654202', '654200', '乌苏市', 3, NULL, 1, 1647931271, 1681978499);
INSERT INTO `yunduan_district` VALUES ('d13f8cd04631d9f5b739a2c5f2d759b9', '141125', '141100', '柳林县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('d1621503266b6ab92953d0ee37c6f21c', '441324', '441300', '龙门县', 3, NULL, 1, 1647931271, 1681975585);
INSERT INTO `yunduan_district` VALUES ('d16670be169c0a075762e5c9ad8b15fe', '620423', '620400', '景泰县', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('d1686314d84424c261b68437b99b8351', '140100', '140000', '太原市', 2, '{\"county\": 10}', 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('d176156de33d25f3d2addcf562dcfb66', '520600', '520000', '铜仁市', 2, '{\"county\": 10}', 1, 1647931271, 1681977721);
INSERT INTO `yunduan_district` VALUES ('d1772361be0e7003d12b7d7d8b84a485', '440783', '440700', '开平市', 3, NULL, 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('d1a25739507c852ba63eafcc992a7a74', '532923', '532900', '祥云县', 3, NULL, 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('d1a8b8f1bd0681d60531575ef078eb18', '360827', '360800', '遂川县', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('d1b7a6735300bc95c6535cf18fa33e26', '140781', '140700', '介休市', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('d1c10650e729e36d3d6b9b225a2fd0e0', '500103', '500100', '渝中区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('d1e86a259feb212a51c8e2f0d251b377', '469005', '469000', '文昌市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('d217f6e1a058855b54aaf22bbc0be07f', '210114', '210100', '于洪区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('d22da0fdc9cb7ef89f9a37637c20033d', '440608', '440600', '高明区', 3, NULL, 1, 1647931271, 1681975593);
INSERT INTO `yunduan_district` VALUES ('d24571b55c0bad44825acf2ab2a5381b', '330103', '330100', '下城区', 3, NULL, 1, 1647931271, 1681975356);
INSERT INTO `yunduan_district` VALUES ('d2b2c621263e7b541c3cba27d0301ce5', '340600', '340000', '淮北市', 2, '{\"county\": 4}', 1, 1647931271, 1681975381);
INSERT INTO `yunduan_district` VALUES ('d2d3a50c1345b0f8b0a4d916c1e8d8cc', '654223', '654200', '沙湾县', 3, NULL, 1, 1647931271, 1681978499);
INSERT INTO `yunduan_district` VALUES ('d2d5d871900e8bcf6f131a1a6fdcafb9', '230622', '230600', '肇源县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('d308f8b54941b991c14f59c2c7298e0c', '310120', '310100', '奉贤区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('d30b445d7f6af59f3896c5910026acfd', '320205', '320200', '锡山区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('d3110e747fcd7629cb1567e62c58e574', '510903', '510900', '船山区', 3, NULL, 1, 1647931271, 1681977544);
INSERT INTO `yunduan_district` VALUES ('d3313daab231c8c74f42814268b836c5', '451082', '451000', '平果市', 3, NULL, 1, 1647931271, 1681975630);
INSERT INTO `yunduan_district` VALUES ('d33bc9566f6a4f3d0bc5cfc60a4b9460', '540225', '540200', '拉孜县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('d345072a05eb00420cd9d700168c2fc2', '511600', '510000', '广安市', 2, '{\"county\": 6}', 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('d357a342ea3fe039b299105f2e2caa5d', '310000', '0', '上海市', 1, '{\"city\": 1, \"county\": 16}', 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('d36fc5860a9fc65e86653d2743ed0bad', '630102', '630100', '城东区', 3, NULL, 1, 1647931271, 1681978314);
INSERT INTO `yunduan_district` VALUES ('d382a1d5bd493975cc3a6be964755ced', '420503', '420500', '伍家岗区', 3, NULL, 1, 1647931271, 1681975533);
INSERT INTO `yunduan_district` VALUES ('d38ad4ded674e8dc34afdd2b3423f202', '441402', '441400', '梅江区', 3, NULL, 1, 1647931271, 1681975581);
INSERT INTO `yunduan_district` VALUES ('d3aa14fe3c372cec739c2e9ecf0dd5cf', '410782', '410700', '辉县市', 3, NULL, 1, 1647931271, 1681975517);
INSERT INTO `yunduan_district` VALUES ('d3b20c08d4c261caa8088b6d8552d9ac', '340000', '0', '安徽省', 1, '{\"city\": 16, \"county\": 104}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('d3c3066f568aafd38f60d653f383a44f', '140105', '140100', '小店区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('d41e64c875848a96d83487f7abdade94', '421087', '421000', '松滋市', 3, NULL, 1, 1647931271, 1681975541);
INSERT INTO `yunduan_district` VALUES ('d420e95e8c2339867ed9795ba37d32ac', '370983', '370900', '肥城市', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('d4223c409810cc62cb74379ccc0447c2', '150725', '150700', '陈巴尔虎旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('d42d3df6d5f84b68f42b60bb0edc5290', '321204', '321200', '姜堰区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('d43cbb3b30b5afdc371b47e7e9364732', '513301', '513300', '康定市', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('d447c10b5a6259941d4b4f9c352fba2f', '440106', '440100', '天河区', 3, NULL, 1, 1647931271, 1681975596);
INSERT INTO `yunduan_district` VALUES ('d44dcb05de7b9a024709022b004553aa', '520500', '520000', '毕节市', 2, '{\"county\": 8}', 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('d46cf8ded2588ba49c9bdd8bf6465b8f', '540103', '540100', '堆龙德庆区', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('d480cc805324b840722a93ea61a17bc2', '610100', '610000', '西安市', 2, '{\"county\": 13}', 1, 1647931271, 1681978071);
INSERT INTO `yunduan_district` VALUES ('d4973d17f3ad393e08ad902e57544777', '441825', '441800', '连山壮族瑶族自治县', 3, NULL, 1, 1647931271, 1681975589);
INSERT INTO `yunduan_district` VALUES ('d4a55b569f73066fd8ca9bb3a0f43529', '220283', '220200', '舒兰市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('d4b358253135841f15b0478581294881', '430423', '430400', '衡山县', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('d4cc6af52fe7900af8f01cfae87e400a', '411603', '411600', '淮阳区', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('d4cfe4b49d8173537e3eaf1bd0479387', '632222', '632200', '祁连县', 3, NULL, 1, 1647931271, 1681978320);
INSERT INTO `yunduan_district` VALUES ('d4d5bc97c7ae4f1450dcf3114886e037', '350900', '350000', '宁德市', 2, '{\"county\": 9}', 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('d4f946c33babd451139c04f7dcd09b92', '640302', '640300', '利通区', 3, NULL, 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('d50ceca0f231dbb364305a6b5114b5e2', '130723', '130700', '康保县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('d51413c5b1e0ceed36f136d6b993ec83', '650105', '650100', '水磨沟区', 3, NULL, 1, 1647931271, 1681978507);
INSERT INTO `yunduan_district` VALUES ('d514dd28614d7edb2655a9695803bb4a', '430802', '430800', '永定区', 3, NULL, 1, 1647931271, 1681975573);
INSERT INTO `yunduan_district` VALUES ('d517fb47dd906c8b29e3acbfe3886e62', '530124', '530100', '富民县', 3, NULL, 1, 1647931271, 1681977890);
INSERT INTO `yunduan_district` VALUES ('d51a25de0869b2f2c4d4ca9ca6fcf2bd', '500240', '500200', '石柱土家族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('d536087b94489478f6306ee381772a73', '341221', '341200', '临泉县', 3, NULL, 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('d542eaa29f534232f109687e7d720c5e', '620725', '620700', '山丹县', 3, NULL, 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('d5781b21ade6c90c9a5901650e173a06', '510311', '510300', '沿滩区', 3, NULL, 1, 1647931271, 1681977540);
INSERT INTO `yunduan_district` VALUES ('d5e1277ee14542f24948ef9da72f9b35', '320211', '320200', '滨湖区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('d5ebaedc2d21faf5005c54a3a9e3195b', '420117', '420100', '新洲区', 3, NULL, 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('d6044beae33934bcfb002c2f606c3cd2', '230822', '230800', '桦南县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('d60de0ee75364b2748f11e3b48068eeb', '341225', '341200', '阜南县', 3, NULL, 1, 1647931271, 1681975378);
INSERT INTO `yunduan_district` VALUES ('d619d4cfd3d44642a8dc6abf4d18c3d8', '540426', '540400', '朗县', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('d629cb26203ff8369591d1516f57f335', '140226', '140200', '左云县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('d62be4b3e4e51319605c94413677fb52', '210211', '210200', '甘井子区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('d62d0fb189a65413e41b521ace9f464b', '620923', '620900', '肃北蒙古族自治县', 3, NULL, 1, 1647931271, 1681978242);
INSERT INTO `yunduan_district` VALUES ('d63ac5a3667e551d0a6f494cf5cd5d6d', '411503', '411500', '平桥区', 3, NULL, 1, 1647931271, 1681975528);
INSERT INTO `yunduan_district` VALUES ('d66d841f45db7525c9cc43e5071066d8', '522600', '520000', '黔东南苗族侗族自治州', 2, '{\"county\": 16}', 1, 1647931271, 1681977731);
INSERT INTO `yunduan_district` VALUES ('d67bd3e075b7e8d2628afd12bc80400a', '530400', '530000', '玉溪市', 2, '{\"county\": 9}', 1, 1647931271, 1681977892);
INSERT INTO `yunduan_district` VALUES ('d68d16407acf475b6f2916fed91cf88f', '420506', '420500', '夷陵区', 3, NULL, 1, 1647931271, 1681975534);
INSERT INTO `yunduan_district` VALUES ('d6b39a161b47072bc0e5eeb560a47eac', '431022', '431000', '宜章县', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('d6f1ba5fbcee63845c08f2748529c9a9', '440305', '440300', '南山区', 3, NULL, 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('d71060b9c84e4ff66b4e8a1eb2e225a8', '150825', '150800', '乌拉特后旗', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('d72c5aad6641fcc89d113bacbe0dcfc1', '530125', '530100', '宜良县', 3, NULL, 1, 1647931271, 1681977892);
INSERT INTO `yunduan_district` VALUES ('d72d210ed6839df4677f792b9e3d057a', '370300', '370000', '淄博市', 2, '{\"county\": 8}', 1, 1647931271, 1681975485);
INSERT INTO `yunduan_district` VALUES ('d73ea86637e2a01d8264bb5925cbcb56', '411303', '411300', '卧龙区', 3, NULL, 1, 1647931271, 1681975520);
INSERT INTO `yunduan_district` VALUES ('d74777afb8dbb6e797de22992ee56c32', '360726', '360700', '安远县', 3, NULL, 1, 1647931271, 1681975446);
INSERT INTO `yunduan_district` VALUES ('d77b9981ea3008fc2de60f2f7c03790a', '360803', '360800', '青原区', 3, NULL, 1, 1647931271, 1681975440);
INSERT INTO `yunduan_district` VALUES ('d77f94c50ffe131291eb2a26c2107633', '350782', '350700', '武夷山市', 3, NULL, 1, 1647931271, 1681975408);
INSERT INTO `yunduan_district` VALUES ('d78bf5ba511d7bb3e938845037d6ba5a', '430603', '430600', '云溪区', 3, NULL, 1, 1647931271, 1681975561);
INSERT INTO `yunduan_district` VALUES ('d7b3236c34d4867e2c8b8e39c6a10b65', '230128', '230100', '通河县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('d7d0626bb9ff1e45b0fd7e09a5403883', '211002', '211000', '白塔区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('d7e2aa5826ad333a6a218dc9cb412cc8', '330000', '0', '浙江省', 1, '{\"city\": 11, \"county\": 90}', 1, 1647931271, 1661325158);
INSERT INTO `yunduan_district` VALUES ('d81dbc0adbae3838225b7bbf81d4ed30', '450126', '450100', '宾阳县', 3, NULL, 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('d83702cdf53d2c5b9d33cfc556c74daa', '610502', '610500', '临渭区', 3, NULL, 1, 1647931271, 1681978079);
INSERT INTO `yunduan_district` VALUES ('d848ae9dc878bfeafd1cbce72c7ca64a', '420202', '420200', '黄石港区', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('d862155ba61e0de5b4545d8c698e3fbb', '320813', '320800', '洪泽区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('d86f1e7c3023e02aaff3395dcab32d49', '440902', '440900', '茂南区', 3, NULL, 1, 1647931271, 1681975582);
INSERT INTO `yunduan_district` VALUES ('d87af4e448cacb4d2c88da0612fe6ec0', '140724', '140700', '昔阳县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('d89470e95d80d6df85c15452e2be375b', '540421', '540400', '工布江达县', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('d899dbff496c7dd2bd23114de14c035d', '150726', '150700', '新巴尔虎左旗', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('d8b06b30c7ec8d417f41c288bfcb6bb3', '450224', '450200', '融安县', 3, NULL, 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('d8b51b32a24b2495414fb4b5784c2269', '411324', '411300', '镇平县', 3, NULL, 1, 1647931271, 1681975519);
INSERT INTO `yunduan_district` VALUES ('d909e5c9f2d622de6adaf94b7ee037ba', '440404', '440400', '金湾区', 3, NULL, 1, 1647931271, 1681975604);
INSERT INTO `yunduan_district` VALUES ('d914f4535f91fe8bb100afab6df915ee', '620402', '620400', '白银区', 3, NULL, 1, 1647931271, 1681978239);
INSERT INTO `yunduan_district` VALUES ('d92d8c3fd595a595d957715a83973887', '410402', '410400', '新华区', 3, NULL, 1, 1647931271, 1681975513);
INSERT INTO `yunduan_district` VALUES ('d946cfe3af99d95b0cc5d25863b4ce75', '512022', '512000', '乐至县', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('d9721e65755a31f86674bfe23ce8b97c', '420204', '420200', '下陆区', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('d995a7b63f20ad334b9209ebea7abe4e', '469024', '469000', '临高县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('d99a3495c29d20ab4b1f46f59015439b', '450502', '450500', '海城区', 3, NULL, 1, 1647931271, 1681975623);
INSERT INTO `yunduan_district` VALUES ('d9a05a120d906bbb914f154c5bd7852d', '150626', '150600', '乌审旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('da0a0e486ded0c3095bb36ab7479f20f', '542527', '542500', '措勤县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('da10d7d257a06f24787300228de94cf1', '652722', '652700', '精河县', 3, NULL, 1, 1647931271, 1681978502);
INSERT INTO `yunduan_district` VALUES ('da14aaee872353df12323f91451871ea', '530723', '530700', '华坪县', 3, NULL, 1, 1647931271, 1681977895);
INSERT INTO `yunduan_district` VALUES ('da45dc39855c2fc4c4100e980ac3a851', '220800', '220000', '白城市', 2, '{\"county\": 5}', 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('daa6dd81b6918b96aa52335754555b10', '230405', '230400', '兴安区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('dab5c422df18e746383e5818e3be9c83', '632324', '632300', '河南蒙古族自治县', 3, NULL, 1, 1647931271, 1681978319);
INSERT INTO `yunduan_district` VALUES ('dac089b998b3a89b333c3b966c9d6d11', '440781', '440700', '台山市', 3, NULL, 1, 1647931271, 1681975580);
INSERT INTO `yunduan_district` VALUES ('dacc3bfdaea1e9273f98ab61b2b5dc9a', '610422', '610400', '三原县', 3, NULL, 1, 1647931271, 1681978069);
INSERT INTO `yunduan_district` VALUES ('dad007553325538c0936c81526b3882f', '430624', '430600', '湘阴县', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('dada8378cbc5b2244d16cd6ecc84af98', '140431', '140400', '沁源县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('dadbe02e62af8ad5d35821c3c726ebc3', '653222', '653200', '墨玉县', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('db19b2626fdae8752b0c02b89e9ca6c0', '360111', '360100', '青山湖区', 3, NULL, 1, 1647931271, 1681975449);
INSERT INTO `yunduan_district` VALUES ('db1ad5365544b2b3e39e6f76b7a518ad', '652800', '650000', '巴音郭楞蒙古自治州', 2, '{\"county\": 9}', 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('db3df51e920c24421c2f039917c4771c', '420802', '420800', '东宝区', 3, NULL, 1, 1647931271, 1681975542);
INSERT INTO `yunduan_district` VALUES ('db68124eb64d49309a491ca136aa85c8', '420325', '420300', '房县', 3, NULL, 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('db78636931792ba5a44895127135d048', '220281', '220200', '蛟河市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('db7d016d9e02d6dad338b7783325e8da', '230804', '230800', '前进区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('db814b603d332f3be098d3945e4e57d3', '320682', '320600', '如皋市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('db8d7504f2c2c31de49b164903d9a6f4', '420114', '420100', '蔡甸区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('dba2c5a71e82976852117856476680df', '320100', '320000', '南京市', 2, '{\"county\": 11}', 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('dbbbe52d555e290fba2120d9eb4e1d89', '610924', '610900', '紫阳县', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('dbde86d011bdca20cf7b22680b2f07e9', '445222', '445200', '揭西县', 3, NULL, 1, 1647931271, 1681975601);
INSERT INTO `yunduan_district` VALUES ('dbe57e2e65ee52ca325998e7a32c14f9', '511902', '511900', '巴州区', 3, NULL, 1, 1647931271, 1681977513);
INSERT INTO `yunduan_district` VALUES ('dbecf7e10b07c9d1458bd03b06a189ad', '632724', '632700', '治多县', 3, NULL, 1, 1647931271, 1681978311);
INSERT INTO `yunduan_district` VALUES ('dbed2f100f85ea81b005192e67145a22', '140700', '140000', '晋中市', 2, '{\"county\": 11}', 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('dbf31b34556a7b74a075538116675242', '130224', '130200', '滦南县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('dc0cb66e761eaf54f374a69bee9fa543', '632524', '632500', '兴海县', 3, NULL, 1, 1647931271, 1681978318);
INSERT INTO `yunduan_district` VALUES ('dc20b99eefd8d5a80db5f289f4d90b13', '532623', '532600', '西畴县', 3, NULL, 1, 1647931271, 1681977882);
INSERT INTO `yunduan_district` VALUES ('dc6caa7d1a1e1802253b8ea779bbafd7', '330824', '330800', '开化县', 3, NULL, 1, 1647931271, 1681975349);
INSERT INTO `yunduan_district` VALUES ('dc74d2462c51eb81c4e777e97c5d03d0', '530825', '530800', '镇沅彝族哈尼族拉祜族自治县', 3, NULL, 1, 1647931271, 1681977875);
INSERT INTO `yunduan_district` VALUES ('dca3283030e391bbf1d6d9ac7f1c2501', '540236', '540200', '萨嘎县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('dca4af823900b333435799fa89879e6e', '371423', '371400', '庆云县', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('dcae765ca98eb385d0dc6a6aaa611dce', '420602', '420600', '襄城区', 3, NULL, 1, 1647931271, 1681975530);
INSERT INTO `yunduan_district` VALUES ('dcc4647c54193950119707cb1822ee8f', '430100', '430000', '长沙市', 2, '{\"county\": 9}', 1, 1647931271, 1681975564);
INSERT INTO `yunduan_district` VALUES ('dcd06b564831f6ef83d25415875b1d40', '442000', '440000', '中山市', 2, NULL, 1, 1647931271, 1681975601);
INSERT INTO `yunduan_district` VALUES ('dcf326dd79e2bb3d565962ca095a45ec', '520522', '520500', '黔西县', 3, NULL, 1, 1647931271, 1681977723);
INSERT INTO `yunduan_district` VALUES ('dd10f9e4722ac5ae245b5cf5a6eff6eb', '361022', '361000', '黎川县', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('dd31bb7e1aeed24c1c620bb38978a376', '230524', '230500', '饶河县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('dd3704a16ec4d4984518f8beae45165b', '130281', '130200', '遵化市', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('dd4276f3c57b6aa1091a0227f271b748', '222401', '222400', '延吉市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('dd980408728380d95556c878cd4de863', '440300', '440000', '深圳市', 2, '{\"county\": 9}', 1, 1647931271, 1681975602);
INSERT INTO `yunduan_district` VALUES ('ddad6f412e9ac7b27dbf469559c6a1ea', '450804', '450800', '覃塘区', 3, NULL, 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('ddc6ba48f5b01544e552ff25b9464c05', '620321', '620300', '永昌县', 3, NULL, 1, 1647931271, 1681978245);
INSERT INTO `yunduan_district` VALUES ('ddd37a60cfce79cc527f85ea2d7cb245', '513329', '513300', '新龙县', 3, NULL, 1, 1647931271, 1681977514);
INSERT INTO `yunduan_district` VALUES ('dde15d8961b1f3e7f5a9329dae51eccb', '140106', '140100', '迎泽区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('ddef332735d33d07225b159f2dbc6bc1', '360723', '360700', '大余县', 3, NULL, 1, 1647931271, 1681975444);
INSERT INTO `yunduan_district` VALUES ('de3b35d7bc428dd4a52d706092ed7716', '410802', '410800', '解放区', 3, NULL, 1, 1647931271, 1681975515);
INSERT INTO `yunduan_district` VALUES ('de532660e345a0148d8d31174bab084f', '360902', '360900', '袁州区', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('de7f6fe1a552e66dbe4ec43eb70d2294', '620622', '620600', '古浪县', 3, NULL, 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('de83868796493ed456a071bed9681931', '421122', '421100', '红安县', 3, NULL, 1, 1647931271, 1681975545);
INSERT INTO `yunduan_district` VALUES ('de9216c1a6359f12e611f19be4a6afd8', '511781', '511700', '万源市', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('de981e439a17199a61c4276a00f92b4f', '430104', '430100', '岳麓区', 3, NULL, 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('deb913720b1da34df8935fd3a5a73a06', '150221', '150200', '土默特右旗', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('dec6211061b958d7667a16bb27a47ef0', '141026', '141000', '安泽县', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('dec877f4895301306586a0cca52c6370', '652827', '652800', '和静县', 3, NULL, 1, 1647931271, 1681978501);
INSERT INTO `yunduan_district` VALUES ('ded30a3c54a47237e6b2b4bfca3ee4c9', '321203', '321200', '高港区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('ded61bc8d6298179371176dc9dc8fe73', '371000', '370000', '威海市', 2, '{\"county\": 4}', 1, 1647931271, 1681975471);
INSERT INTO `yunduan_district` VALUES ('ded793d1f9872e2a97dc9b4f793eb03e', '500107', '500100', '九龙坡区', 3, NULL, 1, 1647931271, 1681977449);
INSERT INTO `yunduan_district` VALUES ('deedcf8292bacc1c212b17f4f4a4a13a', '430681', '430600', '汨罗市', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('df0d9c2b76312f6c691bf24a0385eb68', '611025', '611000', '镇安县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('df1941010de82e2a236b0fb9a8808e29', '610324', '610300', '扶风县', 3, NULL, 1, 1647931271, 1681978076);
INSERT INTO `yunduan_district` VALUES ('df3b940fbef8446c3010a6360257cd6b', '150902', '150900', '集宁区', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('df6b5c513408501d8c6175e19a44c223', '370781', '370700', '青州市', 3, NULL, 1, 1647931271, 1681975487);
INSERT INTO `yunduan_district` VALUES ('df6ef60e0a773e84b86a9318716dbbcd', '320118', '320100', '高淳区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('df798dc53aa36f1c031bfcadde44e12f', '350123', '350100', '罗源县', 3, NULL, 1, 1647931271, 1681975418);
INSERT INTO `yunduan_district` VALUES ('df9b9b3db7b7ad3976a1a4e65c98de39', '520111', '520100', '花溪区', 3, NULL, 1, 1647931271, 1681977725);
INSERT INTO `yunduan_district` VALUES ('dfa5e6a9cdff30b35b16348b0b598327', '360600', '360000', '鹰潭市', 2, '{\"county\": 3}', 1, 1647931271, 1681975446);
INSERT INTO `yunduan_district` VALUES ('dfb5f3909a63531fcd8441efcd4edafa', '433124', '433100', '花垣县', 3, NULL, 1, 1647931271, 1681975562);
INSERT INTO `yunduan_district` VALUES ('dfc3eaa588d73da63aa57345cce053e8', '451226', '451200', '环江毛南族自治县', 3, NULL, 1, 1647931271, 1681975637);
INSERT INTO `yunduan_district` VALUES ('dfe59201f1e67af2b31ffdc129b10fbd', '330212', '330200', '鄞州区', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('e0326c97177b9a6ee75dc9ccdbc9edf2', '210403', '210400', '东洲区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('e03873e8164460255008876a793f9a74', '542523', '542500', '噶尔县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('e03c786fbd211580274220c523c1615f', '321111', '321100', '润州区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('e04acccb74881e8e59812e4aa3349f37', '632622', '632600', '班玛县', 3, NULL, 1, 1647931271, 1681978317);
INSERT INTO `yunduan_district` VALUES ('e066a470c1a83c70297da57833a1c7bf', '370600', '370000', '烟台市', 2, '{\"county\": 11}', 1, 1647931271, 1681975474);
INSERT INTO `yunduan_district` VALUES ('e069a750af5edf0e507c1e3d37434fdc', '510502', '510500', '江阳区', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('e069e21bb2fdfc6369083fe0d29e7e4c', '652327', '652300', '吉木萨尔县', 3, NULL, 1, 1647931271, 1681978497);
INSERT INTO `yunduan_district` VALUES ('e07e70340262941e2337bd148052a2d7', '410902', '410900', '华龙区', 3, NULL, 1, 1647931271, 1681975525);
INSERT INTO `yunduan_district` VALUES ('e09f1133b7ad3ccfce6c4ad91e7baeeb', '640205', '640200', '惠农区', 3, NULL, 1, 1647931271, 1681978322);
INSERT INTO `yunduan_district` VALUES ('e0a0be71c5ec89ccdc0beb943aad43f7', '210203', '210200', '西岗区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('e0be8365ea550b18a637010e9d12862c', '540227', '540200', '谢通门县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('e0fdd5ac8e33d077684b8b3c3662f58e', '520100', '520000', '贵阳市', 2, '{\"county\": 10}', 1, 1647931271, 1681977724);
INSERT INTO `yunduan_district` VALUES ('e14472dfd4674a1bd69203d350c597b0', '130110', '130100', '鹿泉区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('e14583349fdc0b1a1c2428a61ca83627', '653223', '653200', '皮山县', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('e167188d33e5daa534b56557c6508e05', '451200', '450000', '河池市', 2, '{\"county\": 11}', 1, 1647931271, 1681975636);
INSERT INTO `yunduan_district` VALUES ('e176c041041c751c72d97f53d93b72bd', '610681', '610600', '子长市', 3, NULL, 1, 1647931271, 1681978080);
INSERT INTO `yunduan_district` VALUES ('e17e1123d7964a852f61df30fb6903a4', '430502', '430500', '双清区', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('e18378afc7ff3d95dccdb5c9a0a640a7', '410300', '410000', '洛阳市', 2, '{\"county\": 15}', 1, 1647931271, 1681975501);
INSERT INTO `yunduan_district` VALUES ('e196f58e3c8c4dd14599d47109db23a7', '420303', '420300', '张湾区', 3, NULL, 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('e1b10d1ddc7ee7b61239ce9dbd1e4aca', '370283', '370200', '平度市', 3, NULL, 1, 1647931271, 1681975483);
INSERT INTO `yunduan_district` VALUES ('e1b5d60217762f6e040963d95fea6880', '130423', '130400', '临漳县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('e1c0bf2ca0bd470c669771db001fa94d', '350781', '350700', '邵武市', 3, NULL, 1, 1647931271, 1681975409);
INSERT INTO `yunduan_district` VALUES ('e1d1186fec44653c882aca38ef31d0fc', '440308', '440300', '盐田区', 3, NULL, 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('e1d20b44d9018332825c2a65b000c4f0', '620825', '620800', '庄浪县', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('e1dba3ba045c5459410a9ddde16b90e9', '341825', '341800', '旌德县', 3, NULL, 1, 1647931271, 1681975376);
INSERT INTO `yunduan_district` VALUES ('e1e140413aaed613f4157b5ca5b434f8', '410922', '410900', '清丰县', 3, NULL, 1, 1647931271, 1681975526);
INSERT INTO `yunduan_district` VALUES ('e1e4176be2094289ff98ceb052c2a26e', '360483', '360400', '庐山市', 3, NULL, 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('e2088d7f239d4cdfa1f78aed4c91bfbb', '431302', '431300', '娄星区', 3, NULL, 1, 1647931271, 1681975552);
INSERT INTO `yunduan_district` VALUES ('e25838ac176c0008e37118e1e9b62780', '430903', '430900', '赫山区', 3, NULL, 1, 1647931271, 1681975564);
INSERT INTO `yunduan_district` VALUES ('e25f41b14427664e65a8d4561f293099', '621124', '621100', '临洮县', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('e2795f0aca371d4be905a6dad89f2621', '370921', '370900', '宁阳县', 3, NULL, 1, 1647931271, 1681975478);
INSERT INTO `yunduan_district` VALUES ('e27cf6e04666ca53657fc4cb0ebc7bf3', '621225', '621200', '西和县', 3, NULL, 1, 1647931271, 1681978252);
INSERT INTO `yunduan_district` VALUES ('e2842e01fa9e07b6b980fa2741d7d68d', '130322', '130300', '昌黎县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('e29c21976271c9000b8189fdd5bb6956', '150781', '150700', '满洲里市', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('e29e1064ac264ba3979a786e32d2ad9d', '420100', '420000', '武汉市', 2, '{\"county\": 13}', 1, 1647931271, 1681975536);
INSERT INTO `yunduan_district` VALUES ('e2bd6aa6981492ecb393c688144f02d9', '230402', '230400', '向阳区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('e309fe8393a9a8011893548353dfd552', '530128', '530100', '禄劝彝族苗族自治县', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('e3222d59c294b805faae6962c9c78bab', '210115', '210100', '辽中区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('e37038111044a9c931778c05b4344b50', '420105', '420100', '汉阳区', 3, NULL, 1, 1647931271, 1681975537);
INSERT INTO `yunduan_district` VALUES ('e3705487e7475a41418e9709b76ef840', '141126', '141100', '石楼县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('e3759060a4d8f16c6d129ba5b8c72016', '140321', '140300', '平定县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('e38ab802c9f47cd74603944ea1846741', '451402', '451400', '江州区', 3, NULL, 1, 1647931271, 1681975621);
INSERT INTO `yunduan_district` VALUES ('e39806e132b458f7d82241cd4d8b7f01', '341200', '340000', '阜阳市', 2, '{\"county\": 8}', 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('e39a0bfc2f3da6d26fbb19fff9516df8', '220102', '220100', '南关区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('e39f62ec5da458a15d83195ec289c493', '440304', '440300', '福田区', 3, NULL, 1, 1647931271, 1681975603);
INSERT INTO `yunduan_district` VALUES ('e3a120bc6ffdea67eeaf91e8122a4d92', '510422', '510400', '盐边县', 3, NULL, 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('e3a187282595a3b2aa4d99d9157368c1', '230207', '230200', '碾子山区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('e3b87f68c8f5bc52ab0f62600f8e45fe', '360300', '360000', '萍乡市', 2, '{\"county\": 5}', 1, 1647931271, 1681975457);
INSERT INTO `yunduan_district` VALUES ('e3c37e0e278b7d7f99374c9b2fd3047a', '431003', '431000', '苏仙区', 3, NULL, 1, 1647931271, 1681975553);
INSERT INTO `yunduan_district` VALUES ('e3dfb927cd1a4838fe6766810f2ae05f', '330383', '330300', '龙港市', 3, NULL, 1, 1647931271, 1681975350);
INSERT INTO `yunduan_district` VALUES ('e3f3619568ae844791d3dc540bae713a', '360121', '360100', '南昌县', 3, NULL, 1, 1647931271, 1681975449);
INSERT INTO `yunduan_district` VALUES ('e405b7c0035a0856c2f549e4ce4c93ac', '620100', '620000', '兰州市', 2, '{\"county\": 8}', 1, 1647931271, 1681978249);
INSERT INTO `yunduan_district` VALUES ('e40957a22c36e3776494acae3a268803', '231202', '231200', '北林区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('e41e2900c0c6eb3ef8bc089a9ba71f0f', '120104', '120100', '南开区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('e420264ea42879b1cc885060faca1082', '420324', '420300', '竹溪县', 3, NULL, 1, 1647931271, 1681975547);
INSERT INTO `yunduan_district` VALUES ('e42ad12451af71120467e9ca444ff6d7', '650421', '650400', '鄯善县', 3, NULL, 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('e45c0006498da5df3407c926a961da27', '513333', '513300', '色达县', 3, NULL, 1, 1647931271, 1681977516);
INSERT INTO `yunduan_district` VALUES ('e460be830c1dfb7a34f7a41bb39d765c', '440600', '440000', '佛山市', 2, '{\"county\": 5}', 1, 1647931271, 1681975593);
INSERT INTO `yunduan_district` VALUES ('e46e3ae957c02db5d621f594a26c5f51', '650000', '0', '新疆维吾尔自治区', 1, '{\"city\": 14, \"county\": 106}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('e47614f7b6bb7a3f37a5d810d1167e8e', '220882', '220800', '大安市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('e47b99191183aaa10ca4b57623376bdb', '110116', '110100', '怀柔区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('e47f19ecdaa8f2217f03929666301897', '152529', '152500', '正镶白旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('e4896cafdf5e3b2c41b11d4053247196', '320281', '320200', '江阴市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('e4a426688324141f0391533f94f5597e', '230202', '230200', '龙沙区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('e4b0ab370dc2f3571e9fa9e3a4d53741', '130522', '130500', '临城县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('e4bed1ee06b8a2e73c0d891fb93cdd50', '411024', '411000', '鄢陵县', 3, NULL, 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('e4d76fc518101fdbf6324d72471d3b68', '310100', '310000', '上海市', 2, '{\"county\": 16}', 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('e4dfca112a56736bb3964e7956813fa7', '511300', '510000', '南充市', 2, '{\"county\": 9}', 1, 1647931271, 1681977529);
INSERT INTO `yunduan_district` VALUES ('e4eba8a87be76b4ff6fae1e2a069482d', '469027', '469000', '乐东黎族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('e50f3463bdc46c185e925fbcf09954cc', '230103', '230100', '南岗区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('e523177a3786d37266c89ae68b946ff3', '450403', '450400', '万秀区', 3, NULL, 1, 1647931271, 1681975627);
INSERT INTO `yunduan_district` VALUES ('e531e7212cc13b2e167f16a160bd1141', '620802', '620800', '崆峒区', 3, NULL, 1, 1647931271, 1681978248);
INSERT INTO `yunduan_district` VALUES ('e536f8943945bb763db194392d046c8a', '360925', '360900', '靖安县', 3, NULL, 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('e567ca7ffe2f0e8b529f24c6dd7763b0', '411727', '411700', '汝南县', 3, NULL, 1, 1647931271, 1681975522);
INSERT INTO `yunduan_district` VALUES ('e56e3f716cfbbb845a87adf42e46689b', '540124', '540100', '曲水县', 3, NULL, 1, 1647931271, 1681977991);
INSERT INTO `yunduan_district` VALUES ('e5704ea40b04211670981c223ff9614a', '530403', '530400', '江川区', 3, NULL, 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('e571bcdbe006483750454abb23837732', '530521', '530500', '施甸县', 3, NULL, 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('e5760462f6de0ec3d10b656be756d121', '450206', '450200', '柳江区', 3, NULL, 1, 1647931271, 1681975639);
INSERT INTO `yunduan_district` VALUES ('e58b1a1423d657f8b527d7048947bf03', '450329', '450300', '资源县', 3, NULL, 1, 1647931271, 1681975631);
INSERT INTO `yunduan_district` VALUES ('e5a01ea4f447dbb44a22a020692174b4', '430102', '430100', '芙蓉区', 3, NULL, 1, 1647931271, 1681975564);
INSERT INTO `yunduan_district` VALUES ('e5a02b7e8102e5e75fe82a9430f45a81', '510524', '510500', '叙永县', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('e5babeaeccec3871a3cf6e4a632e81c6', '450328', '450300', '龙胜各族自治县', 3, NULL, 1, 1647931271, 1681975631);
INSERT INTO `yunduan_district` VALUES ('e5c569caeff50b91b5302419ac8f98a7', '532801', '532800', '景洪市', 3, NULL, 1, 1647931271, 1681977878);
INSERT INTO `yunduan_district` VALUES ('e5ca9e216d4e2d90666ddc50ab8defa2', '230403', '230400', '工农区', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('e5f6e749b630b02271bac22b551bd6c3', '632802', '632800', '德令哈市', 3, NULL, 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('e62666f2dc916290dd2e191d89b5ab9a', '350428', '350400', '将乐县', 3, NULL, 1, 1647931271, 1681975421);
INSERT INTO `yunduan_district` VALUES ('e627f5824bfe320637d324bdac5d18d3', '652324', '652300', '玛纳斯县', 3, NULL, 1, 1647931271, 1681978498);
INSERT INTO `yunduan_district` VALUES ('e6417e8e805a212914d53074649205ca', '140430', '140400', '沁县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('e64b41c4df018939a146641692bd9652', '623001', '623000', '合作市', 3, NULL, 1, 1647931271, 1681978244);
INSERT INTO `yunduan_district` VALUES ('e67eca9d1f2f1b95401079a41b629af0', '530524', '530500', '昌宁县', 3, NULL, 1, 1647931271, 1681977883);
INSERT INTO `yunduan_district` VALUES ('e67f23738c50eeb71ccc291452e420ab', '361123', '361100', '玉山县', 3, NULL, 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('e6956beee0a934f2f49904735d77f92c', '520423', '520400', '镇宁布依族苗族自治县', 3, NULL, 1, 1647931271, 1681977719);
INSERT INTO `yunduan_district` VALUES ('e69ecdef1a04d17be11d47b91c565457', '210505', '210500', '南芬区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('e6a28ad64a805d10f6b08df1311b7af5', '211481', '211400', '兴城市', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('e6c9c657de28eb2d90f8efa035522944', '522631', '522600', '黎平县', 3, NULL, 1, 1647931271, 1681977734);
INSERT INTO `yunduan_district` VALUES ('e6d9daac348ad67dd0a2cbe9ca17c837', '610426', '610400', '永寿县', 3, NULL, 1, 1647931271, 1681978070);
INSERT INTO `yunduan_district` VALUES ('e6ecb1df2bc4174e23536908fd0a35fa', '540222', '540200', '江孜县', 3, NULL, 1, 1647931271, 1681977986);
INSERT INTO `yunduan_district` VALUES ('e6f9297f2d1a59c9e6ff413f3c1decec', '510722', '510700', '三台县', 3, NULL, 1, 1647931271, 1681977542);
INSERT INTO `yunduan_district` VALUES ('e70a2d44eeb8ccd10ee4b5af25aca38b', '530921', '530900', '凤庆县', 3, NULL, 1, 1647931271, 1681977880);
INSERT INTO `yunduan_district` VALUES ('e70fa52f7a829b9eb347cfa8ee685a9d', '411300', '410000', '南阳市', 2, '{\"county\": 13}', 1, 1647931271, 1681975519);
INSERT INTO `yunduan_district` VALUES ('e7108f995868d328ded2651e9ecaac05', '510117', '510100', '郫都区', 3, NULL, 1, 1647931271, 1681977535);
INSERT INTO `yunduan_district` VALUES ('e716e674d21bc08d0a2957d17feedf18', '320922', '320900', '滨海县', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('e72875915c3a02501b37d1b15ce9bac9', '410322', '410300', '孟津县', 3, NULL, 1, 1647931271, 1681975504);
INSERT INTO `yunduan_district` VALUES ('e733026d0912ea5b74837ae4dbb17b0b', '371724', '371700', '巨野县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('e739a414f60fc644ceb0c0a2435f9793', '230828', '230800', '汤原县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('e75c4be5df8237d454d5c7bcbf7af777', '230717', '230700', '伊美区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('e7882668dcf43a7ed081e80fdf5a46d5', '441225', '441200', '封开县', 3, NULL, 1, 1647931271, 1681975584);
INSERT INTO `yunduan_district` VALUES ('e79360083c2bd2b50b46c22037d0247a', '654022', '654000', '察布查尔锡伯自治县', 3, NULL, 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('e7a267a07016fa79642fb10d143b164d', '140822', '140800', '万荣县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('e7a533d9fbda8a247baf1f58cab9d43e', '410302', '410300', '老城区', 3, NULL, 1, 1647931271, 1681975503);
INSERT INTO `yunduan_district` VALUES ('e7b55781948d0b62bcd24b57fbe64c82', '450127', '450100', '横县', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('e7c0900726171ccae31d31ca02a2f24a', '131082', '131000', '三河市', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('e816fa3098f54e8a2b3615502195a663', '120106', '120100', '红桥区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('e84cfb01fd773b76300e1550d1053418', '210181', '210100', '新民市', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('e867b5ee5d5046ad823a5ae1f02652cf', '540229', '540200', '仁布县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('e896e17573a8c19bbb4642a7aa1719a2', '520102', '520100', '南明区', 3, NULL, 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('e8be1644d96479726371e5c09660cf1f', '350102', '350100', '鼓楼区', 3, NULL, 1, 1647931271, 1681975416);
INSERT INTO `yunduan_district` VALUES ('e8ca1dc24de465a7a3e860e1e3993b7c', '621026', '621000', '宁县', 3, NULL, 1, 1647931271, 1681978245);
INSERT INTO `yunduan_district` VALUES ('e8d4db04bf5880983a090ca5443c9604', '370502', '370500', '东营区', 3, NULL, 1, 1647931271, 1681975493);
INSERT INTO `yunduan_district` VALUES ('e8fc8b5ec3c069d7ac9e0e7c63b97ab8', '371100', '370000', '日照市', 2, '{\"county\": 4}', 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('e90c7a07ba5494830afd0ba297d69264', '130922', '130900', '青县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('e923a96a6b33faca9d00651dc188e06a', '320612', '320600', '通州区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('e943d10dfddfe812379a12e9f2292283', '511322', '511300', '营山县', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('e943ec283fb381c9afec43a408ae904f', '210782', '210700', '北镇市', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('e9647366dcf01c78d1b100bfaa500cff', '510623', '510600', '中江县', 3, NULL, 1, 1647931271, 1681977541);
INSERT INTO `yunduan_district` VALUES ('e97e3d267567533063d51e5ae002e8a6', '361021', '361000', '南城县', 3, NULL, 1, 1647931271, 1681975441);
INSERT INTO `yunduan_district` VALUES ('e98ea738c23628c071e1e5b8f76971ba', '140927', '140900', '神池县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('e9b422da44bdebb6471d8da4fcaae414', '320685', '320600', '海安市', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('e9bcd3f1c92dd76576706f830460aef9', '350628', '350600', '平和县', 3, NULL, 1, 1647931271, 1681975411);
INSERT INTO `yunduan_district` VALUES ('e9c985ed2e137ee46bc022f890b8523c', '130528', '130500', '宁晋县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('e9ce8ae23851378baf89cdf9c59b13a2', '330502', '330500', '吴兴区', 3, NULL, 1, 1647931271, 1681975347);
INSERT INTO `yunduan_district` VALUES ('e9eacd7d4271b8f5b336f6d76bbd3685', '510402', '510400', '东区', 3, NULL, 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('e9eb3afe7932d04a8808f1bd907157d6', '431222', '431200', '沅陵县', 3, NULL, 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('ea053876992278155258538cac09f4cd', '130109', '130100', '藁城区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('ea05c1839123a22143ae4139a7115331', '540100', '540000', '拉萨市', 2, '{\"county\": 8}', 1, 1647931271, 1681977990);
INSERT INTO `yunduan_district` VALUES ('ea1a196d30e2979426e08f5499aea1e3', '430321', '430300', '湘潭县', 3, NULL, 1, 1647931271, 1681975559);
INSERT INTO `yunduan_district` VALUES ('ea1d9ef8ee18fa5e4dbc3e596dc0239d', '411326', '411300', '淅川县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('ea232e71fcfb80478cfadca10fd98977', '321182', '321100', '扬中市', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('ea33ade3d4d044e0167960b37e0a64c2', '371702', '371700', '牡丹区', 3, NULL, 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('ea47d465d4109151312bb0990755dee9', '140502', '140500', '城区', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('ea558f7193e3f19a2a2004669889f10d', '411203', '411200', '陕州区', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('ea69c926100516526109e7a78e4b779b', '430405', '430400', '珠晖区', 3, NULL, 1, 1647931271, 1681975556);
INSERT INTO `yunduan_district` VALUES ('ea910b4e12fdec9d770a44b6316673ab', '371425', '371400', '齐河县', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('ea99f318479c023a33d3ed03125350e2', '654225', '654200', '裕民县', 3, NULL, 1, 1647931271, 1681978499);
INSERT INTO `yunduan_district` VALUES ('eab2284f0aa825c0b900fcfc92697fdb', '430527', '430500', '绥宁县', 3, NULL, 1, 1647931271, 1681975568);
INSERT INTO `yunduan_district` VALUES ('eb081816bf3cc9854f723c83bd56c555', '429021', '429000', '神农架林区', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('eb1fb726a9b1f30ce2edeb0875be7ef8', '331123', '331100', '遂昌县', 3, NULL, 1, 1647931271, 1681975341);
INSERT INTO `yunduan_district` VALUES ('eb2844eac2b22730fff42418204a2527', '330108', '330100', '滨江区', 3, NULL, 1, 1647931271, 1681975355);
INSERT INTO `yunduan_district` VALUES ('eb31f0d0457d6455e67d89a69c6a795f', '220000', '0', '吉林省', 1, '{\"city\": 9, \"county\": 60}', 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('eb424f28e2ece4b68f1a6e7eef19ef29', '511527', '511500', '筠连县', 3, NULL, 1, 1647931271, 1681977539);
INSERT INTO `yunduan_district` VALUES ('eb48c6e89be12878283889813c4eae16', '130525', '130500', '隆尧县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('eb4d362349ea7845112bf16c24226adf', '620621', '620600', '民勤县', 3, NULL, 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('eb62ba2008e1e359fcb346a7342b7f6f', '421223', '421200', '崇阳县', 3, NULL, 1, 1647931271, 1681975549);
INSERT INTO `yunduan_district` VALUES ('eb66f2f1fd4c94c59f4c75c8484aeef3', '430923', '430900', '安化县', 3, NULL, 1, 1647931271, 1681975563);
INSERT INTO `yunduan_district` VALUES ('eb9a9c50049e4312a562b066f8f6859b', '340523', '340500', '和县', 3, NULL, 1, 1647931271, 1681975391);
INSERT INTO `yunduan_district` VALUES ('ebcc265c1f7726928da6c3ca913c34c8', '450105', '450100', '江南区', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('ebd9335a807c9cbf4e70575577f88f3f', '330421', '330400', '嘉善县', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('ebdda34abb965472e4e47b97c173a567', '130626', '130600', '定兴县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('ebef47eaa8e7c9becbbdc6bf2c996265', '610881', '610800', '神木市', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('ebf4002658486afe8a2aa5f796a9c22a', '511800', '510000', '雅安市', 2, '{\"county\": 8}', 1, 1647931271, 1681977520);
INSERT INTO `yunduan_district` VALUES ('ec05da673899840ad94aa8f7e1d8413c', '330482', '330400', '平湖市', 3, NULL, 1, 1647931271, 1681975344);
INSERT INTO `yunduan_district` VALUES ('ec1bd941cfe2edaeb79b897a18b3983b', '340500', '340000', '马鞍山市', 2, '{\"county\": 6}', 1, 1647931271, 1681975390);
INSERT INTO `yunduan_district` VALUES ('ec1d8960478df54012b5a2f020922777', '610822', '610800', '府谷县', 3, NULL, 1, 1647931271, 1681978065);
INSERT INTO `yunduan_district` VALUES ('ec3e57fbf7d664e44bd72bb175e0fb3b', '220300', '220000', '四平市', 2, '{\"county\": 5}', 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('ec429e0e52f62da69f740bcf82f19cfc', '441826', '441800', '连南瑶族自治县', 3, NULL, 1, 1647931271, 1681975589);
INSERT INTO `yunduan_district` VALUES ('ec43848b16670ed64fa9ed117abc418b', '410212', '410200', '祥符区', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('ec47eb2c2ed553046589dd44415187f2', '141128', '141100', '方山县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('ec66b016e2db26ce99eb5c8fc9a453fa', '210700', '210000', '锦州市', 2, '{\"county\": 7}', 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('ec81acf18d92c330a4cddfd6653db787', '210904', '210900', '太平区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('ec8c5fff74c85c9a14cac1cbe71bbf30', '130627', '130600', '唐县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('ec8debf2c62dce1b9b0438d80f893879', '540422', '540400', '米林县', 3, NULL, 1, 1647931271, 1681977994);
INSERT INTO `yunduan_district` VALUES ('ecc69397ea4bfb0077c27ae247575256', '530700', '530000', '丽江市', 2, '{\"county\": 5}', 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('ecf03bdd728e8002204e195f05ec094f', '210204', '210200', '沙河口区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('ed10252beee7295ede903a7b693642c7', '371482', '371400', '禹城市', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('ed2f503ad067f3a8f90a7533e82b4397', '130726', '130700', '蔚县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('ed33ca143cf16b3ed7b8d539bb07094c', '420981', '420900', '应城市', 3, NULL, 1, 1647931271, 1681975532);
INSERT INTO `yunduan_district` VALUES ('ed41c84ef6e924284c70d07b09c10328', '440115', '440100', '南沙区', 3, NULL, 1, 1647931271, 1681975597);
INSERT INTO `yunduan_district` VALUES ('ed5522c3887aac1042d6021ddb253470', '653224', '653200', '洛浦县', 3, NULL, 1, 1647931271, 1681978504);
INSERT INTO `yunduan_district` VALUES ('ed6307ec8c226c7c3ec49d5196d7ad72', '522327', '522300', '册亨县', 3, NULL, 1, 1647931271, 1681977727);
INSERT INTO `yunduan_district` VALUES ('ed88fed81e972b35945edef3f612feb9', '411729', '411700', '新蔡县', 3, NULL, 1, 1647931271, 1681975523);
INSERT INTO `yunduan_district` VALUES ('edc3da921de479bce56844d959dde72e', '340209', '340200', '弋江区 ', 3, NULL, 1, 1647931271, 1681975384);
INSERT INTO `yunduan_district` VALUES ('edd4348b276e63b0a009be5b4ed4e999', '370782', '370700', '诸城市', 3, NULL, 1, 1647931271, 1681975488);
INSERT INTO `yunduan_district` VALUES ('eded70821509b173ff5174aa62e93635', '310151', '310100', '崇明区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('edff6753a46fb1714108b7cd1758a429', '360900', '360000', '宜春市', 2, '{\"county\": 10}', 1, 1647931271, 1681975451);
INSERT INTO `yunduan_district` VALUES ('ee0791e0ed75de9b731c93c145a288ae', '130881', '130800', '平泉市', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('ee12f37e1df0bc739385ea2224002660', '622925', '622900', '和政县', 3, NULL, 1, 1647931271, 1681978240);
INSERT INTO `yunduan_district` VALUES ('ee1a7381975140ac25ba34ba0f3a9fdb', '530402', '530400', '红塔区', 3, NULL, 1, 1647931271, 1681977893);
INSERT INTO `yunduan_district` VALUES ('ee1ff7eb4ae69ececeeccfcfc1979c31', '140181', '140100', '古交市', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('ee36db51659ec618bdf96bf5e74f5cc8', '341722', '341700', '石台县', 3, NULL, 1, 1647931271, 1681975388);
INSERT INTO `yunduan_district` VALUES ('ee3f25ea54c807bac7187a1d368cc7ca', '540231', '540200', '定结县', 3, NULL, 1, 1647931271, 1681977985);
INSERT INTO `yunduan_district` VALUES ('ee411145107d867e45ccfa425f27d014', '500106', '500100', '沙坪坝区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('ee462a9a3a9e6b4f92b342fbd23b381c', '331002', '331000', '椒江区', 3, NULL, 1, 1647931271, 1681975352);
INSERT INTO `yunduan_district` VALUES ('ee4b799d4cd1f25a5f832b4bcb5b7dca', '140223', '140200', '广灵县', 3, NULL, 1, 1647931271, 1661392335);
INSERT INTO `yunduan_district` VALUES ('ee64abab342142f1b420bb34224761f1', '654324', '654300', '哈巴河县', 3, NULL, 1, 1647931271, 1681978492);
INSERT INTO `yunduan_district` VALUES ('ee72537615236e5d25ba011e5b5bb022', '511702', '511700', '通川区', 3, NULL, 1, 1647931271, 1681977522);
INSERT INTO `yunduan_district` VALUES ('ee85e79efc0581c372a8c809a1ba7be7', '540523', '540500', '桑日县', 3, NULL, 1, 1647931271, 1681977996);
INSERT INTO `yunduan_district` VALUES ('ee8713ea5b22e919d67c3106d9e12e75', '210803', '210800', '西市区', 3, NULL, 1, 1647931271, 1661392341);
INSERT INTO `yunduan_district` VALUES ('eebb28e2243f73bd5aef38ee7d84f474', '220524', '220500', '柳河县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('eebdb2590fa53bf20eb6f9681f57331d', '210113', '210100', '沈北新区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('eed65a0087a75b209b316d1acdc512da', '330603', '330600', '柯桥区', 3, NULL, 1, 1647931271, 1681975343);
INSERT INTO `yunduan_district` VALUES ('eed70bf7debb9c56272c510c7db76c89', '620600', '620000', '武威市', 2, '{\"county\": 4}', 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('eee1ea147435fc55d8a3c6e537a7d74b', '420581', '420500', '宜都市', 3, NULL, 1, 1647931271, 1681975535);
INSERT INTO `yunduan_district` VALUES ('ef15aeb0ba389f85f05d9f1b44c02762', '320412', '320400', '武进区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('ef48f5b5e40c029d246f515e0557911d', '511302', '511300', '顺庆区', 3, NULL, 1, 1647931271, 1681977531);
INSERT INTO `yunduan_district` VALUES ('ef6e1053603c1e8ed052688b98ab84c9', '530103', '530100', '盘龙区', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('ef93039a54162a31d4ea98fa88e01f0f', '640300', '640000', '吴忠市', 2, '{\"county\": 5}', 1, 1647931271, 1681978325);
INSERT INTO `yunduan_district` VALUES ('ef96c7d19f6e1e9f9ec258fd92879eaf', '320113', '320100', '栖霞区', 3, NULL, 1, 1647931271, 1661392348);
INSERT INTO `yunduan_district` VALUES ('efa2dac33a6798d7bc91999c4c602c43', '341023', '341000', '黟县', 3, NULL, 1, 1647931271, 1681975390);
INSERT INTO `yunduan_district` VALUES ('efad8be5862946991621ec1043880f2d', '510823', '510800', '剑阁县', 3, NULL, 1, 1647931271, 1681977528);
INSERT INTO `yunduan_district` VALUES ('efba07f678187d4c3919a7b6da8d9c83', '210703', '210700', '凌河区', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('efc4d3f92056ee1306b1ee7be8a76b50', '130100', '130000', '石家庄市', 2, '{\"county\": 22}', 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('efc6c97a7a2b8087ee480ca9f14d1692', '632723', '632700', '称多县', 3, NULL, 1, 1647931271, 1681978312);
INSERT INTO `yunduan_district` VALUES ('efdcd6c3627b5fbfd28ea9fb711e4101', '371726', '371700', '鄄城县', 3, NULL, 1, 1647931271, 1681975477);
INSERT INTO `yunduan_district` VALUES ('eff5ebe4d7e96e162583e5b79ebd198a', '520625', '520600', '印江土家族苗族自治县', 3, NULL, 1, 1647931271, 1681977722);
INSERT INTO `yunduan_district` VALUES ('f008937abb8d0af653b166a0c4f9b188', '522300', '520000', '黔西南布依族苗族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681977726);
INSERT INTO `yunduan_district` VALUES ('f0161669891ad7e796dc0e78d2956ed7', '431123', '431100', '双牌县', 3, NULL, 1, 1647931271, 1681975569);
INSERT INTO `yunduan_district` VALUES ('f01e98c149f58d51821bb86579bca691', '350206', '350200', '湖里区', 3, NULL, 1, 1647931271, 1681975406);
INSERT INTO `yunduan_district` VALUES ('f03c67d653e1b10bddbcc891efcacecf', '370403', '370400', '薛城区', 3, NULL, 1, 1647931271, 1681975496);
INSERT INTO `yunduan_district` VALUES ('f04100cd3e0c6156705a946f40a34d7e', '422826', '422800', '咸丰县', 3, NULL, 1, 1647931271, 1681975539);
INSERT INTO `yunduan_district` VALUES ('f0413a904b8a7df680ed5d874479094c', '350505', '350500', '泉港区', 3, NULL, 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('f041f7560d2db7776c7a01633173c33b', '530181', '530100', '安宁市', 3, NULL, 1, 1647931271, 1681977892);
INSERT INTO `yunduan_district` VALUES ('f095758c318a8e8e01da041e1db4af9e', '152525', '152500', '东乌珠穆沁旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('f09d4877a18beda55090ec6c46fabc14', '150105', '150100', '赛罕区', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('f0dbe62cab8285d106a8a1b4d812cf66', '611023', '611000', '商南县', 3, NULL, 1, 1647931271, 1681978074);
INSERT INTO `yunduan_district` VALUES ('f0ea4418af194098bfb60202180c004f', '411325', '411300', '内乡县', 3, NULL, 1, 1647931271, 1681975520);
INSERT INTO `yunduan_district` VALUES ('f110773429bd12a2a4e3c8845a69943f', '450110', '450100', '武鸣区', 3, NULL, 1, 1647931271, 1681975634);
INSERT INTO `yunduan_district` VALUES ('f1171402a9f78cf871f50664634b030a', '130828', '130800', '围场满族蒙古族自治县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('f141ad83b08d010b76ae74343cde44b1', '370212', '370200', '崂山区', 3, NULL, 1, 1647931271, 1681975484);
INSERT INTO `yunduan_district` VALUES ('f15318b67c7eb27a741c48c2cfebac7c', '420625', '420600', '谷城县', 3, NULL, 1, 1647931271, 1681975531);
INSERT INTO `yunduan_district` VALUES ('f16bfece319ba27ac13477a6ce2b09c9', '510107', '510100', '武侯区', 3, NULL, 1, 1647931271, 1681977534);
INSERT INTO `yunduan_district` VALUES ('f17c89c9627ae2fd3124b9fc50dc392e', '522630', '522600', '台江县', 3, NULL, 1, 1647931271, 1681977732);
INSERT INTO `yunduan_district` VALUES ('f17f76dfb491d829041bb2fb422e5bc4', '621221', '621200', '成县', 3, NULL, 1, 1647931271, 1681978253);
INSERT INTO `yunduan_district` VALUES ('f198b504fdc7bc1696fc807634dba606', '511602', '511600', '广安区', 3, NULL, 1, 1647931271, 1681977519);
INSERT INTO `yunduan_district` VALUES ('f19bb445de71d7d35fc69726acfb4fb6', '632800', '630000', '海西蒙古族藏族自治州', 2, '{\"county\": 6}', 1, 1647931271, 1681978313);
INSERT INTO `yunduan_district` VALUES ('f1a1ee71832e2a173ad0c3e4f6f51609', '500115', '500100', '长寿区', 3, NULL, 1, 1647931271, 1681977451);
INSERT INTO `yunduan_district` VALUES ('f1a71f0f486aee473c3ee1cf846f5736', '430112', '430100', '望城区', 3, NULL, 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('f1cf447c072fcf14fdd1652524a08f7a', '532926', '532900', '南涧彝族自治县', 3, NULL, 1, 1647931271, 1681977889);
INSERT INTO `yunduan_district` VALUES ('f1fb1b9443cddfb7217b992b2fa36890', '451031', '451000', '隆林各族自治县', 3, NULL, 1, 1647931271, 1681975628);
INSERT INTO `yunduan_district` VALUES ('f23bed72b850287af8c6e595e6df7d4a', '445281', '445200', '普宁市', 3, NULL, 1, 1647931271, 1681975601);
INSERT INTO `yunduan_district` VALUES ('f251c63b4df49c1c47f2f411ff726b85', '431226', '431200', '麻阳苗族自治县', 3, NULL, 1, 1647931271, 1681975572);
INSERT INTO `yunduan_district` VALUES ('f254379c131c4a4202351a295761676b', '361026', '361000', '宜黄县', 3, NULL, 1, 1647931271, 1681975443);
INSERT INTO `yunduan_district` VALUES ('f292891df4fc46a38b38e7666ab83ec3', '230600', '230000', '大庆市', 2, '{\"county\": 9}', 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('f2b1c80d0f37e5bfe111d2ae7ae16b10', '130928', '130900', '吴桥县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('f2bc7930ceae59e181b60e18cb990022', '420381', '420300', '丹江口市', 3, NULL, 1, 1647931271, 1681975548);
INSERT INTO `yunduan_district` VALUES ('f2bf56ccce791e46020b1d6597353f65', '371300', '370000', '临沂市', 2, '{\"county\": 12}', 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('f2d3f428d031b9e18620b7da2bdb0d7f', '410727', '410700', '封丘县', 3, NULL, 1, 1647931271, 1681975518);
INSERT INTO `yunduan_district` VALUES ('f2ec32bf4d254e15b033f349da941a61', '469025', '469000', '白沙黎族自治县', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('f307520428e48bb9a50cb87972a3d606', '360781', '360700', '瑞金市', 3, NULL, 1, 1647931271, 1681975445);
INSERT INTO `yunduan_district` VALUES ('f30e64c75e7dbb6dbd923dbbdf638f6d', '371700', '370000', '菏泽市', 2, '{\"county\": 9}', 1, 1647931271, 1681975476);
INSERT INTO `yunduan_district` VALUES ('f30f76a2aa285a4e282a30259de1e1d4', '220302', '220300', '铁西区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('f3753750faf107ec8a420d9e55f02a85', '331081', '331000', '温岭市', 3, NULL, 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('f37e38a5522cd31ed50f1522cc5f66f8', '230111', '230100', '呼兰区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('f38007ad077644ae0918fe9e8167eb90', '511500', '510000', '宜宾市', 2, '{\"county\": 10}', 1, 1647931271, 1681977538);
INSERT INTO `yunduan_district` VALUES ('f39453ecbda42be5cc51020abea0d3f4', '653101', '653100', '喀什市', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('f3986028440e44428d01de67d7a4ab80', '511700', '510000', '达州市', 2, '{\"county\": 7}', 1, 1647931271, 1681977521);
INSERT INTO `yunduan_district` VALUES ('f3a9b69fda6b64e2b34fa0699cc048bc', '230724', '230700', '丰林县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('f3c882ac64bcc2adfc5ed3c01d8c710c', '420107', '420100', '青山区', 3, NULL, 1, 1647931271, 1681975538);
INSERT INTO `yunduan_district` VALUES ('f3c922c2c9bb5f24d52faddb9b9e4deb', '411221', '411200', '渑池县', 3, NULL, 1, 1647931271, 1681975506);
INSERT INTO `yunduan_district` VALUES ('f3cc2287c5c858fa8b599500cb39eb22', '360983', '360900', '高安市', 3, NULL, 1, 1647931271, 1681975452);
INSERT INTO `yunduan_district` VALUES ('f3e67660d486244585777fbefb69fe67', '411323', '411300', '西峡县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('f3ef5eaf19fd356eb6ac52c6477618ba', '320508', '320500', '姑苏区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('f43d014fae439158b86779af21e8bb45', '231223', '231200', '青冈县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('f44feb7c52a8762f7c26d8a7df3c3a55', '130402', '130400', '邯山区', 3, NULL, 1, 1647931271, 1661392327);
INSERT INTO `yunduan_district` VALUES ('f48b34911811e8e700a2d131834c3fe5', '621000', '620000', '庆阳市', 2, '{\"county\": 8}', 1, 1647931271, 1681978245);
INSERT INTO `yunduan_district` VALUES ('f4953f8c6202c1a4894235424a7ea68e', '330211', '330200', '镇海区', 3, NULL, 1, 1647931271, 1681975346);
INSERT INTO `yunduan_district` VALUES ('f4a4879205dbcc004cda621bbd89ca9d', '231222', '231200', '兰西县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('f4c244410ea62166df152b496381c6fa', '130427', '130400', '磁县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('f4c288e008f8e827481f4a563f2230c5', '610827', '610800', '米脂县', 3, NULL, 1, 1647931271, 1681978066);
INSERT INTO `yunduan_district` VALUES ('f4c7a8051de44406d16f7a21e4ff06a5', '420281', '420200', '大冶市', 3, NULL, 1, 1647931271, 1681975546);
INSERT INTO `yunduan_district` VALUES ('f4e2d101537782160aaffbc344948d2e', '440114', '440100', '花都区', 3, NULL, 1, 1647931271, 1681975596);
INSERT INTO `yunduan_district` VALUES ('f4e5172e2e47a70b1ac03c7bb4dc2d14', '231124', '231100', '孙吴县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('f502521e7e500d6cb9355d054a84e461', '141130', '141100', '交口县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('f5204a21557383cc5a1675953285fd2e', '130529', '130500', '巨鹿县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('f5276e99580cda6fd84a088ae6094b0c', '411421', '411400', '民权县', 3, NULL, 1, 1647931271, 1681975505);
INSERT INTO `yunduan_district` VALUES ('f54b4210529fafaf5254e8b0b6770064', '510129', '510100', '大邑县', 3, NULL, 1, 1647931271, 1681977536);
INSERT INTO `yunduan_district` VALUES ('f562b5304460a8f7999d1208c79ae3d2', '430503', '430500', '大祥区', 3, NULL, 1, 1647931271, 1681975567);
INSERT INTO `yunduan_district` VALUES ('f5a469f705a42f82df1df56b4d94cbb5', '320506', '320500', '吴中区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('f5ea5ea83e745b2f7c48b3877be9774b', '150424', '150400', '林西县', 3, NULL, 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('f5f34a5a962804a3ad848e4268618334', '513425', '513400', '会理县', 3, NULL, 1, 1647931271, 1681977523);
INSERT INTO `yunduan_district` VALUES ('f60043dd75d42a724186d042dcc724b5', '654224', '654200', '托里县', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('f60d46f42a842aa7cfedbb2e5decbd92', '371427', '371400', '夏津县', 3, NULL, 1, 1647931271, 1681975492);
INSERT INTO `yunduan_district` VALUES ('f60dd59ba3f733582096d17756fae0b6', '430111', '430100', '雨花区', 3, NULL, 1, 1647931271, 1681975565);
INSERT INTO `yunduan_district` VALUES ('f6181e4aa6472b25db5c29751fa8dcef', '540232', '540200', '仲巴县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('f61cda62a029eb68e331fd96d1f56c8e', '510104', '510100', '锦江区', 3, NULL, 1, 1647931271, 1681977533);
INSERT INTO `yunduan_district` VALUES ('f632c894af917d1bb0b55e6993e0136b', '532925', '532900', '弥渡县', 3, NULL, 1, 1647931271, 1681977888);
INSERT INTO `yunduan_district` VALUES ('f63b939a8a110b86aa5927b6d5553a6c', '341323', '341300', '灵璧县', 3, NULL, 1, 1647931271, 1681975379);
INSERT INTO `yunduan_district` VALUES ('f647c3784498f7823b9bc399fdb36788', '510421', '510400', '米易县', 3, NULL, 1, 1647931271, 1681977518);
INSERT INTO `yunduan_district` VALUES ('f64b688fd28930fe65748b150cd874c5', '341021', '341000', '歙县', 3, NULL, 1, 1647931271, 1681975390);
INSERT INTO `yunduan_district` VALUES ('f65c27402d94168ea731d1f8680e7401', '430500', '430000', '邵阳市', 2, '{\"county\": 12}', 1, 1647931271, 1681975566);
INSERT INTO `yunduan_district` VALUES ('f65c83d508476a168af75b1a47873698', '220621', '220600', '抚松县', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('f66641d0399640dbc445915823166672', '371121', '371100', '五莲县', 3, NULL, 1, 1647931271, 1681975479);
INSERT INTO `yunduan_district` VALUES ('f66ad3a58b2d1e8fa79e2696ba2b5db2', '320706', '320700', '海州区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('f670e63884c1e8c1a832b13fb53c675a', '450323', '450300', '灵川县', 3, NULL, 1, 1647931271, 1681975632);
INSERT INTO `yunduan_district` VALUES ('f689b727c0adf648658970e5dbf93117', '441802', '441800', '清城区', 3, NULL, 1, 1647931271, 1681975590);
INSERT INTO `yunduan_district` VALUES ('f6a76841ca5f029549855fcd30bd66b3', '450802', '450800', '港北区', 3, NULL, 1, 1647931271, 1681975624);
INSERT INTO `yunduan_district` VALUES ('f6b02e822e77bab22b17bbade65fd527', '231226', '231200', '绥棱县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('f6d19a5c2bdc4a09128e318e77968b3b', '140108', '140100', '尖草坪区', 3, NULL, 1, 1647931271, 1661392333);
INSERT INTO `yunduan_district` VALUES ('f6f1704fd5da491f960c01d3d179db99', '320481', '320400', '溧阳市', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('f71d0a1947944972196a813c6098a01d', '341124', '341100', '全椒县', 3, NULL, 1, 1647931271, 1681975375);
INSERT INTO `yunduan_district` VALUES ('f736e5a1d5ac61a9959bcfc697f31dfa', '220403', '220400', '西安区', 3, NULL, 1, 1647931271, 1661392342);
INSERT INTO `yunduan_district` VALUES ('f763ef3f7e099c76bf3d3d2ac41cffe7', '230803', '230800', '向阳区', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('f769f070db6621087fe3e09a97490121', '652824', '652800', '若羌县', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('f77faa5954f7ad9930beeaa0f9c356b7', '152528', '152500', '镶黄旗', 3, NULL, 1, 1647931271, 1661392336);
INSERT INTO `yunduan_district` VALUES ('f7aadf3d55457b03121452e70da98a73', '220582', '220500', '集安市', 3, NULL, 1, 1647931271, 1661392343);
INSERT INTO `yunduan_district` VALUES ('f7ce994e2c2c80ae97edd62263af2219', '440310', '440300', '坪山区', 3, NULL, 1, 1647931271, 1681975602);
INSERT INTO `yunduan_district` VALUES ('f7ec2884faf5542e17fc6b1b643a827d', '361023', '361000', '南丰县', 3, NULL, 1, 1647931271, 1681975443);
INSERT INTO `yunduan_district` VALUES ('f7fd987ba585a7b9903ca1b021ea9346', '441702', '441700', '江城区', 3, NULL, 1, 1647931271, 1681975574);
INSERT INTO `yunduan_district` VALUES ('f8006f91742debf2008288e2a0fc2fdc', '141123', '141100', '兴县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('f8034c8029adeeba71014cb6927496f5', '410423', '410400', '鲁山县', 3, NULL, 1, 1647931271, 1681975513);
INSERT INTO `yunduan_district` VALUES ('f81c895a4e490cc36d98f3c53dda52b3', '451022', '451000', '田东县', 3, NULL, 1, 1647931271, 1681975628);
INSERT INTO `yunduan_district` VALUES ('f81d0b1a9caa465fd7a9c084cddbcd99', '110114', '110100', '昌平区', 3, NULL, 1, 1647931271, 1663565984);
INSERT INTO `yunduan_district` VALUES ('f84329c8c01d28056d5e93fef9735be8', '210922', '210900', '彰武县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('f854c02a365a346eb2821439287b1e79', '440700', '440000', '江门市', 2, '{\"county\": 7}', 1, 1647931271, 1681975579);
INSERT INTO `yunduan_district` VALUES ('f882fbc0988fe01630c943ac45ecb0ec', '231200', '230000', '绥化市', 2, '{\"county\": 10}', 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('f89c27a4832275b4235596422d28dfac', '230422', '230400', '绥滨县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('f918b3b94272ad6628c411d40ea3c7dd', '659009', '659000', '昆玉市', 3, NULL, 1, 1647931271, 0);
INSERT INTO `yunduan_district` VALUES ('f9418481bde05a57f605835af749788e', '130524', '130500', '柏乡县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('f95170aa8f675f133427aff6608d5ca0', '540328', '540300', '芒康县', 3, NULL, 1, 1647931271, 1681977988);
INSERT INTO `yunduan_district` VALUES ('f95c46f86fc0ba1babb0e2b237d9d382', '331125', '331100', '云和县', 3, NULL, 1, 1647931271, 1681975342);
INSERT INTO `yunduan_district` VALUES ('f96467c8da4a7e3127a7fd34107ad52c', '540234', '540200', '吉隆县', 3, NULL, 1, 1647931271, 1681977987);
INSERT INTO `yunduan_district` VALUES ('f96fea5e697d2fe5297cc679a7fc24af', '620623', '620600', '天祝藏族自治县', 3, NULL, 1, 1647931271, 1681978251);
INSERT INTO `yunduan_district` VALUES ('f985f8ae63166df7fa89518c6448813b', '340402', '340400', '大通区', 3, NULL, 1, 1647931271, 1681975383);
INSERT INTO `yunduan_district` VALUES ('f9896d1514cc7f08edfa409d56d624d7', '654321', '654300', '布尔津县', 3, NULL, 1, 1647931271, 1681978491);
INSERT INTO `yunduan_district` VALUES ('f9e3a1b79d1612f63870fb2a11cd26b7', '150800', '150000', '巴彦淖尔市', 2, '{\"county\": 7}', 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('f9fd217432350c4b5681f4852f131062', '440282', '440200', '南雄市', 3, NULL, 1, 1647931271, 1681975576);
INSERT INTO `yunduan_district` VALUES ('fa0a25478b6439f77b413c152e525d2c', '530115', '530100', '晋宁区', 3, NULL, 1, 1647931271, 1681977892);
INSERT INTO `yunduan_district` VALUES ('fa2d6ff41e92bb219a98330029e778c4', '320402', '320400', '天宁区', 3, NULL, 1, 1647931271, 1661392350);
INSERT INTO `yunduan_district` VALUES ('fa543158efcfc6780b6f357a688a126d', '130127', '130100', '高邑县', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('fa595db233dad4bc95cd4de4e36db185', '410221', '410200', '杞县', 3, NULL, 1, 1647931271, 1681975498);
INSERT INTO `yunduan_district` VALUES ('fa6f161deccdc1e41ab7694598430255', '623000', '620000', '甘南藏族自治州', 2, '{\"county\": 8}', 1, 1647931271, 1681978243);
INSERT INTO `yunduan_district` VALUES ('fa72e2f90855399fe9d6b52f8c645738', '451322', '451300', '象州县', 3, NULL, 1, 1647931271, 1681975625);
INSERT INTO `yunduan_district` VALUES ('fa7b8f548bef8bb0c1305b19dcd86704', '130632', '130600', '安新县', 3, NULL, 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('faa36e778e92329e6ea72f50faa62346', '130531', '130500', '广宗县', 3, NULL, 1, 1647931271, 1661392329);
INSERT INTO `yunduan_district` VALUES ('fae0d61ce7d54690755fc6f36b068deb', '530000', '0', '云南省', 1, '{\"city\": 16, \"county\": 129}', 1, 1647931271, 1661303881);
INSERT INTO `yunduan_district` VALUES ('fae9314fe1d7cd59690b3069573364e2', '361125', '361100', '横峰县', 3, NULL, 1, 1647931271, 1681975455);
INSERT INTO `yunduan_district` VALUES ('fae98b01b41462e7db5c0ccd41440f36', '520425', '520400', '紫云苗族布依族自治县', 3, NULL, 1, 1647931271, 1681977720);
INSERT INTO `yunduan_district` VALUES ('faecc3e5dd92385e6118d95b2ba7ff44', '140221', '140200', '阳高县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('faf484409adf9d4b35e96821a32c47e7', '654226', '654200', '和布克赛尔蒙古自治县', 3, NULL, 1, 1647931271, 1681978500);
INSERT INTO `yunduan_district` VALUES ('fb149c3c6b9de873014e79077caa32a1', '340300', '340000', '蚌埠市', 2, '{\"county\": 7}', 1, 1647931271, 1681975371);
INSERT INTO `yunduan_district` VALUES ('fb35168f57c406465ca42d19fa2c5f41', '520200', '520000', '六盘水市', 2, '{\"county\": 4}', 1, 1647931271, 1681977728);
INSERT INTO `yunduan_district` VALUES ('fb7a1b5bde4ef47d0795846e405ebed6', '430626', '430600', '平江县', 3, NULL, 1, 1647931271, 1681975560);
INSERT INTO `yunduan_district` VALUES ('fb823196e6fbae86be203ea18ddecff2', '341204', '341200', '颍泉区', 3, NULL, 1, 1647931271, 1681975377);
INSERT INTO `yunduan_district` VALUES ('fb8f9b39110d725fa1a55cda4bafa529', '532527', '532500', '泸西县', 3, NULL, 1, 1647931271, 1681977885);
INSERT INTO `yunduan_district` VALUES ('fbda30a5aeaaf2f67efd5085e03c22a1', '530702', '530700', '古城区', 3, NULL, 1, 1647931271, 1681977895);
INSERT INTO `yunduan_district` VALUES ('fbe930a2a171f16284d16601001cdb45', '500111', '500100', '大足区', 3, NULL, 1, 1647931271, 1681977448);
INSERT INTO `yunduan_district` VALUES ('fbfe538135d26f9742ad547f20a05b17', '650104', '650100', '新市区', 3, NULL, 1, 1647931271, 1681978506);
INSERT INTO `yunduan_district` VALUES ('fc00f93c0719a675affcdb2a214041d6', '440983', '440900', '信宜市', 3, NULL, 1, 1647931271, 1681975582);
INSERT INTO `yunduan_district` VALUES ('fc18395cf89c64c52ea8ea4bd51e416a', '230321', '230300', '鸡东县', 3, NULL, 1, 1647931271, 1661392345);
INSERT INTO `yunduan_district` VALUES ('fc53cfe5ec859b35c9db1ab9e36c796d', '513336', '513300', '乡城县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('fc60a0e4e5b43ba77b0d8b5613bab6fa', '230604', '230600', '让胡路区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('fc772900069125f55c0f5ddcff86f0b0', '330784', '330700', '永康市', 3, NULL, 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('fc87165083ff437e876bccda7cdad863', '231005', '231000', '西安区', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('fc8f76f7972c80accabc2f43c426584f', '632722', '632700', '杂多县', 3, NULL, 1, 1647931271, 1681978312);
INSERT INTO `yunduan_district` VALUES ('fc9ffed96559270ea055c411b9a9df5c', '130400', '130000', '邯郸市', 2, '{\"county\": 18}', 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('fca48d51c71174cdbb9bab9c1b0ed185', '540628', '540600', '巴青县 ', 3, NULL, 1, 1647931271, 1681977992);
INSERT INTO `yunduan_district` VALUES ('fcbdbdb01810186f0a96a4bc42b16f13', '230621', '230600', '肇州县', 3, NULL, 1, 1647931271, 1661392344);
INSERT INTO `yunduan_district` VALUES ('fcca4bc724b41f5bfcad5184da2c0056', '120112', '120100', '津南区', 3, NULL, 1, 1647931271, 1661392326);
INSERT INTO `yunduan_district` VALUES ('fcdc4cca32c00306d2845926cdf4322b', '130732', '130700', '赤城县', 3, NULL, 1, 1647931271, 1661392331);
INSERT INTO `yunduan_district` VALUES ('fcefca3faff3cdaa9aa72bea54751ea5', '540529', '540500', '隆子县', 3, NULL, 1, 1647931271, 1681977996);
INSERT INTO `yunduan_district` VALUES ('fcf79e61e751b7ada85d9d761479b1c4', '650422', '650400', '托克逊县', 3, NULL, 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('fd0fb15b36686af57dd4206f60665949', '140525', '140500', '泽州县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('fd1bb0748763b87623e7cc7865e657a0', '211003', '211000', '文圣区', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('fd286c085c1b7f6bbb9a3e789acfd5db', '440233', '440200', '新丰县', 3, NULL, 1, 1647931271, 1681975575);
INSERT INTO `yunduan_district` VALUES ('fd4a9879a14286f6b267fb1e06aefcbc', '131000', '130000', '廊坊市', 2, '{\"county\": 10}', 1, 1647931271, 1661392330);
INSERT INTO `yunduan_district` VALUES ('fd8bea7944cb6486f6f1fb4813a62c3f', '320324', '320300', '睢宁县', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('fd926720d06d07258edaf41d3821fcfd', '230127', '230100', '木兰县', 3, NULL, 1, 1647931271, 1661392346);
INSERT INTO `yunduan_district` VALUES ('fd94f6cf8d18d818845754a9ebea9a49', '150981', '150900', '丰镇市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('fd9a04d4d351de0c8b385b927f60f5ce', '610730', '610700', '佛坪县', 3, NULL, 1, 1647931271, 1681978083);
INSERT INTO `yunduan_district` VALUES ('fda74659b3fb0c866d835b65d860945d', '610400', '610000', '咸阳市', 2, '{\"county\": 14}', 1, 1647931271, 1681978067);
INSERT INTO `yunduan_district` VALUES ('fdb7c15e2a45dc7e228eae6e1e93fa70', '140829', '140800', '平陆县', 3, NULL, 1, 1647931271, 1661392334);
INSERT INTO `yunduan_district` VALUES ('fdc17a639acd7aee4b2222e84fc0dd35', '522701', '522700', '都匀市', 3, NULL, 1, 1647931271, 1681977735);
INSERT INTO `yunduan_district` VALUES ('fdc400f038ccfa219e3eeea15db6f6ee', '360681', '360600', '贵溪市', 3, NULL, 1, 1647931271, 1681975447);
INSERT INTO `yunduan_district` VALUES ('fdc49e94160769a80a244f550dd846f5', '350800', '350000', '龙岩市', 2, '{\"county\": 7}', 1, 1647931271, 1681975415);
INSERT INTO `yunduan_district` VALUES ('fdc6512dcf273108a3cf82cf1a144131', '650402', '650400', '高昌区', 3, NULL, 1, 1647931271, 1681978505);
INSERT INTO `yunduan_district` VALUES ('fde35c5b51a2c6ff937b229e015735fd', '513334', '513300', '理塘县', 3, NULL, 1, 1647931271, 1681977515);
INSERT INTO `yunduan_district` VALUES ('fde4e8270355ac0da4cda764b5a31d0a', '150600', '150000', '鄂尔多斯市', 2, '{\"county\": 9}', 1, 1647931271, 1661392338);
INSERT INTO `yunduan_district` VALUES ('fe03a75b7e87fa54f8c674db3ad0e04a', '310110', '310100', '杨浦区', 3, NULL, 1, 1647931271, 1661392347);
INSERT INTO `yunduan_district` VALUES ('fe0955838d2e5056153a704c9ea7d84b', '450102', '450100', '兴宁区', 3, NULL, 1, 1647931271, 1681975633);
INSERT INTO `yunduan_district` VALUES ('fe10375388c8bc801e87b2e7b9b7e10b', '150822', '150800', '磴口县', 3, NULL, 1, 1647931271, 1661392339);
INSERT INTO `yunduan_district` VALUES ('fe640f819e04d7541d941f0855bc0080', '110119', '110100', '延庆区', 3, NULL, 1, 1647931271, 1663565985);
INSERT INTO `yunduan_district` VALUES ('fe920702937e9d7a2fde6622ae570be2', '331004', '331000', '路桥区', 3, NULL, 1, 1647931271, 1681975353);
INSERT INTO `yunduan_district` VALUES ('feba8e5bd5ff90ec7a19de3e4282a7ea', '340321', '340300', '怀远县', 3, NULL, 1, 1647931271, 1681975373);
INSERT INTO `yunduan_district` VALUES ('fec7c37f085463f7ade3fb3be93670cc', '411103', '411100', '郾城区', 3, NULL, 1, 1647931271, 1681975500);
INSERT INTO `yunduan_district` VALUES ('fecd75b475ab23c85d3b6190f1a6d241', '532323', '532300', '牟定县', 3, NULL, 1, 1647931271, 1681977873);
INSERT INTO `yunduan_district` VALUES ('feeb7f8b0749ad319276741a06c77b30', '451103', '451100', '平桂区', 3, NULL, 1, 1647931271, 1681975622);
INSERT INTO `yunduan_district` VALUES ('fef05ac1a6276795e870db54500a3ac0', '410225', '410200', '兰考县', 3, NULL, 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('fef4d233afdfa0d58b4d992df2405705', '510521', '510500', '泸县', 3, NULL, 1, 1647931271, 1681977532);
INSERT INTO `yunduan_district` VALUES ('ff27a8b2aa22534cf5f8c12593e5a850', '140429', '140400', '武乡县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('ff2d502fd68297f9600f0f02d02559dd', '530629', '530600', '威信县', 3, NULL, 1, 1647931271, 1681977887);
INSERT INTO `yunduan_district` VALUES ('ff33d7594ca6eb3a8ef741c24c14eb09', '411328', '411300', '唐河县', 3, NULL, 1, 1647931271, 1681975521);
INSERT INTO `yunduan_district` VALUES ('ff4dcb157e41df12a492fd38681e4dfa', '653225', '653200', '策勒县', 3, NULL, 1, 1647931271, 1681978503);
INSERT INTO `yunduan_district` VALUES ('ff57ab998e452de33b3de848c05d301d', '530111', '530100', '官渡区', 3, NULL, 1, 1647931271, 1681977891);
INSERT INTO `yunduan_district` VALUES ('ff61d9475777a8e97ad912379f2d6cee', '410205', '410200', '禹王台区', 3, NULL, 1, 1647931271, 1681975499);
INSERT INTO `yunduan_district` VALUES ('ff78a83291f7b35f9b412c6ba62f1207', '441200', '440000', '肇庆市', 2, '{\"county\": 8}', 1, 1647931271, 1681975583);
INSERT INTO `yunduan_district` VALUES ('ff8679fdad33efc06fc18ab89fd4f437', '540323', '540300', '类乌齐县', 3, NULL, 1, 1647931271, 1681977990);
INSERT INTO `yunduan_district` VALUES ('ffad76a0933f9b68a0bd83219304031a', '530722', '530700', '永胜县', 3, NULL, 1, 1647931271, 1681977894);
INSERT INTO `yunduan_district` VALUES ('ffb369e1ff0d1ceaca5f6aee5628d5dd', '150581', '150500', '霍林郭勒市', 3, NULL, 1, 1647931271, 1661392337);
INSERT INTO `yunduan_district` VALUES ('ffbed0dde4ed554acad1a7ffb5d04e8a', '130111', '130100', '栾城区', 3, NULL, 1, 1647931271, 1661392328);
INSERT INTO `yunduan_district` VALUES ('ffcbf024548afa8e83a978c39868e171', '141129', '141100', '中阳县', 3, NULL, 1, 1647931271, 1661392332);
INSERT INTO `yunduan_district` VALUES ('ffd351c2798be8ca70bbba1fc5eb41e6', '341504', '341500', '叶集区', 3, NULL, 1, 1647931271, 1681975386);
INSERT INTO `yunduan_district` VALUES ('ffe21639dffbda6a77cc09aa5be0c417', '320602', '320600', '崇川区', 3, NULL, 1, 1647931271, 1661392349);
INSERT INTO `yunduan_district` VALUES ('ffebd4d54257b81422e2a1e2393192da', '211223', '211200', '西丰县', 3, NULL, 1, 1647931271, 1661392340);
INSERT INTO `yunduan_district` VALUES ('ffef9dfd51c2d6fc35983a9926771990', '410526', '410500', '滑县', 3, NULL, 1, 1647931271, 1681975511);
INSERT INTO `yunduan_district` VALUES ('fff954bd15b2d32bb286af6e3d594ff6', '370700', '370000', '潍坊市', 2, '{\"county\": 12}', 1, 1647931271, 1681975487);

-- ----------------------------
-- Table structure for yunduan_doc
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_doc`;
CREATE TABLE `yunduan_doc`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文档名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文档描述',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文档密码',
  `is_open` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否公开(公开不需要密码)',
  `is_del` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文档表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yunduan_doc
-- ----------------------------
INSERT INTO `yunduan_doc` VALUES ('3694ac930e6ea7e18277d214560628b1', '用户信息', '用户信息', '4b11c76fd58ef288b50cce4177667ff7', 0, 0, 1682066437, 1683536979);
INSERT INTO `yunduan_doc` VALUES ('a06b273953bcb037d0404a2b12321c96', '开发文档', '开发文档', '', 1, 0, 1684916238, 1685066418);

-- ----------------------------
-- Table structure for yunduan_doc_article
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_doc_article`;
CREATE TABLE `yunduan_doc_article`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `doc_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文档ID，关联doc表主键',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文章名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文章描述',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文章内容',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文档详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_goods
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_goods`;
CREATE TABLE `yunduan_goods`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `code` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品编码',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品描述',
  `logo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品样图',
  `picture` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品图片',
  `price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.01 COMMENT '商品价格',
  `yh_price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.01 COMMENT '卷后价',
  `category_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品类别',
  `sub_category_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品子类',
  `merit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '优点',
  `youh` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '优惠',
  `sales` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已卖出件数',
  `is_top` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否推荐，0-否 1-是',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，4-下架 1-上架',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_goods_category
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_goods_category`;
CREATE TABLE `yunduan_goods_category`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `pid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '父类ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类别名称',
  `icon` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类别图标',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品数量',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1-启用 0-禁用',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_grade
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_grade`;
CREATE TABLE `yunduan_grade`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `grade_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '年级名称',
  `school_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '学校ID，关联school表主键',
  `school_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '学校名称',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '年级总人数',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1-正常 4-注销 9-删除',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '年级表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_org
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_org`;
CREATE TABLE `yunduan_org`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `org_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机构名称',
  `org_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机构编码',
  `short_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机构简称',
  `platform` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属平台',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上级ID',
  `parent_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上级名称',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户ID',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名称',
  `province_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '省级编号',
  `city_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '市级编号',
  `area_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区级编号',
  `town_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '街道/乡镇编号',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `certified` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否认证，0-否 1-是',
  `category` tinyint(3) NOT NULL COMMENT '机构类别，1-幼儿园 2-小学 3-初中 4-高中 5-大专 6-大学 7-职校',
  `logo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机构LOGO',
  `source` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '来源',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '机构状态，1-正常 2-解散 4-注销',
  `district_province` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政省编号',
  `district_city` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政市编号',
  `district_area` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政区编号',
  `district_town` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政街道/乡镇编号',
  `district_level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '行政级别，1-国家 2-省级 3-地级 4-县级 5-乡级',
  `org_attr` json NULL COMMENT '机构属性',
  `vld` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效期至',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机构表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_org_attr
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_org_attr`;
CREATE TABLE `yunduan_org_attr`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `org_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '机构ID，关联org表主键',
  `attr_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段名',
  `attr_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段值',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '机构属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_rel_class
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_rel_class`;
CREATE TABLE `yunduan_rel_class`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `school_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学校ID，关联school表主键',
  `grade_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '年级ID，关联grade表主键',
  `class_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '班级ID，关联class表主键',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户ID，关联user表主键',
  `user_category` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户类型，1-学生 2-家长 3-教师 4-校长 5-教师和家长 6-家长和校长 7-教师和校长 9-家长和教师和校长',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级和用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_rel_grade
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_rel_grade`;
CREATE TABLE `yunduan_rel_grade`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `school_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学校ID，关联school表主键',
  `grade_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '年级ID，关联grade表主键',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户ID，关联user表主键',
  `user_category` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户类型，1-学生 2-家长 3-教师 4-校长 5-教师和家长 6-家长和校长 7-教师和校长 9-家长和教师和校长',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '年级和用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_rel_school
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_rel_school`;
CREATE TABLE `yunduan_rel_school`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `school_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学校ID，关联school表主键',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户ID，关联user表主键',
  `user_category` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户类型，1-学生 2-家长 3-教师 4-校长 5-教师和家长 6-家长和校长 7-教师和校长 9-家长和教师和校长',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学校和用户关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_roymq_message
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_roymq_message`;
CREATE TABLE `yunduan_roymq_message`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '方键',
  `msg_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '消息内容',
  `queue_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属队列',
  `topic_name` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属主题',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `receive_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '接收时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'roymq消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_roymq_queue
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_roymq_queue`;
CREATE TABLE `yunduan_roymq_queue`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `queue_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列说明',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'roymq消息队列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_roymq_topic
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_roymq_topic`;
CREATE TABLE `yunduan_roymq_topic`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `topic_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '主题名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '说明，最多128个字符',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_topic_name`(`topic_name`) USING BTREE COMMENT '主题名称唯一索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'roymq主题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_school
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_school`;
CREATE TABLE `yunduan_school`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `school_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学校名称',
  `school_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学校代码',
  `short_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学校简称',
  `platform` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属平台',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上级ID',
  `parent_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上级名称',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户ID，关联user表主键',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员用户名称',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '学校总人数',
  `province_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省级编号',
  `city_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '市级编号',
  `area_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '区级编号',
  `town_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '街道/乡镇编号',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `certified` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否认证，0-否  1-是',
  `nature` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '办学性质，1-公办 2-民办 3-合作办校',
  `category` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '学校类别，1-幼儿园 2-小学 3-初中 4-高中 5-大专 6-大学 7-职校',
  `logo` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学校LOGO',
  `source` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '来源',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '学校状态，1-正常 4-注销 9-删除',
  `district_province` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政省编号',
  `district_city` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政市编号',
  `district_area` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政区编号',
  `district_town` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '行政街道/乡镇编号',
  `district_level` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '行政级别，1-国家 2-省级 3-地级 4-县级 5-乡级',
  `school_attr` json NULL COMMENT '学校属性',
  `vld` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效期至',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学校表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_school_attr
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_school_attr`;
CREATE TABLE `yunduan_school_attr`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `school_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学校ID，关联school表主键',
  `attr_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段名',
  `attr_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段值',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学校属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_sms_code
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_sms_code`;
CREATE TABLE `yunduan_sms_code`  (
  `mobile` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `code` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '验证码',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_table_custom_field
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_table_custom_field`;
CREATE TABLE `yunduan_table_custom_field`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `table_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '所属表',
  `field_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段名',
  `field_txt` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '显示名',
  `field_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段类型',
  `field_value` json NULL COMMENT '字段可以选值，当类型为select或radio时有用',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间是',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_table_field`(`table_name`, `field_name`) USING BTREE COMMENT '表和字段唯一索引'
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义表字段' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for yunduan_user
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_user`;
CREATE TABLE `yunduan_user`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `category` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户类型，1-学生 2-家长 3-教师 4-校长 5-教师和家长 6-家长和校长 7-教师和校长 9-家长和教师和校长 10-机构人员',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录账号',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `mobile` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户状态，0-待审核 1-正常 4-禁用',
  `login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `login_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录次数',
  `avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `idcard` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '身份证号码',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别，0-保密  1-男  2-女',
  `age` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '年龄',
  `user_attr` json NULL COMMENT '用户属性',
  `source` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '来源',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_user_attr
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_user_attr`;
CREATE TABLE `yunduan_user_attr`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户ID，关联user表主键',
  `attr_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段名',
  `attr_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '属性字段值',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_user_chat
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_user_chat`;
CREATE TABLE `yunduan_user_chat`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户ID',
  `to_user_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '聊天对象ID',
  `to_nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '聊天对象昵称',
  `to_avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '聊天对象头像',
  `note` varchar(210) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最近一句话',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户聊天表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_user_friend
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_user_friend`;
CREATE TABLE `yunduan_user_friend`  (
  `id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `user_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户ID，关联user表主键',
  `friend_id` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '好友ID，关联user表主键',
  `friend_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '好友名称',
  `friend_avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '好友头像',
  `name_letter` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '好友名称拼音首字母',
  `is_star` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为星标朋友，0-否  1-是',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `mtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE COMMENT '用户ID索引'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户好友表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for yunduan_user_log
-- ----------------------------
DROP TABLE IF EXISTS `yunduan_user_log`;
CREATE TABLE `yunduan_user_log`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户ID，关联yunduan_user表主键',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户账号',
  `mobile` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户手机号',
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录IP',
  `ctime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录日志表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
